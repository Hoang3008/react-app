import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {NetworkProvider} from 'react-native-offline';
import {PersistGate} from 'redux-persist/integration/react';
import * as Sentry from '@sentry/react-native';

import createStore from './store';
import RootWrapper from './RootWrapper';
import moment from 'moment';
import PermissionRequest from './utils/PermissionRequest';

const {store, persistor} = createStore();

// Sentry.init({
//   dsn: 'https://355cf0edb02041fd96694edfeb713f38@sentry.io/3333649',
// });

moment.locale('vn');

class App extends Component {
  constructor() {
    super();
    PermissionRequest();
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NetworkProvider>
            <RootWrapper />
          </NetworkProvider>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
