import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import RouteNames from './RouteNames';

import AuthLogin from './screens/auth/AuthLogin';
import SelectProjectScreen from './screens/SelectProjectScreen';
import DefectMain from './screens/defect/DefectMain';
import DefectDetail from './screens/DefectDetail/DefectDetail';
import DefectUpdate from './screens/DefectUpdate/DefectUpdate';
import ImageFullScreen from './screens/ImageFullScreen';
import DrawingListScreen from './screens/drawing/DrawingList';
import DrawingScreen from './screens/drawing/Drawing';
import NewChecklistScreen from './screens/NewCheckList/NewCheckList';
import PDFFullScreen from './screens/PDFFullScreen';
import SelectDefectCode from './screens/SelectDefectCode/SelectDefectCode';
import CaptureImage from './screens/CaptureImage/CaptureImage';
import NewDrawing from './screens/NewDrawing/NewDrawing';
import InspectionTabs from './screens/InspectionTabs/InspectionTabs';
import InspectionSubFolder from './screens/InspectionSubFolder/InspectionSubFolder';
import InspectionList from './screens/InspectionList/InspectionList';
import InspectionDetail from './screens/InspectionDetail/InspectionDetail';
import InspectionUpdate from './screens/InspectionUpdate/InspectionUpdate';
import InspectionAttachmentList from './screens/InspectionAttachmentList/InspectionAttachmentList';
import InspectionWorkflowComment from './screens/InspectionWorkflowComment/InspectionWorkflowComment';
import ScheduleList from './screens/ScheduleList/ScheduleList';
import ScheduleDetail from './screens/ScheduleDetail/ScheduleDetail';
import ScheduleUpdate from './screens/ScheduleUpdate/ScheduleUpdate';
import PhotoList from './screens/PhotoList/PhotoList';
import AddPhoto from './screens/AddPhoto/AddPhoto';
import EditPhoto from './screens/EditPhoto/EditPhoto';
import CreateUser from './screens/CreateUser/CreateUser';
import CreateProject from './screens/CreateProject/CreateProject';
import ForgotPassword from './screens/ForgotPassword/ForgotPassword';
import SketchDrawing from './screens/SketchDrawing/SketchDrawing';
import ProjectDashboard from './screens/ProjectDashboard/ProjectDashboard';
import PhotoComments from './screens/PhotoComments/PhotoComments';
import TaskList from './screens/TaskListV2/TaskListV2';
import TaskDetail from './screens/TaskDetail/TaskDetail';
import TaskUpdate from './screens/TaskUpdate/TaskUpdate';
import DrawingTabs from './screens/DrawingTabs/DrawingTabs';
import CreateDrawingArea from './screens/CreateDrawingArea/CreateDrawingArea';
import ProjectOverviewDashboard from './screens/ProjectOverviewDashboard/ProjectOverviewDashboard';
import SelectFromList from './screens/SelectFromList/SelectFromList';
import SelectFromListLogin from './screens/SelectFromList/SelectFromListLogin';
import NewInspectionFolder from './screens/NewInspectionFolder/NewInspectionFolder';
import DrawingFilter from './screens/DrawingFilter/DrawingFilter';
import MeetingList from './screens/MeetingList/MeetingList';
import MeetingDetail from './screens/MeetingDetail/MeetingDetail';
import InspectionWorkflowLog from './screens/InspectionWorkflowLog/InspectionWorkflowLog';
import InspectionMethodList from './screens/InspectionMethodList/InspectionMethodList';
import DrawingSubFolder from './screens/DrawingSubFolder/DrawingSubFolder';
import PhotoListTabs from './screens/PhotoTabs/PhotoListTabs';
import PhotoSubFolder from './screens/PhotoSubFolder/PhotoSubFolder';
import CreatePhotoArea from './screens/CreatePhotoArea/CreatePhotoArea';
import NotificationTab from './screens/NotificationList/NotificationTab';
import NotificationDefect from './screens/NotificationList/NotificationsDefect';
import NotificationsTask from './screens/NotificationList/NotificationsTask';
import NotificationMeeting from './screens/NotificationList/NotificationMeeting';
import NotificationSchedule from './screens/NotificationList/NotificationSchedule';
import NotificationITP from './screens/NotificationList/NotificationITP';


const AuthStack = {
  [RouteNames.AuthLogin]: {screen: AuthLogin},
  [RouteNames.CreateAccount]: {screen: CreateUser},
  [RouteNames.ForgotPassword]: {screen: ForgotPassword},
};

const Stack = createStackNavigator();

const AuthStackComponent = () => {
  return (
    <Stack.Navigator>
      {Object.keys(AuthStack).map((key) => (
        <Stack.Screen key={key} name={key} component={AuthStack[key].screen} />
      ))}
    </Stack.Navigator>
  );
};

const MainTabs = {
  [RouteNames.AuthStack]: {screen: AuthStackComponent},
  [RouteNames.SelectProjectScreen]: {screen: SelectProjectScreen},
  [RouteNames.BottomTabs]: {screen: ProjectDashboard},
  [RouteNames.DrawingScreen]: {screen: DrawingScreen},
  [RouteNames.DrawingTabs]: {screen: DrawingTabs},
  [RouteNames.DrawingListScreen]: {screen: DrawingListScreen},
  [RouteNames.DrawingSubFolder]: {screen: DrawingSubFolder},
};

const Tab = createBottomTabNavigator();

const MainTabComponent = () => (
  <Tab.Navigator options={{tabBarVisible: false}} tabBar={() => null}>
    {Object.keys(MainTabs).map((key) => (
      <Tab.Screen key={key} name={key} component={MainTabs[key].screen} />
    ))}
  </Tab.Navigator>
);

export const RootStack = {
  [RouteNames.HomeTabsWrapper]: {screen: MainTabComponent},
  [RouteNames.CreateProject]: {screen: CreateProject},
  [RouteNames.DefectMain]: {screen: DefectMain},
  [RouteNames.DefectDetail]: {screen: DefectDetail},
  [RouteNames.DefectUpdate]: {screen: DefectUpdate},
  [RouteNames.ImageFullScreen]: {screen: ImageFullScreen},
  [RouteNames.NewChecklistScreen]: {screen: NewChecklistScreen},
  [RouteNames.PDFFullScreen]: {screen: PDFFullScreen},
  [RouteNames.SelectDefectCode]: {screen: SelectDefectCode},
  [RouteNames.SketchDrawing]: {screen: SketchDrawing},
  [RouteNames.CaptureImage]: {screen: CaptureImage},
  [RouteNames.NewDrawing]: {screen: NewDrawing},
  [RouteNames.InspectionTabs]: {screen: InspectionTabs},
  [RouteNames.InspectionList]: {screen: InspectionList},
  [RouteNames.InspectionSubFolder]: {screen: InspectionSubFolder},
  [RouteNames.InspectionDetail]: {screen: InspectionDetail},
  [RouteNames.InspectionUpdate]: {screen: InspectionUpdate},
  [RouteNames.InspectionAttachmentList]: {screen: InspectionAttachmentList},
  [RouteNames.InspectionWorkflowComment]: {
    screen: InspectionWorkflowComment,
  },
  [RouteNames.ScheduleList]: {screen: ScheduleList},
  [RouteNames.ScheduleDetail]: {screen: ScheduleDetail},
  [RouteNames.ScheduleUpdate]: {screen: ScheduleUpdate},
  [RouteNames.PhotoList]: {screen: PhotoList},
  [RouteNames.AddPhoto]: {screen: AddPhoto},
  [RouteNames.EditPhoto]: {screen: EditPhoto},
  [RouteNames.PhotoComments]: {screen: PhotoComments},
  [RouteNames.TaskList]: {screen: TaskList},
  [RouteNames.TaskDetail]: {screen: TaskDetail},
  [RouteNames.TaskUpdate]: {screen: TaskUpdate},
  // [RouteNames.DrawingTabs]: {screen: DrawingTabs},
  // [RouteNames.DrawingListScreen]: {screen: DrawingListScreen},
  [RouteNames.CreateDrawingArea]: {screen: CreateDrawingArea},
  [RouteNames.ProjectOverviewDashboard]: {screen: ProjectOverviewDashboard},
  [RouteNames.SelectFromList]: {screen: SelectFromList},
  [RouteNames.SelectFromListLogin]: {screen: SelectFromListLogin},
  [RouteNames.NewInspectionFolder]: {screen: NewInspectionFolder},
  [RouteNames.DrawingFilter]: {screen: DrawingFilter},
  [RouteNames.MeetingList]: {screen: MeetingList},
  [RouteNames.MeetingDetail]: {screen: MeetingDetail},
  [RouteNames.InspectionWorkflowLog]: {screen: InspectionWorkflowLog},
  [RouteNames.InspectionMethodList]: {screen: InspectionMethodList},
  [RouteNames.PhotoListTabs]: {screen: PhotoListTabs},
  [RouteNames.PhotoSubFolder]: {screen: PhotoSubFolder},
  [RouteNames.CreatePhotoArea]: {screen: CreatePhotoArea},
  [RouteNames.NotificationTab]: {screen: NotificationTab},
  [RouteNames.NotificationsTask]: {screen: NotificationsTask},
  [RouteNames.NotificationMeeting]: {screen: NotificationMeeting},
  [RouteNames.NotificationITP]: {screen: NotificationITP},
};
