import API from './API.service';
import {getUserByUserNameAndPasswordUrl, getAllMasterData} from './urls';
import AsyncStorage from '@react-native-community/async-storage';


export const requestGetUserByUserNameAndPassword = (ADUserName, ADPassword) =>
  request(getUserByUserNameAndPasswordUrl(), {ADUserName, ADPassword});

export const requestGetAllMasterData = () => request(getAllMasterData());

const request = (url, params) => {
  return new Promise(async (resolve, reject) => {
    try {
      const data = await AsyncStorage.getItem('token').then((token) => {
        return API.get(url, {
          headers: {Authorization: `Bearer ${token}`},
        });
    });
      resolve(data);
    } catch (e) {
      reject(e);
    }
  });
};
