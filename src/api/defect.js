import API from './API.service';
import FormData from 'form-data';
import {Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import {
  getAllTaskByUserIdUrl,
  getAllTaskDocByTaskIdUrl,
  getAllTaskItemByTaskIdUrl,
  getAllTaskCommentByTaskIdUrl,
  getCreateTaskCommentUrl,
  getCreateTaskDocumentUrl,
  getCreateTaskUrl,
  getUpdateTaskUrl,
  getDeleteTaskUrl,
  getUpdateTaskItemUrl,
} from './urls';

export const requestGetAllTaskByUserId = ({userId}) =>
  request(getAllTaskByUserIdUrl({userId}));

export const requestGetAllTaskDocByTaskId = ({taskId}) =>
  request(getAllTaskDocByTaskIdUrl({taskId}));
export const requestGetAllTaskItemByTaskId = ({taskId, projectId}) =>
  request(getAllTaskItemByTaskIdUrl({taskId, projectId}));
export const requestGetAllTaskCommentByTaskId = ({taskId}) =>
  request(getAllTaskCommentByTaskIdUrl({taskId}));

export const requestCreateTask = (body, callback) =>
  requestPostPut(getCreateTaskUrl(), body, null, callback);
export const requestUpdateTask = (body, callback) =>
  requestPostPut(getUpdateTaskUrl(), body, null, callback, 'put');
export const requestDeleteTask = (body, callback) => {
  const data = {
    PMTaskTypeCombo: body?.PMTaskTypeCombo
  }
  
  return requestPostPut(getDeleteTaskUrl(body), data, null, callback, 'delete');
}
  
export const requestUpdateTaskItem = (body, callback) =>
  requestPostPut(getUpdateTaskItemUrl(), body, null, callback, 'put');

export const requestCreateTaskDocument = (body, photo, callback) =>
  requestFormDataCreate(getCreateTaskDocumentUrl(body, photo), body, photo, callback);
export const requestCreateTaskComment = (body, photo, callback) =>
  requestFormDataCreate(getCreateTaskCommentUrl(), body, photo, callback);

const request = (url) =>
  new Promise(async (resolve, reject) => {
    try {
      const data = AsyncStorage.getItem('token').then((token) => {
          return API.get(url, {
            headers: {Authorization: `Bearer ${token}`},
          });
      });
      resolve(data);
    } catch (e) {
      reject(e);
    }
  });

const requestPostPut = (
  url,
  body,
  photo,
  callback = () => {},
  method = 'post',
) =>
  new Promise(async (resolve, reject) => {
    let config;
    await AsyncStorage.getItem('token').then((token) => {
    config =  {
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        timeout: 120000,
      }
  });

    try {
      let data;
      if (method === 'put') {
        data = await API.put(url, body, ...config);
      }else if (method === 'delete'){
        data = await API.delete(url, {
          data: body,
          ...config
        });
      } else {
        data = await API.post(url, body, ...config);
      }
      resolve(data);
    } catch (e) {
      reject(e);
    }
  });

const requestFormDataCreate = (url, body, photo, callback = () => {}) =>
  new Promise(async (resolve, reject) => {
    try {
      const formData = createFormData(body, photo);

      const data = await API.post(url, formData, {
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'multipart/form-data',
        },
        onUploadProgress: callback,
        timeout: 120000,
      });
      resolve(data);
    } catch (e) {
      reject(e);
    }
  });

export const createFormData = (body, photo, fileField = 'file') => {
  const data = new FormData();

  if (photo) {
    data.append(fileField, {
      name: photo.filename,
      type: photo.type,
      uri:
        Platform.OS === 'android'
          ? photo.uri
          : photo.uri.replace('file://', ''),
    });
  }

  Object.keys(body).forEach((key) => {
    data.append(key, body[key]);
  });

  return data;
};
