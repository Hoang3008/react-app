import Configs from '../global/configs';

export const BASE_URL = () => Configs.apiBasedURL;

export const withKey = (url) => `${BASE_URL()}${url}`;

//AUTHENTICATION
export const getUserByUserNameAndPasswordUrl = () =>
  withKey('/ADUsers/GetObjectByADUserNameAndPassword');

export const getUserAvatarUrlByUsername = (user) =>
  'http://115.79.198.149:4000/assets/images/avatar_2.png'; //withKey(``);

//MASTER DATA
export const getAllMasterData = () => withKey(`/settings/priorityConfig`);

//TASK - DEFECT
export const getAllTaskByUserIdUrl = ({userId}) =>
  withKey(`/PMTasks/GetAllDataByUserID?ADUserID=${userId}`);

export const getCreateTaskUrl = () => withKey(`/PMTasks/CreateObject`);
export const getUpdateTaskUrl = () => withKey(`/PMTasks/UpdateObject`);
export const getDeleteTaskUrl = (data) => { 
  const TaskID = data?.PMTaskID;
  const projectID = data?.FK_PMProjectID;
  return withKey(`/task/v2/${projectID}/${TaskID}`) 
};
export const getDeleteDocumentUrl = () => withKey('/PMTaskDocs/DeleteObject');

//TASK ITEM - CHECK LIST
export const getAllTaskItemByTaskIdUrl = ({taskId, projectId}) =>
  withKey(`/task/item/${projectId}/${taskId}`);
export const getUpdateTaskItemUrl = () => withKey(`/PMTaskItems/UpdateObject`);

//TASK DOCUMENT - IMAGE + PDF
export const getAllTaskDocByTaskIdUrl = ({taskId}) =>
  withKey(`/PMTaskDocs/GetAllObjectByPMTaskID?PMTaskID=${taskId}`);
export const getCreateTaskDocumentUrl = (taskId, projectId) => {
  return withKey(`/task/doc/${taskId}/${projectId}`);
}
//TASK COMMENT
export const getAllTaskCommentByTaskIdUrl = ({taskId}) =>
  withKey(`/PMTaskComments/GetAllObjectByPMTaskID?PMTaskID=${taskId}`);
export const getCreateTaskCommentUrl = () =>
  withKey(`/PMTaskComments/CreateObject`);

export const getTaskCommentFileUrl = ({fileName}) => {
  if (!fileName) {
    return fileName;
  }

  if (fileName.startsWith('http')) {
    return fileName;
  }

  return withKey(`/PMTaskComments/ViewDocument?file=${encodeURI(fileName)}`);
};

export const getTaskDocumentFileUrl = ({fileName}) => {
  if (!fileName) {
    return fileName;
  }

  if (fileName.startsWith('http')) {
    return fileName;
  }

  return withKey(`/PMTaskDocs/ViewDocument?file=${encodeURI(fileName)}`);
};

export const getInspectionPhotoUrl = (fileName) => {
  if (!fileName) {
    return fileName;
  }

  if (fileName.startsWith('http')) {
    return fileName;
  }

  return withKey(
    `/PMProjectInsStpInspPics/ViewDocument?file=${encodeURI(fileName)}`,
  );
};

export const getInspectionAttachmentUrl = (fileName) =>
  withKey(`/PMProjectInsStpInspDocs/ViewDocument?file=${encodeURI(fileName)}`);

export const getPhotoUrl = (fileName, isFullScreen=false) => {
  if (!fileName) {
    return fileName;
  }
  let url = `/PMPhotos/ViewDocument?file=${encodeURI(fileName)}&small=1`;

  if (isFullScreen) {
    url  = `/PMPhotos/ViewDocument?file=${encodeURI(fileName)}`;
  }

  if (fileName.startsWith('http')) {
    return fileName;
  }

  console.log(fileName);
  return withKey(url);
};

export const getConstructionMethodFileUrl = (fileName) => {
  if (!fileName) {
    return fileName;
  }

  if (fileName.startsWith('http')) {
    return fileName;
  }

  return withKey(`/PMWorkInsStepDtls/ViewDocument?file=${encodeURI(fileName)}`);
};

export const getConstructionMethodPDFFileUrl = (fileName) => {
  if (!fileName) {
    return fileName;
  }

  if (fileName.startsWith('http')) {
    return fileName;
  }

  return withKey(`/PMWorkInstSteps/ViewDocument?file=${encodeURI(fileName)}`);
};

// PUT https://api.rocez.com/user/mobile-token
// {
//   "mobileToken": "..."
// }
