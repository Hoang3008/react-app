import axios from 'axios';

const instance = axios.create({
  httpsAgent: {
    rejectUnauthorized: false,
  },
});

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // debugger;
    return Promise.reject(error.response);
  },
);

export default instance;

// import axios from 'axios';
// import {propOr} from 'ramda';

const commonHeader = {
  // Accept: 'application/json',
  // 'Content-Type': 'application/json',
};

// const makeRequest = (url, requestOptions) =>
//   fetch(url, requestOptions)
//     .then((response) => {
//       const status = propOr(0, 'status', response);
//       if (status >= 200 && status < 300) {
//         return response.json();
//       } else {
//         throw new Error(response);
//       }
//     })
//     .catch((e) => {
//       console.log('Call API error: ', e);
//     });
//
// const instance = {
//   post: (url, body, config = {}) => {
//     const options = {
//       method: 'POST',
//       headers: {
//         ...commonHeader,
//         ...config,
//       },
//       body,
//     };
//
//     return makeRequest(url, options);
//   },
//   get: (url, config = {}) => {
//     const options = {
//       method: 'GET',
//       headers: {
//         ...commonHeader,
//         ...config,
//       },
//     };
//
//     return makeRequest(url, options);
//   },
//   put: (url, body, config = {}) => {
//     const options = {
//       method: 'PUT',
//       headers: {
//         ...commonHeader,
//         ...config,
//       },
//       body,
//     };
//
//     return makeRequest(url, options);
//   },
//   patch: (url, body, config = {}) => {
//     const options = {
//       method: 'PATCH',
//       headers: {
//         ...commonHeader,
//         ...config,
//       },
//       body,
//     };
//
//     return makeRequest(url, options);
//   },
//   delete: (url, config = {}) => {
//     const options = {
//       method: 'PATCH',
//       headers: {
//         ...commonHeader,
//         ...config,
//       },
//     };
//
//     return makeRequest(url, options);
//   },
// };
//
// export default instance;
