import { Dimensions } from 'react-native';

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

const DESIGN_WIDTH = 375;
const DESIGN_HEIGHT = 810;

const scaleWidth = SCREEN_WIDTH / DESIGN_WIDTH;
const scaleHeight = SCREEN_HEIGHT / DESIGN_HEIGHT;

export function scaledWidth(dw) {
    return dw * scaleWidth;
}

export function scaledHeight(dh) {
    return dh * scaleHeight;
}
