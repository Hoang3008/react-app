import moment from 'moment';

export const getTodayDateMoment = () => {
  const todayDate = moment();
  todayDate.hour(0);
  todayDate.minute(0);
  todayDate.second(0);
  todayDate.millisecond(0);
  return todayDate;
};

export const setMomentLocale = () => moment.locale('vn');

export const makeMomentTimeTo0 = time => {};
