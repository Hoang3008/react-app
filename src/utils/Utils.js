import {groupBy, identity} from 'ramda';

const statusOrder = {Inprogress: 1, New: 2, Close: 3};

export const groupDataByKey = (data, key, itemToLabelFunc = identity) => {
  const resultObject = groupBy((item) => itemToLabelFunc(item[key]))(data);
  return Object.keys(resultObject)
    .sort((a, b) => {
      const aIndex = statusOrder[a] || 4;
      const bIndex = statusOrder[b] || 4;

      return aIndex - bIndex;
    })
    .map((key) => ({
      title: key,
      data: resultObject[key] || [],
    }));
};
