import { Alert } from 'react-native';

export function showAlert(title = '', message = '', callbackClose, cancelable = true){
    Alert.alert(
        title,
        message,
        [
            {text: 'Close', onPress: () => {callbackClose && callbackClose()}},
        ],
        {cancelable: cancelable},
    );
};
