import {PermissionsAndroid, Platform} from 'react-native';

const PermissionRequest = async () => {
  try {
    if (Platform.OS === 'android') {
      await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.CAMERA,
      ]);
    }
  } catch (err) {
    console.warn(err);
  }
};

export default PermissionRequest;
