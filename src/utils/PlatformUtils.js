import { equals } from 'ramda';

const isDEV = () => equals(process.env.NODE_ENV, 'development');

export { isDEV };
