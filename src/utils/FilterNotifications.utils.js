
const DEFECT = 'DEFECT';
const TASK = 'TASK';
const SCHEDULE = 'SCHEDULE';
const MEETING = 'MEETING';
const ITP = 'ITP';


const filterNotificatios = (notifications , notificatiosDefect, notificationsTask, notificationsSchedule, notificationsMeeting, notificationsITP) => {
    notifications && notifications.forEach((item) => {

        if (item.type.includes('DEFECT')) {
            notificatiosDefect.push(item);
        }else if(item.type.includes('TASK')){
            notificationsTask.push(item);
        }else if(item.type.includes('SCHEDULE')){
            notificationsSchedule.push(item);
        }else if(item.type.includes('MEETING')){
            notificationsMeeting.push(item);
        }else if(item.type.includes('ITP')){
            notificationsITP.push(item);
        }
    });

}

export { 
    filterNotificatios
}