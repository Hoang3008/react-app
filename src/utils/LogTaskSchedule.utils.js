import {
  propOr
} from 'ramda';
import moment from 'moment';

const createLogList = (currentData, newData, metaData) => {
  const logs = [];
  const {
    categories,
    defectCodeList,
    locations,
    peopleInProject
  } = metaData;

  if (currentData.PMTaskPriorityCombo !== newData.PMTaskPriorityCombo) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed priority to ${newData.PMTaskPriorityCombo}`,
    });
  }

  if (currentData.FK_ADAssignUserID !== newData.FK_ADAssignUserID) {
    const people = (peopleInProject || []).find(
      ({
        PMProjectPeopleID,
        ADUserID
      }) => PMProjectPeopleID === newData.FK_ADAssignUserID || ADUserID === newData.FK_ADAssignUserID,
    );

    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed assignee to ${propOr('', 'fullName', people)}`,
    });
  }

  if (currentData.PMTaskStartDate !== newData.PMTaskStartDate) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed start date to ${moment(
        newData.PMTaskStartDate,
      ).format('DD/MM/YYYY')}`,
    });
  }

  if (currentData.PMTaskCompletePct !== newData.PMTaskCompletePct) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Change Percent Complete to ${newData.PMTaskCompletePct}`,
    });
  }

  if (currentData.PMTaskDesc !== newData.PMTaskDesc) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Change Description to ${newData.PMTaskDesc}`,
    });
  }

  if (currentData.PMTaskEndDate !== newData.PMTaskEndDate) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed end date to ${moment(
        newData.PMTaskEndDate,
      ).format('DD/MM/YYYY')}`,
    });
  }

  if (currentData.PMTaskName !== newData.PMTaskName) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed title to ${newData.PMTaskName}`,
    });
  }

  return logs;
};

export {
  createLogList
}