import {propOr} from 'ramda';
import moment from 'moment';

export const createLogList = (currentData, newData, metaData) => {
  const logs = [];
  const {categories, defectCodeList, locations, peopleInProject} = metaData;

  if (currentData.PMTaskPriorityCombo !== newData.PMTaskPriorityCombo) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed priority to ${newData.PMTaskPriorityCombo}`,
    });
  }

  if (currentData.FK_PMProjectCategoryID !== newData.FK_PMProjectCategoryID) {
    const category = (categories || []).find(
      ({PMProjectCategoryID}) =>
        PMProjectCategoryID === newData.FK_PMProjectCategoryID,
    );
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed category to ${propOr(
        '',
        'PMProjectCategoryName',
        category,
      )}`,
    });
  }

  if (currentData.FK_PMTaskGroupID !== newData.FK_PMTaskGroupID) {
    const defectCode = (defectCodeList || []).find(
      ({PMTaskGroupID}) => PMTaskGroupID === newData.FK_PMTaskGroupID,
    );

    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed defect code to ${propOr(
        '',
        'PMTaskGroupName',
        defectCode,
      )}`,
    });
  }

  if (currentData.FK_PMProjectLocationID !== newData.FK_PMProjectLocationID) {
    const location = (locations || []).find(
      ({PMProjectLocationID}) =>
        PMProjectLocationID === newData.FK_PMProjectLocationID,
    );

    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed location to ${propOr(
        '',
        'PMProjectLocationName',
        location,
      )}`,
    });
  }

  if (currentData.FK_ADAssignUserID !== newData.FK_ADAssignUserID) {
    const people = (peopleInProject || []).find(
      ({ADUserID}) => ADUserID === newData.FK_ADAssignUserID,
    );

    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed assignee to ${propOr('', 'fullName', people)}`,
    });
  }

  if (currentData.PMTaskStartDate !== newData.PMTaskStartDate) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed start date to ${moment(
        newData.PMTaskStartDate,
      ).format('DD/MM/YYYY')}`,
    });
  }

  if (currentData.PMTaskEndDate !== newData.PMTaskEndDate) {
    logs.push({
      taskCommentType: 'USER_LOG',
      taskCommentDesc: `Changed end date to ${moment(
        newData.PMTaskEndDate,
      ).format('DD/MM/YYYY')}`,
    });
  }

  return logs;
};
