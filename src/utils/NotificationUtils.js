import RouteNames from '../RouteNames';
import {EMPTY, NOTIFICATION_TYPE} from '../global/constants';
import {pathOr, propOr} from 'ramda';
import { isEmpty } from 'lodash';


export const handleClickNotification = (
  navigation,
  readNotification,
  defectCurrent,
  user,
  project,
  getDefectListByDrawingId,
  refreshNotificationList = EMPTY.FUNC,
) => (notification) =>  {
  if (!notification) {
    return;
  }

  const {type, taskId, drawingId, itpId} = notification;


  const currentDefect = propOr(EMPTY.OBJECT, drawingId, defectCurrent);
  const token = propOr('', 'token', user);
  const projectId = propOr('', 'PMProjectID', project);

  if (NOTIFICATION_TYPE.ASSIGNED_DEFECT ||
    NOTIFICATION_TYPE.COMMENT_DEFECT || 
    NOTIFICATION_TYPE.CHANGE_STATUS_DEFECT) {
      getDefectListByDrawingId({user, drawingId, projectId}, token);
    }

  switch (type) {
    case NOTIFICATION_TYPE.ASSIGNED_DEFECT:
      readNotification(notification.id).then(refreshNotificationList);
      navigation.navigate(RouteNames.DefectDetail, {
        defect: {PMTaskID: taskId},
        drawingId,
        drawingPage: true // set drawingPage to get data from defectListByDrawingId
      });
      break;

    case NOTIFICATION_TYPE.COMMENT_DEFECT:
      readNotification(notification.id).then(refreshNotificationList);
      navigation.navigate(RouteNames.DefectDetail, {
        defect: {PMTaskID: taskId},
        drawingId,
        drawingPage: true // set drawingPage to get data from defectListByDrawingId
      });
      break;

    case NOTIFICATION_TYPE.CHANGE_STATUS_DEFECT:
      readNotification(notification.id).then(refreshNotificationList);
      navigation.navigate(RouteNames.DefectDetail, {
        defect: {PMTaskID: taskId},
        drawingId,
        drawingPage: true // set drawingPage to get data from defectListByDrawingId
      });
      break;

    case NOTIFICATION_TYPE.ASSIGNED_MEETING:
      readNotification(notification.id).then(refreshNotificationList);
      break;

    case NOTIFICATION_TYPE.ASSIGNED_SCHEDULE:
      readNotification(notification.id).then(refreshNotificationList);
      navigation.navigate(RouteNames.ScheduleDetail, {PMTaskID: taskId});
      break;

    case NOTIFICATION_TYPE.ASSIGNED_TASK:
      readNotification(notification.id).then(refreshNotificationList);
      navigation.navigate(RouteNames.TaskDetail, {PMTaskID: taskId});
      break;

    case NOTIFICATION_TYPE.CHANGE_STATUS_TASK:
      readNotification(notification.id).then(refreshNotificationList);
      navigation.navigate(RouteNames.TaskDetail, {PMTaskID: taskId});
      break;

    case NOTIFICATION_TYPE.COMMENT_TASK:
      readNotification(notification.id).then(refreshNotificationList);
      navigation.navigate(RouteNames.TaskDetail, {PMTaskID: taskId});
      break;

    case NOTIFICATION_TYPE.NOTIFY_USER:
      readNotification(notification.id).then(refreshNotificationList);
      break;

    default:
      readNotification(notification.id).then(refreshNotificationList);
      navigation.navigate(RouteNames.InspectionDetail, {PMProjectInspID: itpId});
      break;
  }
};
