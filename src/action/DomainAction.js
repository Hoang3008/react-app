import {Domain} from './type';

export const userAddDomain = (masterData) => ({
  type: Domain.USER_ADD_DOMAIN,
  payload: masterData,
});

export const userDeleteDomain = (masterData) => ({
  type: Domain.USER_DELETE_DOMAIN,
  payload: masterData,
});
