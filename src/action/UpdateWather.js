import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import {getUniqueId} from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';
import API from '../api/API.service';
import {withKey} from '../api/urls';

export const actionType = 'UPDATE_WATCHER';

// https://api.rocez.com/user/mobile-token

const putAPI = (projectId, taskId, selectedPeople, typeWatch) => {
  return AsyncStorage.getItem('token').then((token) => {
    const body = {
        typeWatch: typeWatch,
        emails: selectedPeople
    };
    return API.put(
        withKey(`/watcher/${projectId}/${taskId}`),
        body,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
  })
};

const action = createAction(actionType, putAPI);

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: identity,
    FAILURE: identity,
  },
};

export default {action, reducer};
