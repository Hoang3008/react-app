import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import {getUniqueId} from 'react-native-device-info';

import API from '../api/API.service';
import {withKey} from '../api/urls';

const actionType = 'POST_TOKEN';

// https://api.rocez.com/user/mobile-token

const putAPI = (mobileToken, userToken) => {
  const deviceUUID = getUniqueId();
  return API.put(
    withKey('/user/mobile-token'),
    {
      mobileToken,
      deviceUUID,
    },
    {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    },
  );
};

const action = createAction(actionType, putAPI);

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: identity,
    FAILURE: identity,
  },
};

export default {action, reducer};
