import {createAction} from 'redux-actions';
import API from '../api/API.service';
import {GET_WATCHER_LIST} from './type';
import {withKey} from '../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const getWatcherListAPI = (projectId, taskId, peopleInProject) => {
  return AsyncStorage.getItem('token').then((token) => {
      return API.get(withKey(`/watcher/${projectId}/${taskId}?appFlag=1`), {
        headers: {Authorization: `Bearer ${token}`},
      });
  });
};

export const getWatcherListAction = createAction(
  GET_WATCHER_LIST,
  getWatcherListAPI,
  (...param) => param
);
