import {Auth} from './type';

export const logout = () => ({type: Auth.LOGOUT});
export const loadProjectIntoRedux = (project) => ({
  type: Auth.PROJECT_SELECTED,
  payload: project,
});
export const loadMasterDataIntoRedux = (masterData) => ({
  type: Auth.MASTER_DATA_UPDATED,
  payload: masterData,
});
