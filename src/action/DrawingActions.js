import {createAction} from 'redux-actions';
import API from '../api/API.service';
import {GET_DRAWING_LIST} from './type';
import {withKey} from '../api/urls';
import AsyncStorage from '@react-native-community/async-storage';


// http://api.gmc.solutions/PPDrawings/GetAllDataByPMProjectID?PMProjectID=35

const getDrawingListUrl = (PMProjectID) =>
  withKey(`/drawing-area/drawing?projectId=${PMProjectID}&appFlag=1`);

// https://apidev.rocez.com/drawing-area/3/drawings

const getDrawingListAPI = (areaId, PMProjectID, token) => {
  return AsyncStorage.getItem('token').then((token) => {
    if (areaId) {
      return API.get(withKey(`/drawing-area/${areaId}/drawings`), {
        headers: {Authorization: `Bearer ${token}`},
      });
    }
      return API.get(getDrawingListUrl(PMProjectID), {
        headers: {Authorization: `Bearer ${token}`},
      });
  });
};

export const getDrawingListAction = createAction(
  GET_DRAWING_LIST,
  getDrawingListAPI,
);
