import envJSON from '../env';

export default {
  apiBasedURL: envJSON.apiBasedURL,
  drawingBasedURL: envJSON.drawingBasedURL,
  useRocezLoginIcon: envJSON.useRocezLoginIcon,
};
