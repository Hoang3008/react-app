export const EMPTY = {
  OBJECT: {},
  ARRAY: [],
  STRING: '',
  FUNC: () => {},
};
export const PERMISSION_TYPE = {
  TYPE_NONE: 'NONE',
  MODULE_DEFECT: 'defect',
  PERMISSION_CHANGEALL: 'CHANGE_ALL'
}

export const OFFLINE_ACTION_TYPES = {
  CREATE_DEFECT: 'CREATE_DEFECT',
  UPDATE_DEFECT: 'UPDATE_DEFECT',
  ADD_PHOTO: 'ADD_PHOTO',
  ADD_COMMENT: 'ADD_COMMENT',
  UPDATE_ITP_OVERVIEW: 'UPDATE_ITP_OVERVIEW',
  POST_ITP_WORKFLOW_COMMENT: 'POST_ITP_WORKFLOW_COMMENT',
  ADD_ITP_WORKFLOW_PHOTO: 'ADD_ITP_WORKFLOW_PHOTO',
  ADD_ITP_WORKFLOW_DOC: 'ADD_ITP_WORKFLOW_DOC',
  UPDATE_ITP_WORKFLOW_CONTENT_STATUS: 'UPDATE_ITP_WORKFLOW_CONTENT_STATUS',
};

export const NOTIFICATION_TYPE = {
  ASSIGNED_DEFECT: 'ASSIGNED_DEFECT',
  ASSIGNED_TASK: 'ASSIGNED_TASK',
  ASSIGNED_SCHEDULE: 'ASSIGNED_SCHEDULE',
  ASSIGNED_MEETING: 'ASSIGNED_MEETING',
  CHANGE_STATUS_TASK: 'CHANGE_STATUS_TASK',
  NOTIFY_USER: 'NOTIFY_USER',
  COMMENT_TASK: 'COMMENT_TASK',
  COMMENT_DEFECT: 'COMMENT_DEFECT',
  CHANGE_STATUS_DEFECT: 'CHANGE_STATUS_DEFECT',
};
