import React, {useEffect, useState, useRef} from 'react';
import moment from 'moment';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import {groupBy, propOr} from 'ramda';
import styles from './styles.js';
import WeekView, {TimeList} from '../WeekView/WeekView';
import WeekViewStyles from '../WeekView/styles';
import ViewPager from '@react-native-community/viewpager';

const PriorityColor = {
  Priority1: '#FFEB33',
  Priority2: '#FFA800',
  Priority3: '#FFA800',
  Complete: '#62E787',
  Verify: '#519D59',
};

const TIME_COLUMN_WIDTH = 0;

const TaskWeekView = (props) => {
  const {data, onDateChanged, date, onItemClick} = props;
  const [dataByDate, setDataByDate] = useState({});
  const pagerRef = useRef();
  const timeScrollViewRef = useRef();
  const pageItemRefs = [useRef(), useRef(), useRef(), useRef(), useRef()];
  const [contentOffset, setContentOffset] = useState({x: 0, y: 0});

  useEffect(() => {
    const dataByDate = groupBy(({PMTaskStartDate}) =>
      moment(PMTaskStartDate).format('YYYY-MM-DD HH'),
    )(data);
    setDataByDate(dataByDate);
  }, [data]);

  const handleScroll = (index) => (event) => {
    // if (index !== 2) {
    //   return;
    // }
    //
    // const {
    //   nativeEvent: {
    //     contentOffset: {x, y},
    //   },
    // } = event;
    // const {y: currentY} = contentOffset;
    // if (Math.round(currentY) !== Math.round(y)) {
    //   timeScrollViewRef.current.scrollTo({x, y, animated: false});
    // }
    // setContentOffset({x, y});
    // pageItemRefs.forEach((ref, index) => {
    //   if (index === 2) {
    //     return;
    //   }
    //
    //   ref.current.scrollTo({x, y, animated: false});
    // });
  };

  const renderDate = (date, time) => {
    const timeStr = moment(time, 'H').format('HH');
    const dateTimeStr = `${date} ${timeStr}`;

    const taskList = propOr([], dateTimeStr, dataByDate);

    return (
      <>
        {taskList.map((task) => {
          const {PMTaskName, PMTaskPriorityCombo} = task;
          return (
            <TouchableOpacity
              onPress={() => onItemClick(task)}
              style={[
                styles.textContainer,
                {backgroundColor: PriorityColor[PMTaskPriorityCombo]},
              ]}>
              <Text style={[styles.text]}>{PMTaskName}</Text>
            </TouchableOpacity>
          );
        })}
      </>
    );
  };

  const renderPage = (dateMoment, index) => {
    const key = dateMoment.format('YYYY-MM-DD');
    const contentOffsetProps = index === 2 ? {} : {contentOffset};
    return (
      <View key={key} style={styles.container}>
        <WeekView
          ref={pageItemRefs[index]}
          onScroll={handleScroll(index)}
          date={dateMoment.toDate()}
          renderDate={renderDate}
          {...contentOffsetProps}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {/*<View style={[styles.leftCol, {width: TIME_COLUMN_WIDTH}]}>*/}
      {/*  <View style={[WeekViewStyles.header]} />*/}
      {/*  <ScrollView*/}
      {/*    ref={timeScrollViewRef}*/}
      {/*    scrollEnabled={false}*/}
      {/*    indicatorStyle="white"*/}
      {/*    style={styles.scrollView}*/}
      {/*    bounces={false}>*/}
      {/*    {TimeList.map((time, index) => {*/}
      {/*      if (index === TimeList.length - 1) {*/}
      {/*        return null;*/}
      {/*      }*/}

      {/*      return (*/}
      {/*        <View key={`${time}`} style={[styles.timeView]}>*/}
      {/*          {index > 0 && index && (*/}
      {/*            <Text style={{marginTop: -9, fontSize: 14}}>*/}
      {/*              {moment(time, 'H').format('HH:mm')}*/}
      {/*            </Text>*/}
      {/*          )}*/}
      {/*        </View>*/}
      {/*      );*/}
      {/*    })}*/}
      {/*  </ScrollView>*/}
      {/*</View>*/}

      <ViewPager
        ref={pagerRef}
        initialPage={2}
        style={{flex: 1, height: '100%'}}
        onPageSelected={(event) => {
          const {
            nativeEvent: {position},
          } = event;

          if (position === 2) {
            return;
          }

          const newDate =
            position < 2
              ? moment(date).subtract(2 - position, 'week')
              : moment(date).add(position - 2, 'week');
          onDateChanged(newDate);
          pagerRef.current.setPageWithoutAnimation(2);
        }}>
        {renderPage(moment(date).subtract(2, 'week'), 0)}
        {renderPage(moment(date).subtract(1, 'week'), 1)}
        {renderPage(moment(date), 2)}
        {renderPage(moment(date).add(1, 'week'), 3)}
        {renderPage(moment(date).add(2, 'week'), 4)}
      </ViewPager>
    </View>
  );
};

// AACreatedDate: "2020-04-12T09:35:52.843Z"
// AACreatedUser: "evenhieu@gmail.com"
// AAStatus: "Alive"
// AAUpdatedDate: "2020-04-21T20:05:40.207Z"
// AAUpdatedUser: "evenhieu@gmail.com"
// FK_ADAssignUserID: 74
// FK_ADUserID: 196
// FK_PMProjectCategoryID: 0
// FK_PMProjectID: 117
// FK_PMProjectLocationID: 0
// FK_PMTaskGroupID: 0
// FK_PPDrawingID: 0
// FK_PPMDRID: null
// PMTaskCompleteCheck: false
// PMTaskCompletePct: 100
// PMTaskCost: 0
// PMTaskDesc: "test 123"
// PMTaskEndDate: "2020-04-16T00:00:00.000Z"
// PMTaskID: 1421
// PMTaskLat: 0
// PMTaskLng: 0
// PMTaskLocation: ""
// PMTaskManPower: 0
// PMTaskName: "test"
// PMTaskNo: "0"
// PMTaskPriorityCombo: "Priority3"
// PMTaskStartDate: "2020-04-30T00:00:00.000Z"
// PMTaskTags: ""
// PMTaskTypeCombo: "task"

// https://apidev.rocez.com/PMTasks/GetAllDataByCondition?FK_PMProjectID=117&PMTaskTypeCombo=task&dtFromEndDate=1900-01-01&dtToEndDate=2020-05-24T10:16:49.674Z

export default TaskWeekView;
