export default {
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    marginBottom: 3,
    marginLeft: 15,
    marginRight: 15,
    fontSize: 16,
    color: '#ef8742',
  },
  button: {
    padding: 4,
  },
};
