import * as React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import IconEntypo from 'react-native-vector-icons/Entypo';

import styles from './DateNavigator.styles';

const DateNavigator = ({style = {}, label, onPressPrev, onPressNext}) => {
  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity style={styles.button} onPress={onPressPrev}>
        <IconEntypo size={38} color="#ef8742" name="chevron-small-left" />
      </TouchableOpacity>
      <Text style={styles.text}>{label}</Text>
      <TouchableOpacity style={styles.button} onPress={onPressNext}>
        <IconEntypo size={38} color="#ef8742" name="chevron-small-right" />
      </TouchableOpacity>
    </View>
  );
};

export default DateNavigator;
