import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import RouteNames from '../RouteNames';
import Theme from '../Theme';

export default class LinkButton extends React.PureComponent {
  render() {
    const {text, onPress, containerStyle} = this.props;

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.buttonContainer, containerStyle || {}]}>
        <Text style={styles.buttonText}>{text}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  buttonContainer: {
    alignItems: 'center',
    padding: 8,
  },
  buttonText: {
    color: '#245894',
    fontSize: 14,
  },
};
