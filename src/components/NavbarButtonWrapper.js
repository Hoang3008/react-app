import React from 'react';
import { View, TouchableOpacity } from 'react-native';

class NavbarButtonWrapper extends React.Component {
    render() {
        const { children, style, onPress } = this.props;
        return (
            <View style={style}>
                <TouchableOpacity onPress={onPress} style={{ flex: 1 }}>
                    {children}
                </TouchableOpacity>
            </View>
        );
    }
}

export default NavbarButtonWrapper;
