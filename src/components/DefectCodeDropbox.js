import React from 'react';
import {View, Text, Image} from 'react-native';
import Theme from '../Theme';
import ModalDropdown from 'react-native-modal-dropdown';
import {get} from 'lodash';

export default class DefectCodeDropBox extends React.Component {
  constructor(props) {
    super(props);
    this.defaultValue = 'Select ' + this.props.label;
  }

  getDefaultValue() {
    this.props.options.map((item) => {
      if (this.props.selectedValue === get(item, 'PMTaskGroupID')) {
        this.defaultValue = get(item, 'PMTaskGroupName');
      }
    });
  }

  onSelect = (index, item) => {
    this.props.onSelect && this.props.onSelect(get(item, 'PMTaskGroupID'));
  };

  renderRow = (rowData) => {
    const {PMTaskGroupNo, PMTaskGroupName, PMTaskGroupDesc} = rowData;

    // PMTaskGroupNo: "HT-XA-10"
    // PMTaskGroupName: "Không tưới nước bão dưỡng tường sẽ nứt  "
    // PMTaskGroupDate: "2019-12-10T00:00:00.000Z"
    // PMTaskGroupDesc: "Phải tưới nước gạch và tường trước và sau khi xây theo quy trình   "
    // AAStatus: "Alive"

    return (
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'flex-start',
          paddingVertical: Theme.margin.m4,
        }}>
        <Text style={styles.textNo}>{PMTaskGroupNo}</Text>
        <Text style={styles.textValue}>{PMTaskGroupName}</Text>
        <Text style={styles.textNo}>{PMTaskGroupDesc}</Text>
      </View>
    );
  };

  renderButtonText = (data) => {
    return get(data, 'PMTaskGroupNo');
  };

  render() {
    let {label, options, onClick} = this.props;

    this.getDefaultValue();

    return (
      <View style={styles.container}>
        <Text style={styles.textNormalSecond}>{label}</Text>
        <View style={{flexDirection: 'column', marginLeft: Theme.margin.m10}}>
          <ModalDropdown
            options={options}
            defaultValue={this.defaultValue || ''}
            onSelect={this.onSelect}
            textStyle={styles.textNormal}
            renderButtonText={this.renderButtonText}
            renderRow={(rowData, rowId, highlighted) =>
              this.renderRow(rowData, rowId, highlighted)
            }
            onDropdownWillShow={() => {
              onClick();
              return false;
            }}
            dropdownStyle={styles.dropBoxItemsContainer}
          />
          <Image
            style={styles.icDropdown}
            source={require('../assets/img/ic_dropdown.png')}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    marginHorizontal: Theme.margin.m15,
    marginVertical: Theme.margin.m8,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.border,
    borderRadius: Theme.margin.m4,
    paddingVertical: Theme.margin.m12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textNormalSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
    marginLeft: Theme.margin.m14,
  },
  textNormal: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.normal,
    marginRight: Theme.margin.m30,
    textAlign: 'center',
  },
  textValue: {
    textAlign: 'left',
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.normal,
    marginRight: Theme.margin.m10,
    marginLeft: Theme.margin.m5,
    paddingVertical: Theme.margin.m4,
  },
  textNo: {
    textAlign: 'left',
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.small_12,
    marginRight: Theme.margin.m10,
    marginLeft: Theme.margin.m5,
    paddingVertical: Theme.margin.m4,
  },
  dropBoxItemsContainer: {
    borderRadius: Theme.margin.m4,
    borderColor: Theme.colors.border,
    paddingHorizontal: Theme.margin.m16,
    maxWidth: '96%',
  },
  textSmall: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.small_12,
  },
  leftContainer: {
    width: Theme.margin.m24,
    height: Theme.margin.m24,
    borderRadius: Theme.margin.m24 / 2,
    color: Theme.colors.textNormal,
    borderWidth: 0.5,
    margin: Theme.margin.m4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icDropdown: {
    position: 'absolute',
    right: Theme.margin.m12,
    top: 0,
    bottom: 0,
    width: Theme.margin.m8,
    height: 20,
    resizeMode: 'contain',
  },
};
