import React from 'react';
import {Text, View} from 'react-native';

import styles from './SectionHeader.styles';

import Theme from '../../Theme';

const SectionHeader = (props) => {
  const {color, title = ''} = props;
  return (
    <View style={styles.row}>
      <View
        style={{
          height: Theme.specifications.headerSectionHeight,
          width: Theme.margin.m2,
          backgroundColor: color,
        }}
      />
      <Text style={styles.sectionHeaderText}>{title}</Text>
    </View>
  );
};

export default SectionHeader;
