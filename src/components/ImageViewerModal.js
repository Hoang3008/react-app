import React from 'react';
import {View} from 'react-native';
import {Modal} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';

function ImageViewerModal(props) {
  const {images, show, onDismiss} = props;

  return (
    <View>
      <Modal visible={show} transparent={true}>
        <ImageViewer
          renderIndicator={() => {}}
          imageUrls={images}
          onCancel={onDismiss}
          onSwipeDown={onDismiss}
          enableSwipeDown
        />
      </Modal>
    </View>
  );
}

export default ImageViewerModal;
