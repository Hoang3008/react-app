import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

import Theme from '../Theme';

export default class UniDatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDateTimePickerVisible: false,
    };
  }

  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = (date) => {
    this.hideDateTimePicker();
    this.props.onSelect && this.props.onSelect(date);
    // this.props.onSelect && this.props.onSelect(`${date.getFullYear()}-${(date.getMonth()+1)<10?'0':''}${date.getMonth()+1}-${date.getDay()<10?'0':''}${date.getDay()}`);
  };

  render() {
    let {label, selectedValue, mode = 'date'} = this.props;
    let date = new Date(selectedValue);

    return (
      <View style={styles.container}>
        <Text style={styles.textNormalSecond}>{label}</Text>
        <TouchableOpacity onPress={this.showDateTimePicker}>
          <Text style={styles.textNormal}>
            {mode === 'date'
              ? moment(date).format('MM/DD/YYYY')
              : moment(date).format('MM/DD/YYYY HH:mm')}
          </Text>
          <Image
            style={styles.icDropdown}
            source={require('../assets/img/ic_dropdown.png')}
          />
        </TouchableOpacity>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
          date={date}
          mode={mode}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    marginHorizontal: Theme.margin.m15,
    marginVertical: Theme.margin.m8,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.border,
    borderRadius: Theme.margin.m4,
    paddingVertical: Theme.margin.m12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textNormalSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
    marginLeft: Theme.margin.m14,
  },
  textNormal: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.normal,
    marginRight: Theme.margin.m30,
    textAlign: 'center',
  },
  icDropdown: {
    position: 'absolute',
    right: Theme.margin.m12,
    top: 0,
    bottom: 0,
    width: Theme.margin.m8,
    height: 20,
    resizeMode: 'contain',
  },
};
