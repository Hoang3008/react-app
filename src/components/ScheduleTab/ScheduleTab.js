import * as React from 'react';
import {Animated, TouchableOpacity, Text} from 'react-native';

const Tab = ({style = {}, focusAnim, title, onPress}) => {
  return (
    <TouchableOpacity style={style} onPress={onPress}>
      <Animated.View
        style={{
          height: '100%',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
          borderRadius: 6,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,
          elevation: focusAnim.interpolate({
            inputRange: [0, 99, 100],
            outputRange: [0, 0, 3],
          }),
          backgroundColor: focusAnim.interpolate({
            inputRange: [0, 1],
            outputRange: ['transparent', 'white'],
          }),
        }}>
        <Text
          style={{
            textAlign: 'center',
            color: '#000',
          }}>
          {title}
        </Text>
      </Animated.View>
    </TouchableOpacity>
  );
};

export default Tab;
