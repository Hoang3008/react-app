import React, {
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
  useRef,
} from 'react';
import moment from 'moment';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
import {groupBy, propOr} from 'ramda';
import Carousel from 'react-native-snap-carousel';
import styles from './styles.js';
import {getTodayDateMoment} from '../../utils/DateUtils';
import DayView from '../DayView/DayView';

const PriorityColor = {
  Priority1: '#FFEB33',
  Priority2: '#FFA800',
  Priority3: '#FFA800',
  Complete: '#62E787',
  Verify: '#519D59',
};

const MAX_SCROLL = 800;
const carouselWidth = Math.round(Dimensions.get('window').width);

const TaskDayView = (props, ref) => {
  const {data, onDateChanged, onItemClick} = props;
  const [fakeList, setFakeList] = useState([]);
  const [dataByDate, setDataByDate] = useState({});
  const pagerRef = useRef();
  const timeScrollViewRef = useRef();

  useImperativeHandle(ref, () => ({
    viewToday: () => {
      pagerRef.current.snapToItem(MAX_SCROLL / 2, true, true);
    },
  }));

  useEffect(() => {
    const dataByDate = groupBy(({PMTaskStartDate}) =>
      moment(PMTaskStartDate).format('YYYY-MM-DD HH'),
    )(data);
    setDataByDate(dataByDate);

    const fakeList = [];
    const todayMoment = getTodayDateMoment().subtract(MAX_SCROLL / 2, 'day');

    for (let i = 0; i < MAX_SCROLL; i++) {
      fakeList.push(todayMoment.format('YYYY-MM-DD'));
      todayMoment.add(1, 'day');
    }
    setFakeList(fakeList);
  }, [data]);

  const handleScroll = (event) => {
    // const {
    //   nativeEvent: {
    //     contentInset: {bottom, left, right, top},
    //     contentOffset: {x, y},
    //     contentSize: {height, width},
    //     layoutMeasurement: {height, width},
    //     zoomScale
    //   }
    // } = event;
    if (!timeScrollViewRef.current) {
      return;
    }

    const {
      nativeEvent: {
        contentOffset: {x, y},
      },
    } = event;

    timeScrollViewRef.current.scrollTo({x, y, animated: false});
  };

  const renderDate = (date, time) => {
    const dateStr = moment(date).format('YYYY-MM-DD');
    const timeStr = moment(time, 'H').format('HH');
    const dateTimeStr = `${dateStr} ${timeStr}`;

    const taskList = propOr([], dateTimeStr, dataByDate);

    return (
      <>
        {taskList.map((task) => {
          const {PMTaskName, PMTaskPriorityCombo} = task;
          return (
            <TouchableOpacity
              onPress={() => onItemClick(task)}
              style={[
                styles.textContainer,
                {backgroundColor: PriorityColor[PMTaskPriorityCombo]},
              ]}>
              <Text style={[styles.text]}>{PMTaskName}</Text>
            </TouchableOpacity>
          );
        })}
      </>
    );
  };

  const renderItem = ({item, index}) => {
    return (
      <View key={item} style={styles.container}>
        <DayView
          date={moment(item, 'YYYY-MM-DD').toDate()}
          renderDate={renderDate}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Carousel
        ref={pagerRef}
        data={fakeList}
        renderItem={renderItem}
        sliderWidth={carouselWidth}
        itemWidth={carouselWidth}
        inactiveSlideOpacity={1}
        inactiveSlideScale={1}
        firstItem={MAX_SCROLL / 2}
        initialScrollIndex={MAX_SCROLL / 2}
        getItemLayout={(data, index) => ({
          length: carouselWidth,
          offset: carouselWidth * index,
          index,
        })}
        onSnapToItem={(index) => onDateChanged(fakeList[index])}
      />
    </View>
  );
};

// AACreatedDate: "2020-04-12T09:35:52.843Z"
// AACreatedUser: "evenhieu@gmail.com"
// AAStatus: "Alive"
// AAUpdatedDate: "2020-04-21T20:05:40.207Z"
// AAUpdatedUser: "evenhieu@gmail.com"
// FK_ADAssignUserID: 74
// FK_ADUserID: 196
// FK_PMProjectCategoryID: 0
// FK_PMProjectID: 117
// FK_PMProjectLocationID: 0
// FK_PMTaskGroupID: 0
// FK_PPDrawingID: 0
// FK_PPMDRID: null
// PMTaskCompleteCheck: false
// PMTaskCompletePct: 100
// PMTaskCost: 0
// PMTaskDesc: "test 123"
// PMTaskEndDate: "2020-04-16T00:00:00.000Z"
// PMTaskID: 1421
// PMTaskLat: 0
// PMTaskLng: 0
// PMTaskLocation: ""
// PMTaskManPower: 0
// PMTaskName: "test"
// PMTaskNo: "0"
// PMTaskPriorityCombo: "Priority3"
// PMTaskStartDate: "2020-04-30T00:00:00.000Z"
// PMTaskTags: ""
// PMTaskTypeCombo: "task"

// https://apidev.rocez.com/PMTasks/GetAllDataByCondition?FK_PMProjectID=117&PMTaskTypeCombo=task&dtFromEndDate=1900-01-01&dtToEndDate=2020-05-24T10:16:49.674Z

export default forwardRef(TaskDayView);
