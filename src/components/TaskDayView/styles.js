export default {
  container: {
    width: '100%',
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
  },

  item: {
    width: '100%',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  textContainer: {
    zIndex: 9999,
    width: '97%',
    height: 50,
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 2,
    paddingBottom: 2,
    borderBottomRightRadius: 2,
    borderTopRightRadius: 2,
  },
  text: {
    height: '100%',
    color: 'white',
    fontSize: 12,
    width: '100%',
  },
  scrollView: {
    flex: 1,
    marginTop: 0,
    borderColor: '#ebebeb',
    borderStyle: 'solid',
    borderRightWidth: 1,
  },

  leftCol: {
    display: 'flex',
    flexDirection: 'column',
  },
  timeView: {
    height: 60,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
};
