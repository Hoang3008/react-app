import React, {useEffect, useState, useRef} from 'react';
import moment from 'moment';
import {View, Text, TouchableOpacity} from 'react-native';
import {propOr, groupBy} from 'ramda';
import ViewPager from '@react-native-community/viewpager';
import styles from './styles.js';
import MonthView from '../MonthView/MonthView';

const PriorityColor = {
  Priority1: '#FFEB33',
  Priority2: '#FFA800',
  Priority3: '#FFA800',
  Complete: '#62E787',
  Verify: '#519D59',
};

const TaskMonthView = (props) => {
  const {data, onDateChanged, date, onItemClick} = props;
  const [dataByDate, setDataByDate] = useState({});
  const pagerRef = useRef();

  useEffect(() => {
    const dataByDate = groupBy(({PMTaskStartDate}) =>
      moment(PMTaskStartDate).format('YYYY-MM-DD'),
    )(data);
    setDataByDate(dataByDate);
  }, [data]);

  const renderDate = (date) => {
    const taskList = propOr([], date, dataByDate);
    return (
      <>
        {taskList.map((task) => {
          const {PMTaskID, PMTaskName, PMTaskPriorityCombo} = task;
          return (
            <TouchableOpacity
              key={PMTaskID}
              onPress={() => {
                onItemClick(task);
              }}
              style={[
                styles.textContainer,
                {backgroundColor: PriorityColor[PMTaskPriorityCombo]},
              ]}>
              <Text style={[styles.text]} numberOfLines={1}>
                {PMTaskName}
              </Text>
            </TouchableOpacity>
          );
        })}
      </>
    );
  };

  const renderPage = (dateMoment) => {
    const key = dateMoment.format('YYYY-MM-DD');
    return (
      <View key={key} style={styles.container}>
        <MonthView date={dateMoment.toDate()} renderDate={renderDate} />
      </View>
    );
  };

  return (
    <ViewPager
      pagingEnabled={true}
      ref={pagerRef}
      initialPage={3}
      style={{flex: 1, height: '100%'}}
      onPageSelected={(event) => {
        const {
          nativeEvent: {position},
        } = event;

        if (position === 3) {
          return;
        }

        const newDate =
          position < 3
            ? moment(date).subtract(3 - position, 'month')
            : moment(date).add(position - 3, 'month');
        onDateChanged(newDate);
        pagerRef.current.setPageWithoutAnimation(3);
      }}>
      {renderPage(moment(date).subtract(3, 'month'))}
      {renderPage(moment(date).subtract(2, 'month'))}
      {renderPage(moment(date).subtract(1, 'month'))}
      {renderPage(moment(date))}
      {renderPage(moment(date).add(1, 'month'))}
      {renderPage(moment(date).add(2, 'month'))}
      {renderPage(moment(date).add(3, 'month'))}
    </ViewPager>
  );
};

// AACreatedDate: "2020-04-12T09:35:52.843Z"
// AACreatedUser: "evenhieu@gmail.com"
// AAStatus: "Alive"
// AAUpdatedDate: "2020-04-21T20:05:40.207Z"
// AAUpdatedUser: "evenhieu@gmail.com"
// FK_ADAssignUserID: 74
// FK_ADUserID: 196
// FK_PMProjectCategoryID: 0
// FK_PMProjectID: 117
// FK_PMProjectLocationID: 0
// FK_PMTaskGroupID: 0
// FK_PPDrawingID: 0
// FK_PPMDRID: null
// PMTaskCompleteCheck: false
// PMTaskCompletePct: 100
// PMTaskCost: 0
// PMTaskDesc: "test 123"
// PMTaskEndDate: "2020-04-16T00:00:00.000Z"
// PMTaskID: 1421
// PMTaskLat: 0
// PMTaskLng: 0
// PMTaskLocation: ""
// PMTaskManPower: 0
// PMTaskName: "test"
// PMTaskNo: "0"
// PMTaskPriorityCombo: "Priority3"
// PMTaskStartDate: "2020-04-30T00:00:00.000Z"
// PMTaskTags: ""
// PMTaskTypeCombo: "task"

// https://apidev.rocez.com/PMTasks/GetAllDataByCondition?FK_PMProjectID=117&PMTaskTypeCombo=task&dtFromEndDate=1900-01-01&dtToEndDate=2020-05-24T10:16:49.674Z

export default TaskMonthView;
