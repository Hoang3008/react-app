export default {
  container: {
    width: '100%',
    flex: 1,
    display: 'flex',
  },

  textContainer: {
    width: '97%',
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 2,
    paddingBottom: 2,
    borderBottomRightRadius: 2,
    borderTopRightRadius: 2,
  },
  text: {
    color: 'white',
    fontSize: 12,
    width: '100%',
  },
};
