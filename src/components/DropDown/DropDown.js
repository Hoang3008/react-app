import React from 'react';
import {View, Text, Image} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import {propOr} from 'ramda';

import styles from './styles';

import Theme from '../../Theme';

const DropDown = (props) => {
  const {
    label,
    options,
    onSelect,
    onShowDropdown,
    value,
    idKey,
    nameKey = '',
    noKey = '',
    descriptionKey = '',
    containerStyle = {},
  } = props;

  const defaultValue = `Select ${label}`;

  const getValue = () => {
    return (options.find((item) => value === propOr('', idKey, item)) || {})[
      nameKey
    ];
  };

  const renderButtonText = (data) => {
    return propOr('', noKey, data);
  };

  const renderRow = (rowData) => {
    const no = rowData[noKey] || '';
    const name = rowData[nameKey] || '';
    const description = rowData[descriptionKey] || '';


    return (
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'flex-start',
          paddingVertical: Theme.margin.m4,
        }}>
        <Text style={styles.textNo}>{no}</Text>
        <Text style={styles.textValue}>{name}</Text>
        <Text style={styles.textNo}>{description}</Text>
      </View>
    );
  };

  return (
    <View style={[styles.container, containerStyle]}>
      <Text style={styles.textNormalSecond}>{label}</Text>
      <View style={{flexDirection: 'column', marginLeft: Theme.margin.m10}}>
        <ModalDropdown
          options={options}
          defaultValue={getValue() || defaultValue}
          onSelect={onSelect} /* smell code */
          textStyle={styles.textNormal}
          renderButtonText={renderButtonText}
          onDropdownWillShow={() => {
            onShowDropdown();
            return false;
          }}
          // neu khong co onDropdownWillShow thi se show renderRow
          renderRow={(rowData, rowId, highlighted) =>
            renderRow(rowData, rowId, highlighted)
          }
          dropdownStyle={styles.dropBoxItemsContainer}
        />
        <Image
          style={styles.icDropdown}
          source={require('../../assets/img/ic_dropdown.png')}
        />
      </View>
    </View>
  );
};

export default DropDown;
