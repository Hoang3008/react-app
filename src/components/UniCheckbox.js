import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import Theme from '../Theme';

export default class UniCheckbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: this.props.checked || false,
      component: this.props?.component
    };
  }

  onChangeValue = () => {
    let newValue = !this.props.checked;
    this.props.onChangeValue && this.props.onChangeValue(newValue);
    this.setState({checked: newValue});
  };

  render() {
    return (
      <TouchableOpacity style={styles.container } onPress={this.onChangeValue}>
        {this.props.checked ? (
          <View
            style={{
              width: 8,
              height: 8,
              borderRadius: 4,
              backgroundColor: '#336699',
            }}
          />
        ) : null}
      </TouchableOpacity>
    );
  }
}

const styles = {
  container: {
    width: Theme.specifications.checkBoxSize,
    height: Theme.specifications.checkBoxSize,
    borderRadius: 3,
    borderColor: Theme.colors.border,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  containerDrawingFilter: {
    width: Theme.specifications.checkboxDrawingFilter,
    height: Theme.specifications.checkboxDrawingFilter,
    borderRadius: 3,
    borderColor: Theme.colors.border,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
};
