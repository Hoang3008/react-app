import React from 'react';

import Theme from '../../Theme';
import UniImageButton from '../UniImageButton';

const FloaterAddButton = (props) => {
  const {onPress} = props;
  return (
    <UniImageButton
      containerStyle={styles.floatButton}
      source={require('../../assets/img/ic_add.png')}
      onPress={onPress}
    />
  );
};

const styles = {
  floatButton: {
    position: 'absolute',
    bottom: Theme.margin.m16,
    right: Theme.margin.m20,
    width: Theme.specifications.floatButtonSize,
    height: Theme.specifications.floatButtonSize,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#34AA44',
    borderRadius: Theme.specifications.floatButtonSize / 2,
    borderWidth: 0,
  },
};

export default FloaterAddButton;
