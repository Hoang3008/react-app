import React from 'react';
import {View, Text, Image} from 'react-native';
import Theme from '../Theme';
import ModalDropdown from 'react-native-modal-dropdown';
import {get} from 'lodash';

export default class UniDropbox extends React.Component {
  constructor(props) {
    super(props);
    this.defaultValue = 'Select ' + this.props.label;
  }

  getDefaultValue() {
    const {selectedKey, pathLeft} = this.props;
    const compareValue = selectedKey || pathLeft;
    this.props.options.map((item) => {
      if (this.props.selectedValue === get(item, compareValue)) {
        this.defaultValue = get(item, this.props.pathValue);
      }
    });
  }

  onSelect = (index, item) => {
    this.props.onSelect &&
      this.props.onSelect(get(item, this.props.pathLeft), item);
  };

  renderRow = (rowData) => {
    const {pathValue, pathLeft} = this.props;

    let left = get(rowData, pathLeft);
    let value = get(rowData, pathValue);

    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: Theme.margin.m4,
        }}>
        {!this.props.hideLeft ? (
          <View style={styles.leftContainer}>
            <Text style={styles.textSmall}>{left}</Text>
          </View>
        ) : null}
        <Text style={styles.textValue}>{value}</Text>
      </View>
    );
  };

  renderButtonText = (data) => {
    const {pathValue} = this.props;
    return get(data, pathValue);
  };

  render() {
    let {label} = this.props;
    this.getDefaultValue();
    return (
      <View style={styles.container}>
        <Text style={styles.textNormalSecond}>{label}</Text>
        <View
          style={{flexDirection: 'row', flex: 1, justifyContent: 'flex-end'}}>
          <ModalDropdown
            disabled={this.props.disabled}
            style={{maxWidth: '90%', opacity: this.props.disabled ? 0.6 : 1}}
            options={this.props.options}
            defaultValue={this.defaultValue || ''}
            onSelect={this.onSelect}
            textStyle={styles.textNormal}
            renderButtonText={this.renderButtonText}
            renderRow={(rowData, rowId, highlighted) =>
              this.renderRow(rowData, rowId, highlighted)
            }
            dropdownStyle={styles.dropBoxItemsContainer}
          />
          <Image
            style={styles.icDropdown}
            source={require('../assets/img/ic_dropdown.png')}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    marginHorizontal: Theme.margin.m15,
    marginVertical: Theme.margin.m8,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.border,
    borderRadius: Theme.margin.m4,
    paddingVertical: Theme.margin.m12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textNormalSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
    marginLeft: Theme.margin.m14,
  },
  textNormal: {
    maxWidth: '100%',
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.normal,
    marginRight: Theme.margin.m30,
    textAlign: 'center',
  },
  textValue: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.normal,
    marginRight: Theme.margin.m10,
    marginLeft: Theme.margin.m5,
    textAlign: 'center',
    paddingVertical: Theme.margin.m4,
  },
  dropBoxItemsContainer: {
    elevation: 1,
    shadowColor: '#000',
    shadowOpacity: 0.3,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    borderRadius: Theme.margin.m4,
    borderColor: Theme.colors.border,
    paddingHorizontal: Theme.margin.m16,
    height: 280,
    minWidth: 230,
  },
  textSmall: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.small_12,
  },
  leftContainer: {
    width: Theme.margin.m24,
    height: Theme.margin.m24,
    borderRadius: Theme.margin.m24 / 2,
    color: Theme.colors.textNormal,
    borderWidth: 0.5,
    margin: Theme.margin.m4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icDropdown: {
    position: 'absolute',
    right: Theme.margin.m12,
    top: 0,
    bottom: 0,
    width: Theme.margin.m8,
    height: 20,
    resizeMode: 'contain',
  },
};
