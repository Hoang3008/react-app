import ViewPager from '@react-native-community/viewpager';
import React, {useEffect, useRef, useState} from 'react';

const InfiniteViewPager = (props) => {
  const {data, renderCount, onPageChanged, renderPage} = props;
  const [pageList, setPageList] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const pagerRef = useRef();

  useEffect(() => {
    const pageList = [];
    for (let i = 0; i < renderCount; i++) {
      pageList.push(i);
    }
    setPageList(pageList);

    // setTimeout(() => {
    //   pagerRef.current.setPageWithoutAnimation(centerIndex);
    // }, 100);
  }, [renderCount, data, currentPage]);

  const centerIndex = Math.ceil(renderCount / 2);

  return (
    <ViewPager
      ref={pagerRef}
      initialPage={centerIndex}
      style={{flex: 1, height: '100%'}}
      onPageSelected={(event) => {
        const {
          nativeEvent: {position},
        } = event;

        if (position === centerIndex && currentPage === centerIndex) {
          return;
        }

        onPageChanged(position - centerIndex);
        setCurrentPage(centerIndex);
        pagerRef.current.setPageWithoutAnimation(centerIndex);
      }}>
      {pageList.map((page, index) => renderPage(index - centerIndex))}
    </ViewPager>
  );
};

export default InfiniteViewPager;
