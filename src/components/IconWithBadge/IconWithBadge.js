import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';

const IconWithBadge = props => {
  const {name, badgeCount, color, size} = props;
  return (
    <View style={{width: 40, height: 40, margin: 0, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
      <Icon style={{ marginTop: 4 }} name={name} size={size} color={color} />
      {badgeCount > 0 && (
        <View
          style={{
            // On React Native < 0.57 overflow outside of parent will not work on Android, see https://git.io/fhLJ8
            position: 'absolute',
            right: -2,
            top: -2,
            backgroundColor: 'red',
            borderRadius: 8,
            width: 15,
            height: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white', fontSize: 10, fontWeight: 'bold'}}>
            {badgeCount}
          </Text>
        </View>
      )}
    </View>
  );
};

export default IconWithBadge;
