import React from 'react';
import {TextInput, Platform, View, TouchableOpacity,Text} from 'react-native';
import {getBrand} from 'react-native-device-info';

import Theme from '../Theme';

const {Version} = Platform;

const brandsNeedingWorkaround = ['redmi', 'xiaomi', 'poco', 'pocophone'];
const needsXiaomiWorkaround =
  brandsNeedingWorkaround.includes(getBrand().toLowerCase()) && Version > 28;

export default class UniInputBtn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hackCaretHidden: !!needsXiaomiWorkaround,
      value: this.props.initValue || '',
    };
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.initValue !== prevProps.initValue) {
      this.setState({value: this.props.initValue});
    }
  }

  onChangeText = (text) => {
      // return value callback to parent component
    this.props.onChangeText && this.props.onChangeText(text);
    this.setState({value: text});
  };

  render() {
    let {
      forceValueFromProp = false,
      value: valueFromProp,
      autoSelectAllWhenTextIs,
      containerStyle,
      disable,
      secureTextEntry,
      placeholder = '',
      numberOfLines = 1,
      keyboardType = 'default',
      onPress
    } = this.props;

    const {value = '', hackCaretHidden} = this.state;
    const otherProps = hackCaretHidden ? {caretHidden: true} : {};
    

    return (
        <View style={{flexDirection:'row'}}>
            <View style={{flex:5}}>
                <TextInput
                    {...otherProps}
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType={keyboardType}
                    multiline={numberOfLines > 1}
                    numberOfLines={numberOfLines}
                    selectTextOnFocus={autoSelectAllWhenTextIs === value}
                    placeholder={placeholder}
                    style={[styles.container, containerStyle || {}]}
                    onChangeText={(text) => this.onChangeText(text)}
                    value={forceValueFromProp ? valueFromProp : value}
                    editable={!disable}
                    secureTextEntry={secureTextEntry}
                />
            </View>
            <View style={{flex:2}}>
            < TouchableOpacity
                    onPress={onPress}
                    style={[styles.buttonContainer, containerStyle]}>
                    <Text style={styles.buttonText}>ADD</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
  }
}

const styles = {
  container: {
    alignSelf: 'stretch',
    height: Theme.specifications.editTextNormalHeight,
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textNormal,
    paddingHorizontal: 12,
    paddingVertical: 9,
    borderWidth: 1,
    borderRadius: Theme.specifications.editTextNormalBorder,
    borderColor: Theme.colors.border,
  },
  buttonContainer: {
    alignItems: 'center',
    padding: 14,
    backgroundColor: 'green',
    borderRadius: 4,
    flexDirection: 'row',
  },
  buttonText: {
    flex: 1,
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
};
