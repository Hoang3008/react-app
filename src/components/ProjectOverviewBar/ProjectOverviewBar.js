import React from 'react';
import {View} from 'react-native';

import styles from './styles';

const ProjectOverviewBar = (props) => {
  const {values, style = {}} = props;

  const shownValues = values.filter(({value}) => !!value);

  return (
    <View style={[styles.container, style]}>
      {shownValues.map(({value, color}, index) => {
        const leftCorner = index === 0 ? styles.leftCorner : {};
        const rightCorner =
          index === shownValues.length - 1 ? styles.rightCorner : {};

        return (
          <View
            key={`${value}`}
            style={[
              {backgroundColor: color, flex: value},
              leftCorner,
              rightCorner,
            ]}
          />
        );
      })}
    </View>
  );
};

export default ProjectOverviewBar;
