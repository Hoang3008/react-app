export default {
  container: {
    height: 10,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  leftCorner: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  rightCorner: {
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
};
