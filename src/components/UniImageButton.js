import React from 'react';
import {
    TouchableOpacity,
    Image,
} from 'react-native';
import Theme from '../Theme';

export default class UniImageButton extends React.PureComponent {
    render() {
        let { width, height, source, onPress, containerStyle } = this.props;
        return(
            <TouchableOpacity style={containerStyle || {}} onPress={() => onPress && onPress()}>
                <Image
                    style={{width: width || Theme.specifications.icSmall, height: height || Theme.specifications.icSmall, resizeMode: 'contain'}}
                    source={source || require('../assets/img/unicore.png')}
                />
            </TouchableOpacity>
        );
    };
}
