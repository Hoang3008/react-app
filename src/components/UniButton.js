import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Theme from '../Theme';

export default class UniButton extends React.PureComponent {
  render() {
    const {text, onPress, containerStyle, disabled = false} = this.props;

    const disabledStyle = disabled ? {opacity: 0.4} : {};

    return (
      <TouchableOpacity
        disabled={disabled}
        onPress={disabled ? () => {} : onPress}
        style={[styles.buttonContainer, containerStyle || {}, disabledStyle]}>
        <Text style={styles.buttonText}>{text}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  buttonContainer: {
    alignItems: 'center',
    padding: 14,
    backgroundColor: Theme.colors.primary,
    borderRadius: 4,
    flexDirection: 'row',
  },
  buttonText: {
    flex: 1,
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
  },
};
