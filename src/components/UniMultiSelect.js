import React, {useEffect, useState, useRef} from 'react';
import Theme from '../Theme';
import {View} from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import {connect} from 'react-redux';
import UpdateWather from './../action/UpdateWather';
import {getWatcherListAction} from '../action/GetWatcherActions';
import {pathOr, propOr} from 'ramda';

const UniMultiSelect = (props) => {
  const { canChange , projectId, taskId, getWatcherList, peopleInProject, UpdateWather, watcher, type} = props;
  const [selectedPeople, setSelectedPeople] = useState([]);
  const peopleRef = useRef(null);

  useEffect(() => {
    getWatcher();
    if (canChange) {
      peopleRef.current._submitSelection = () => {
        handleClick();
        peopleRef.current._clearSearchTerm();
        peopleRef.current._toggleSelector();
      }
    }

  }, [peopleRef]);
  

  useEffect(() => {
    setSelectedPeople(watcher?.watcherList)
  }, [watcher]);
  

  const getWatcher = () => {
		getWatcherList(projectId, taskId, peopleInProject);
  }
	
	const handleClick = () => {
    const peoples = peopleRef.current.props.selectedItems;
    const reduced = [];
    peopleInProject.forEach(o => {
      peoples.includes(o.ADUserID) && reduced.push(o.PMProjectPeopleEmailAddress);
    } );
   UpdateWather(projectId,taskId,reduced,type);
	}
	
		return (
        <>
        <View style={{height: 10}} />
  			<View style={{ flex: 1, marginRight: Theme.margin.m16, marginLeft: Theme.margin.m16,}}>
        <MultiSelect
        styleInputGroup={{
          padding: 10,
          paddingLeft: 10,
          paddingRight: 5,
        }}
        styleDropdownMenuSubsection={{
          borderRadius: Theme.margin.m4,
          borderColor: Theme.colors.border,
          borderWidth: 1,
          padding: 10,
          paddingLeft: 10,
          paddingRight: 5,
          height: 50,
        }}
        styleListContainer={{maxHeight: 150}}
        ref={peopleRef}
        hideTags
        items={peopleInProject}
        uniqueKey="ADUserID" // "
        onSelectedItemsChange={setSelectedPeople}
        selectedItems={selectedPeople}
        selectText="Select Watchers"
        searchInputPlaceholderText="Search Watchers..."
        tagRemoveIconColor="#CCC"
        tagBorderColor="#CCC"
        tagTextColor="#333"
        selectedItemTextColor="#000"
        selectedItemIconColor="#aaa"
        itemTextColor="#000"
        displayKey="fullName"
        searchInputStyle={{color: '#CCC'}}
        submitButtonColor={Theme.colors.primary}
        submitButtonText="Save"
      />
      {peopleRef.current && (
      <View>
        {peopleRef.current.getSelectedItemsExt(selectedPeople)}
        
      </View>
    )}
     </View>
        </>
    );
  }


const mapStateToProps = (
  {auth: {user, project}, commonData, photoList, watcher},
  props,
) => {
  return {
		user,
    watcher,
    peopleInProject: pathOr([], ['currentProject', 'users'], commonData),
  };
};

const mapDispatchToProps = {
	UpdateWather: UpdateWather.action,
	getWatcherList: getWatcherListAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UniMultiSelect);



const styles = {
  container: {
    alignSelf: 'stretch',
    height: Theme.specifications.editTextNormalHeight,
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textNormal,
    paddingHorizontal: 12,
    paddingVertical: 9,
    borderWidth: 1,
    borderRadius: Theme.specifications.editTextNormalBorder,
    borderColor: Theme.colors.border,
  },
  buttonContainer: {
    alignItems: 'center',
    padding: 14,
    backgroundColor: 'green',
    borderRadius: 4,
    flexDirection: 'row',
  },
  buttonText: {
    flex: 1,
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
};
