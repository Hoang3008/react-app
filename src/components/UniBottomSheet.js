import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';

import Theme from '../Theme';

export default class UniBottomSheet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPDF: false,
    };
  }

  show = (callback, showPDF = false) => {
    this.callback = callback;

    this.setState(
      {
        showPDF,
      },
      () => {
        this.bottomSheet && this.bottomSheet.snapTo(0);
      },
    );
  };

  hide = () => {
    this.bottomSheet && this.bottomSheet.snapTo(2);
  };

  onPressItem = (item) => {
    this.hide();
    this.callback && this.callback(item);
  };

  renderContent = () => {
    const {showPDF} = this.state;
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => this.hide()}
        style={{
          height: '100%',
          backgroundColor: 'rgba(0,0,0,0)',
          justifyContent: 'flex-end',
          elevation: 15,
        }}>
        <TouchableOpacity activeOpacity={1} style={styles.container}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}>Options</Text>
            <TouchableOpacity
              style={{padding: Theme.margin.m12}}
              onPress={this.hide}>
              <Image
                style={{
                  width: Theme.specifications.icSmall,
                  height: Theme.specifications.icSmall,
                }}
                source={require('../assets/img/ic_close_grey.png')}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.itemContainer}
            onPress={() => this.onPressItem('camera')}>
            <Text style={styles.contentText}>Camera</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.itemContainer}
            onPress={() => this.onPressItem('photos-device')}>
            <Text style={styles.contentText}>Photos from Device</Text>
          </TouchableOpacity>
          {showPDF && (
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={() => this.onPressItem('files-device')}>
              <Text style={styles.contentText}>PDF from Device</Text>
            </TouchableOpacity>
          )}
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <BottomSheet
        ref={(ref) => (this.bottomSheet = ref)}
        snapPoints={['100%', '100%', 0]}
        renderContent={this.renderContent}
        renderHeader={() => null}
        initialSnap={2}
      />
    );
  }
}

const styles = {
  container: {
    backgroundColor: 'white',
    borderRadius: Theme.margin.m15,
    borderWidth: 0,
    paddingBottom: Theme.margin.m44,
    elevation: Theme.margin.m20,
    shadowColor: '#2f2f2f',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    zIndex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    marginTop: Theme.margin.m6,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerText: {
    marginLeft: Theme.margin.m20,
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
  },
  itemContainer: {
    paddingHorizontal: Theme.margin.m20,
    paddingVertical: Theme.margin.m14,
  },
  contentText: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.header_16,
  },
};
