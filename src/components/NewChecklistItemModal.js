import React, {useState} from 'react';
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';
import Modal from 'react-native-modal';
import Theme from '../Theme';

function NewChecklistItemModal(props) {
  const {show, onDismiss, onPostChecklist} = props;
  const [value, setValue] = useState('');
  const [checked, setChecked] = useState(false);

  const handlePostChecklist = () => {
    onPostChecklist(value, checked);
    onDismiss();
  };

  return (
    <View>
      <Modal isVisible={show}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'column',
            backgroundColor: Theme.colors.backgroundWhite,
            borderRadius: 6,
            padding: Theme.margin.m24,
          }}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              style={{
                paddingRight: Theme.margin.m12,
                paddingVertical: Theme.margin.m4,
              }}
              onPress={() => setChecked(!checked)}>
              <Image
                style={{
                  width: Theme.specifications.checkBoxSize,
                  height: Theme.specifications.checkBoxSize,
                }}
                source={
                  checked
                    ? require('../assets/img/ic_checkbox_active.png')
                    : require('../assets/img/ic_checkbox_inactive.png')
                }
              />
            </TouchableOpacity>
            <TextInput
              style={styles.inputComment}
              multiline={false}
              onChangeText={setValue}
            />
          </View>
          <View
            style={{
              marginTop: 15,
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
            }}>
            <TouchableOpacity
              style={styles.buttonContainer}
              onPress={onDismiss}>
              <Text style={styles.textButton}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonContainerActive}>
              <Text
                style={styles.textButtonActive}
                onPress={handlePostChecklist}>
                Post checklist
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = {
  inputComment: {
    flex: 1,
    minHeight: Theme.margin.m36,
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textNormal,
    marginHorizontal: Theme.margin.m12,
    paddingHorizontal: Theme.margin.m12,
    paddingVertical: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderRadius: Theme.specifications.editTextNormalBorder,
    borderColor: Theme.colors.border,
  },
  buttonContainerActive: {
    padding: Theme.margin.m8,
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.accent,
    backgroundColor: Theme.colors.accent,
  },
  textButtonActive: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textWhite,
  },
  buttonContainer: {
    marginRight: Theme.margin.m8,
    padding: Theme.margin.m8,
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.border,
    alignItems: 'center',
  },
  textButton: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textPrimary,
  },
};

export default NewChecklistItemModal;
