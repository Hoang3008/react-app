import React from 'react';
import {
    View,
    Platform,
} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {SCREEN_WIDTH} from '../utils/DimensionUtils';
import Theme from '../Theme';

export default class UniStatusBarBackground extends React.PureComponent{
    render() {
        return (
            <View style={{width: SCREEN_WIDTH, height: Platform.select({android: 0, ios: getStatusBarHeight()}), backgroundColor: Theme.colors.primary}}/>
        )
    }
}
