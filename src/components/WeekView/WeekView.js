import React, {
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
  useRef,
} from 'react';
import moment from 'moment';
import {View, Text, ScrollView} from 'react-native';
import styles from './styles.js';
import {getTodayDateMoment, setMomentLocale} from '../../utils/DateUtils';

export const TimeList = [
  0,
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23,
];

const WeekView = (props, ref) => {
  const {date, renderDate = () => {}, onScroll, contentOffset} = props;
  const [weekList, setWeekList] = useState([]);
  const scrollRef = useRef();

  useImperativeHandle(ref, () => ({
    scrollTo: (param) => {
      scrollRef.current.scrollTo(param);
    },
  }));

  useEffect(() => {
    setMomentLocale();
    let dateMoment = moment(date);
    dateMoment = dateMoment.startOf('isoWeek');

    const weekList = [];
    for (let i = 0; i < 7; i++) {
      weekList.push(dateMoment.format('YYYY-MM-DD'));
      dateMoment.add(1, 'd');
    }

    setWeekList(weekList);
  }, [date]);

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={{width: 70}} />
        <View style={styles.header}>
          <View style={styles.rowHeader}>
            {weekList.length > 0 &&
              weekList.map((day) => {
                const isToday = getTodayDateMoment().isSame(day, 'day');
                const dayOfWeek = moment(day, 'YYYY-MM-DD').format('dd');
                return (
                  <Text
                    key={dayOfWeek}
                    style={[
                      styles.textDayOfWeek,
                      isToday ? styles.todayText : {},
                    ]}>
                    {dayOfWeek}
                  </Text>
                );
              })}
          </View>
          <View style={styles.rowDateHeader}>
            {weekList.length > 0 &&
              weekList.map((day) => {
                const isToday = getTodayDateMoment().isSame(day, 'day');
                const dayOfWeek = moment(day, 'YYYY-MM-DD').format('D');
                return (
                  <Text
                    key={dayOfWeek}
                    style={[
                      styles.dateOfWeekText,
                      isToday ? styles.todayText : {},
                    ]}>
                    {dayOfWeek}
                  </Text>
                );
              })}
          </View>
          <View style={styles.rowMakeUp}>
            <View style={styles.makeupCell} />
            <View style={styles.makeupCell} />
            <View style={styles.makeupCell} />
            <View style={styles.makeupCell} />
            <View style={styles.makeupCell} />
            <View style={styles.makeupCell} />
            <View style={styles.makeupCell} />
          </View>
        </View>
      </View>
      <ScrollView
        ref={scrollRef}
        disableIntervalMomentum
        scrollEventThrottle={1}
        bounces={false}
        style={{flex: 1, marginTop: -1}}
        onScroll={onScroll}>
        {TimeList.map((time, index) => {
          return (
            <View key={`${time}`} style={styles.timeRow}>
              <View style={styles.timeView}>
                <Text style={[{marginTop: -9, fontSize: 14}]}>
                  {index === 0 ? '' : moment(time, 'H').format('HH:mm')}
                </Text>
              </View>
              {weekList.map((day) => {
                return (
                  <View key={day} style={styles.cell}>
                    {renderDate(day, time)}
                  </View>
                );
              })}
            </View>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default forwardRef(WeekView);
