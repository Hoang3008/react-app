import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  container: {
    flex: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  headerContainer: {
    height: 70,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    borderColor: '#ebebeb',
    borderStyle: 'solid',
    borderBottomWidth: 1,
  },
  header: {
    flex: 1,
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },

  rowMakeUp: {
    width: '100%',
    height: 6,
    display: 'flex',
    flexDirection: 'row',
  },
  makeupCell: {
    flex: 1,
    borderRightColor: '#ebebeb',
    borderRightStyle: 'solid',
    borderRightWidth: 1,
  },

  rowHeader: {
    marginTop: 15,
    marginBottom: 3,
    display: 'flex',
    flexDirection: 'row',
  },
  textDayOfWeek: {
    alignSelf: 'center',
    fontSize: 13,
    color: '#888',
    flex: 1,
    textAlign: 'center',
  },
  dateOfWeekContainer: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dateOfWeekText: {
    fontSize: 22,
    color: '#444',
    flex: 1,
    textAlign: 'center',
  },
  todayText: {
    color: 'blue',
  },
  rowDateHeader: {
    flex: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },

  cell: {
    height: '100%',
    flex: 1,
    paddingTop: 4,
    paddingBottom: 4,
    borderColor: '#ebebeb',
    borderStyle: 'solid',

    borderRightWidth: 1,
    borderTopWidth: 1,
  },

  timeRow: {
    minHeight: 60,
    maxHeight: 60,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  timeView: {
    width: 70,
    height: 60,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: '#ebebeb',
    borderStyle: 'solid',
    borderRightWidth: 1,
  },
};
