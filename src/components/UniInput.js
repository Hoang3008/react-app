import React from 'react';
import {TextInput, Platform} from 'react-native';
import {getBrand} from 'react-native-device-info';

import Theme from '../Theme';

const {Version} = Platform;

const brandsNeedingWorkaround = ['redmi', 'xiaomi', 'poco', 'pocophone'];
const needsXiaomiWorkaround =
  brandsNeedingWorkaround.includes(getBrand().toLowerCase()) && Version > 28;

export default class UniInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hackCaretHidden: !!needsXiaomiWorkaround,
      value: this.props.initValue || '',
    };
  }

  onChangeText = (text) => {
    this.props.onChangeText && this.props.onChangeText(text);
    this.setState({value: text});
  };

  render() {
    let {
      forceValueFromProp = false,
      value: valueFromProp,
      autoSelectAllWhenTextIs,
      containerStyle,
      disable,
      secureTextEntry,
      placeholder = '',
      numberOfLines = 1,
      keyboardType = 'default',
    } = this.props;

    const {value = '', hackCaretHidden} = this.state;
    const otherProps = hackCaretHidden ? {caretHidden: true} : {};

    return (
      <TextInput
        {...otherProps}
        autoCapitalize="none"
        autoCorrect={false}
        keyboardType={keyboardType}
        multiline={numberOfLines > 1}
        numberOfLines={numberOfLines}
        selectTextOnFocus={autoSelectAllWhenTextIs === value}
        placeholder={placeholder}
        style={[styles.container, containerStyle || {}]}
        onChangeText={(text) => this.onChangeText(text)}
        value={forceValueFromProp ? valueFromProp : value}
        editable={!disable}
        secureTextEntry={secureTextEntry}
      />
    );
  }
}

const styles = {
  container: {
    alignSelf: 'stretch',
    height: Theme.specifications.editTextNormalHeight,
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textNormal,
    paddingHorizontal: 12,
    paddingVertical: 9,
    borderWidth: 1,
    borderRadius: Theme.specifications.editTextNormalBorder,
    borderColor: Theme.colors.border,
  },
};
