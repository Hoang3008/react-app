import React from 'react';
import {
    Text
} from 'react-native';
import Theme from '../Theme';

export default class UniText extends React.PureComponent {
    render() {
        return(
            <Text style={[styles.text, this.props.containerStyle || {}]}>{this.props.children}</Text>
        )
    }
}

const styles = {
    text: {
        color: Theme.colors.textNormal,
        fontSize: Theme.fontSize.normal,
    }
};
