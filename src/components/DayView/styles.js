import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  container: {
    flex: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    minHeight: 70,
    backgroundColor: '#ffffff',
    borderColor: '#ebebeb',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  rowHeader: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  timeHeader: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: 70,
    minWidth: 70,
    height: 70,
    borderColor: '#ebebeb',
    borderStyle: 'solid',
    borderRightWidth: 1,
  },
  textDayOfWeek: {
    marginTop: 10,
    alignSelf: 'center',
    fontSize: 13,
    color: '#888',
    textAlign: 'center',
  },
  dateOfWeekText: {
    marginTop: 6,
    fontSize: 24,
    color: '#444',
    textAlign: 'center',
  },
  todayText: {
    color: 'blue',
  },
  rowDateHeader: {
    flex: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  rowMakeUp: {
    height: 6,
    display: 'flex',
    flexDirection: 'row',
  },
  makeupCell: {
    flex: 1,
    borderRightColor: '#ebebeb',
    borderRightStyle: 'solid',
    borderRightWidth: 1,
  },

  cell: {
    height: '100%',
    flex: 1,
    paddingTop: 4,
    paddingBottom: 4,
    borderColor: '#ebebeb',
    borderStyle: 'solid',
    borderTopWidth: 1,
  },

  timeRow: {
    minHeight: 60,
    maxHeight: 60,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },

  timeView: {
    width: 70,
    height: 60,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: '#ebebeb',
    borderStyle: 'solid',
    borderRightWidth: 1,
  },

  scrollView: {
    width: '100%',
    flex: 1,
    marginTop: -1,
  },
};
