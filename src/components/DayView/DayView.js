import React from 'react';
import moment from 'moment';
import {View, Text, ScrollView} from 'react-native';
import styles from './styles.js';
import {getTodayDateMoment} from '../../utils/DateUtils';

export const TimeList = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];

const DayView = props => {
  const {
    date,
    renderDate,
  } = props;

  const isToday = getTodayDateMoment().isSame(date, 'day');
  const dayOfMonth = moment(date, 'YYYY-MM-DD').format('D');
  const dayOfWeek = moment(date, 'YYYY-MM-DD').format('ddd');

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.timeHeader}>
          <Text style={[styles.textDayOfWeek, isToday ? styles.todayText : {}]}>
            {dayOfWeek}
          </Text>
          <Text style={[styles.dateOfWeekText, isToday ? styles.todayText : {}]}>
            {dayOfMonth}
          </Text>
        </View>
        <View style={styles.rowDateHeader}>

        </View>
      </View>
      <ScrollView
        bounces={false}
        indicatorStyle="white"
        style={styles.scrollView}
      >
        <View>
          {TimeList.map((time, index) => {
            return (
              <View key={`${time}`} style={styles.timeRow}>
                <View style={[styles.timeView]}>
                  {(index > 0 && index) &&
                    <Text style={{ marginTop: -9, fontSize: 14 }}>{moment(time, 'H').format('HH:mm')}</Text>
                  }
                </View>
                <View style={styles.cell}>
                  {
                    renderDate(date, time)
                  }
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default DayView;
