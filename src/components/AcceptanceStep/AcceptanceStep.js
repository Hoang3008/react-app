import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Fontisto';
import IconFeather from 'react-native-vector-icons/Feather';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconEntypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { contains, split, isNil } from 'ramda';

const AcceptanceStep = (props) => {
  const {
    allowEdit = true,
    PMProjectInsStpInspID,
    PMProjectInsStpInspDesc,
    PMProjectInsStpInspStatus,
    PMProjectInsStpInspRequired,
    PMProjectInsStpInspPicRequired,
    PMProjectInsStpInspDocRequired,
    PMProjectInsStpInspDocCount,
    PMProjectInsStpInspPicCount,
    PMInsChklstInspVerifiedby,
    user,
    onUpdateStatus,
    onClickItem,
    onClickAttachment,
    onClickLog,
    onClickPhoto,
    permissions,
    inspection,
    peopleID
  } = props;

  const canAttach = (permissions.permissions?.length > 0 && permissions.permissions[0] === 'CHANGE_ALL') 
  || peopleID === inspection?.FK_PMInspectPeopleID
  || peopleID === inspection?.FK_PMProjectPeopleID
  || peopleID === inspection?.FK_PMProjectApprovedPeopleID;

  const canComment = (permissions.permissions?.length > 0 && permissions.permissions[0] === 'CHANGE_ALL') 
  || peopleID === inspection?.FK_PMInspectPeopleID
  || peopleID === inspection?.FK_PMProjectPeopleID
  || peopleID === inspection?.FK_PMProjectApprovedPeopleID
  || inspection?.AACreatedUser === user?.ADUserName;


  const shouldNotShowPassButton =
    !(!allowEdit ||
    PMProjectInsStpInspStatus === 'Pass' ||
    ((!PMProjectInsStpInspDocRequired || (PMProjectInsStpInspDocRequired && PMProjectInsStpInspDocCount)) 
    && (!PMProjectInsStpInspPicRequired || (PMProjectInsStpInspPicRequired && PMProjectInsStpInspPicCount)) )) &&
   (peopleID === inspection?.FK_PMInspectPeopleID || (permissions.permissions?.length > 0 && permissions.permissions[0] === 'CHANGE_ALL'));
  
   const shouldNotShowFailedButton =
   PMProjectInsStpInspStatus === 'Fail'
   && (peopleID === inspection?.FK_PMInspectPeopleID 
   || (permissions.permissions?.length > 0 && permissions.permissions[0] === 'CHANGE_ALL'));

  const shouldNotShowVerifiedButton =
  !allowEdit ||( !isNil(PMInsChklstInspVerifiedby) && (split('|',PMInsChklstInspVerifiedby).includes(user.ADUserName)
  || (permissions.permissions?.length > 0 && permissions.permissions[0] === 'CHANGE_ALL')));
 

  const shouldNotShowNAButton =
    !allowEdit || PMProjectInsStpInspStatus === 'Open';

  const handleUpdateStatus = (newStatus) => () => {
    onUpdateStatus(newStatus);
  };

  return (
    <View
      style={{
        flexDirection: 'column',
        backgroundColor: '#fff',
        paddingLeft: 4,
        paddingRight: 15,
      }}>
      <TouchableOpacity style={[styles.row]} onPress={onClickItem}>
        <Text style={[styles.status, statusStyle[PMProjectInsStpInspStatus]]}>
          {PMProjectInsStpInspStatus}
        </Text>
        <Text style={[styles.headerText, {flex: 1, marginRight: 15}]}>
          {`${PMProjectInsStpInspID}. ${PMProjectInsStpInspDesc}`}
          {PMProjectInsStpInspRequired && (
            <Text style={styles.requiredText}>*</Text>
          )}
        </Text>
        <Icon size={12} color={'#9EA0A5'} name="angle-right" />
      </TouchableOpacity>

      <View
        style={[styles.row, {marginLeft: 10, paddingTop: 0, paddingBottom: 0}]}>
        <TouchableOpacity
          onPress={onClickLog}
          disabled={!canComment}
          style={[{flexDirection: 'row', flex: 10, alignItems: 'center'}]}>
          <MaterialIcons size={20} color="#235894" name="history" />
          <Text style={[styles.contentText, {marginLeft: 6}, {opacity: !canAttach ? 0.4 : 1}]}>Lịch sử</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={onClickAttachment}
          style={[{flexDirection: 'row', flex: 11, alignItems: 'center'}]}
          disabled={!canAttach}>
          <IconEntypo size={14} color="#235894" name="attachment" />
          <Text style={[styles.contentText, {marginLeft: 6}, {opacity: !canAttach ? 0.4 : 1}]}>
            Đính kèm
            {PMProjectInsStpInspDocRequired && (
              <Text style={styles.requiredText}>*</Text>
            )}
            ({PMProjectInsStpInspDocCount})
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={onClickPhoto}
          style={[
            {
              marginLeft: 15,
              flexDirection: 'row',
              flex: 11,
              alignItems: 'center',
            },
          ]}
          disabled={!canAttach}>
          <IconFontAwesome size={14} color="#235894" name="camera" />
          <Text style={[styles.contentText, {marginLeft: 6}, {opacity: !canAttach ? 0.4 : 1}]}>
            Hình ảnh
            {PMProjectInsStpInspPicRequired && (
              <Text style={styles.requiredText}>*</Text>
            )}
            ({PMProjectInsStpInspPicCount})
          </Text>
        </TouchableOpacity>
      </View>

      <View style={[styles.row, {justifyContent: 'flex-end', marginTop: 10}]}>
        <TouchableOpacity
          onPress={handleUpdateStatus('Pass')}
          disabled={shouldNotShowPassButton}
          style={[styles.action, {opacity:shouldNotShowPassButton ? 0.4 : 1}]}>
          <IconFeather size={24} color={'#34AA44'} name="check" />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={handleUpdateStatus('Fail')}
          disabled={shouldNotShowFailedButton}
          style={[
            styles.action,
            {opacity: shouldNotShowFailedButton ? 0.4 : 1},
          ]}>
          <IconAnt size={24} color={'#E6492D'} name="close" />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={handleUpdateStatus('Verify')}
          disabled={!shouldNotShowVerifiedButton}
          style={[
            styles.action,
            {
              paddingTop: 0,
              paddingLeft: 0,
              width: 80,
              opacity: !shouldNotShowVerifiedButton ? 0.4 : 1,
            },
          ]}>
          <Text style={[styles.headerText, {fontSize: 14, color: '#007bff'}]}>
            Verified
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleUpdateStatus('Open')}
          disabled={!shouldNotShowNAButton}
          style={[
            styles.action,
            {
              paddingTop: 0,
              paddingLeft: 0,
              opacity: !shouldNotShowNAButton ? 0.4 : 1,
            },
          ]}>
          <Text
            style={[
              styles.headerText,
              {fontSize: 14, opacity: shouldNotShowNAButton ? 0.4 : 1},
            ]}>
            N/A
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = {
  action: {
    paddingTop: 3,
    paddingLeft: 1,
    marginLeft: 10,
    marginRight: 10,
    width: 40,
    height: 40,
    borderRadius: 20,
    borderColor: '#EFEFF3',
    borderWidth: 1,
    overflow: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 17,
    paddingBottom: 17,
  },
  headerText: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    color: '#3E3F42',
  },
  requiredText: {
    color: '#dc3545',
    fontSize: 16,
  },
  status: {
    alignSelf: 'flex-start',
    width: 40,
    textAlign: 'center',
    marginRight: 10,
    paddingHorizontal: 3,
    paddingVertical: 3,
    fontSize: 9,
    marginTop: 4,
    borderRadius: 2,
    borderWidth: 1,
    overflow: 'hidden',
  },
  openStatus: {
    color: '#fff',
    borderColor: '#6c757d',
    backgroundColor: '#6c757d',
  },
  failedStatus: {
    color: '#fff',
    borderColor: '#dc3545',
    backgroundColor: '#dc3545',
  },
  passStatus: {
    color: '#fff',
    borderColor: '#28a745',
    backgroundColor: '#28a745',
  },
  verifiedStatus: {
    color: '#fff',
    borderColor: '#007bff',
    backgroundColor: '#007bff',
  },
  contentText: {
    fontSize: 16,
    lineHeight: 18,
    color: '#235894',
  },
};

const statusStyle = {
  Open: styles.openStatus,
  Verify: styles.verifiedStatus,
  Fail: styles.failedStatus,
  Pass: styles.passStatus,
};

export default AcceptanceStep;
