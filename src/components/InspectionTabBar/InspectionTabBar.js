import React from 'react';
import {TouchableOpacity, View, Text} from 'react-native';

const InspectionTabBar = (props) => {
  const {style = {}, activeTab = 0, onChangeTab} = props;

  return (
    <View
      style={[
        {
          height: 42,
          backgroundColor: '#F6F6F8',
          flexDirection: 'row',
          paddingHorizontal: 4,
          paddingVertical: 4,
        },
        style,
      ]}>
      <TouchableOpacity
        style={activeTab === 0 ? styles.activeTab : styles.inactiveTab}
        onPress={() => onChangeTab(0)}>
        <Text style={activeTab === 0 ? styles.activeText : styles.inactiveText}>
          Nội dung nghiệm thu
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={activeTab === 1 ? styles.activeTab : styles.inactiveTab}
        onPress={() => onChangeTab(1)}>
        <Text style={activeTab === 1 ? styles.activeText : styles.inactiveText}>
          Hình ảnh mẫu
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  activeTab: {
    flex: 1,
    height: '100%',
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  activeText: {
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 22,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#3E3F42',
  },
  inactiveTab: {
    flex: 1,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  inactiveText: {
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 22,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#245894',
  },
};

export default InspectionTabBar;
