import React from 'react';
import {TouchableOpacity, View, Image} from 'react-native';

import Theme from '../../Theme';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import FastImage from 'react-native-fast-image';
import {getTaskDocumentFileUrl} from '../../api/urls';
import {clone, identity, pathOr} from 'ramda';

const BeforeAfter = (props) => {
  const {data = [], onPressPhoto = identity} = props;

  const imageWidth = (SCREEN_WIDTH - Theme.margin.m15 * 2 - 10) / 2;
  const height = (imageWidth * 5) / 4;

  const showData = clone(data).slice(0, 2);
  return (
    <View
      style={{
        marginTop: Theme.margin.m15,
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        height,
        paddingBottom: Theme.margin.m15,
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}>
      {showData.map((item, index) => {
         // alert(JSON.stringify(item.PMTaskDocName));

        let url = item.PMTaskDocName
          ? getTaskDocumentFileUrl({fileName: item.PMTaskDocName})
          : pathOr('', ['tempImage', 'uri'], item);

        return (
          <>
            <TouchableOpacity
              key={`${item.PMTaskDocID}-${index}`}
              onPress={() => onPressPhoto(item, url)}>
              <Image
                resizeMode={FastImage.resizeMode.contain}
                style={{
                  width: imageWidth,
                  height,
                }}
                source={{
                  uri: url,
                }}
              />
            </TouchableOpacity>
            {index === 0 && (
              <View
                key={`${item.PMTaskDocID}-${index}-view`}
                style={{
                  height: 40,
                  width: 1,
                  backgroundColor: '#aaa',
                  marginLeft: 7,
                  marginRight: 7,
                }}
              />
            )}
          </>
        );
      })}
    </View>
  );
};

export default BeforeAfter;
