import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {propOr} from 'ramda';

import Theme from '../../Theme';
import {PRIORITY} from '../../screens/defect/DefectHelper';

const TaskListItem = (props) => {
  const {item, categories = [], showPin = true, onPress} = props;

  const category = categories.find(
    ({PMProjectCategoryID}) =>
      PMProjectCategoryID === item.FK_PMProjectCategoryID,
  );
  const categoryNo = propOr('', 'PMProjectCategoryNo', category);

  const getPin = (item) => {
    if (!item.priority) {
      return require('../../assets/img/ic_pin_grey.png');
    }

    switch (item.priority) {
      case PRIORITY.OVERDUE:
        return require('../../assets/img/ic_pin_overdue.png');
      case PRIORITY.PRIORITY1:
        return require('../../assets/img/ic_pin_priority1.png');
      case PRIORITY.PRIORITY2:
        return require('../../assets/img/ic_pin_priority2.png');
      case PRIORITY.PRIORITY3:
        return require('../../assets/img/ic_pin_priority2.png');
      case PRIORITY.COMPLETE:
        return require('../../assets/img/ic_pin_complete.png');
      case PRIORITY.VERIFY:
        return require('../../assets/img/ic_pin_verify.png');
      default:
        return require('../../assets/img/ic_pin_grey.png');
    }
  };

  return (
    <TouchableOpacity
      style={{
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'white',
      }}
      onPress={() => onPress(item)}>
      <View>
        {showPin && (
          <Image
            style={{
              marginHorizontal: Theme.margin.m16,
              width: Theme.margin.m22,
              height: Theme.margin.m30,
            }}
            source={getPin(item)}
          />
        )}
        <Text
          style={{
            position: 'absolute',
            top: 4,
            left: 1,
            right: 0,
            color: Theme.colors.textWhite,
            textAlign: 'center',
            fontSize: Theme.fontSize.small_12,
          }}>
          {categoryNo}
        </Text>
      </View>
      <View style={{flex: 1, paddingVertical: 8}}>
        <Text
          style={{
            color: Theme.colors.textSecond,
            fontSize: Theme.fontSize.small_12,
            paddingBottom: Theme.margin.m3,
          }}>
          {`#${item.PMTaskID}`}
        </Text>
        <Text
          style={{
            color: Theme.colors.textNormal,
            fontSize: Theme.fontSize.header_16,
            paddingRight: Theme.margin.m20,
          }}>
          {item.PMTaskName || ''}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default TaskListItem;
