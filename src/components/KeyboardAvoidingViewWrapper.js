import React from 'react';
import {KeyboardAvoidingView, Platform, View} from 'react-native';

const KeyboardAvoidingViewWrapper = props => {
  if (Platform.OS === 'ios') {
    return <KeyboardAvoidingView {...props} />;
  }

  return <View {...props} />;
};

export default KeyboardAvoidingViewWrapper;
