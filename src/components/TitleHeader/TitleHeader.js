import React from 'react';
import {Text, View} from 'react-native';

import styles from './TitleHeader.styles';

import UniImageButton from '../UniImageButton';
import Theme from '../../Theme';
import {EMPTY} from '../../global/constants';

const TitleHeader = (props) => {
  const {children, title, onBack = EMPTY.FUNC} = props;
  return (
    <View style={styles.navigationBar}>
      <View style={styles.row}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={onBack}
        />
        <Text style={styles.textHeader}>{title}</Text>
      </View>
      <View style={styles.row}>{children}</View>
    </View>
  );
};

export default TitleHeader;
