import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  container: {
    flex: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  rowHeader: {
    marginTop: 15,
    marginBottom: 3,
    display: 'flex',
    flexDirection: 'row',
  },
  textContainer: {
    width: 22,
    height: 22,
    borderRadius: 12,
    alignSelf: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 2,
  },
  text: {
    alignSelf: 'center',
    fontSize: 13,
    color: '#888',
  },
  rowMakeUp: {
    height: 6,
    display: 'flex',
    flexDirection: 'row',
  },
  makeupCell: {
    flex: 1,
    borderRightColor: '#ebebeb',
    borderRightStyle: 'solid',
    borderRightWidth: 1,
  },
  row: {
    flex: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  cell: {
    height: '100%',
    flex: 1,
    paddingTop: 4,
    paddingBottom: 4,

    borderRightColor: '#ebebeb',
    borderRightStyle: 'solid',
    borderRightWidth: 1,

    borderTopColor: '#ebebeb',
    borderTopStyle: 'solid',
    borderTopWidth: 1,
  },
};
