import React, {useEffect, useState} from 'react';
import moment from 'moment';
import {View, Text} from 'react-native';
import styles from './styles.js';
import {getTodayDateMoment, setMomentLocale} from '../../utils/DateUtils';

const MonthView = props => {
  const {date, renderDate = () => {}} = props;
  const [weekList, setWeekList] = useState([]);

  useEffect(() => {
    setMomentLocale();
    let dateMoment = moment(date);
    dateMoment = dateMoment.startOf('month').startOf('isoWeek');

    const weekList = [[], [], [], [], [], []];

    weekList.forEach(week => {
      for (let i = 0; i < 7; i++) {
        week.push(dateMoment.format('YYYY-MM-DD'));
        dateMoment.add(1, 'd');
      }
    });

    setWeekList(weekList);
  }, [date]);

  return (
    <View style={styles.container}>
      <View style={styles.rowHeader}>
        {weekList.length > 0 &&
          weekList[0].map(day => {
            const dayOfWeek = moment(day, 'YYYY-MM-DD').format('dd');
            return (
              <Text key={dayOfWeek} style={[{flex: 1, textAlign: 'center'}, styles.text]}>
                {dayOfWeek}
              </Text>
            );
          })}
      </View>
      <View style={styles.rowMakeUp}>
        <View style={styles.makeupCell} />
        <View style={styles.makeupCell} />
        <View style={styles.makeupCell} />
        <View style={styles.makeupCell} />
        <View style={styles.makeupCell} />
        <View style={styles.makeupCell} />
        <View style={styles.makeupCell} />
      </View>
      <View style={{ display: 'flex', flexDirection: 'column', flex: 1, width: '100%', marginBottom: 0 }}>
        {weekList.map((week, idx) => {
          return (
            <View key={idx} style={styles.row}>
              {week.map(day => {
                const dayMoment = moment(day, 'YYYY-MM-DD');
                const dayOfMonth = dayMoment.format('D');
                const isToday = getTodayDateMoment().isSame(day, 'day');
                const isOutOfMonth = !moment(date).isSame(day, 'month');
                const containerStyle = isToday
                  ? {backgroundColor: isToday ? 'blue' : 'transparent', borderWidth: 1, borderColor: 'blue'} : {};
                const textStyle = isToday ? {color: 'white'} : {color: isOutOfMonth ? '#888' : '#444'};

                return (
                  <View key={day} style={styles.cell}>
                    <View style={[styles.textContainer, containerStyle]}>
                      <Text key={dayOfMonth} style={[styles.text, { marginBottom: 5 }, textStyle]}>
                        {dayOfMonth}
                      </Text>
                    </View>
                    {!isOutOfMonth && renderDate(day)}
                  </View>
                );
              })}
            </View>
          );
        })}
      </View>
    </View>
  );
};

export default MonthView;
