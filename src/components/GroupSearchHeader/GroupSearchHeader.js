import {Image, TouchableOpacity} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import React, {useRef} from 'react';

import styles from './GroupSearchHeader.styles';

import TitleHeader from '../TitleHeader/TitleHeader';
import {EMPTY} from '../../global/constants';

const GroupSearchHeader = (props) => {
  const {
    title,
    onBack,
    defaultGroupIndex = 0,
    dropDownStyle = {},
    groupDropdownData = EMPTY.ARRAY,
    onGroupSelect = EMPTY.FUNC,
    onToggleSearch = EMPTY.FUNC,
  } = props;
  const groupDropDownRef = useRef(null);

  const showGroupDropDown = () => {
    if (!groupDropDownRef || !groupDropDownRef.current) {
      return;
    }

    groupDropDownRef.current.show();
  };

  return (
    <TitleHeader title={title} onBack={onBack}>
      <TouchableOpacity onPress={showGroupDropDown}>
        <Image
          style={styles.navIcon}
          source={require('../../assets/img/ic_font.png')}
        />
      </TouchableOpacity>
      <ModalDropdown
        ref={groupDropDownRef}
        defaultIndex={defaultGroupIndex}
        options={groupDropdownData}
        dropdownTextStyle={styles.dropdownTextStyle}
        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
        dropdownStyle={{...styles.dropdownStyle, ...dropDownStyle}}
        textStyle={{display: 'none'}}
        defaultValue=""
        renderButtonText={() => ''}
        onSelect={onGroupSelect}
      />
      <TouchableOpacity onPress={onToggleSearch}>
        <Image
          style={styles.navIcon}
          source={require('../../assets/img/ic_search.png')}
        />
      </TouchableOpacity>
    </TitleHeader>
  );
};

export default GroupSearchHeader;
