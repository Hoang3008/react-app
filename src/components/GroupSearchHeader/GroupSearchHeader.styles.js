import Theme from '../../Theme';

export default {
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  dropdownStyle: {
    width: 200,
    height: 321,
  },

  dropdownTextStyle: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.header_16,
    marginLeft: Theme.margin.m12,
    marginRight: Theme.margin.m12,
  },
  dropdownTextHighlightStyle: {
    color: Theme.colors.textPrimary,
  },
};
