import React, {useEffect, useRef, useState} from 'react';
import {ScrollView, Text, TouchableOpacity, View, Platform} from 'react-native';
import ImageCapInset from 'react-native-image-capinsets';

import BgComplete from '../../assets/img/bg-flow-item-complete.png';
import BgCompleteFocus from '../../assets/img/bg-flow-item-complete-focus.png';
import BgInProcess from '../../assets/img/bg-flow-item-in-process.png';
import BgInProcessFocus from '../../assets/img/bg-flow-item-in-process-focus.png';
import BgNormal from '../../assets/img/bg-flow-item-normal.png';
import BgNormalFocus from '../../assets/img/bg-flow-item-normal-focus.png';
import BgFail from '../../assets/img/bg-flow-item-fail.png';
import BgFailFocus from '../../assets/img/bg-flow-item-fail-focus.png';

const isIOS = Platform.OS === 'ios';

const OverviewStatuses = {
  Pass: [BgComplete, BgCompleteFocus, '#ffffff'],
  Fail: [BgFail, BgFailFocus, '#ffffff'],
  'In Progress': [BgInProcess, BgInProcessFocus, '#ffffff'],
  Open: [BgNormal, BgNormalFocus, '#235894'],
};

const HorizontalScrollInspectionFlow = (props) => {
  const {
    flowList = [],
    activeIndex = 0,
    style = {},
    onChangeActiveIndex,
  } = props;

  const scrollRef = useRef(null);
  let disabledClickInNextStep = false;
  const [arrayX, setArrayX] = useState([]);

  useEffect(() => {
    if (!scrollRef || !scrollRef.current || arrayX.length <= activeIndex) {
      return;
    }
    scrollRef.current.scrollTo({x: arrayX[activeIndex], y: 0, animated: true});
  }, [activeIndex, arrayX]);

  const saveLayout = (event, index) => {
    if (!event.currentTarget) {
      return;
    }

    const {
      layout: {x},
    } = event.nativeEvent;

    const newArrayX = index >= x.length ? [...arrayX, 0] : [...arrayX];
    newArrayX[index] = x;
    setArrayX(newArrayX);
  };

  return (
    <ScrollView
      ref={scrollRef}
      style={[{width: '100%', height: 45, paddingBottom: 7}, style]}
      horizontal>
      {flowList.map((item, index) => {
        const {
          OverviewStatus,
          PMProjectInsStpDesc,
          PMProjectInsStpID,
          Required,
        } = item;

        const disabled = disabledClickInNextStep;
        disabledClickInNextStep =
          disabledClickInNextStep ||
          (Required && OverviewStatus !== 'Pass' && OverviewStatus !== 'Fail');

        const bgIndex = index === activeIndex ? 1 : 0;

        const statusState = OverviewStatuses[OverviewStatus];

        if (!statusState) {
          return <View key={`${PMProjectInsStpID}`} />;
        }

        return (
          <TouchableOpacity
            onLayout={(event) => saveLayout(event, index)}
            disabled={disabled}
            style={{
              height: isIOS ? 36 : 38,
              marginLeft: index === 0 ? 15 : -5,
            }}
            key={`${PMProjectInsStpID}`}
            onPress={() => onChangeActiveIndex(index)}>
            <ImageCapInset
              source={statusState[bgIndex]}
              style={{
                height: isIOS ? 36 : 38,
              }}
              capInsets={{top: 18, left: 25, right: 25, bottom: 18}}>
              <Text
                style={[
                  styles.active,
                  {
                    color: statusState[2],
                    minWidth: 150,
                  },
                ]}>
                {PMProjectInsStpDesc}
              </Text>
            </ImageCapInset>
          </TouchableOpacity>
        );
      })}
    </ScrollView>
  );
};

const styles = {
  inactive: {
    marginTop: 6,
    marginLeft: 14,
    fontSize: 16,
    lineHeight: 24,
    color: '#235894',
  },
  active: {
    marginTop: 6,
    paddingLeft: 20,
    paddingRight: 20,
    fontSize: 16,
    lineHeight: 24,
    color: '#FFFFFF',
  },
  normal: {
    marginTop: 6,
    paddingLeft: 20,
    paddingRight: 20,
    fontSize: 16,
    lineHeight: 24,
    color: '#245894',
  },
};

export default HorizontalScrollInspectionFlow;
