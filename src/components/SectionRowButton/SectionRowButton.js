import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';

const SectionRowButton = props => {
  const {style = {}, label, description, onPress} = props;
  return (
    <TouchableOpacity style={[styles.row, style]} onPress={onPress}>
      <View
        style={{
          flexDirection: 'column',
          flex: 1,
          justifyContent: 'center',
          marginRight: 15,
        }}>
        <Text style={styles.headerText}>{label}</Text>
        {description && (
          <Text
            style={styles.description}
            numberOfLines={1}
            ellipsizeMode="tail">
            {description}
          </Text>
        )}
      </View>
      <Icon
        style={{marginBottom: 4}}
        size={12}
        color={'#9EA0A5'}
        name="angle-right"
      />
    </TouchableOpacity>
  );
};

const styles = {
  row: {
    backgroundColor: '#ffffff',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingTop: 19,
    paddingBottom: 19,
    paddingLeft: 15,
    paddingRight: 15,
  },
  headerText: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    color: '#3E3F42',
  },
  normalText: {
    fontSize: 14,
    lineHeight: 18,
    color: '#3E3F42',
  },
};

export default SectionRowButton;
