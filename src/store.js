import {createStore, applyMiddleware, compose} from 'redux';
import ReduxThunk from 'redux-thunk';
import {createPromise} from 'redux-promise-middleware';
import {persistStore, persistReducer, createMigrate} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import AuthMiddleware from './screens/auth/AuthMiddleware';
import AuthHoang from './screens/auth/AuthTextHoang';

import reducers from './reducers';

const migrations = {
  0: (state) => {
    return {
      ...state,
    };
  },
  1: (state) => {
    return {
      ...state,
      commonData: {
        offlineQueue: [],
        photos: {},
      },
    };
  },
  2: (state) => {
    return {
      ...state,
      commonData: {
        offlineQueue: [],
        photos: {},
      },
    };
  },
};

const persistConfig = {
  key: 'root',
  version: 2,
  storage: AsyncStorage,
  keyPrefix: '',
  whitelist: ['auth', 'commonData','domains'],
  migrate: createMigrate(migrations, {debug: false}),
};

const persistedReducer = persistReducer(persistConfig, reducers);

const middleware = [
  ReduxThunk,
  AuthMiddleware,
  AuthHoang,
  createPromise({
    promiseTypeSuffixes: ['BEGIN', 'SUCCESS', 'FAILURE'],
  }),
];
// const enhancers = [applyMiddleware(...middleware)];

let enhancers = [applyMiddleware(...middleware)];

if(__DEV__) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  enhancers = [composeEnhancers(applyMiddleware(...middleware))]
}

export default () => {
  const store = createStore(persistedReducer, ...enhancers);

  const persistor = persistStore(store);

  return {
    store,
    persistor,
  };
};
