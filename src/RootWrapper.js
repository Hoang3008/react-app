import React, {Component} from 'react';
import {View, Vibration} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {equals, isEmpty, pathOr, propOr} from 'ramda';
import RNFS from 'react-native-fs';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {RootStack} from './Routes';
import {EMPTY, OFFLINE_ACTION_TYPES} from './global/constants';
import {actions} from './screens/CommonProjectData/CommonProjectData.reducer';
import {actions as NotificationListActions} from './screens/NotificationList/NotificationList.reducer';
import messaging from '@react-native-firebase/messaging';
import RouteNames from './RouteNames';
import {actions as ITPActions} from './screens/InspectionDetail/InspectionDetail.reducer';
import {actions as updateITPActions} from './screens/InspectionUpdate/InspectionUpdate.reducer';
import {actions as ITPWorkFlowActions} from './screens/InspectionWorkflowComment/InspectionWorkflowComment.reducer';
import Configs from './global/configs';
import envJSON from './env';

const Stack = createStackNavigator();

const ONE_SECOND_IN_MS = 1000;

const PATTERN = [2 * ONE_SECOND_IN_MS, ONE_SECOND_IN_MS, ONE_SECOND_IN_MS];

function MainStack() {
  return (
    <Stack.Navigator headerMode="none">
      {Object.keys(RootStack).map((key) => (
        <Stack.Screen key={key} name={key} component={RootStack[key].screen} />
      ))}
    </Stack.Navigator>
  );
}

class RootWrapper extends Component {
  constructor() {
    super();
    AsyncStorage.getItem('apiObject').then((jsonStr) => {
      if (!jsonStr) {
        Configs.apiBasedURL = envJSON.apiBasedURL;
        Configs.drawingBasedURL = envJSON.drawingBasedURL;

        this.navigateToNotification.bind(this);
        return;
      }

      const api = JSON.parse(jsonStr);
      let url = api.api || '';
      if (url.endsWith('/')) {
        url = url.substring(0, url.length - 1);
      }
      Configs.drawingBasedURL = api.drawingBasedURL;
      Configs.apiBasedURL = url;

      this.navigateToNotification.bind(this);
    });
  }
  componentWillUnmount(): void {
    this.clearTimer();
    if (this.fireStoreSubscriber) {
      this.fireStoreSubscriber();
    }
  }

  componentDidMount(): void {
    const {
      resetSyncStatusAllActionInQueue,
      user,
      getAllProjectNotificationUnreadCount,
    } = this.props;
    resetSyncStatusAllActionInQueue();
    this.handleRunningQueue(true);

    console.log('RootWrapper componentDidMount', this.props);

    if (user && user.ADUserID) {
      this.loadNotificationCount();
      // this.subscribeToLoadNotification();
    }

    messaging()
      .getInitialNotification()
      .then((notification) => {
        console.log('getInitialNotification', notification);
        this.navigateToNotification(notification);
      });

    messaging().onNotificationOpenedApp((notification) => {
      console.log('onNotificationOpenedApp', notification);
      this.navigateToNotification(notification);
    });

    messaging().onMessage((remoteMessage) => {
      console.log('ON_MESSAGE', remoteMessage);
      Vibration.vibrate(PATTERN);

      let projectId = pathOr('0', ['data', 'projectId'], remoteMessage);
      projectId = parseInt(projectId, 10);
      getAllProjectNotificationUnreadCount(user.ADUserID, [projectId]);
    });
  }

  navigateToNotification(notification) {
    const {isLoggedIn, readNotification} = this.props;

    if (!notification || !isLoggedIn || !this.navigationRef) {
      return;
    }

    const {data, messageId} = notification;

    readNotification(messageId);
    this.navigationRef.navigate(RouteNames.SelectProjectScreen, {
      notification: data,
      time: Date.now(),
    });
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {offlineQueue, user, isLoggedIn, token} = this.props;

    AsyncStorage.setItem('token', token);

    if (!isLoggedIn && prevProps.isLoggedIn) {
      this.navigationRef && this.navigationRef.navigate(RouteNames.AuthLogin);
      return;
    }

    if (offlineQueue.length !== prevProps.offlineQueue.length) {
      this.clearTimer();

      if (offlineQueue.length > 0) {
        if (!this.queueTimeoutId) {
          clearTimeout(this.queueTimeoutId);
        }

        this.queueTimeoutId = setTimeout(() => {
          this.handleRunningQueue(false);
        }, 100);
      }
    }

    if (
      (user && user.ADUserID !== pathOr('', ['user', 'ADUserID'], prevProps)) ||
      (user && !equals(prevProps.projectList, this.props.projectList))
    ) {
      this.loadNotificationCount();
      // this.subscribeToLoadNotification();
    }
  }

  loadNotificationCount = async () => {
    const {
      user,
      projectList,
      getAllProjectNotificationUnreadCount,
      project
    } = this.props;

    if (!projectList || isEmpty(projectList)) {
      return;
    }

    getAllProjectNotificationUnreadCount(
      user.ADUserID,
      projectList.map(({PMProjectID}) => PMProjectID),
    );
  };

  clearTimer = () => {
    if (this.timerId) {
      clearInterval(this.timerId);
    }
  };

  startTimer = () => {
    this.clearTimer();
    this.timerId = setTimeout(this.handleRunningQueue, 15000);
  };

  handleRunningQueue = (forced = false) => {
    const {isLoggedIn, project ,offlineQueue = []} = this.props;

    if (!isLoggedIn) {
      return;
    }

    console.log('Syncing start ', offlineQueue);

    const offlineAction = offlineQueue.find((item) => {
      const {data} = item;
      const FK_PMTaskID = pathOr('', ['body', 'FK_PMTaskID'], data);
      return (
        (forced || !item.data.syncing) &&
        `${FK_PMTaskID}`.length < '1587365555443'.length
      );
    });

    if (offlineAction) {
      console.log('Syncing item', offlineQueue, offlineAction);
      this.handleQueueAction(offlineAction).catch((error) => {
        this.startTimer();
      });
    }
  };

  handleQueueAction = ({type, data, currentData, attachment, logs}) => {
    const {
      createDefect,
      addPhotoIntoDefect,
      removeActionOutOfQueue,
      updateDefect,
      addComment,
      updateInspection,
      token,
      sendComment,
      uploadPhoto,
      uploadDoc,
      updateWorkFlowStepContent,
      project
    } = this.props;

    switch (type) {
      case OFFLINE_ACTION_TYPES.CREATE_DEFECT:
        return createDefect(data, attachment);
      case OFFLINE_ACTION_TYPES.UPDATE_DEFECT:
        return updateDefect(data, currentData, logs);
      case OFFLINE_ACTION_TYPES.ADD_PHOTO:
        const {
          image: {uri},
        } = data;

        if (!uri) {
          removeActionOutOfQueue(data, token);
          return Promise.resolve();
        }

        return RNFS.exists(uri).then((result) => {
          if (!result) {
            removeActionOutOfQueue(data, token);
            return Promise.resolve();
          }

          const projectId = propOr(undefined, 'PMProjectID', project);
          return addPhotoIntoDefect(data, token, projectId);
        });
      case OFFLINE_ACTION_TYPES.ADD_COMMENT:
        const {image} = data;
        const imageUri = propOr('', 'uri', image);

        if (imageUri) {
          return RNFS.exists(imageUri).then((result) => {
            if (!result) {
              removeActionOutOfQueue(data);
              return Promise.resolve();
            }

            return addComment(data, token);
          });
        } else {
          return addComment(data, token);
        }
      case OFFLINE_ACTION_TYPES.UPDATE_ITP_OVERVIEW:
        return updateInspection(data);
      case OFFLINE_ACTION_TYPES.POST_ITP_WORKFLOW_COMMENT:
        const projectId = propOr(undefined, 'PMProjectID', project);
        return sendComment(data, projectId);
      case OFFLINE_ACTION_TYPES.ADD_ITP_WORKFLOW_PHOTO:
        if (!data.image || !data.image.uri) {
          removeActionOutOfQueue(data, token);
          return Promise.resolve();
        }

        return RNFS.exists(data.image.uri).then((result) => {
          if (!result) {
            removeActionOutOfQueue(data, token);
            return Promise.resolve();
          }
          const projectId = propOr(undefined, 'PMProjectID', project);
          return uploadPhoto(data, projectId);
        });
      case OFFLINE_ACTION_TYPES.ADD_ITP_WORKFLOW_DOC:
        if (!data.image || !data.image.uri) {
          removeActionOutOfQueue(data, token);
          return Promise.resolve();
        }

        return RNFS.exists(data.image.uri).then((result) => {
          // if (!result) {
          //   removeActionOutOfQueue(data, token);
          //   return Promise.resolve();
          // }

          const projectId = propOr(undefined, 'PMProjectID', project);
          return uploadDoc(data, projectId);
        });
      case OFFLINE_ACTION_TYPES.UPDATE_ITP_WORKFLOW_CONTENT_STATUS:
        const projectID = propOr(undefined, 'PMProjectID', project);
        return updateWorkFlowStepContent(data, projectID);

      default:
        return Promise.resolve();
    }
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <NavigationContainer ref={(rel) => (this.navigationRef = rel)}>
          <MainStack />
        </NavigationContainer>
      </View>
    );
  }
}

const mapStateToProps = ({auth: {isLoggedIn, user, project}, commonData}) => {
  return {
    user,
    token: propOr('', 'token', user),
    isLoggedIn,
    offlineQueue: pathOr(EMPTY.ARRAY, ['offlineQueue'], commonData),
    projectList: propOr(EMPTY.ARRAY, 'projectList', commonData),
    project: project
  };
};

const mapDispatchToProps = {
  createDefect: actions.createDefect,
  addPhotoIntoDefect: actions.addPhotoIntoDefect,
  removeActionOutOfQueue: actions.removeActionOutOfQueue,
  resetSyncStatusAllActionInQueue: actions.resetSyncStatusAllActionInQueue,
  updateDefect: actions.updateDefect,
  addComment: actions.addComment,
  updateNotificationList: NotificationListActions.updateNotificationList,
  readNotification: NotificationListActions.readNotification,
  getAllProjectNotificationUnreadCount:
    actions.getAllProjectNotificationUnreadCount,
  updateInspection: updateITPActions.updateInspection,
  uploadPhoto: ITPActions.uploadPhoto,
  uploadDoc: ITPActions.uploadDoc,
  sendComment: ITPWorkFlowActions.sendComment,
  updateWorkFlowStepContent: ITPActions.updateWorkFlowStepContent,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootWrapper);
