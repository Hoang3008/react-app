import {combineReducers} from 'redux';
import AuthReducers from './AuthReducers';
import DomainReducers from './DomainReducers';
import drawing from './DrawingReducers';
import watcher from './GetWatcherReducers';
import commonData from '../screens/CommonProjectData/CommonProjectData.reducer';
import newChecklist from '../screens/NewCheckList/NewCheckList.reducer';
import createUser from '../screens/CreateUser/CreateUser.reducer';
import forgotPassword from '../screens/ForgotPassword/ForgotPassword';
import createProject from '../screens/CreateProject/CreateProject.reducer';
import newDrawing from '../screens/NewDrawing/NewDrawing.reducer';
import inspectionList from '../screens/InspectionList/InspectionList.reducer';
import inspectionDetail from '../screens/InspectionDetail/InspectionDetail.reducer';
import inspectionAttachmentList from '../screens/InspectionAttachmentList/InspectionAttachmentList.reducer';
import inspectionWorkflowComment from '../screens/InspectionWorkflowComment/InspectionWorkflowComment.reducer';
import scheduleList from '../screens/ScheduleList/ScheduleList.reducer';
import photoList from '../screens/PhotoList/PhotoList.reducer';
import notificationList from '../screens/NotificationList/NotificationList.reducer';
import photoComments from '../screens/PhotoComments/PhotoComments.reducer';
import taskList from '../screens/TaskList/TaskList.reducer';
import drawingArea from '../screens/DrawingArea/DrawingArea.reducer';
import projectOverviewDashboard from '../screens/ProjectOverviewDashboard/ProjectOverviewDashboard.reducer';
import inspectionFolder from '../screens/InspectionFolder/InspectionFolder.reducer';
import newInspectionFolder from '../screens/NewInspectionFolder/NewInspectionFolder.reducer';
import meetingList from '../screens/MeetingList/MeetingList.reducer';
import meetingDetail from '../screens/MeetingDetail/MeetingDetail.reducer';
import inspectionWorkflowLog from '../screens/InspectionWorkflowLog/InspectionWorkflowLog.reducer';
import inspectionSubFolder from '../screens/InspectionSubFolder/InspectionSubFolder.reducer';
import inspectionMethodList from '../screens/InspectionMethodList/InspectionMethodList.reducer';
import drawingSubFolder from '../screens/DrawingSubFolder/ DrawingSubFolder.reducer';
import drawingAreaList from '../screens/CreateDrawingArea/CreateDrawingArea.reducer';
import photoAreaList from '../screens/PhotoArea/PhotoArea.reducer';
import photoSubFolder from '../screens/PhotoSubFolder/PhotoSubFolder.reducer';
import createPhotoArea from '../screens/CreatePhotoArea/CreatePhotoArea.reducer';


 export default combineReducers({
  auth: AuthReducers,
  drawing,
  commonData,
  newChecklist,
  createUser,
  createProject,
  forgotPassword,
  newDrawing,
  inspectionList,
  inspectionDetail,
  inspectionAttachmentList,
  inspectionWorkflowComment,
  scheduleList,
  photoList,
  notificationList,
  photoComments,
  taskList,
  drawingArea,
  projectOverviewDashboard,
  inspectionFolder,
  newInspectionFolder,
  meetingList,
  meetingDetail,
  inspectionWorkflowLog,
  inspectionSubFolder,
  inspectionMethodList,
  drawingSubFolder,
  photoAreaList,
  photoSubFolder,
  watcher,
  // createPhotoArea,
 // drawingAreaList, khong su dung vi su dung chung initialstate 
  domains: DomainReducers
});


// export default (state, action) => {
//   rootReducer(action.type === 'USER_LOGOUT' ? undefined : state, action)
// }

