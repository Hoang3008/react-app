import {Domain} from '../action/type';

const INITIAL_STATE = {
  domains: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Domain.USER_ADD_DOMAIN:
      return state = {...state, domains: [...state.domains, action]};
    case Domain.USER_DELETE_DOMAIN:
        return state = {...state, domains: state.domains.filter(i => {
          if(i.payload.key === action.payload.key) {
            return false
          }

          return true;
        })};
    default:
      return state;
  }
};
