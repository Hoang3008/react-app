import typeToReducer from 'type-to-reducer';
import {identity} from 'ramda';

import {GET_WATCHER_LIST} from '../action/type';

const initialState = {
    watcherList: [],
};

const handleGetWatcherList = (state, {payload, meta}) => {
  let reduced = [];
  if (payload && payload.data.length > 0) {
    meta[2].forEach(o => {
      payload.data.includes(o.PMProjectPeopleEmailAddress) && reduced.push(o?.ADUserID);
    } );
  }

  return {
    ...state,
    watcherList: reduced,
  };
};

export default typeToReducer(
  {
    [GET_WATCHER_LIST]: {
      BEGIN: (state, {payload}) => ({...state, watcherList: []}),
      SUCCESS: handleGetWatcherList,
      FAILURE: identity,
    },
  },
  initialState,
);
