import typeToReducer from 'type-to-reducer';
import {identity} from 'ramda';

import {GET_DRAWING_LIST} from '../action/type';

const initialState = {
  drawingList: [],
};

const handleGetDrawingList = (state, {payload}) => {
  return {
    ...state,
    drawingList: payload.data,
  };
};

export default typeToReducer(
  {
    [GET_DRAWING_LIST]: {
      BEGIN: (state, {payload}) => ({...state, drawingList: []}),
      SUCCESS: handleGetDrawingList,
      FAILURE: identity,
    },
  },
  initialState,
);
