import {Auth} from '../action/type';

const INITIAL_STATE = {
  project: {},
  user: {},
  master: null,
  isLoggedIn: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Auth.PROJECT_SELECTED:
      return {...state, project: action.payload};
    case Auth.USER_INFO_UPDATED:
      return {...state, user: action.payload, isLoggedIn: true};
    case Auth.MASTER_DATA_UPDATED:
      return {...state, master: action.payload};
    case Auth.LOGOUT: {
      return {
        ...INITIAL_STATE,
      };
    }
    case 'LOGIN_SUCCESS': {
      const {
        payload: {data},
      } = action;
      return {
        ...state,
        isLoggedIn: true,
        user: {
          ...data,
          ADUserPicture: null,
        },
      };
    }
    default:
      return state;
  }
};
