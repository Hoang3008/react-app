const Theme = {};

const colors = {
  primary: '#245894',
  bottomNavbar: 'white',
  activeTint: '#245894',
  inactiveTint: '#B5C0DB',
  border: '#D8DCE6',
  accent: '#3AA949',

  backgroundColor: '#F2F2F2',
  backgroundWhite: 'white',
  backgroundHeader: '#F6F6F8',
  backgroundComment: '#ECEDF0',

  textPrimary: '#245894',
  textNormal: '#3E3F42',
  textSecond: '#9EA0A5',
  textNavigation: 'white',
  textDisable: '#9EA0A5',
  textWhite: '#ffffff',
  textDangerous: '#FF0404',
};

Theme.specifications = {
  bottomNavbarHeight: 81,
  editTextNormalHeight: 44,
  editTextNormalBorder: 4,
  checkBoxSize: 16,
  navigationBarHeight: 48,
  headerSectionHeight: 48,
  commentAvatarSize: 38,
  inputCommentContainerHeight: 60,
  floatButtonSize: 56,
  icSmall: 24,
  icBig: 44,
  checkboxDrawingFilter: 30,
};

Theme.margin = {
  m1: 1,
  m2: 2,
  m3: 3,
  m4: 4,
  m5: 5,
  m6: 6,
  m8: 8,
  m10: 10,
  m12: 12,
  m14: 14,
  m15: 15,
  m16: 16,
  m18: 18,
  m20: 20,
  m22: 22,
  m24: 24,
  m30: 30,
  m36: 36,
  m44: 44,
  m52: 52,
};

Theme.fontSize = {
  normal: 14,
  header_16: 16,
  small_12: 12,
};

Theme.colors = {
  ...colors,
};

export default Theme;
