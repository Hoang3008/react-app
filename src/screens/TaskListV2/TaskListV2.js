import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {
  SectionList,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {isEmpty, pathOr, propOr} from 'ramda';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Fontisto';

import styles from './TaskListV2.styles';

import {actions} from '../TaskList/TaskList.reducer';
import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import GroupSearchHeader from '../../components/GroupSearchHeader/GroupSearchHeader';
import {EMPTY} from '../../global/constants';
import {
  groupDefectByAssignee,
  groupDefectByCategory,
  groupDefectByDate,
  groupDefectByLocations,
  groupDefectByPriority,
} from '../defect/DefectHelper';
import FloaterAddButton from '../../components/FloaterAddButton/FloaterAddButton';
import SectionHeader from '../../components/SectionHeader/SectionHeader';
import RouteNames from '../../RouteNames';
import {PRIORITY_COLOR} from '../ScheduleList/ScheduleList';

const GROUP_DROPDOWN_DATA = [
  {
    label: 'Status',
    key: 'priority',
  },
  {
    label: 'Category',
    key: 'FK_PMProjectCategoryID',
  },
  {
    label: 'Assignee',
    key: 'FK_ADAssignUserID',
  },
  {
    label: 'Location',
    key: 'FK_PMProjectLocationID',
  },
  {
    label: 'Start date',
    key: 'PMTaskStartDate',
  },
  {
    label: 'End date',
    key: 'PMTaskEndDate',
  },
  {
    label: 'Last modified',
    key: 'AAUpdatedDate',
  },
];

const TaskListV2 = (props) => {
  const {
    navigation,
    getTaskList,
    project,
    taskList,
    categories,
    peopleInProject,
    locations,
  } = props;
  const PMProjectID = propOr(0, 'PMProjectID', project);
  const [searchText, setSearchText] = useState('');
  const [showSearch, setShowSearch] = useState(false);
  const [groupKey, setGroupKey] = useState(GROUP_DROPDOWN_DATA[0].key);
  const [data, setData] = useState(EMPTY.ARRAY);

  useEffect(() => {
    getTaskList(PMProjectID);
  }, [getTaskList, PMProjectID]);

  useEffect(() => {
    if (!taskList || isEmpty(taskList)) {
      setData(EMPTY.ARRAY);
    }

    const lowerCaseSearch = searchText ? searchText.toLowerCase() : '';

    let groupedData = taskList.filter(({PMTaskName, FK_ADAssignUserID}) => {
      if (PMTaskName && PMTaskName.toLowerCase().includes(lowerCaseSearch)) {
        return true;
      }

      return false;

      // const assignUser = peopleInProject.find(
      //   ({ADUserID}) => ADUserID === FK_ADAssignUserID,
      // );
      //
      // return (
      //   assignUser &&
      //   assignUser.fullName &&
      //   assignUser.fullName.toLowerCase().includes(lowerCaseSearch)
      // );
    });

    switch (groupKey) {
      case 'priority':
        groupedData = groupDefectByPriority(groupedData);
        break;
      case 'FK_PMProjectCategoryID':
        groupedData = groupDefectByCategory(groupedData, categories);
        break;
      case 'FK_ADAssignUserID':
        groupedData = groupDefectByAssignee(
          groupedData,
          peopleInProject,
          'PMProjectPeopleID',
        );
        break;
      case 'FK_PMProjectLocationID':
        groupedData = groupDefectByLocations(groupedData, locations);
        break;
      case 'PMTaskStartDate':
        groupedData = groupDefectByDate(
          groupedData,
          'PMTaskStartDate',
          'start date',
        );
        break;
      case 'PMTaskEndDate':
        groupedData = groupDefectByDate(
          groupedData,
          'PMTaskEndDate',
          'end date',
        );
        break;
      case 'AAUpdatedDate':
        groupedData = groupDefectByDate(
          groupedData,
          'AAUpdatedDate',
          'updated date',
        );
        break;
    }

    setData(groupedData);
  }, [taskList, searchText, groupKey]);

  const handleSelectGroup = (index) => {
    const {key} = GROUP_DROPDOWN_DATA[index];

    if (groupKey === key) {
      return;
    }

    setGroupKey(key);
    // handleGroupData(key);
  };

  const toggleShowSearch = () => {
    setSearchText('');
    setShowSearch(!showSearch);
  };

  const handleAddNewTask = () => {
    navigation.push(RouteNames.TaskUpdate, {
      onGoBack: () => getTaskList(PMProjectID),
    });
  };

  const handleOpenTask = (task) => () => {
    const {PMTaskID} = task;
    navigation.navigate(RouteNames.TaskDetail, {
      PMTaskID,
      onDelete: () => getTaskList(PMProjectID),
    });
  };

  const renderSectionHeader = (title, color = 'transparent', data = []) => {
    return (
      <SectionHeader
        color={color}
        title={title.toUpperCase() + ' (' + data.length + ')'}
      />
    );
  };

  const renderRowItem = (item) => {
    const {
      PMTaskID,
      PMTaskName,
      PMTaskStartDate,
      PMTaskEndDate,
      PMTaskPriorityCombo,
      PMTaskCompletePct,
    } = item;

    const todayISO = moment().toISOString();
    const isExpired = todayISO > PMTaskEndDate;

    return (
      <TouchableOpacity onPress={handleOpenTask(item)}>
        <View style={styles.itemContainer}>
          <View
            style={[
              styles.priority,
              {
                backgroundColor: isExpired
                  ? PRIORITY_COLOR.Overdue
                  : PMTaskCompletePct === 100
                  ? PRIORITY_COLOR.Complete
                  : PRIORITY_COLOR[PMTaskPriorityCombo],
              },
            ]}
          />
          <View style={styles.itemMainContent}>
            <Text style={styles.itemText}>
              #{PMTaskID} {PMTaskName}
            </Text>
            <Text style={styles.itemDate}>
              {moment(PMTaskStartDate).format('M/D/YY')} -{' '}
              {moment(PMTaskEndDate).format('M/D/YY')}
            </Text>
          </View>
          <Text style={styles.itemPercentText}>{PMTaskCompletePct}%</Text>
          <Icon
            style={{marginBottom: 4}}
            size={12}
            color={'#9EA0A5'}
            name="angle-right"
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      <GroupSearchHeader
        title="All Tasks"
        onBack={navigation.goBack}
        groupDropdownData={GROUP_DROPDOWN_DATA.map((item) => item.label)}
        onGroupSelect={handleSelectGroup}
        onToggleSearch={toggleShowSearch}
      />
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        {showSearch && (
          <SearchBar
            placeholder="Type Here..."
            value={searchText}
            onChangeText={setSearchText}
            inputStyle={{
              color: Theme.colors.textNormal,
            }}
            containerStyle={{
              borderBottomColor: '#0000',
              backgroundColor: '#ffffff',
            }}
            inputContainerStyle={{
              backgroundColor: '#ddd',
            }}
          />
        )}
        <SectionList
          sections={data}
          keyExtractor={(item, index) => index + '-' + item.title}
          renderItem={({item}) => renderRowItem(item)}
          renderSectionHeader={({section: {title, data, color}}) =>
            renderSectionHeader(title, color, data)
          }
          ItemSeparatorComponent={() => <View style={styles.divider} />}
        />
      </View>
      <FloaterAddButton onPress={handleAddNewTask} />
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, taskList, commonData}) => {
  const {PMProjectID} = project;

  return {
    user,
    project,
    taskList: taskList.taskList || [],
    categories:
      pathOr(EMPTY.ARRAY, ['currentProject', 'categories'], commonData) ||
      EMPTY.ARRAY,
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
  };
};

const mapDispatchToProps = {
  getTaskList: actions.getTaskList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TaskListV2);
