import typeToReducer from 'type-to-reducer';
import getAllChecklist from './actions/getAllChecklist.action';
import getChecklistItems from './actions/getChecklistItems.action';

const initialState = {
  checklists: [],
  checklistItems: [],
};

export const actions = {
  getAllChecklist: getAllChecklist.action,
  getChecklistItems: getChecklistItems.action,
};

export default typeToReducer(
  {
    ...getAllChecklist.reducer,
    ...getChecklistItems.reducer,
  },
  initialState,
);
