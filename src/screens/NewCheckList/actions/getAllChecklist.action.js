import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, propOr} from 'ramda';

const actionType = 'GET_ALL_CHECKLIST';

const getAllCheckListURL = () => withKey('/checklist');
const getAllCheckListAPI = ({projectId},token) =>
  API.get(withKey(`/checklist?projectId=${projectId}`), {headers: {Authorization: `Bearer ${token}`}});

const action = createAction(actionType, getAllCheckListAPI);

const handleAPISuccess = (state, {payload}) => {
  const checklists = propOr([], 'data', payload);
  return {
    ...state,
    checklists,
    checklistItems: [],
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
