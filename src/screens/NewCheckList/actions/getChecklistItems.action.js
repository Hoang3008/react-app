import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';

import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';


const actionType = 'GET_CHECKLIST_ITEMS';

const getCheckListItemsURL = (id) =>
   // withKey(`/checklist/items?appFlag=1`);
   withKey(`/checklist/items?checkListId=${id}&appFlag=1`);
//const getCheckListItemsAPI = (id) => API.get(getCheckListItemsURL(id));

const getCheckListItemsAPI = (id) =>
  AsyncStorage.getItem('token').then((token) => {
    return API.get(getCheckListItemsURL(id), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });

const action = createAction(actionType, getCheckListItemsAPI);

const handleAPISuccess = (state, {payload}) => {
  const checklistItems = propOr([], 'data', payload);
  return {
    ...state,
    checklistItems,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: (state) => ({
      ...state,
      checklistItems: [],
    }),
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
