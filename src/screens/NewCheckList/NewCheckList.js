import React from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import {connect} from 'react-redux';

import {actions} from './NewCheckList.reducer';

import Theme from '../../Theme';
import {
  UniStatusBarBackground,
  UniImageButton,
  UniDropbox,
} from '../../components';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {identity, pathOr} from 'ramda';

class NewCheckList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedChecklistItems: {},
    };
  }

  componentDidMount() {
    const {getAllChecklist, project,token} = this.props;
    const projectId = project?.PMProjectID;
    getAllChecklist({projectId: projectId},token);
  }

  handlePostChecklist = () => {
    const {navigation, checklistItems} = this.props;

    const onPostChecklist = pathOr(
      identity,
      ['route', 'params', 'onPostChecklist'],
      this.props,
    );

    const {selectedChecklistItems} = this.state;

    onPostChecklist(
      checklistItems.map((item) => ({
        PMTaskItemCompleteCheck: !!selectedChecklistItems[
          item.PMCheckListItemID
        ],
        PMTaskItemDesc: item.PMCheckListItemName,
      })),
    );
    navigation.goBack();
  };

  renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() =>
              this.props.navigation && this.props.navigation.goBack()
            }
          />
          <View>
            <View style={styles.row}>
              <Text style={styles.textHeader}>New Checklist</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity style={styles.buttonContainerActive}>
          <Text
            style={styles.textButtonActive}
            onPress={this.handlePostChecklist}>
            Pos checklist
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderChecklistItem = ({item}) => {
    // PMCheckListItemID: 32
    // PMCheckListItemNo: "HT-TN-5"
    // PMCheckListItemName: "Nứt tường khi kéo dây điện "
    // PMCheckListItemDesc: "Ống M&E phải xiếc kẽm chặt và khoảng cách @500 "
    // AAStatus: "Alive"
    // AACreatedDate: "2019-11-19T09:52:11.437Z"
    // AACreatedUser: null
    // AAUpdatedDate: "2019-11-19T09:52:11.437Z"
    // AAUpdatedUser: null
    // FK_PMCheckListID: 20
    // PMCheckListItemLineNo: "5"
    const {PMCheckListItemID, PMCheckListItemName} = item;
    const {selectedChecklistItems} = this.state;

    return (
      <View
        style={[
          styles.row,
          {
            paddingVertical: Theme.margin.m8,
            marginHorizontal: Theme.margin.m15,
          },
        ]}>
        <TouchableOpacity
          style={{
            paddingRight: Theme.margin.m12,
            paddingVertical: Theme.margin.m4,
          }}
          onPress={() => {
            this.setState({
              selectedChecklistItems: {
                ...selectedChecklistItems,
                [PMCheckListItemID]: selectedChecklistItems[PMCheckListItemID]
                  ? undefined
                  : true,
              },
            });
          }}>
          <Image
            style={{
              width: Theme.specifications.checkBoxSize,
              height: Theme.specifications.checkBoxSize,
            }}
            source={
              selectedChecklistItems[PMCheckListItemID]
                ? require('../../assets/img/ic_checkbox_active.png')
                : require('../../assets/img/ic_checkbox_inactive.png')
            }
          />
        </TouchableOpacity>
        <Text
          style={{
            flex: 1,
            color: Theme.colors.textNormal,
            fontSize: Theme.fontSize.normal,
          }}>
          {PMCheckListItemName}
        </Text>
      </View>
    );
  };

  render() {
    const {checklists, checklistItems, getChecklistItems} = this.props;

    // PMCheckListID: 18
    // PMCheckListNo: "XA"
    // PMCheckListName: "CÔNG TÁC XÂY"
    // PMCheckListDate: "2019-11-19T00:00:00.000Z"
    // PMCheckListDesc: "CÔNG TÁC HOÀN THIỆN"
    // AAStatus: "Alive"
    // AACreatedDate: "2019-11-19T09:38:20.237Z"
    // AACreatedUser: null
    // AAUpdatedDate: "2019-11-19T09:38:20.237Z"
    // AAUpdatedUser: null
    // FK_PPProjectDiscID: 3
    // PMCheckListTypeCombo: "XA"
    return (
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {this.renderNavigation()}

        <UniDropbox
          label={'Checklist'}
          selectedValue={this.state.PMCheckListID}
          options={checklists}
          pathLeft={'PMCheckListID'}
          pathValue={'PMCheckListName'}
          onSelect={(item) => {
            getChecklistItems(item);
            this.setState({
              PMCheckListID: item,
              selectedChecklistItems: {},
            });
          }}
        />

        <FlatList
          data={checklistItems}
          renderItem={this.renderChecklistItem}
          keyExtractor={(item) => `${item.PMCheckListItemID}`}
        />
      </View>
    );
  }
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonContainerActive: {
    padding: Theme.margin.m8,
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.accent,
    backgroundColor: Theme.colors.accent,
  },
  textButtonActive: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textWhite,
  },
};

const mapStateToProps = ({auth: {user, project}, newChecklist}) => ({
  user,
  token: user.token,
  project,
  checklistItems: newChecklist.checklistItems,
  checklists: newChecklist.checklists,
});

const mapDispatchToProps = {
  getAllChecklist: actions.getAllChecklist,
  getChecklistItems: actions.getChecklistItems,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewCheckList);
