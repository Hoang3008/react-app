export default {
  itemContainer: {
    width: '100%',
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  status: {
    width: 12,
    height: 12,
    borderRadius: 6,
  },
  itemMainContent: {
    marginLeft: 10,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  itemText: {
    width: '100%',
    fontSize: 16,
    maxHeight: 40,
    fontWeight: '400',
  },
  itemDate: {
    width: '100%',
    fontSize: 13,
    fontWeight: '400',
  },
};
