import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';
import API from '../../../api/API.service';

import {actionType as AddPhotoActionType} from '../../AddPhoto/actions/addPhoto.action';
import {actionType as UpdatePhotoActionType} from '../../EditPhoto/actions/updatePhoto.action';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_MEETING_LIST';

// https://apidev.rocez.com/meeting/search

const getListAPI = (projectId) => {
  const url = withKey('/meeting/search');
  return AsyncStorage.getItem('token').then((token) => {
    return API.post(
      url,
      {projectId},
      {
        headers: {
          Accept: 'application/json, text/plain, */*',
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, getListAPI, (...param) => param);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
    meetingList: propOr([], 'data', payload).sort((b1, a1) => {
      const a = a1.PMProjectMeetingDate || '';
      const b = b1.PMProjectMeetingDate || '';

      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }

      return 0;
    }),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
