import typeToReducer from 'type-to-reducer';
import getMeetingList from './actions/getMeetingList.action';

const initialState = {
  meetingList: [],
};

export const actions = {
  getMeetingList: getMeetingList.action,
};

export default typeToReducer(
  {
    ...getMeetingList.reducer,
  },
  initialState,
);
