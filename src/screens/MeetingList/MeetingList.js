import {
  SectionList,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SearchBar} from 'react-native-elements';
import {isEmpty, pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';

import styles from './styles';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import GroupSearchHeader from '../../components/GroupSearchHeader/GroupSearchHeader';
import {EMPTY} from '../../global/constants';
import {actions} from './MeetingList.reducer';
import {groupDataByKey} from '../../utils/Utils';
import moment from 'moment';
import SectionHeader from '../../components/SectionHeader/SectionHeader';
import Icon from 'react-native-vector-icons/Fontisto';
import RouteNames from '../../RouteNames';

const GROUP_DROPDOWN_DATA = [
  {
    label: 'Date',
    key: 'PMProjectMeetingDate',
  },
  {
    label: 'Status',
    key: 'PMProjectMeetingStatus',
  },
  {
    label: 'Creator',
    key: 'UserCreatorName',
  },
  {
    label: 'Location',
    key: 'PMProjectMeetingLocation',
  },
];

export const statusColor = {
  Close: '#6c757d',
  New: '#28a745',
  Inprogress: '#ffc107',
};

const MeetingList = (props) => {
  const {navigation, getMeetingList, project, meetingList} = props;

  const [searchText, setSearchText] = useState('');
  const [showSearch, setShowSearch] = useState(false);
  const [groupKey, setGroupKey] = useState(GROUP_DROPDOWN_DATA[0].key);
  const [data, setData] = useState(EMPTY.ARRAY);

  const projectId = propOr(0, 'PMProjectID', project);

  useEffect(() => {
    getMeetingList(projectId);
  }, [getMeetingList, projectId]);

  useEffect(() => {
    if (!meetingList || isEmpty(meetingList)) {
      setData(EMPTY.ARRAY);
    }

    const lowerCaseSearch = searchText ? searchText.toLowerCase() : '';

    let groupedData = meetingList.filter(({PMProjectMeetingName}) => {
      if (
        PMProjectMeetingName &&
        PMProjectMeetingName.toLowerCase().includes(lowerCaseSearch)
      ) {
        return true;
      }

      return false;
    });

    switch (groupKey) {
      case 'PMProjectMeetingStatus':
        groupedData = groupDataByKey(groupedData, 'PMProjectMeetingStatus');
        break;
      case 'UserCreatorName':
        groupedData = groupDataByKey(groupedData, 'UserCreatorName');
        break;
      case 'PMProjectMeetingLocation':
        groupedData = groupDataByKey(
          groupedData,
          'PMProjectMeetingLocation',
          (location) => location || 'NO LOCATION',
        );
        break;
      case 'PMProjectMeetingDate':
        groupedData = groupDataByKey(
          groupedData,
          'PMProjectMeetingDate',
          (date) => moment(date).format('MMM DD, YYYY'),
        );
        break;
    }

    setData(groupedData);
  }, [meetingList, searchText, groupKey]);

  // AACreatedDate: "2020-07-21T07:09:09.740Z"
  // AACreatedUser: "vuong.ho@gmail.com"
  // AAStatus: "Alive"
  // AAUpdatedDate: "2020-08-17T15:31:10.707Z"
  // AAUpdatedUser: "evenhieu@gmail.com"
  // FK_PMMeetingTypeID: 2
  // FK_PMProjectID: 125
  // PMProjectMeetingDate: "2020-07-05T00:00:00.000Z"
  // PMProjectMeetingDesc: ""
  // PMProjectMeetingDocServerpath: "https://rocez-media-dev.s3.ap-southeast-1.amazonaws.com/projects/125/Screen-Shot-2020-08-15-at-3.03.35-PM_1597677128176.png,https://rocez-media-dev.s3.ap-southeast-1.amazonaws.com/projects/125/A3-Trang-ngang_1597677313110.pdf,https://rocez-media-dev.s3.ap-southeast-1.amazonaws.com/projects/125/baseline-favorite_border-24px_1597677354226.svg"
  // PMProjectMeetingEndTime: "2020-07-05T14:30:00.000Z"
  // PMProjectMeetingID: 1066
  // PMProjectMeetingLocation: "Phòng A3"
  // PMProjectMeetingName: "METTING 01"
  // PMProjectMeetingNo: "MT01"
  // PMProjectMeetingOverview: "<h1>Overview123</h1>"
  // PMProjectMeetingStartTime: "2020-07-05T09:00:00.000Z"
  // PMProjectMeetingStatus: "Close"
  // PMProjectMeetingSubject: "Meeting Subject:"
  // UserCreatorId: 195
  // UserCreatorName: "vuong.ho@gmail.com"

  const toggleShowSearch = () => {
    setSearchText('');
    setShowSearch(!showSearch);
  };

  const handleSelectGroup = (index) => {
    const {key} = GROUP_DROPDOWN_DATA[index];

    if (groupKey === key) {
      return;
    }

    setGroupKey(key);
  };

  const renderSectionHeader = (title, color = 'transparent', data = []) => {
    return (
      <SectionHeader
        color={color}
        title={title.toUpperCase() + ' (' + data.length + ')'}
      />
    );
  };

  const renderRowItem = (item) => {
    const {
      PMProjectMeetingID,
      PMProjectMeetingNo,
      PMProjectMeetingName,
      PMProjectMeetingDate,
      PMProjectMeetingStartTime,
      PMProjectMeetingEndTime,
      PMProjectMeetingStatus,
    } = item;

    // const todayISO = moment().toISOString();
    // const isExpired = todayISO > PMTaskEndDate;

    return (
      <TouchableOpacity
        key={PMProjectMeetingID}
        onPress={() =>
          navigation.push(RouteNames.MeetingDetail, {ID: PMProjectMeetingID})
        }>
        <View style={styles.itemContainer}>
          <View
            style={[
              styles.status,
              {
                backgroundColor: statusColor[PMProjectMeetingStatus],
              },
            ]}
          />
          <View style={styles.itemMainContent}>
            <Text style={styles.itemText}>
              [{PMProjectMeetingNo}] {PMProjectMeetingName}
            </Text>
            <Text style={styles.itemDate}>
              {moment(PMProjectMeetingDate).format('M/D/YY')}{' '}
              {moment(PMProjectMeetingStartTime).format('hh:mm a')} -{' '}
              {moment(PMProjectMeetingEndTime).format('hh:mm a')}
            </Text>
          </View>
          <Icon
            style={{marginBottom: 4}}
            size={12}
            color={'#9EA0A5'}
            name="angle-right"
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundWhite,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      <GroupSearchHeader
        title="Meeting List"
        onBack={navigation.goBack}
        dropDownStyle={{height: 160}}
        groupDropdownData={GROUP_DROPDOWN_DATA.map((item) => item.label)}
        onGroupSelect={handleSelectGroup}
        onToggleSearch={toggleShowSearch}
      />
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        {showSearch && (
          <SearchBar
            placeholder="Type Here..."
            value={searchText}
            onChangeText={setSearchText}
            inputStyle={{
              color: Theme.colors.textNormal,
            }}
            containerStyle={{
              borderBottomColor: '#0000',
              backgroundColor: '#ffffff',
            }}
            inputContainerStyle={{
              backgroundColor: '#ddd',
            }}
          />
        )}
        <SectionList
          sections={data}
          keyExtractor={(item, index) => index + '-' + item.title}
          renderItem={({item}) => renderRowItem(item)}
          renderSectionHeader={({section: {title, data, color}}) =>
            renderSectionHeader(title, color, data)
          }
          ItemSeparatorComponent={() => <View style={styles.divider} />}
        />
      </View>
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, meetingList, commonData}) => {
  const {PMProjectID} = project;

  return {
    user,
    project,
    meetingList: meetingList.meetingList || [],
    categories: pathOr(EMPTY.ARRAY, ['currentProject', 'categories'], commonData),
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
  };
};

const mapDispatchToProps = {
  getMeetingList: actions.getMeetingList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MeetingList);
