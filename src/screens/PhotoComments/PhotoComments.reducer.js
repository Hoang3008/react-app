import typeToReducer from 'type-to-reducer';
import getPhotoCommentList from './actions/getPhotoCommentList.action';
import sendComment from './actions/sendComment.action';

const initialState = {
  commentList: [],
};

export const actions = {
  getPhotoCommentList: getPhotoCommentList.action,
  sendComment: sendComment.action,
};

export default typeToReducer(
  {
    ...getPhotoCommentList.reducer,
    ...sendComment.reducer,
  },
  initialState,
);
