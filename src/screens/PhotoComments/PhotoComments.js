import {connect} from 'react-redux';
import {pathOr, propOr} from 'ramda';
import {
  FlatList,
  Image,
  ImageBackground,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import moment from 'moment';

import {actions} from './PhotoComments.reducer';
import styles from './PhotoComments.styles';

import Theme from '../../Theme';
import {UniImageButton, UniStatusBarBackground} from '../../components';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import {getUserAvatarUrlByUsername} from '../../api/urls';

const PhotoComments = (props) => {
  const {
    navigation,
    commentList,
    getPhotoCommentList,
    user,
    sendComment,
    project
  } = props;
  const [comment, setComment] = useState('');
  const id = pathOr('', ['route', 'params', 'id'], props);
  const PMProjectID = propOr('', 'PMProjectID', project);

  useEffect(() => {
    getPhotoCommentList(id, PMProjectID);
  }, [getPhotoCommentList, id]);

  const handleSendComment = () => {
    const item = {
      AACreatedDate: new Date().toISOString(),
      AACreatedUser: user.ADUserName,
      AAStatus: 'Alive',
      AAUpdatedDate: new Date().toISOString(),
      AAUpdatedUser: user.ADUserName,
      FK_PMPhotoID: id,
      PMPhotoCommentDate: new Date().toISOString(),
      PMPhotoCommentDesc: comment,
      PMPhotoCommentID: 0,
    };

    sendComment(item, PMProjectID).then(() => {
      setComment('');
    });
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={{...styles.row, flex: 1}}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <View>
          <View style={styles.row}>
            <Text style={styles.textHeader} numberOfLines={1}>
              Comments
            </Text>
          </View>
        </View>
      </View>
    </View>
  );

  const renderCommentItem = ({item}) => {
    // AACreatedDate: "2020-04-20T23:35:10.930Z"
    // AACreatedUser: "rove2012@gmail.com"
    // AAStatus: "Alive"
    // AAUpdatedDate: "2020-04-20T23:35:10.930Z"
    // AAUpdatedUser: "rove2012@gmail.com"
    // FK_PMPhotoID: 175
    // PMPhotoCommentDate: "2020-04-20T00:00:00.000Z"
    // PMPhotoCommentDesc: "test 2"
    // PMPhotoCommentID: 2
    const {AACreatedUser, AACreatedDate, PMPhotoCommentDesc} = item;

    const showDate = moment(AACreatedDate).format('YYYY/MM/DD hh:mm A');

    const avatar = getUserAvatarUrlByUsername(AACreatedUser);
    const name = AACreatedUser;

    if (
      user.ADUserName === AACreatedUser ||
      `${user.ADUserID}` === AACreatedUser
    ) {
      return (
        <View style={{padding: 15}}>
          <View
            style={{
              ...styles.row,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              display: 'flex',
            }}>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
                textAlign: 'right',
              }}>
              {showDate}
            </Text>
          </View>
          <View
            style={{
              paddingLeft:
                Theme.specifications.commentAvatarSize + Theme.margin.m12,
              alignItems: 'flex-end',
            }}>
            {PMPhotoCommentDesc && PMPhotoCommentDesc.length > 0 ? (
              <Text
                style={{
                  marginTop: Theme.margin.m6,
                  paddingVertical: Theme.margin.m8,
                  paddingHorizontal: Theme.margin.m10,
                  backgroundColor: '#3483F6',
                  borderRadius: 10,
                  borderWidth: 0,
                  color: '#fff',
                  fontStyle: 'normal',
                  fontSize: 14,
                }}>
                {PMPhotoCommentDesc}
              </Text>
            ) : null}
          </View>
        </View>
      );
    }

    return (
      <View style={{padding: 15}}>
        <View style={styles.row}>
          <ImageBackground
            style={{
              resizeMode: 'center',
              width: Theme.specifications.commentAvatarSize,
              height: Theme.specifications.commentAvatarSize,
              borderRadius: Theme.specifications.commentAvatarSize / 2,
            }}
            source={require('../../assets/img/default_avatar.png')}>
            <Image
              style={{
                resizeMode: 'center',
                width: Theme.specifications.commentAvatarSize,
                height: Theme.specifications.commentAvatarSize,
                borderRadius: Theme.specifications.commentAvatarSize / 2,
              }}
              source={{uri: avatar}}
            />
          </ImageBackground>
          <View style={{marginLeft: Theme.margin.m12}}>
            <Text
              style={{
                paddingVertical: Theme.margin.m2,
                fontSize: Theme.fontSize.normal,
                color: Theme.colors.textNormal,
                fontWeight: 'bold',
              }}>
              {name}
            </Text>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
              }}>
              {showDate}
            </Text>
          </View>
        </View>
        <View
          style={{
            paddingLeft:
              Theme.specifications.commentAvatarSize + Theme.margin.m12,
            alignItems: 'flex-start',
          }}>
          {PMPhotoCommentDesc && PMPhotoCommentDesc.length > 0 ? (
            <Text
              style={{
                marginTop: Theme.margin.m6,
                paddingVertical: Theme.margin.m8,
                paddingHorizontal: Theme.margin.m10,
                backgroundColor: Theme.colors.backgroundComment,
                borderRadius: 10,
                borderWidth: 0,
                fontStyle: 'normal',
                fontSize: 14,
              }}>
              {PMPhotoCommentDesc}
            </Text>
          ) : null}
        </View>
      </View>
    );
  };

  return (
    <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="height" enabled>
      <View
        style={{
          flex: 1,
          backgroundColor: Theme.colors.backgroundColor,
          display: 'flex',
          flexDirection: 'column',
        }}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {renderNavigation()}

        <FlatList
          style={{flex: 1}}
          scrollEnabled={true}
          data={commentList}
          renderItem={renderCommentItem}
          keyExtractor={(item) => `${item.PMPhotoCommentID}`}
          showsVerticalScrollIndicator={false}
        />

        <View style={styles.inputCommentContainer}>
          <TextInput
            placeholder="Type your comment here..."
            value={comment}
            style={styles.inputComment}
            multiline
            onChangeText={setComment}
          />
          <TouchableOpacity
            onPress={handleSendComment}
            style={styles.buttonSendComment}>
            <Text style={{color: 'white', fontSize: Theme.fontSize.normal}}>
              Send
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAvoidingViewWrapper>
  );
};

const mapStateToProps = ({auth: {user, project}, photoComments}) => {
  return {
    user,
    project,
    commentList: photoComments.commentList,
  };
};

const mapDispatchToProps = {
  getPhotoCommentList: actions.getPhotoCommentList,
  sendComment: actions.sendComment,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotoComments);
