import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_PHOTO_COMMENT_LIST';

const getAPI = (id, PMProjectID) => {
  const url = withKey(`/PMPhotoComments/GetAllDataByPhoto?FK_PMPhotoID=${id}`);
  return AsyncStorage.getItem('token').then((token) => {
      return API.get(withKey(`/photo/comment?projectId=${PMProjectID}&photoID=${id}`), {
        headers: {Authorization: `Bearer ${token}`},
      });
  });
};

const action = createAction(actionType, getAPI, (...param) => param);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    commentList: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
