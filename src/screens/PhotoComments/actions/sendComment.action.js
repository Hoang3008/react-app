import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';
import AsyncStorage from '@react-native-community/async-storage';
import {withKey} from '../../../api/urls';

const actionType = 'SEND_PHOTO_COMMENT';

const createTaskAPI = (item, PMProjectID) => {
  return AsyncStorage.getItem('token').then((token) => {
    return API.post(
      withKey(`/photo/comment`),
      {
        projectId: PMProjectID,
        FK_PMPhotoID: item?.FK_PMPhotoID,
        PMPhotoCommentDesc: item?.PMPhotoCommentDesc
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
});
};
// // https://apidev.rocez.com/PMPhotoComments/CreateObject

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    commentList: [...state.commentList, payload.data],
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
