import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {UniStatusBarBackground} from '../../components';
import Theme from '../../Theme';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import RouteNames from '../../RouteNames';
import {requestGetAllMasterData} from '../../api/auth';
import {loadMasterDataIntoRedux} from '../../action/AuthActions';
import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {isEmpty, pathOr, propOr} from 'ramda';
import { PERMISSION_TYPE } from '../../global/constants';

class DashboardMain extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loadMasterDataDone: false,
    };
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {loadMasterDataDone} = this.state;
    const {master} = this.props;

    if (loadMasterDataDone && !master) {
      this.loadMasterData();
    }
  }

  componentDidMount() {
    this.loadMasterData();

    const {
      getDefectCodeList,
      getProjectCategories,
      getProjectLocations,
      getPeopleInProject,
   //   getDefectList, TODO
      project: {PMProjectID},
      user,
      token
    } = this.props;

    // getDefectCodeList({projectId: PMProjectID}, token); TODO
    // getProjectLocations(PMProjectID); TODO
    // getProjectCategories(PMProjectID); TODO
    // getPeopleInProject({ 
    //   PMProjectID: PMProjectID, TODO
    //   UserName: user.ADUserName,
    // });
   // getDefectList({userId: user.ADUserID, projectId: PMProjectID}, user); TODO
  }

  loadMasterData = () => {
    const {user} = this.props;
    if (isEmpty(user)) {
      return;
    }

    this.setState({
      loadMasterDataDone: false,
    });
    requestGetAllMasterData()
      .then((response) => {
        if (response && response.data) {
          this.props.loadMasterDataIntoRedux(response.data);
          this.setState({
            loadMasterDataDone: true,
          });
        }
      })
      .catch((error) => {
        this.setState({
          loadMasterDataDone: true,
        });
      });
  };

  renderNavigation = () => {
    let {PMProjectName} = this.props.project || {};
    return (
      <View style={styles.navigationBar}>
        <View>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate(RouteNames.SelectProjectScreen)
            }>
            <View
              style={{
                ...styles.row,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.textHeader}>{PMProjectName || ''}</Text>
              <Image
                style={{
                  width: Theme.specifications.icSmall,
                  height: Theme.specifications.icSmall,
                }}
                source={require('../../assets/img/ic_arrow_down.png')}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  onPressDrawing = () => {
    this.props.navigation.navigate(RouteNames.DrawingTabs, {
      time: Date.now(),
    });
  };

  onPressDefects = () => {
    this.props.navigation && this.props.navigation.push(RouteNames.DefectMain);
  };
  onPressInspection = () => {
    this.props.navigation.push(RouteNames.InspectionTabs);
  };

  onPressSchedule = () => {
    this.props.navigation.push(RouteNames.ScheduleList);
  };

  onPressPhotos = () => {
    this.props.navigation.push(RouteNames.PhotoListTabs);
  };

  onPressTask = () => {
    this.props.navigation.push(RouteNames.TaskList);
  };

  onPressDashboard = () => {
    this.props.navigation.push(RouteNames.ProjectOverviewDashboard);
  };

  onPressMeeting = () => {
    this.props.navigation.push(RouteNames.MeetingList);
  };

  render() {
    const {modulePermissions} = this.props;
    const isDisableTab = (tab) => {
      let isDiable = 
            modulePermissions.find((item) => item.module === tab && item.permissions != PERMISSION_TYPE.TYPE_NONE) ?
            false :
            true;
      
            return isDiable;
    };

    return (
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundColor}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {this.renderNavigation()}
        <ScrollView bounces={false}>
          <View style={{width: SCREEN_WIDTH, flex: 1}}>
            <View style={styles.row}>
              <TouchableOpacity
                style={!isDisableTab('defect') ? styles.button : styles.buttonDisable}
                onPress={this.onPressDrawing}
                disabled={isDisableTab('defect')}>
                <Image
                  style={styles.icon}
                  source={require('../../assets/img/ic_tool_drawing.png')}
                />
                <Text style={styles.text}>Drawing</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={!isDisableTab('defect') ? styles.button : styles.buttonDisable}
                onPress={this.onPressDefects}
                disabled={isDisableTab('defect')}>
                <Image
                  style={styles.icon}
                  source={require('../../assets/img/ic_tool_defect.png')}
                />
                <Text style={styles.text}>Defect</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.row}>
              <TouchableOpacity
                style={!isDisableTab('itp') ? styles.button : styles.buttonDisable}
                onPress={this.onPressInspection}
                disabled={isDisableTab('itp')}>
                <Image
                  style={styles.icon}
                  source={require('../../assets/img/ic_tool_inspection.png')}
                />
                <Text style={styles.text}>Inspection</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={!isDisableTab('schedule') ? styles.button : styles.buttonDisable}
                onPress={this.onPressSchedule}
                disabled={isDisableTab('schedule')}>
                <Image
                  style={styles.icon}
                  source={require('../../assets/img/ic_tool_schedule.png')}
                />
                <Text style={styles.text}>Schedule</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.row}>
              <TouchableOpacity
                style={!isDisableTab('photo') ? styles.button : styles.buttonDisable}
                onPress={this.onPressPhotos}
                disabled={isDisableTab('photo')}>
                <Image
                  style={styles.icon}
                  source={require('../../assets/img/ic_tool_photo.png')}
                />
                <Text style={styles.text}>Photos</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={!isDisableTab('task') ? styles.button : styles.buttonDisable}
                onPress={this.onPressTask}
                disabled={isDisableTab('task')}>
                <View
                  style={{
                    width: 60,
                    height: 60,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingTop: 10,
                  }}>
                  <IconIonicons size={40} color="#df9500" name="ios-list" />
                </View>
                <Text style={styles.text}>Task</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.row}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.onPressDashboard}>
              <View
                style={{
                  width: 60,
                  height: 60,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingTop: 10,
                }}>
                <SimpleLineIcons size={40} color="#df9500" name="speedometer" />
              </View>
              <Text style={styles.text}>Dashboard</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={!isDisableTab('meeting') ? styles.button : styles.buttonDisable}
              onPress={this.onPressMeeting}
              disabled={isDisableTab('meeting')}>
              <View
                style={{
                  width: 60,
                  height: 60,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingTop: 10,
                }}>
                <MaterialCommunityIcons
                  size={40}
                  color="#df9500"
                  name="account-group"
                />
              </View>
              <Text style={styles.text}>Meeting</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              ...styles.row,
              height: 10,
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingHorizontal: Theme.margin.m16,
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
  },
  button: {
    width: (SCREEN_WIDTH - 15 * 3) / 2,
    height: 122,
    marginLeft: 15,
    marginTop: 15,
    paddingTop: 12,
    paddingBottom: 12,
    backgroundColor: 'white',
    borderRadius: Theme.margin.m10,
    alignItems: 'center',
    justifyContent: 'center',

    elevation: 1,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 2,
      height: 2,
    },
  },

  buttonDisable: {
    width: (SCREEN_WIDTH - 15 * 3) / 2,
    height: 122,
    marginLeft: 15,
    marginTop: 15,
    paddingTop: 12,
    paddingBottom: 12,
    backgroundColor: 'grey',
    borderRadius: Theme.margin.m10,
    alignItems: 'center',
    justifyContent: 'center',

    elevation: 1,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 2,
      height: 2,
    },
  },
  text: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNormal,
    paddingVertical: Theme.margin.m6,
  },
  icon: {
    flex: 1,
    resizeMode: 'contain',
    width: 56,
    height: 56,
  },
};

const mapStateToProps = ({auth: {project, user, master},  commonData}) => {

  const modulePermissions = pathOr([],['Permission'],commonData);
  
  return {
    project,
    user,
    master,
    modulePermissions,
    token: user.token,
  }
};

const mapDispatchToProps = {
  loadMasterDataIntoRedux,
  getDefectCodeList: actions.getDefectCodeList,
  getProjectCategories: actions.getProjectCategories,
  getProjectLocations: actions.getProjectLocations,
  getPeopleInProject: actions.getPeopleInProject,
//  getDefectList: actions.getDefectList, TODO
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashboardMain);
