import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  Image,
  StatusBar,
  SectionList,
  TouchableOpacity,
} from 'react-native';
import {clone, isEmpty, pathOr, propOr} from 'ramda';
import {SearchBar} from 'react-native-elements';
import messaging from '@react-native-firebase/messaging';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {UniStatusBarBackground} from '../components';
import {SCREEN_WIDTH} from '../utils/DimensionUtils';
import Theme from '../Theme';
import RouteNames from '../RouteNames';
import {loadProjectIntoRedux} from '../action/AuthActions';
import UniImageButton from '../components/UniImageButton';
import postTokenAction from '../action/postToken.action';
import {actions} from './CommonProjectData/CommonProjectData.reducer';
import {getDrawingListAction} from '../action/DrawingActions';
import {logout} from '../action/AuthActions';


async function requestUserPermission() {
  const settings = await messaging().requestPermission();

  if (settings) {
    console.log('Permission settings:', settings);
  }
}

class SelectProjectScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSearchBox: false,
      search: '',
      index: 0,
      routes: [
        {key: 'all', title: 'All'}
      ],
      loading: true,
      rawProjectList: [],
      allProjects: [],
    };
  }

  async componentDidMount() {
    await this.getData();
    requestUserPermission();

    // Get the device token
    messaging()
      .getToken()
      .then((token) => {
        return this.sendTokenToServer(token);
      });

    // Listen to whether the token changes
    messaging().onTokenRefresh((token) => {
      this.sendTokenToServer(token);
    });
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {rawProjectList} = this.state;

    const prevTime = pathOr(0, ['route', 'params', 'time'], prevProps);
    const time = pathOr(0, ['route', 'params', 'time'], this.props);

    if (prevTime !== time && time !== 0 && rawProjectList.length > 0) {
      const notification = pathOr(
        0,
        ['route', 'params', 'notification'],
        this.props,
      );

      if (notification) {
        const {projectId} = notification;
        const project = (rawProjectList[0].data || []).find(
          ({PMProjectID}) => `${PMProjectID}` === projectId,
        );

        if (!project) {
          return;
        }

        this.onPressProject(project, notification);
      }
    }

    const {user} = this.props;
    if (
      user.ADUserName &&
      user.ADUserName !== pathOr('', ['user', 'ADUserName'], prevProps)
    ) {
      this.getData();
    }
  }

  sendTokenToServer = async (pushToken) => {
    // postToken
    const {user, postToken} = this.props;
    const token = propOr('', 'token', user);

    if (!pushToken || !token) {
      return;
    }

    try {
      await postToken(pushToken, token);
      console.log('Sent token to server done');
    } catch (e) {
      console.log('Sent token to server error', JSON.stringify(e));
    }
  };

  toggleShowSearch = () => {
    const {showSearchBox} = this.state;
    this.setState({
      showSearchBox: !showSearchBox,
      search: '',
    });
  };

  updateSearch = (search) => {
    const {rawProjectList} = this.state;

    const lowerCaseSearch = (search || '').toLowerCase();

    const newAllProjects = clone(rawProjectList).map((item) => ({
      ...item,
      data: item.data.filter(({PMProjectName}) =>
        PMProjectName.toLowerCase().includes(lowerCaseSearch),
      ),
    }));

    this.setState({search, allProjects: newAllProjects});
  };

  getData = async () => {
    const {getAllProjectList} = this.props;

    let response = await getAllProjectList(this.props.user.ADUserName);
    const responseData = pathOr([], ['action', 'payload', 'data'], response);
    const data = [];

    if (responseData && !isEmpty(responseData)) {
      data.push({title: 'All Projects', data: response.action.payload.data});
      this.setState({
        loading: false,
        rawProjectList: data,
        allProjects: data,
      });
    }
  };

  /*
    event change project
   */
  onPressProject = (project, notification) => {
    const {loadProjectIntoRedux} = this.props;
    loadProjectIntoRedux(project);
    null;
    let PMProjectID = project?.PMProjectID,
      user = this.props.user;

    /*load list drawing when change project*/
    if (PMProjectID) {
      this.props.getDrawingList(null, PMProjectID, null);
    }
    // this.props.navigation.navigate(RouteNames.HomeStack, {project: project});
    this.props.navigation.navigate(RouteNames.BottomTabs, {
      project: project,
      search: '',
      notification,
      time: Date.now(),
      projectId: project?.PMProjectID,
    });
  };

  handleLogout = () => {
    const {logout, navigation} = this.props;

    logout();
    navigation.navigate(RouteNames.AuthStack);
  };

  renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <Text style={styles.textHeader}>Select project</Text>
        <TouchableOpacity style={{ position: 'absolute',right: 70}} onPress={this.handleLogout}>
          <AntDesign 
          style={{
           color: '#fff',
           fontSize: 20,
           marginTop: 2
          }}
          name={'logout'}
            />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.toggleShowSearch}>
          <Image
            style={{
              width: Theme.specifications.icSmall,
              height: Theme.specifications.icSmall,
            }}
            source={require('../assets/img/ic_search.png')}
          />
        </TouchableOpacity>
      </View>
    );
  };

  renderTabAll = () => {
    const {showSearchBox, search, allProjects} = this.state;

    return (
      <View style={{flex: 1}}>
        {showSearchBox && (
          <SearchBar
            placeholder="Type Here..."
            value={search}
            onChangeText={this.updateSearch}
            inputStyle={{
              color: Theme.colors.textNormal,
            }}
            containerStyle={{
              borderBottomColor: '#0000',
              backgroundColor: '#ffffff',
            }}
            inputContainerStyle={{
              backgroundColor: '#ddd',
            }}
          />
        )}
        {!isEmpty(allProjects) && (
          <SectionList
            sections={allProjects}
            extraData={this.state}
            keyExtractor={(item, index) => index + '-' + item.title}
            renderItem={({item}) => this.renderItem(item)}
            renderSectionHeader={({section: {title}}) =>
              this.renderSectionHeader(title)
            }
            ItemSeparatorComponent={() => <View style={styles.divider} />}
          />
        )}
      </View>
    );
  };

  renderSectionHeader = (title) => {
    return (
      <Text
        style={{
          paddingHorizontal: Theme.margin.m15,
          paddingBottom: Theme.margin.m6,
          paddingTop: Theme.margin.m22,
          color: Theme.colors.textNormal,
          backgroundColor: Theme.colors.backgroundHeader,
        }}>
        {title}
      </Text>
    );
  };

  renderItem = (item) => {
    const {projectNotificationCount} = this.props;
    const notificationCount = propOr(
      0,
      item.PMProjectID,
      projectNotificationCount,
    );

    return (
      <TouchableOpacity
        style={{
          paddingHorizontal: 16,
          paddingVertical: 14,
          display: 'flex',
          flexDirection: 'row',
        }}
        onPress={() => this.onPressProject(item)}>
        <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
          <Text
            style={{
              color: Theme.colors.textNormal,
              fontSize: Theme.fontSize.header_16,
            }}>
            {item.PMProjectName || ''}
          </Text>
          <Text
            style={{
              color: Theme.colors.textSecond,
              fontSize: Theme.fontSize.normal,
              marginTop: 2,
            }}>
            {item.PMProjectName || ''}
          </Text>
        </View>
        {notificationCount > 0 && (
          <View
            style={{
              backgroundColor: 'red',
              borderRadius: 8,
              width: 15,
              height: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: 'white', fontSize: 10, fontWeight: 'bold'}}>
              {notificationCount}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  };

  onPressNewProject = () => {
    this.props.navigation.navigate(RouteNames.CreateProject, {
      onGoBack: () => this.getData(),
    });
  };

  render() {
    const {user} = this.props;
    const userType = propOr('', 'ADUserType', user);

    return (
      <View style={{flex: 1}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {this.renderNavigation()}
        {this.renderTabAll()}
        {userType === 'SUPERADMIN' && (
          <UniImageButton
            containerStyle={styles.floatButton}
            source={require('../assets/img/ic_add.png')}
            onPress={this.onPressNewProject}
          />
        )}
      </View>
    );
  }
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingHorizontal: Theme.margin.m16,
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
    position: 'relative',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m16,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },

  floatButton: {
    position: 'absolute',
    bottom: Theme.margin.m16,
    right: Theme.margin.m20,
    width: Theme.specifications.floatButtonSize,
    height: Theme.specifications.floatButtonSize,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#34AA44',
    borderRadius: Theme.specifications.floatButtonSize / 2,
    borderWidth: 0,
  },
};

const mapStateToProps = ({auth: {project, user}, commonData}) => ({
  project,
  user,
  projectNotificationCount: commonData.projectNotificationCount,
  projectList: commonData.projectList,
});

const mapDispatchToProps = {
  getDrawingList: getDrawingListAction,
  loadProjectIntoRedux,
  postToken: postTokenAction.action,
  getAllProjectList: actions.getAllProjectList,
  // getDefectList: actions.getDefectList, TODO
  getAllPermissionsByProject: actions.getAllPermissionsByProject,
  logout: logout
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectProjectScreen);
