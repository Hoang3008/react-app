import React, {useEffect, useRef, useState} from 'react';
import {connect} from 'react-redux';

import {actions} from './InspectionList.reducer';
import Theme from '../../Theme';
import {
  Image,
  SectionList,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {UniStatusBarBackground} from '../../components';
import {SearchBar} from 'react-native-elements';
import UniImageButton from '../../components/UniImageButton';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {pathOr, propOr} from 'ramda';
import RouteNames from '../../RouteNames';
import ModalDropdown from 'react-native-modal-dropdown';
import {groupData} from './InspectionList.utils';
import {EMPTY} from '../../global/constants';

const GROUP_DROPDOWN_DATA = [
  {
    label: 'Category',
    key: 'FK_PMProjectCategoryID',
  },
  {
    label: 'Created User',
    key: 'AACreatedUser',
  },
  {
    label: 'Inspector',
    key: 'FK_PMInspectPeopleID',
  },
  {
    label: 'Main Contact',
    key: 'FK_PMProjectPeopleID',
  },
  {
    label: 'Signed By',
    key: 'FK_PMProjectApprovedPeopleID',
  },
  {
    label: 'Location',
    key: 'FK_PMProjectLocationID',
  },
];

const InspectionList = (props) => {
  const {
    navigation,
    categories,
    peopleInProject,
    inspectionList,
    locations,
    getInspectionList,
    getInspectionListByFolderId,
    project,
    showSearchBox,
    groupByKey,
  } = props;

  const [searchText, setSearchText] = useState('');
  const [groupedInspectionList, setGroupedInspectionList] = useState([]);
  const [showSearchBoxLocal, setShowSearchBoxLocal] = useState(false);
  const [groupByKeyLocal, setGroupByKeyLocal] = useState(
    'FK_PMProjectCategoryID',
  );
  const groupDropDownRef = useRef(null);

  const folderId = pathOr(undefined, ['route', 'params', 'folderId'], props);

  const PMProjectID = propOr(0, 'PMProjectID', project);

  useEffect(() => {
    if (folderId) {
      getInspectionListByFolderId(folderId);
      return;
    }
    getInspectionList(PMProjectID);
  }, [getInspectionList, getInspectionListByFolderId, PMProjectID, folderId]);

  useEffect(() => {
    if (!inspectionList) {
      setGroupedInspectionList([]);
      return;
    }

    const combineGroupKey = groupByKey || groupByKeyLocal;

    const lowerCaseSearch = (searchText || '').toLowerCase();

    const filteredList = !searchText
      ? inspectionList
      : inspectionList.filter(({PMProjectInspName}) =>
          PMProjectInspName.toLowerCase().includes(lowerCaseSearch),
        );

    const result = groupData(filteredList, combineGroupKey, {
      peopleInProject,
      categories,
      locations,
    });

    setGroupedInspectionList(
      Object.keys(result).map((key) => ({
        title: key,
        data: result[key],
      })),
    );
  }, [inspectionList, searchText, categories, groupByKey, groupByKeyLocal]);

  const handleItemClick = (item, index) => {
    navigation.push(RouteNames.InspectionDetail, item);
  };

  const onPressNewTask = () => {
    navigation.push(RouteNames.InspectionUpdate);
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => handleItemClick(item, index)}>
        <View
          style={{
            padding: 15,
            display: 'flex',
            flexDirection: 'column',
            shadowColor: '#555',
            backgroundColor: 'white',
            borderRadius: 5,
          }}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text
              style={{
                fontSize: 12,
                color: '#9EA0A5',
                fontWeight: 'bold'
              }}>
              Create User : 
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: '#9EA0A5',
                }}>
              {` ${item.AACreatedUser}`}
            </Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text
              style={{
                fontSize: 12,
                color: '#9EA0A5',
                fontWeight: 'bold'
              }}>
              Create Date : 
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: '#9EA0A5',
                }}>
              {` ${item.AACreatedDate}`}
            </Text>
          </View>
          <Text
            style={{
              marginTop: 7,
              fontSize: 16,
              color: '#3E3F42',
            }}>
            {item.PMProjectInspName}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderSectionHeader = (title) => {
    return (
      <Text
        style={{
          paddingHorizontal: Theme.margin.m15,
          paddingBottom: Theme.margin.m6,
          paddingTop: Theme.margin.m22,
          color: Theme.colors.textNormal,
          fontSize: 17,
          backgroundColor: Theme.colors.backgroundHeader,
        }}>
        {title}
      </Text>
    );
  };

  const showGroupDropDown = () => {
    if (!groupDropDownRef.current) {
      return;
    }
    groupDropDownRef.current.show();
  };

  const handleSelectGroup = (index) => {
    const {key} = GROUP_DROPDOWN_DATA[index];

    if (groupByKeyLocal === key) {
      return;
    }

    setGroupByKeyLocal(key);
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={styles.row}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <Text style={styles.textHeader}>Inspection</Text>
      </View>
      <View style={styles.row}>
        <TouchableOpacity onPress={showGroupDropDown}>
          <Image
            style={styles.navIcon}
            source={require('../../assets/img/ic_font.png')}
          />
        </TouchableOpacity>
        <ModalDropdown
          ref={groupDropDownRef}
          defaultIndex={0}
          options={GROUP_DROPDOWN_DATA.map((item) => item.label)}
          dropdownTextStyle={styles.dropdownTextStyle}
          dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
          dropdownStyle={styles.dropdownStyle}
          textStyle={{display: 'none'}}
          defaultValue=""
          renderButtonText={() => ''}
          onSelect={handleSelectGroup}
        />
        <TouchableOpacity onPress={() => setShowSearchBoxLocal(!showSearchBox)}>
          <Image
            style={styles.navIcon}
            source={require('../../assets/img/ic_search.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
      {folderId && (
        <>
          <StatusBar
            backgroundColor={Theme.colors.primary}
            barStyle="light-content"
          />
          <UniStatusBarBackground />
          {renderNavigation()}
        </>
      )}

      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        {(showSearchBox || showSearchBoxLocal) && (
          <SearchBar
            placeholder="Type Here..."
            value={searchText}
            onChangeText={setSearchText}
            inputStyle={{
              color: Theme.colors.textNormal,
            }}
            containerStyle={{
              borderBottomColor: '#0000',
              backgroundColor: '#ffffff',
            }}
            inputContainerStyle={{
              backgroundColor: '#ddd',
            }}
          />
        )}
        <SectionList
          sections={groupedInspectionList}
          keyExtractor={(item, index) => item.PMProjectInspID}
          renderItem={renderItem}
          renderSectionHeader={({section: {title}}) =>
            renderSectionHeader(title)
          }
          ItemSeparatorComponent={() => <View style={styles.divider} />}
        />
      </View>
      <UniImageButton
        containerStyle={styles.floatButton}
        source={require('../../assets/img/ic_add.png')}
        onPress={onPressNewTask}
      />
    </View>
  );
};

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m52,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  sectionHeaderText: {
    flex: 1,
    paddingHorizontal: Theme.margin.m12,
    paddingBottom: Theme.margin.m6,
    paddingTop: Theme.margin.m22,
    color: Theme.colors.textNormal,
    backgroundColor: Theme.colors.backgroundHeader,
  },
  floatButton: {
    position: 'absolute',
    bottom: Theme.margin.m16,
    right: Theme.margin.m20,
    width: Theme.specifications.floatButtonSize,
    height: Theme.specifications.floatButtonSize,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#34AA44',
    borderRadius: Theme.specifications.floatButtonSize / 2,
    borderWidth: 0,
  },
  dropdownStyle: {
    width: 200,
    height: 241,
  },
  dropdownTextStyle: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.header_16,
    marginLeft: Theme.margin.m12,
    marginRight: Theme.margin.m12,
  },
  dropdownTextHighlightStyle: {
    color: Theme.colors.textPrimary,
  },
};

const mapStateToProps = (
  {auth: {user, project, master}, commonData, inspectionList},
  props,
) => {
  const PMProjectID = propOr(0, 'PMProjectID', project);

  const folderId = pathOr(undefined, ['route', 'params', 'folderId'], props);

  const inspects = folderId
    ? propOr([], 'inspectionListByFolder', inspectionList)
    : propOr([], 'inspectionList', inspectionList);

  return {
    user,
    project,
    categories: master ? master.categories : [],
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    inspectionList: inspects,
  };
};

const mapDispatchToProps = {
  getInspectionList: actions.getInspectionList,
  getInspectionListByFolderId: actions.getInspectionListByFolderId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionList);
