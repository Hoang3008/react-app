import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_INSPECTION_LIST';

// https://apidev.rocez.com/itp/of-group/13
// https://apidev.rocez.com/itp/of-project/125

const createTaskAPI = (projectId) => {
  const url = withKey(`/itp/of-project/${projectId}`);
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    inspectionList: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
