import {groupBy} from 'ramda';

const groupByCategory = ({FK_PMProjectCategoryID}, {categories}) => {
  const category = categories.find(
    ({PPProjectDiscID}) => PPProjectDiscID === FK_PMProjectCategoryID,
  );

  if (!category) {
    return 'NO CATEGORY';
  }

  return `[${category['PPProjectDiscNo']}] - ${category['PPProjectDiscName']}`;
};

const groupByPeopleInProject = (itemId, {peopleInProject}) => {
  const people = peopleInProject.find(
    ({PMProjectPeopleID}) => PMProjectPeopleID === itemId,
  );

  if (!people) {
    return 'NO USER';
  }

  return `[${people.shortName}] - ${people.fullName}`;
};

const groupByCreatedUser = (itemId, {peopleInProject}) => {
  const people = peopleInProject.find(
    ({PMProjectPeopleEmailAddress}) => PMProjectPeopleEmailAddress === itemId,
  );

  if (!people) {
    return 'NO USER';
  }

  return `[${people.shortName}] - ${people.fullName}`;
};

const groupByLocation = (itemId, {locations}) => {
  const location = locations.find(
    ({PMProjectLocationID}) => PMProjectLocationID === itemId,
  );

  if (!location) {
    return 'NO LOCATION';
  }

  return `[${location['PMProjectLocationNo']}] - ${
    location['PMProjectLocationName']
  }`;
};

const groupByItem = (item, groupByKey, metaData) => {
  switch (groupByKey) {
    case 'FK_PMProjectCategoryID':
      return groupByCategory(item, metaData);
    case 'AACreatedUser':
      return groupByCreatedUser(item.AACreatedUser, metaData);
    case 'FK_PMInspectPeopleID':
      return groupByPeopleInProject(item.FK_PMInspectPeopleID, metaData);
    case 'FK_PMProjectPeopleID':
      return groupByPeopleInProject(item.FK_PMProjectPeopleID, metaData);
    case 'FK_PMProjectApprovedPeopleID':
      return groupByPeopleInProject(
        item.FK_PMProjectApprovedPeopleID,
        metaData,
      );
    case 'FK_PMProjectLocationID':
      return groupByLocation(item.FK_PMProjectLocationID, metaData);
  }
};

export const groupData = (data, groupByKey, metaData) => {
  return groupBy((item) => groupByItem(item, groupByKey, metaData), data);
};
