import typeToReducer from 'type-to-reducer';
import getInspectionList from './actions/getInspectionList.action';
import getInspectionListByFolderId from './actions/getInspectionListByFolderId.action';

const initialState = {
  inspectionList: [],
  inspectionListByFolder: [],
};

export const actions = {
  getInspectionList: getInspectionList.action,
  getInspectionListByFolderId: getInspectionListByFolderId.action,
};

export default typeToReducer(
  {
    ...getInspectionList.reducer,
    ...getInspectionListByFolderId.reducer,
  },
  initialState,
);
