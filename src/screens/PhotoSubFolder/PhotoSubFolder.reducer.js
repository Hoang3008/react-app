import typeToReducer from 'type-to-reducer';
import getPhotoSubFolder from './actions/PhotoSubFolder.action';

const initialState = {
  photoSubFolder: []
}
 

export const actions = {
    getPhotoSubFolder: getPhotoSubFolder.action,
};

export default typeToReducer(
  {
    ...getPhotoSubFolder.reducer,
  },
  initialState,
);
