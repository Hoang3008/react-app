import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {SwipeListView} from 'react-native-swipe-list-view';
import {
  Alert,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native';

import {actions} from './PhotoSubFolder.reducer';
import styles from './PhotoSubFolder.style';

import Theme from '../../Theme';
import UniImageButton from '../../components/UniImageButton';
import {pathOr, propOr} from 'ramda';
import RouteNames from '../../RouteNames';
import {UniStatusBarBackground} from '../../components';

const PhotoSubFolder = (props) => {
  const {
    token,
    navigation,
    photoSubFolder,
    getPhotoSubFolder,
    project,
  } = props;

  const PMProjectID = propOr(0, 'PMProjectID', project);
  const folderId = pathOr(undefined, ['route', 'params', 'folderId'], props);
  const name = pathOr(undefined, ['route', 'params', 'name'], props);

  useEffect(() => {
    getPhotoSubFolder({projectId:PMProjectID, parentId:folderId},token);
  }, [getPhotoSubFolder, PMProjectID, folderId]);

  const handleItemClick = (item, index) => {
    if (item.types === 0) {
      navigation.navigate(RouteNames.PhotoList, {
        areaId: item.id,
        time: Date.now(),
        folderId: item.id
      });
    } else {
      navigation.navigate(RouteNames.PhotoSubFolder, {
        folderId: item.ID,
        name: item.name,
        folderId: item.id
      });
    }
  };

  const onPressNewTask = () => {
    navigation.push(RouteNames.CreatePhotoArea, {
      parentId: folderId
    });
  };

  const handleDelete = (item) => () => {
    Alert.alert(
      'Delete',
      'Do you want to delete this folder?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => {
            deleteDrawingArea(item.ID, token);
          },
        },
      ],
      {cancelable: false},
    );
  };

  const handleEdit = (item) => () => {
    navigation.push(RouteNames.CreateDrawingArea, {area: item});
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={styles.row}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <Text style={styles.textHeader}>{name}</Text>
      </View>
      <View style={styles.row} />
    </View>
  );

  const renderItem = ({item}) => {
    const {name} = item;
    return (
      <TouchableHighlight onPress={() => handleItemClick(item)}>
        <View
          style={{
            padding: 15,
            display: 'flex',
            flexDirection: 'row',
            shadowColor: '#555',
            backgroundColor: 'white',
            alignItems: 'center',
          }}>
          <Image
            style={styles.icon}
            source={require('../../assets/img/ico_folder.png')}
          />
          <Text
            style={{
              flex: 1,
              marginLeft: 15,
              fontSize: 16,
              color: '#3E3F42',
            }}>
            {name}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  const renderHiddenItem = ({item}) => {
    return (
      <View style={styles.rowBack}>
        <TouchableOpacity
          onPress={handleDelete(item)}
          style={{
            width: 100,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={styles.delete}>Delete</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleEdit(item)}
          style={{
            width: 100,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'green',
          }}>
          <Text style={styles.delete}>Edit</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
     <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        <SwipeListView
          style={{width: '100%'}}
          data={photoSubFolder}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          keyExtractor={(item) => item.id}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          leftOpenValue={0}
          rightOpenValue={-200}
          closeOnRowBeginSwipe
          disableRightSwipe
          scrollEnabled={true}
          showsVerticalScrollIndicator={true}
          recalculateHiddenLayout={true}
        />
      </View>
      <UniImageButton
        containerStyle={styles.floatButton}
        source={require('../../assets/img/ic_add.png')}
        onPress={onPressNewTask}
      />
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, commonData, photoSubFolder}) => {
  const PMProjectID = propOr(0, 'PMProjectID', project);
  const areaList = pathOr([],['photoSubFolder'], photoSubFolder);


  return {
    user,
    project,
    token: user.token,
    categories: pathOr([], ['currentProject', 'categories'], commonData),
    photoSubFolder: areaList,
  };
};

const mapDispatchToProps = {
  getPhotoSubFolder: actions.getPhotoSubFolder,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotoSubFolder);
