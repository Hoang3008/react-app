import {createAction} from 'redux-actions';
import {identity} from 'ramda';

import {actionType as createPhotoSubAreaActionType } from '../../CreatePhotoArea/actions/createPhotoSubArea.action'


import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';

const actionType = 'GET_PHOTO_SUB_FOLDER';

// https://apidev.rocez.com/drawing-area?projectId=
// type = 2 lay tat ca folder return type = 0 || 1 || 2
// types = 0 trong do chi chua drawing
// types = 1 trong do chi chua folder
// type = 2 trong do k co j ca
const createTaskAPI = ({projectId, parentId = -1}, token) => {

  const baseUrl = `/album?projectId=${projectId}&parentId=${parentId}`;

  const url = withKey(baseUrl);
  return API.get(url, {headers: {Authorization: `Bearer ${token}`}});
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {

  return {
    ...state,
    photoSubFolder: payload.data?.children,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
  [createPhotoSubAreaActionType]: {
    SUCCESS: (state, {payload, meta}) => {
      return {...state ,photoSubFolder: [...state.photoSubFolder, payload.data] };
  }},
};

export default {action, reducer};
