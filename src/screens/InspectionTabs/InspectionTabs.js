import React, {useEffect, useRef, useState} from 'react';
import {connect} from 'react-redux';
import {Animated, Image, TouchableOpacity} from 'react-native';
import {StatusBar, View} from 'react-native';

import styles from './InspectionTabs.styles';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import Tab from '../../components/ScheduleTab/ScheduleTab';
import InspectionList from '../InspectionList/InspectionList';
import ModalDropdown from 'react-native-modal-dropdown';
import InspectionFolder from '../InspectionFolder/InspectionFolder';

const GROUP_DROPDOWN_DATA = [
  {
    label: 'Category',
    key: 'FK_PMProjectCategoryID',
  },
  {
    label: 'Created User',
    key: 'AACreatedUser',
  },
  {
    label: 'Inspector',
    key: 'FK_PMInspectPeopleID',
  },
  {
    label: 'Main Contact',
    key: 'FK_PMProjectPeopleID',
  },
  {
    label: 'Signed By',
    key: 'FK_PMProjectApprovedPeopleID',
  },
  {
    label: 'Location',
    key: 'FK_PMProjectLocationID',
  },
];

const InspectionTabs = (props) => {
  const {navigation} = props;
  const pagerRef = useRef();
  const [tab, setTab] = useState(0);
  const [groupByKey, setGroupByKey] = useState('FK_PMProjectCategoryID');
  const [showSearchBox, setShowSearchBox] = useState(false);
  const groupDropDownRef = useRef(null);

  useEffect(() => {
    if (!pagerRef.current) {
      return;
    }
    pagerRef.current.setPage(tab);
  }, [tab]);

  const position = new Animated.Value(tab);

  const showGroupDropDown = () => {
    if (!groupDropDownRef.current) {
      return;
    }
    groupDropDownRef.current.show();
  };

  const handleSelectGroup = (index) => {
    const {key} = GROUP_DROPDOWN_DATA[index];

    if (groupByKey === key) {
      return;
    }

    setGroupByKey(key);
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={[styles.row, {flex: 1}]}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m8,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
      </View>

      <View
        style={{
          width: 160,
          height: 40,
          backgroundColor: Theme.colors.backgroundColor,
          flexDirection: 'row',
          marginLeft: 10,
          marginRight: 10,
          borderRadius: 6,
          padding: 3,
        }}>
        {['Inspection', 'Folder'].map((route, index) => {
          const focusAnim = position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [0, 1, 0],
          });
          return (
            <Tab
              key={route}
              style={{flex: 1}}
              focusAnim={focusAnim}
              title={route}
              onPress={() => setTab(index)}
            />
          );
        })}
      </View>

      <View style={[styles.row, {flex: 1, justifyContent: 'flex-end'}]}>
        {(
          <>
            <TouchableOpacity onPress={showGroupDropDown}>
              <Image
                style={styles.navIcon}
                source={require('../../assets/img/ic_font.png')}
              />
            </TouchableOpacity>
            <ModalDropdown
              ref={groupDropDownRef}
              defaultIndex={0}
              options={GROUP_DROPDOWN_DATA.map((item) => item.label)}
              dropdownTextStyle={styles.dropdownTextStyle}
              dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
              dropdownStyle={styles.dropdownStyle}
              textStyle={{display: 'none'}}
              defaultValue=""
              renderButtonText={() => ''}
              onSelect={handleSelectGroup}
            />
            <TouchableOpacity onPress={() => setShowSearchBox(!showSearchBox)}>
              <Image
                style={styles.navIcon}
                source={require('../../assets/img/ic_search.png')}
              />
            </TouchableOpacity>
          </>
        )}
      </View>
    </View>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundWhite,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        {tab === 0 && (
          <InspectionList
            navigation={navigation}
            shouldShowTitleBar={false}
            groupByKey={groupByKey}
            showSearchBox={showSearchBox}
          />
        )}
        {tab === 1 && <InspectionFolder navigation={navigation}  showSearchBox={showSearchBox}/>}
      </View>
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, photoList}) => {
  return {};
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionTabs);
