import typeToReducer from 'type-to-reducer';
import createDrawing from './actions/createDrawing.action';
import updateDrawing from './actions/updateDrawing.action';
import updateDrawingArea from './actions/updateDrawingArea.action';

const initialState = {
  offlineQueue: [],
  photos: {},
  comments: {},
};

export const actions = {
  createDrawing: createDrawing.action,
  updateDrawing: updateDrawing.action,
  updateDrawingArea: updateDrawingArea.action,
};

export default typeToReducer(
  {
    ...createDrawing.reducer,
    ...updateDrawing.reducer,
    ...updateDrawingArea.reducer,
  },
  initialState,
);
