import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {createFormData} from '../../../api/defect';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'CREATE_DRAWING';

const createTaskAPI = ({body, file}) => {
  const formData = createFormData(body, file);

  return AsyncStorage.getItem('token').then((token) => {
    return API.post(withKey('/PPDrawings/CreateObject'), formData, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        //'Content-Type': 'application/x-www-form-urlencoded',
       'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
