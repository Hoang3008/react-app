import {createAction} from 'redux-actions';
import {identity} from 'ramda';

import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';

const actionType = 'UPDATE_DRAWING_AREA';

const createTaskAPI = (areaId, drawingId, token) => {
  return API.patch(
    withKey(`/drawing-area/${areaId}/drawings`),
    {
      drawingIds: [drawingId],
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
