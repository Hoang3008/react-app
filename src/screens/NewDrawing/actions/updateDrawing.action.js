import {createAction} from 'redux-actions';
import {identity, propOr, pathOr} from 'ramda';
import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'UPDATE_DRAWING';

const createTaskAPI = (item) => {
   const drawingID = item.PPDrawingID;
   const projectId = item.PMProjectID;
   return AsyncStorage.getItem('token').then((token) => {
    return API.put(
      withKey(`/photo/${projectId}/${drawingID}`),
      {
        item
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
});
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
