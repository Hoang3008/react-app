import React, {useState} from 'react';
import {connect} from 'react-redux';
import {isEmpty, pathOr, propOr} from 'ramda';
import DocumentPicker from 'react-native-document-picker';
import moment from 'moment';

import {actions} from './NewDrawing.reducer';
import Theme from '../../Theme';
import {
  Image,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import {
  UniDropbox,
  UniImageButton,
  UniInput,
  UniStatusBarBackground,
} from '../../components';
import {scaledWidth, SCREEN_WIDTH} from '../../utils/DimensionUtils';
import UniButton from '../../components/UniButton';
import updateDrawingArea from './actions/updateDrawingArea.action';

const NewDrawing = (props) => {
  const {
    navigation,
    categories,
    project,
    user,
    createDrawing,
    updateDrawingArea,
  } = props;

  const [no, setNo] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [category, setCategory] = useState({});
  const [file, setFile] = useState({});
  const [loading, setLoading] = useState(false);

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => navigation.goBack()}
          />
          <View>
            <Text style={styles.textHeader}>New Drawing</Text>
          </View>
        </View>
      </View>
    );
  };

  const onPressAttachFile = () => {
    // const options = {
    //   cropping: false,
    //   writeTempFile: false,
    //   compressImageMaxWidth: 1024,
    //   compressImageMaxHeight: 1024,
    // };
    // ImagePicker.openPicker(options).then(image => {
    //   const path = image.path;
    //   setFile({
    //     uri: path || '',
    //     filename: getFileName(path),
    //     type: 'image/png',
    //   });
    // });
    // SUCCESS;

    DocumentPicker.pick({
      type: [DocumentPicker.types.pdf],
    }).then((file) => {
      setFile({
        uri: file.uri || '',
        filename: file.name,
        type: file.type,
      });
    });
  };

  const areaId = pathOr(undefined, ['route', 'params', 'areaId'], props);

  const handleCreateDrawing = () => {
    const {PMProjectID} = project;
    const {ADUserID, ADUserName, token} = user;

    const date = moment().format('YYYY-MM-DD');

    const body = {
      AACreatedUser: ADUserName,
      PPDrawingNo: no,
      PPDrawingName: name,
      PPDrawingDesc: description,
      FK_PMProjectID: PMProjectID,
      FK_ADUserID: ADUserID,
      PPDrawingEstStartDate: date,
      PPDrawingEstEndDate: date,
      PPDrawingActStartDate: date,
      PPDrawingActEndDate: date,
      FK_PPProjectDiscID: category.PPProjectDiscID,
      PPDrawingStatus: 'NEW',
    };

    setLoading(true);
    createDrawing({body, file})
      .then((res) => {
        if (!areaId) {
          return Promise.resolve(res);
        }

        const {PPDrawingID} = pathOr({}, ['value', 'data'], res);
        return updateDrawingArea(areaId, PPDrawingID, token);
      })
      .then((response) => {
        setLoading(false);
        props.route.params.onDone();
        navigation.goBack();
      })
      .catch((error) => {
        setLoading(false);
      });
  };

  const disableCreateButton =
    loading ||
    isEmpty(file) ||
    isEmpty(no) ||
    isEmpty(name) ||
    isEmpty(category);

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <ScrollView style={{paddingTop: 10}}>
        <UniInput
          placeholder="No."
          initValue={no}
          onChangeText={setNo}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder="Name"
          initValue={name}
          onChangeText={setName}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder="Description"
          initValue={description}
          onChangeText={setDescription}
          containerStyle={styles.editText}
        />
        <UniDropbox
          label={'Category'}
          selectedKey="PPProjectDiscID"
          selectedValue={propOr('', 'FK_PMProjectCategoryID', category)}
          options={categories}
          pathLeft={'PPProjectDiscNo'}
          pathValue={'PPProjectDiscName'}
          onSelect={(no, item) => setCategory(item)}
        />

        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            marginHorizontal: Theme.margin.m15,
            marginTop: Theme.margin.m15,
            marginBottom: Theme.margin.m8,
            borderWidth: 1,
            borderRadius: Theme.specifications.editTextNormalBorder,
            borderColor: Theme.colors.border,
            height: Theme.specifications.editTextNormalHeight,
            fontSize: Theme.fontSize.normal,
            paddingLeft: 12,
            paddingVertical: 9,
          }}>
          <Text style={styles.text}>Sketch File: </Text>
          <Text style={styles.sketchFile}>{propOr('', 'filename', file)}</Text>
          <TouchableOpacity
            onPress={onPressAttachFile}
            style={{
              paddingVertical: Theme.margin.m6,
              paddingHorizontal: Theme.margin.m10,
            }}>
            <Image
              style={{width: Theme.margin.m20, height: Theme.margin.m20}}
              source={require('../../assets/img/ic_attach.png')}
            />
          </TouchableOpacity>
        </View>

        <UniButton
          disabled={disableCreateButton}
          text={'Create Drawing'}
          onPress={handleCreateDrawing}
          containerStyle={styles.button}
        />
      </ScrollView>
    </View>
  );
};

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  text: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textNormal,
    fontWeight: '400',
  },
  sketchFile: {
    flex: 1,
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textPrimary,
    fontWeight: '400',
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },
  button: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 20,
  },
};

const mapStateToProps = ({auth: {user, project, master}, drawing}) => ({
  user,
  project,
  categories: propOr([], 'categories', master),
});

const mapDispatchToProps = {
  createDrawing: actions.createDrawing,
  updateDrawingArea: actions.updateDrawingArea,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewDrawing);
