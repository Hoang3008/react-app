import typeToReducer from 'type-to-reducer';
import createInspectionFolder from './actions/createInspectionFolder.action';
import updateInspectionFolder from './actions/updateInspectionFolder.action';

const initialState = {
  offlineQueue: [],
  photos: {},
  comments: {},
};

export const actions = {
  createInspectionFolder: createInspectionFolder.action,
  updateInspectionFolder: updateInspectionFolder.action,
};

export default typeToReducer(
  {
    ...createInspectionFolder.reducer,
    ...updateInspectionFolder.reducer,
  },
  initialState,
);
