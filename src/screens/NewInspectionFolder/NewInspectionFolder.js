import React, {useState} from 'react';
import {connect} from 'react-redux';
import {identity, isEmpty, pathOr, propOr} from 'ramda';

import {actions} from './NewInspectionFolder.reducer';
import Theme from '../../Theme';
import {StatusBar, Text, View, ScrollView} from 'react-native';
import {
  UniImageButton,
  UniInput,
  UniStatusBarBackground,
} from '../../components';
import {scaledWidth, SCREEN_WIDTH} from '../../utils/DimensionUtils';
import UniButton from '../../components/UniButton';

const NewInspectionFolder = (props) => {
  const {
    navigation,
    project,
    createInspectionFolder,
    token,
    updateInspectionFolder,
  } = props;

  const folder = pathOr({}, ['route', 'params', 'folder'], props);
  const folderId = pathOr(undefined, ['route', 'params', 'folderId'], props);
  const callback = pathOr(identity, ['route', 'params', 'callback'], props);

  const [loading, setLoading] = useState(false);
  const [no, setNo] = useState(folder.No || '');
  const [name, setName] = useState(folder.Name || '');
  const [description, setDescription] = useState(folder.Description || '');

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => navigation.goBack()}
          />
          <View>
            <Text style={styles.textHeader}>
              {!isEmpty(folder) ? 'Update Folder' : 'Create new Folder'}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  const handleCreateInspectionFolder = () => {
    const {PMProjectID} = project;
    const body = {
      no,
      name,
      description,
      projectId: PMProjectID,
      parentId: folderId,
    };

    if (!isEmpty(folder)) {
      updateInspectionFolder(folder.ID, body, token)
        .then((res) => {
          callback();
          navigation.goBack();
        })
        .catch((error) => {});
      return;
    }

    setLoading(true);
    createInspectionFolder(body).then(() => {
      setLoading(false);
      callback();
      navigation.goBack();
    });
  };

  const disableCreateButton = loading || isEmpty(no) || isEmpty(name);

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <ScrollView style={{paddingTop: 10}}>
        <UniInput
          placeholder="No."
          initValue={no}
          onChangeText={setNo}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder="Name"
          initValue={name}
          onChangeText={setName}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder="Description"
          initValue={description}
          onChangeText={setDescription}
          containerStyle={styles.editText}
        />

        <UniButton
          disabled={disableCreateButton}
          text={!isEmpty(folder) ? 'Update' : 'Create'}
          onPress={handleCreateInspectionFolder}
          containerStyle={styles.button}
        />
      </ScrollView>
    </View>
  );
};

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  text: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textNormal,
    fontWeight: '400',
  },
  sketchFile: {
    flex: 1,
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textPrimary,
    fontWeight: '400',
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },
  button: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 20,
  },
};

const mapStateToProps = ({auth: {user, project, master}, drawing}) => ({
  user,
  project,
  categories: propOr([], 'categories', master),
  token: user.token,
});

const mapDispatchToProps = {
  createInspectionFolder: actions.createInspectionFolder,
  updateInspectionFolder: actions.updateInspectionFolder,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewInspectionFolder);
