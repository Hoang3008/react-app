import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {createFormData} from '../../../api/defect';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

export const actionType = 'CREATE_INSPECTION_FOLDER';

// https://apidev.rocez.com/itp-group
//   Request Method: POST
// {"no":"test no","name":"test name","description":"Description","projectId":125}

const createTaskAPI = (data) => {
  return AsyncStorage.getItem('token').then((token) => {
    return API.post(withKey('/itp-group'), data, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
