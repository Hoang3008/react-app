import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';

export const actionType = 'UPDATE_INSPECTION_FOLDER';

// https://apidev.rocez.com/itp-group/14
//   Request Method: PUT

const createUserURL = (id) => withKey(`/itp-group/${id}`);

const createUserAPI = (id, body, token) =>
  API.put(createUserURL(id), body, {
    headers: {Authorization: `Bearer ${token}`},
  });

const action = createAction(actionType, createUserAPI);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
