import React, {useEffect, useRef, useState} from 'react';
import {connect} from 'react-redux';
import {propOr} from 'ramda';
import {StatusBar, Text, TouchableOpacity, View} from 'react-native';
import moment from 'moment';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {actions} from './TaskList.reducer';
import styles from './TaskList.styles';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import TaskMonthView from '../../components/TaskMonthView/TaskMonthView';
import {getTodayDateMoment} from '../../utils/DateUtils';
import ModalDropdown from 'react-native-modal-dropdown';
import TaskWeekView from '../../components/TaskWeekView/TaskWeekView';
import TaskDayView from '../../components/TaskDayView/TaskDayView';
import RouteNames from '../../RouteNames';

const GROUP_DROPDOWN_DATA = ['Month', 'Week', 'Day'];

const TaskList = (props) => {
  const {navigation, project, getTaskList, taskList} = props;
  const PMProjectID = propOr(0, 'PMProjectID', project);
  const [viewMode, setViewMode] = useState('0');
  const [viewDate, setViewDate] = useState(getTodayDateMoment());
  const monthViewRef = useRef();
  const viewModeRef = useRef();

  useEffect(() => {
    getTaskList(PMProjectID);
  }, [getTaskList, PMProjectID]);

  const handleAddNewTask = () => {
    navigation.push(RouteNames.TaskUpdate, {
      onGoBack: () => getTaskList(PMProjectID),
    });
  };

  const handleSelectGroup = (value) => {
    setViewMode(value);
  };

  const handleOpenTask = (task) => {
    const {PMTaskID} = task;
    navigation.navigate(RouteNames.TaskDetail, {PMTaskID});
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={[styles.row, {flex: 1}]}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m8,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <Text style={[styles.textHeader, {marginLeft: 0}]}>
          Task of {moment(viewDate, 'YYYY-MM-DD').format('MMMM, YYYY')}
        </Text>
      </View>
      <View style={styles.row}>
        <TouchableOpacity onPress={() => viewModeRef.current.show()}>
          <IconMaterialIcons size={24} color="#ffffff" name="view-week" />
        </TouchableOpacity>
        <ModalDropdown
          ref={viewModeRef}
          defaultIndex={0}
          options={GROUP_DROPDOWN_DATA.map((item) => item)}
          dropdownTextStyle={styles.dropdownTextStyle}
          dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
          dropdownStyle={styles.dropdownStyle}
          textStyle={{display: 'none'}}
          defaultValue="Month"
          renderButtonText={() => ''}
          onSelect={handleSelectGroup}
        />
        <TouchableOpacity
          onPress={() => {
            setViewDate(getTodayDateMoment());

            if (monthViewRef.current) {
              monthViewRef.current.viewToday();
            }
          }}
          style={{marginLeft: 10}}>
          <IconMaterialIcons size={24} color="#ffffff" name="today" />
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundWhite,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}

      {viewMode === '0' && (
        <TaskMonthView
          data={taskList}
          date={viewDate}
          onDateChanged={setViewDate}
          onItemClick={handleOpenTask}
        />
      )}

      {viewMode === '1' && (
        <TaskWeekView
          data={taskList}
          date={viewDate}
          onDateChanged={setViewDate}
          onItemClick={handleOpenTask}
        />
      )}

      {viewMode === '2' && (
        <TaskDayView
          ref={monthViewRef}
          data={taskList}
          onDateChanged={setViewDate}
          onItemClick={handleOpenTask}
        />
      )}

      <UniImageButton
        containerStyle={styles.floatButton}
        source={require('../../assets/img/ic_add.png')}
        onPress={handleAddNewTask}
      />
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, taskList, commonData}) => {
  // const PMProjectID = propOr(0, 'PMProjectID', project)
  const {ADUserID} = user;

  return {
    user,
    project,
    taskList: taskList.taskList || [],
  };
};

const mapDispatchToProps = {
  getTaskList: actions.getTaskList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TaskList);
