import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_TASK_DETAIL';

const getAPIURL = (id) => withKey(`/task/${id}`);
const getAPI = (id) =>
  AsyncStorage.getItem('token').then((token) => {
    return API.get(getAPIURL(id), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });

const action = createAction(actionType, getAPI);

const handleSuccess = (state, {payload}) => {
  const task = propOr({}, 'data', payload);
  const {PMTaskID} = task;
  const taskList = propOr([], 'taskList', state);

  return {
    ...state,
    taskList: taskList.map((item) => {
      if (item.PMTaskID === PMTaskID) {
        return task;
      }

      return item;
    }),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleSuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
