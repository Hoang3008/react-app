import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';
import API from '../../../api/API.service';

import {actionType as createDefectActionType} from '../../CommonProjectData/actions/createDefect.action';
import {actionType as updateDefectActionType} from '../../CommonProjectData/actions/updateDefect.action';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_TASK_LIST';

const createTaskAPI = (projectId) => {
  const url = withKey('/task/search');

  return AsyncStorage.getItem('token').then((token) => {
    return API.post(
      url,
      {projectId},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    taskList: payload.data,
  };
};

const handleCreateSuccess = (state, {payload}) => {
  const {data} = payload;
  const type = propOr('', 'PMTaskTypeCombo', data);

  if (type !== 'task') {
    return state;
  }

  return {
    ...state,
    taskList: [...state.taskList, data],
  };
};

const handleUpdateSuccess = (state, {payload}) => {
  const {data} = payload;
  const type = propOr('', 'PMTaskTypeCombo', data);

  if (type !== 'task') {
    return state;
  }

  return {
    ...state,
    taskList: state.taskList.map((item) => {
      if (item.PMTaskID === data.PMTaskID) {
        return {
          ...item,
          ...data,
        };
      }

      return item;
    }),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: (state, {payload}) => {
      return {
        ...state,
      };
    },
  },
  [createDefectActionType]: {
    SUCCESS: handleCreateSuccess,
  },
  [updateDefectActionType]: {
    SUCCESS: handleUpdateSuccess,
  },
};

export default {action, reducer};
