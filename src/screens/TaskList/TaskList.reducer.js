import typeToReducer from 'type-to-reducer';
import getTaskList from './actions/getTaskList.action';
import getTaskDetail from './actions/getTaskById.action';

const initialState = {
  taskList: [],
};

export const actions = {
  getTaskList: getTaskList.action,
  getTaskDetail: getTaskDetail.action,
};

export default typeToReducer(
  {
    ...getTaskList.reducer,
    ...getTaskDetail.reducer,
  },
  initialState,
);
