import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {View, Text, StatusBar} from 'react-native';
import {
  UniImageButton,
  UniInput,
  UniStatusBarBackground,
} from '../../components';
import Theme from '../../Theme';
import {scaledWidth, SCREEN_WIDTH} from '../../utils/DimensionUtils';
import UniButton from '../../components/UniButton';
import {actions} from './CreateProject.reducer';
import moment from 'moment';

const CreateProject = (props) => {
  const {navigation, createProject, user} = props;
  const [name, setName] = useState('');
  const [code, setCode] = useState('');

  useEffect(() => {
    return () => {
      props.route.params.onGoBack();
    };
  }, []);

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => navigation.goBack()}
          />
          <View>
            <Text style={styles.textHeader}>Create new project</Text>
          </View>
        </View>
      </View>
    );
  };

  const handleCreateProject = () => {
    createProject({
      PMProjectNo: code,
      PMProjectName: name,
      FK_ADUserID: user.ADUserID,
      AACreatedUser: user.ADUserName,
      AAUpdatedUser: user.ADUserName,
      PMProjectActiveCheck: true,
      PMProjectDate: moment().format('YYYY-MM-DD'),
      AAUpdatedDate: moment().format('YYYY-MM-DD'),
      AACreatedDate: moment().format('YYYY-MM-DD'),
      PMProjectDesc: '',
      FK_ARCustomerID: 0,
      PMProjectEstimatedStartDate: moment().format('YYYY-MM-DD'),
      PMProjectActualStartDate: moment().format('YYYY-MM-DD'),
      PMProjectEstimatedEndDate: moment().format('YYYY-MM-DD'),
      PMProjectActualEndDate: moment().format('YYYY-MM-DD'),
      PMProjectEstimatedTotalDays: 0,
      PMProjectActualTotalDays: 0,
      PMProjectEstimatedTotalHours: 0,
      PMProjectActualTotalHours: 0,
      PMProjectEstimatedExtendDays: 0,
      PMProjectActualExtendDays: 0,
      PMProjectEstimatedExtendHours: 0,
      PMProjectActualExtendHours: 0,
      FK_HREmployeeID: 0,
      PMProjectStatusTypeCombo: '',
    })
      .then(() => navigation.goBack())
      .catch((error) => {});
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundColor}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{marginTop: 22}}>
        <UniInput
          placeholder={'Project name'}
          initValue={name}
          onChangeText={setName}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder={'Project code (Optional)'}
          initValue={code}
          onChangeText={setCode}
          containerStyle={styles.editText}
        />

        <UniButton
          text={'Create Project'}
          onPress={handleCreateProject}
          containerStyle={styles.button}
        />
      </View>
    </View>
  );
};

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  editText: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
  button: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
};

const mapStateToProps = ({auth: {user}}) => ({user});

const mapDispatchToProps = {
  createProject: actions.createProject,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateProject);
