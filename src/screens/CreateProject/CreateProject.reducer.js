import typeToReducer from 'type-to-reducer';
import createProject from './actions/createProject.action';

const initialState = {};

export const actions = {
  createProject: createProject.action,
};

export default typeToReducer(
  {
    ...createProject.reducer,
  },
  initialState,
);
