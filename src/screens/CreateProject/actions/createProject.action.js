import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';

const actionType = 'CREATE_PROJECT';

// const createUserURL = () => withKey('/PMProjects/CreateObject');
const createUserURL = () => withKey('/project​/add');
const createUserAPI = (userInfo) => API.post(createUserURL(), userInfo);

const action = createAction(actionType, createUserAPI);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
