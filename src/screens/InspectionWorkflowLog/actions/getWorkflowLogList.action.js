import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_WORKFLOW_LOG_LIST';

// https://apid.rocez.com/itp/log/content-of-step?insStpInspID=5817
const createTaskAPI = (id) => {
  const url = withKey(`/itp/log/content-of-step?insStpInspID=${id}`);

  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    logList: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: (state) => ({...state, logList: []}),
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
