import React, {useEffect} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  StatusBar,
  Text,
  View,
} from 'react-native';
import moment from 'moment';
import {pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';

import styles from './InspectionWorkflowLog.styles';
import {actions} from './InspectionWorkflowLog.reducer';

import Theme from '../../Theme';
import {UniImageButton, UniStatusBarBackground} from '../../components';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';

const InspectionWorkflowLog = (props) => {
  const {workflowStep, navigation, logList = [], getWorkflowLogList} = props;

  const {PMProjectInsStpInspID} = workflowStep;

  useEffect(() => {
    getWorkflowLogList(PMProjectInsStpInspID);
  }, [getWorkflowLogList, PMProjectInsStpInspID]);

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={{...styles.row, flex: 1}}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <View>
          <View style={styles.row}>
            <Text style={styles.textHeader} numberOfLines={1}>
              Log #{PMProjectInsStpInspID}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );

  const renderCommentItem = ({item}) => {
    // AACreatedDate: "2020-09-16T10:14:05.410Z"
    // AACreatedUser: "nga.nguyen@unicons.vn"
    // PMProjectInsStpInspActivitieDesc: "Change status from Pass to Fail"
    // PMProjectInsStpInspActivitieType: 6
    const {
      AACreatedUser,
      AACreatedDate,
      PMProjectInsStpInspActivitieType,
      PMProjectInsStpInspActivitieDesc,
    } = item;

    const showDate = moment(AACreatedDate).format('YYYY/MM/DD hh:mm A');

    let text = '';
    switch (PMProjectInsStpInspActivitieType) {
      case 1:
        text = 'đã đính kèm tài liệu';
        break;
      case 2:
        text = 'đã xoá đính kèm tài liệu';
        break;
      case 3:
        text = 'đã đính kèm hình ảnh';
        break;
      case 4:
        text = 'đã xoá hình ảnh';
        break;
      case 5:
        text = `đã bình luận ${PMProjectInsStpInspActivitieDesc}`;
        break;
      case 6:
        text = 'đã xác nhận Failed';
        break;
      case 7:
        text = 'đã xác nhận Pass';
        break;
      case 8:
        text = 'đã xác nhận Verified';
        break;

      default:
        break;
    }

    return (
      <View style={{padding: 15}}>
        <Text
          style={{
            fontSize: Theme.fontSize.small_12,
            color: Theme.colors.textSecond,
          }}>
          {showDate}
        </Text>
        <Text
          style={{
            paddingVertical: Theme.margin.m2,
            fontSize: Theme.fontSize.normal,
            color: Theme.colors.textNormal,
          }}>
          {AACreatedUser === '1' ? 'System' : AACreatedUser}
        </Text>
        <Text
          style={{
            color: Theme.colors.textNormal,
            fontStyle: 'italic',
            fontSize: 12,
          }}>
          {text}
        </Text>
      </View>
    );
  };

  return (
    <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="height" enabled>
      <View
        style={{
          flex: 1,
          backgroundColor: Theme.colors.backgroundColor,
          display: 'flex',
          flexDirection: 'column',
        }}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {renderNavigation()}

        <FlatList
          style={{flex: 1}}
          scrollEnabled={true}
          data={logList}
          renderItem={renderCommentItem}
          keyExtractor={(item) => `${item.AACreatedDate}`}
          showsVerticalScrollIndicator={true}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
        />
      </View>
    </KeyboardAvoidingViewWrapper>
  );
};

const mapStateToProps = (
  {
    auth: {user, project},
    commonData,
    inspectionList,
    inspectionDetail,
    inspectionWorkflowLog,
  },
  props,
) => {
  const id = pathOr('', ['route', 'params', 'id'], props);
  const inspectionId = pathOr('', ['route', 'params', 'inspectionId'], props);

  const inspection = propOr({}, inspectionId, inspectionDetail);
  const acceptanceSteps = propOr([], 'acceptanceSteps', inspection);

  return {
    user,
    project,
    logList: propOr([], 'logList', inspectionWorkflowLog),
    workflowStep:
      acceptanceSteps.find(
        ({PMProjectInsStpInspID}) => id === PMProjectInsStpInspID,
      ) || {},
  };
};

const mapDispatchToProps = {
  getWorkflowLogList: actions.getWorkflowLogList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionWorkflowLog);
