import typeToReducer from 'type-to-reducer';

import getWorkflowLogList from './actions/getWorkflowLogList.action';

const initialState = {
  logList: [],
};

export const actions = {
  getWorkflowLogList: getWorkflowLogList.action,
};

export default typeToReducer(
  {
    ...getWorkflowLogList.reducer,
  },
  initialState,
);
