import React from 'react';
import {View, StatusBar, Text, Dimensions} from 'react-native';
import Pdf from 'react-native-pdf';
import {pathOr} from 'ramda';
import {UniImageButton, UniStatusBarBackground} from '../components';
import Theme from '../Theme';
import {SCREEN_WIDTH} from '../utils/DimensionUtils';

export default class PDFFullScreen extends React.Component {
  renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../assets/img/ic_back.png')}
            onPress={() =>
              this.props.navigation && this.props.navigation.goBack()
            }
          />
          <View>
            <Text style={styles.textHeader}>View PDF</Text>
          </View>
        </View>
      </View>
    );
  };

  render() {
    let uri = pathOr(undefined, ['route', 'params', 'url'], this.props);

    return (
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {this.renderNavigation()}
        <View style={{flex: 1}}>
          <Pdf
            source={{uri}}
            onLoadComplete={(numberOfPages, filePath) => {
              console.log(`number of pages: ${numberOfPages}`);
            }}
            onPageChanged={(page, numberOfPages) => {
              console.log(`current page: ${page}`);
            }}
            onError={(error) => {
              console.log(error);
            }}
            onPressLink={(uri) => {
              console.log(`Link presse: ${uri}`);
            }}
            style={styles.pdf}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textSectionHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNormal,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  priorityContainer: {
    width: Theme.specifications.icBig,
    height: Theme.specifications.icBig,
    marginHorizontal: Theme.margin.m5,
    borderRadius: Theme.specifications.icBig / 2,
    borderWidth: 1,
    borderColor: Theme.colors.border,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNormalSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
  },
  textSmallSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.small_12,
  },
  button: {
    marginLeft: Theme.margin.m15,
    marginVertical: Theme.margin.m16,
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },
};
