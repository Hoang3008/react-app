import typeToReducer from 'type-to-reducer';
import createPhotoArea from './actions/createPhotoArea.action';
import createPhotoSubArea from './actions/createPhotoSubArea.action';


const initialState = {};

export const actions = {
  createPhotoArea: createPhotoArea.action,
  createPhotoSubArea: createPhotoSubArea.action,
};

export default typeToReducer(
  {
    ...createPhotoArea.reducer,
    ...createPhotoSubArea.reducer,
  },
  initialState,
);
