import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';

export  const actionType = 'CREATE_NEW_LIST_PHOTO_AREA';

const createUserURL = () => withKey('/album');
const createUserAPI = (body, token) => {
  return API.post(createUserURL(), body, {
    headers: {Authorization: `Bearer ${token}`},
  })
};

const action = createAction(actionType, createUserAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  // khong vao vi khong khai vao trong reducer
  return {
    ...state,
    photoSubFolder: [...state.photoSubFolder, payload.data],
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
