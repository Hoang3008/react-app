import typeToReducer from 'type-to-reducer';

import searchDefectCode from './actions/searchDefectCode.action';

const initialState = {
  defectCodeList: [],
};

export const actions = {
  searchDefectCode: searchDefectCode.action,
};

export default typeToReducer(
  {
    ...searchDefectCode.reducer,
  },
  initialState,
);
