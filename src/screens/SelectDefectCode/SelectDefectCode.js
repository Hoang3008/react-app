import React, {useState, useEffect} from 'react';
import {View, Text, StatusBar, FlatList, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';

import Theme from '../../Theme';
import {
  UniStatusBarBackground,
  UniImageButton,
  UniInput,
} from '../../components';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
// import {actions} from './SelectDefectCode.reducer';
import {pathOr} from 'ramda';

const SelectDefectCode = (props) => {
  const {defectCodeList = [], navigation, route, PMProjectID} = props;
  const [defectCodeListFromState, setDefectCodeListFromState] = useState([]);
  const [filterValue, setFilterValue] = useState('');

  const categoryId = pathOr('', ['params', 'categoryId'], route);

  useEffect(() => {
    const showDefectCodeList = defectCodeList.filter(
      ({PMTaskGroupNo, PMTaskGroupName, FK_PMCategoryID, FK_PMProjectID}) => {
        if (PMProjectID !== FK_PMProjectID) {
          return false;
        }

        if (categoryId && FK_PMCategoryID !== categoryId) {
          return false;
        }

        if (!filterValue) {
          return true;
        }

        return (
          (PMTaskGroupNo &&
            PMTaskGroupNo.toLowerCase().includes(filterValue.toLowerCase())) ||
          (PMTaskGroupName &&
            PMTaskGroupName.toLowerCase().includes(filterValue.toLowerCase()))
        );
      },
    );
    setDefectCodeListFromState(showDefectCodeList);
  }, [defectCodeList, filterValue, PMProjectID, categoryId]);

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={navigation.goBack}
          />
          <View>
            <View style={styles.row}>
              <Text style={styles.textHeader}>Select Defect Code</Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  const renderRow = ({item}) => {
    const {
      route: {
        params: {onSelectDefectCode},
      },
    } = props;
    const {PMTaskGroupNo, PMTaskGroupName, PMTaskGroupDesc} = item;

    // PMTaskGroupNo: "HT-XA-10"
    // PMTaskGroupName: "Không tưới nước bão dưỡng tường sẽ nứt  "
    // PMTaskGroupDate: "2019-12-10T00:00:00.000Z"
    // PMTaskGroupDesc: "Phải tưới nước gạch và tường trước và sau khi xây theo quy trình   "
    // AAStatus: "Alive"

    return (
      <TouchableOpacity
        onPress={() => {
          onSelectDefectCode(item);
          navigation.goBack();
        }}>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'flex-start',
            paddingVertical: Theme.margin.m4,
            borderColor: '#aaa',
            borderBottomWidth: 1,
            padding: 10,
          }}>
          <Text style={styles.textNo}>{PMTaskGroupNo}</Text>
          <Text style={styles.textValue}>{PMTaskGroupName}</Text>
          <Text style={styles.textNo}>{PMTaskGroupDesc}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}

      <UniInput
        initValue={filterValue}
        onChangeText={setFilterValue}
        containerStyle={styles.editText}
        placeholder="Type to search"
      />

      <FlatList
        style={{flex: 1}}
        data={defectCodeListFromState}
        renderItem={renderRow}
        keyExtractor={(item) => `${item.PMTaskGroupID}`}
      />
    </View>
  );
};

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonContainerActive: {
    padding: Theme.margin.m8,
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.accent,
    backgroundColor: Theme.colors.accent,
  },
  textButtonActive: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textWhite,
  },
  textValue: {
    textAlign: 'left',
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.normal,
    marginRight: Theme.margin.m10,
    marginLeft: Theme.margin.m5,
    paddingVertical: Theme.margin.m4,
  },
  textNo: {
    textAlign: 'left',
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.small_12,
    marginRight: Theme.margin.m10,
    marginLeft: Theme.margin.m5,
    paddingVertical: Theme.margin.m4,
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },
};

const mapStateToProps = ({commonData, auth: {project}}) => ({
  defectCodeList: commonData.defectCodeList,
  PMProjectID: project.PMProjectID,
});

export default connect(
  mapStateToProps,
  null,
  // {
  //   searchDefectCode: actions.searchDefectCode,
  // },
)(SelectDefectCode);
