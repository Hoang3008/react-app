import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';

const actionType = 'SEARCH_DEFECT_CODE';

// http://api.gmc.solutions/PMTaskItems/CreateObjectByList

const apiURL = () => withKey('/defect-code/search');
const postAPI = (data) => API.post(apiURL(), data);

const action = createAction(actionType, postAPI);

const handleLoadAPIDone = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleLoadAPIDone,
    FAILURE: identity,
  },
};

export default {action, reducer};
