import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  SectionList,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {connect} from 'react-redux';

import {UniStatusBarBackground} from '../../components';
import Theme from '../../Theme';
import RouteNames from '../../RouteNames';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import UniImageButton from '../../components/UniImageButton';
import {
  groupDefectByPriority,
  PRIORITY,
  groupDefectByCategory,
  groupDefectByLocations,
  groupDefectByAssignee,
  groupDefectByDate,
  groupDefectByDrawing,
} from './DefectHelper';
import {pathOr, propOr} from 'ramda';

import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import {EMPTY} from '../../global/constants';
import GroupSearchHeader from '../../components/GroupSearchHeader/GroupSearchHeader';
import FloaterAddButton from '../../components/FloaterAddButton/FloaterAddButton';
import SectionHeader from '../../components/SectionHeader/SectionHeader';
import TaskListItem from '../../components/TaskListItem/TaskListItem';

const GROUP_DROPDOWN_DATA = [
  {
    label: 'Status',
    key: 'priority',
  },
  {
    label: 'Category',
    key: 'FK_PMProjectCategoryID',
  },
  {
    label: 'Assignee',
    key: 'FK_ADAssignUserID',
  },
  {
    label: 'Drawing',
    key: 'FK_PPDrawingID',
  },
  {
    label: 'Location',
    key: 'FK_PMProjectLocationID',
  },
  {
    label: 'Start date',
    key: 'PMTaskStartDate',
  },
  {
    label: 'End date',
    key: 'PMTaskEndDate',
  },
  {
    label: 'Last modified',
    key: 'AAUpdatedDate',
  },
];

class DefectMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      showSearchBox: false,
      groupKey: 'priority',
      groupedDefect: [],
      showDefects: [],
    };
  }

  componentDidMount() {
    const {allDefects} = this.props;
    const {groupKey} = this.state;

    this.getData();
    this.handleGroupData(groupKey, allDefects);
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {allDefects} = this.props;
    const {groupKey} = this.state;

    if (prevProps.allDefects !== allDefects) {
      this.handleGroupData(groupKey, allDefects);
    }
  }

  getData = () => {
    const {
      getProjectCategories,
      getProjectLocations,
      getPeopleInProject,
      getDefectList,
      project: {PMProjectID},
      user,
    } = this.props;
    return Promise.all([
      getProjectCategories(PMProjectID),
      getProjectLocations(PMProjectID),
      getPeopleInProject({PMProjectID, UserName: user.ADUserName}),
      getDefectList({userId: user.ADUserID, projectId: PMProjectID}, user),
    ]);
  };

  handleGroupData = (groupKey) => {
    const {
      categories,
      locations,
      allDefects,
      peopleInProject,
      drawingList,
    } = this.props;
    let groupedDefect = [];

    let cloneAllDefect = [];
    // combine multiple arrays to array
    if (!Array.isArray(allDefects)) {
      let arrAllDefects = [];
      Object.values(allDefects).forEach((item) => {
        arrAllDefects = [...arrAllDefects, ...item];
      });
      cloneAllDefect = arrAllDefects;
    }

    switch (groupKey) {
      case 'priority':
        groupedDefect = groupDefectByPriority(allDefects);
        break;
      case 'FK_PMProjectCategoryID':
        groupedDefect = groupDefectByCategory(cloneAllDefect, categories);
        break;
      case 'FK_ADAssignUserID':
        groupedDefect = groupDefectByAssignee(cloneAllDefect, peopleInProject);
        break;
      case 'FK_PMProjectLocationID':
        groupedDefect = groupDefectByLocations(cloneAllDefect, locations);
        break;
      case 'FK_PPDrawingID':
        groupedDefect = groupDefectByDrawing(cloneAllDefect, drawingList);
        break;
      // case 'PMTaskName':
      //   groupedDefect = groupDefectByDate(allDefects, 'PMTaskStartDate', 'start date');
      //   break;
      case 'PMTaskStartDate':
        groupedDefect = groupDefectByDate(
          cloneAllDefect,
          'PMTaskStartDate',
          'start date',
        );
        break;
      case 'PMTaskEndDate':
        groupedDefect = groupDefectByDate(
          cloneAllDefect,
          'PMTaskEndDate',
          'end date',
        );
        break;
      case 'AAUpdatedDate':
        groupedDefect = groupDefectByDate(
          cloneAllDefect,
          'AAUpdatedDate',
          'updated date',
        );
        break;
    }

    this.setState({
      groupedDefect,
      showDefects: groupedDefect,
      groupKey,
      search: '',
      showSearchBox: false,
    });
  };

  handleSelectGroup = (index) => {
    const {key} = GROUP_DROPDOWN_DATA[index];
    const {groupKey} = this.state;

    if (groupKey === key) {
      return;
    }

    this.handleGroupData(key);
  };

  onPressDefect = (defect) => {
    this.props.navigation &&
      this.props.navigation.push(RouteNames.DefectDetail, {
        defect,
        onDelete: () => this.getData(),
        onUpdate: () => this.getData(),
      });
  };

  onPressNewTask = () => {
    this.props.navigation.push(RouteNames.DefectUpdate, {
      onGoBack: () => this.getData(),
    });
  };

  updateSearch = (search) => {
    const {peopleInProject} = this.props;
    const {groupedDefect} = this.state;

    const lowerCaseSearch = search ? search.toLowerCase() : '';
    const newShowDefects = groupedDefect.map((item) => {
      const newData = item.data.filter(({PMTaskName, FK_ADAssignUserID}) => {
        if (PMTaskName && PMTaskName.toLowerCase().includes(lowerCaseSearch)) {
          return true;
        }

        const assignUser = peopleInProject.find(
          ({ADUserID}) => ADUserID === FK_ADAssignUserID,
        );

        return (
          assignUser &&
          assignUser.fullName &&
          assignUser.fullName.toLowerCase().includes(lowerCaseSearch)
        );
      });

      return {
        ...item,
        data: newData,
      };
    });

    this.setState({search, showDefects: newShowDefects});
  };

  toggleShowSearch = () => {
    const {showSearchBox} = this.state;
    this.setState({
      showSearchBox: !showSearchBox,
      search: '',
    });
  };

  renderNavigation = () => {
    return (
      <GroupSearchHeader
        title="All Defects"
        onBack={() => this.props.navigation && this.props.navigation.goBack()}
        groupDropdownData={GROUP_DROPDOWN_DATA.map((item) => item.label)}
        onGroupSelect={this.handleSelectGroup}
        onToggleSearch={this.toggleShowSearch}
      />
    );
  };

  renderSectionHeader = (title, color = 'transparent', data = []) => {
    return (
      <SectionHeader
        color={color}
        title={title.toUpperCase() + ' (' + data.length + ')'}
      />
    );
  };

  renderItem = (item) => {
    const {categories} = this.props;
    return (
      <TaskListItem
        item={item}
        categories={categories}
        onPress={this.onPressDefect}
      />
    );
  };

  render() {
    const {showSearchBox, search, showDefects} = this.state;

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: Theme.colors.backgroundColor,
          display: 'flex',
          flexDirection: 'column',
        }}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {this.renderNavigation()}
        <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
          {showSearchBox && (
            <SearchBar
              placeholder="Type Here..."
              value={search}
              onChangeText={this.updateSearch}
              inputStyle={{
                color: Theme.colors.textNormal,
              }}
              containerStyle={{
                borderBottomColor: '#0000',
                backgroundColor: '#ffffff',
              }}
              inputContainerStyle={{
                backgroundColor: '#ddd',
              }}
            />
          )}
          <SectionList
            sections={showDefects}
            extraData={this.state}
            keyExtractor={(item, index) => index + '-' + item.title}
            renderItem={({item}) => this.renderItem(item)}
            renderSectionHeader={({section: {title, data, color}}) =>
              this.renderSectionHeader(title, color, data)
            }
            ItemSeparatorComponent={() => <View style={styles.divider} />}
          />
        </View>
        <FloaterAddButton onPress={this.onPressNewTask} />
      </View>
    );
  }
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  dropdownStyle: {
    width: 200,
    height: 321,
  },
  dropdownTextStyle: {
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.header_16,
    marginLeft: Theme.margin.m12,
    marginRight: Theme.margin.m12,
  },
  dropdownTextHighlightStyle: {
    color: Theme.colors.textPrimary,
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m52,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
};

const mapStateToProps = ({
  auth: {user, project, master},
  commonData,
  drawing,
}) => {
  // const {PMProjectID} = project; TODO
  const allDefects = propOr(EMPTY.ARRAY, 'defectList', commonData);

  return {
    user,
    master,
    project,
    categories: pathOr(
      EMPTY.ARRAY,
      ['currentProject', 'categories'],
      commonData,
    ),
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    allDefects,
    drawingList: propOr([], 'drawingList', drawing),
    peopleInProject: pathOr(
      EMPTY.ARRAY,
      ['currentProject', 'users'],
      commonData,
    ),
  };
};

const mapDispatchToProps = {
  getProjectCategories: actions.getProjectCategories,
  getProjectLocations: actions.getProjectLocations,
  getPeopleInProject: actions.getPeopleInProject,
  getDefectList: actions.getDefectList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DefectMain);
