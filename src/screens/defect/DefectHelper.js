import {isEmpty, reject} from 'ramda';
import moment from 'moment';

import Theme from '../../Theme';

export const PRIORITY = {
  OVERDUE: 'Overdue',
  PRIORITY1: 'Priority1',
  PRIORITY2: 'Priority2',
  PRIORITY3: 'Priority3',
  COMPLETE: 'Complete',
  VERIFY: 'Verify',
};

function isOverdue(item) {
  if (item.PMTaskEndDate) {
    let endDate = new Date(item.PMTaskEndDate);
    endDate.setHours(0, 0, 0, 0);
    let now = new Date();
    now.setHours(0, 0, 0, 0);
    if (now > endDate) {
      return true;
    }
  }
  return false;
}

export const groupDefectByCategory = (allDefects, categories) => {
  const result = categories.map(
    ({PMProjectCategoryID, PMProjectCategoryName}) => ({
      PMProjectCategoryID,
      title: PMProjectCategoryName,
      color: Theme.colors.backgroundHeader,
      data: [],
    }),
  );

  result.push({
    title: 'NO CATEGORY',
    color: Theme.colors.backgroundHeader,
    data: [],
  });

  allDefects.forEach((defect) => {
    const {FK_PMProjectCategoryID} = defect;
    const index = result.findIndex(
      ({PMProjectCategoryID}) => PMProjectCategoryID === FK_PMProjectCategoryID,
    );

    if (index < 0) {
      result[result.length - 1].data.push(defect);
    } else {
      result[index].data.push(defect);
    }
  });

  return reject(({data}) => isEmpty(data))(result);
};

export const groupDefectByLocations = (allDefects, locations) => {
  const result = locations.map(
    ({PMProjectLocationID, PMProjectLocationName}) => ({
      PMProjectLocationID,
      title: PMProjectLocationName,
      color: Theme.colors.backgroundHeader,
      data: [],
    }),
  );

  result.push({
    title: 'NO LOCATION',
    color: Theme.colors.backgroundHeader,
    data: [],
  });

  allDefects.forEach((defect) => {
    const {FK_PMProjectCategoryID} = defect;
    const index = result.findIndex(
      ({PMProjectLocationID}) => PMProjectLocationID === FK_PMProjectCategoryID,
    );

    if (index < 0) {
      result[result.length - 1].data.push(defect);
    } else {
      result[index].data.push(defect);
    }
  });

  return reject(({data}) => isEmpty(data))(result);
};

export const groupDefectByDrawing = (allDefects, drawingList) => {
  const result = drawingList.map(({PPDrawingID, PPDrawingName}) => ({
    PPDrawingID,
    title: PPDrawingName,
    color: Theme.colors.backgroundHeader,
    data: [],
  }));

  result.push({
    title: 'NO DRAWING',
    color: Theme.colors.backgroundHeader,
    data: [],
  });

  allDefects.forEach((defect) => {
    const {FK_PPDrawingID} = defect;
    const index = result.findIndex(
      ({PPDrawingID}) => PPDrawingID === FK_PPDrawingID,
    );

    if (index < 0) {
      result[result.length - 1].data.push(defect);
    } else {
      result[index].data.push(defect);
    }
  });

  return reject(({data}) => isEmpty(data))(result);
};

export const groupDefectByAssignee = (
  allDefects,
  peopleInProject,
  key = 'ADUserID',
) => {
  const result = peopleInProject.map((item) => {
    const {fullName} = item;

    return {
      [key]: item[key],
      title: fullName,
      color: Theme.colors.backgroundHeader,
      data: [],
    };
  });

  result.push({
    title: 'NO ASSIGNEE',
    color: Theme.colors.backgroundHeader,
    data: [],
  });

  allDefects.forEach((defect) => {
    const {FK_ADAssignUserID} = defect;
    const index = result.findIndex((item) => item[key] === FK_ADAssignUserID);

    if (index < 0) {
      result[result.length - 1].data.push(defect);
    } else {
      result[index].data.push(defect);
    }
  });

  return reject(({data}) => isEmpty(data))(result);
};

export const groupDefectByDate = (allDefects, groupKey, label) => {
  const result = [];
  const noGroup = [];

  allDefects
    .sort((b, a) => {
      if (a[groupKey] < b[groupKey]) {
        return -1;
      }
      if (a[groupKey] > b[groupKey]) {
        return 1;
      }
      return 0;
    })
    .forEach((defect) => {
      const date = defect[groupKey];

      if (!date) {
        noGroup.push(defect);
        return;
      }

      const dateLabel = moment(date).format('MMM DD, YYYY');
      const index = result.findIndex(({title}) => title === dateLabel);

      if (index < 0) {
        result.push({
          title: dateLabel,
          color: Theme.colors.backgroundHeader,
          data: [defect],
        });
      } else {
        result[index].data.push(defect);
      }
    });

  if (noGroup.length > 0) {
    result.push({
      title: `NO ${label}`,
      color: Theme.colors.backgroundHeader,
      data: noGroup,
    });
  }

  return result;
};

export function groupDefectByPriority(allDefects) {
  let result = [];
  let priority0Overdue = {title: 'Overdue', color: '#FB5054', data: []};
  let priority1 = {title: 'Priority 1', color: '#2A5C93', data: []};
  let priority2 = {title: 'Priority 2', color: '#F5AB41', data: []};
  let priority3 = {title: 'Priority 3', color: '#c2cc06', data: []};
  let priority4Complete = {title: 'Complete', color: '#65D985', data: []};
  let priority5Verify = {title: 'Verify', color: '#5EBD68', data: []};

  // combine multiple arrays to array
  if (!Array.isArray(allDefects)) {
    let arrAllDefects = [];
    Object.values(allDefects).forEach((item => {
      arrAllDefects = [...arrAllDefects,...item]
    }));
    allDefects = arrAllDefects;
  }

  allDefects.map((item, index) => {
    if (!item || !item.PMTaskPriorityCombo) {
      return;
    }

    switch (item.PMTaskPriorityCombo.toLowerCase()) {
      case PRIORITY.OVERDUE.toLowerCase():
        item.priority = PRIORITY.OVERDUE;
        priority0Overdue.data.push(item);
        break;
      case PRIORITY.PRIORITY1.toLowerCase():
        if (!isOverdue(item)) {
          item.priority = PRIORITY.PRIORITY1;
          priority1.data.push(item);
        } else {
          item.priority = PRIORITY.OVERDUE;
          priority0Overdue.data.push(item);
        }
        break;
      case PRIORITY.PRIORITY2.toLowerCase():
        if (!isOverdue(item)) {
          item.priority = PRIORITY.PRIORITY2;
          priority2.data.push(item);
        } else {
          item.priority = PRIORITY.OVERDUE;
          priority0Overdue.data.push(item);
        }
        break;
      case PRIORITY.PRIORITY3.toLowerCase():
        if (!isOverdue(item)) {
          item.priority = PRIORITY.PRIORITY3;
          priority3.data.push(item);
        } else {
          item.priority = PRIORITY.OVERDUE;
          priority0Overdue.data.push(item);
        }
        break;
      case PRIORITY.COMPLETE.toLowerCase():
        item.priority = PRIORITY.COMPLETE;
        priority4Complete.data.push(item);
        break;
      case PRIORITY.VERIFY.toLowerCase():
        item.priority = PRIORITY.VERIFY;
        priority5Verify.data.push(item);
        break;
      default:
        break;
    }
  });

  result.push(
    priority0Overdue,
    priority1,
    priority2,
    priority3,
    priority4Complete,
    priority5Verify,
  );
  return result;
}

export function getFileName(urlStr) {
  if (urlStr) {
    let fileName = urlStr.substring(urlStr.lastIndexOf('/') + 1, urlStr.length);
    // return fileName.substring(0, fileName.lastIndexOf('.'));
    return fileName;
  }
}

export function isCommentSystemType(item) {
  const commentType = (item.PMTaskCommentType || '').toLowerCase();

  if (commentType === 'user_input') {
    return false;
  }

  if (commentType === 'system' || commentType === 'user_log') {
    return true;
  }

  return (
    (item.AAStatus && item.AAStatus !== 'Alive') ||
    item.PMTaskCommentUserName === '1'
  );
}

export function filterStatusResponseList(list, showAll = false) {
  if (showAll) {
    return list;
  }

  return list.filter((item) => !isCommentSystemType(item));
}
