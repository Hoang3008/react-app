import typeToReducer from 'type-to-reducer';
import getPhotoList from './actions/getPhotoList.action';
import deletePhoto from './actions/deletePhoto.action';

const initialState = {
  photoList: [],
};

export const actions = {
  getPhotoList: getPhotoList.action,
  deletePhoto: deletePhoto.action,
};

export default typeToReducer(
  {
    ...getPhotoList.reducer,
    ...deletePhoto.reducer,
  },
  initialState,
);
