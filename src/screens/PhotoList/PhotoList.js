import React, {useEffect, useRef, useState} from 'react';
import {StatusBar, Text, TouchableOpacity, View, ActivityIndicator} from 'react-native';
import { Image } from 'react-native-elements';
import {connect} from 'react-redux';
import {Animated} from 'react-native';
import moment from 'moment';
import {groupBy, pathOr, propOr} from 'ramda';
import {SectionGrid} from 'react-native-super-grid';
import IconOcticons from 'react-native-vector-icons/Octicons';
import ImagePicker from 'react-native-image-crop-picker';
import ModalDropdown from 'react-native-modal-dropdown';
import {SearchBar} from 'react-native-elements';

import {actions} from './PhotoList.reducer';
import styles from './PhotoList.styles';

import Theme from '../../Theme';
import {UniBottomSheet, UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import Tab from '../../components/ScheduleTab/ScheduleTab';
import {getPhotoUrl} from '../../api/urls';
import RouteNames from '../../RouteNames';
import {EMPTY} from '../../global/constants';

const tabList = ['Day', 'Week', 'Month', 'Category', 'Location', 'Creator'];

const getPhotoDateLabel = (date, level) => {
  const result = moment(date);
  if (level === 0) {
    return result.format('MMMM DD, YYYY');
  }

  if (level === 1) {
    const startDate = moment(date).startOf('isoWeek');
    const endDate = moment(date).endOf('isoWeek');
    return `${startDate.format('MMMM DD, YYYY')} - ${endDate.format(
      'MMMM DD, YYYY',
    )}`;
  }

  return result.format('MMMM YYYY');
};

const PhotoList = (props) => {
  const {
    navigation,
    project,
    data,
    getPhotoList,
    deletePhoto,
    categories,
    locations,
    peopleInProject,
  } = props;

  const dropDownRef = useRef();

  const [tabIndex, setTabIndex] = useState(0);
  const [searchText, setSearchText] = useState('');
  const [showSearchBox, setShowSearchBox] = useState(true);
  const [filteredData, setFilteredData] = useState([]);
  const [pageIndex, setPageIndex] = useState(1);
  const [keyword, setKeyWord] = useState('');
  const [verticalAxis, setVerticalAxis] = useState(-1);
  const bottomSheetRef = useRef();

  const PMProjectID = propOr(0, 'PMProjectID', project);
  const folderId = pathOr(undefined, ['route', 'params', 'folderId'], props);
  const showHeader = pathOr(true, ['showHeader'], props);

  useEffect(() => {
    const tabIndexNumber = parseInt(tabIndex, 10);
    const result = groupBy((item) => {
      const {
        AACreatedDate,
        FK_PMProjectCategoryID,
        FK_PMProjectLocationID,
        AACreatedUser,
      } = item;

      if (tabIndexNumber < 3) {
        return getPhotoDateLabel(AACreatedDate, tabIndexNumber);
      }

      const groupKey = tabList[tabIndexNumber];

      switch (groupKey) {
        case 'Category':
          const category = categories.find(
            ({PPProjectDiscID}) => PPProjectDiscID === FK_PMProjectCategoryID,
          );

          if (!category) {
            return 'NO CATEGORY';
          }

          return `[${category['PPProjectDiscNo']}] - ${
            category['PPProjectDiscName']
          }`;
        case 'Location':
          const location = locations.find(
            ({PMProjectLocationID}) =>
              PMProjectLocationID === FK_PMProjectLocationID,
          );

          if (!location) {
            return 'NO LOCATION';
          }

          return `[${location['PMProjectLocationNo']}] - ${
            location['PMProjectLocationName']
          }`;
        case 'Creator':
          const people = peopleInProject.find(
            ({PMProjectPeopleEmailAddress}) =>
              PMProjectPeopleEmailAddress === AACreatedUser,
          );

          if (!people) {
            return 'NO USER';
          }

          return `[${people.shortName}] - ${people.fullName}`;
        default:
          return '';
      }
    }, data);

    setFilteredData(
      Object.keys(result).map((key) => ({
        title: key,
        data: result[key],
      })),
    );
  }, [data, tabIndex]);

  useEffect(() => {
    const {user, data} = props;
    getPhotoList(PMProjectID, pageIndex, 20, keyword, user?.ADUserName, data, folderId);
  }, [getPhotoList, PMProjectID, pageIndex, keyword]);

  const handleLoadMore = () => {
    let canLoadMore = false;
    if (data.length > 0) {
      const firstItem = data[0];
      const TotalCount = firstItem.TotalCount;
      canLoadMore = pageIndex * 20 < TotalCount;
    }

    if (canLoadMore) {
      setPageIndex(pageIndex + 1);
    }
  };

  const onScroll = (event) => {
    if (event.nativeEvent.contentOffset.y > verticalAxis) {
      setVerticalAxis(event.nativeEvent.contentOffset.y);
      handleLoadMore();
   }
  }

  const handleSearch = () => {
    if (searchText !== keyword) {
      setPageIndex(1);
      setKeyWord(searchText);
    }
  };

  const handleViewPhoto = (item) => () => {
    const { data } = props;
    const url = getPhotoUrl(item.PMPhotoServerPath, true);
    const reduced = [];
    data.forEach(o => {
      o?.PMPhotoID !== item?.PMPhotoID && reduced.push( getPhotoUrl(o.PMPhotoServerPath, true) );
    } );

    let dataImages =  data.filter(o => (o?.PMPhotoID !== item?.PMPhotoID));

    dataImages = [item,...dataImages]
    navigation.push(RouteNames.ImageFullScreen, {
      title: 'Photo',
      onEdit: handleEditPhoto(item),
      onDelete: handleDeletePhoto(item),
      onShowInfo: handleShowInfo(item),
      onShowComment: handleShowComment(item),
      info: {
        information: '',
        uploadedBy: '',
        category: '',
        location: '',
      },
      item,
      url,
      reduced,
      dataImages
    });
  };

  const handleEditPhoto = (item) => () => {};

  const handleDeletePhoto = (item) => () => deletePhoto(item);

  const handleShowInfo = (item) => () => {
    navigation.push(RouteNames.EditPhoto, {item});
  };

  const handleShowComment = (item) => () => {
    navigation.push(RouteNames.PhotoComments, {id: item.PMPhotoID});
  };

  const handleUploadPhoto = (images, folderId) => {
    navigation.push(RouteNames.AddPhoto, {images, folderId});
  };

  const handleShowSelectPhoto = () => {
    bottomSheetRef.current.show(handleSelectedPhoto, false);
  };

  const handleSelectedPhoto = (type) => {
    let options = {
      // cropping: false,
      // writeTempFile: false,
      compressImageMaxWidth: 1024,
      compressImageMaxHeight: 1024,
      multiple: true,
      maxFiles: 30,
    };
    switch (type) {
      case 'camera':
        navigation.push(RouteNames.CaptureImage, {
          shouldGoToDrawing: false,
          onSave: (path) => handleUploadPhoto([{path}], folderId),
        });
        break;
      case 'photos-device':
        ImagePicker.openPicker(options)
          .then((images, ...params) => {
            handleUploadPhoto(images, folderId);
          })
          .catch(() => {});
        break;
      default:
        break;
    }
  };

  const position = new Animated.Value(0);

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={[styles.row, {flex: 1}]}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m8,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <Text style={[styles.textHeader, {marginLeft: 0}]}>Photos</Text>
      </View>

      <View
        style={{
          display: 'none',
          width: 150,
          height: 40,
          backgroundColor: Theme.colors.backgroundColor,
          flexDirection: 'row',
          marginLeft: 10,
          marginRight: 10,
          borderRadius: 6,
          padding: 3,
        }}>
        {['Timeline', 'Album'].map((route, index) => {
          const focusAnim = position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [0, 1, 0],
          });
          return (
            <Tab
              key={route}
              style={{flex: 1}}
              focusAnim={focusAnim}
              title={route}
              onPress={() => {}}
            />
          );
        })}
      </View>

      <View style={[styles.row, {flex: 1, justifyContent: 'flex-end'}]} />
    </View>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundWhite,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      {!!showHeader && <UniStatusBarBackground />}
      {!!showHeader && renderNavigation()}
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          {showSearchBox && (
            <SearchBar
              returnKeyLabel="search"
              onSubmitEditing={handleSearch}
              placeholder="Type Here..."
              value={searchText}
              onChangeText={setSearchText}
              inputStyle={{
                color: Theme.colors.textNormal,
              }}
              containerStyle={{
                borderTopColor: 'transparent',
                borderBottomColor: 'transparent',
                backgroundColor: '#ffffff',
                flex: 1,
              }}
              inputContainerStyle={{
                backgroundColor: '#ddd',
              }}
            />
          )}
          <TouchableOpacity onPress={() => dropDownRef.current.show()}>
            <IconOcticons
              style={{marginLeft: 6, marginRight: 10}}
              size={26}
              color={'#f66'}
              name="settings"
            />
          </TouchableOpacity>

          <ModalDropdown
            ref={dropDownRef}
            defaultIndex={tabIndex}
            options={tabList}
            dropdownTextStyle={styles.dropdownTextStyle}
            dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
            dropdownStyle={styles.dropdownStyle}
            textStyle={{display: 'none'}}
            defaultValue=""
            renderButtonText={() => ''}
            onSelect={setTabIndex}
          />
        </View>
        <SectionGrid
          style={{marginTop: 10}}
          itemDimension={90}
          spacing={3}
          sections={filteredData}
          renderItem={({item}) => (
            <TouchableOpacity onPress={handleViewPhoto(item)}>
              <Image
                resizeMode="cover"
                style={styles.image}
                source={{uri: getPhotoUrl(item.PMPhotoServerPath)}}
                PlaceholderContent={<ActivityIndicator />}
              />
            </TouchableOpacity>
          )}
          keyExtractor={(item) => item.PMPhotoID}
          onEndReachedThreshold={0.5}
         // onEndReached={handleLoadMore}
          //onScrollEndDrag={() => console.log('onScrollEndDrag')}
          onMomentumScrollEnd={onScroll}
          //onScroll={onScroll}
          renderSectionHeader={({section}) => (
            <Text style={styles.header}>{section.title}</Text>
          )}
        />
      </View>
      <UniImageButton
        containerStyle={styles.floatButton}
        source={require('../../assets/img/ic_add.png')}
        onPress={handleShowSelectPhoto}
      />
      <UniBottomSheet ref={bottomSheetRef} />
    </View>
  );
};

const mapStateToProps = ({
  auth: {user, project, master},
  commonData,
  photoList,
}) => {
  const PMProjectID = propOr(0, 'PMProjectID', project);

  return {
    user,
    project,
    data: photoList.photoList,
    categories: master ? master.categories : [],
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
  };
};

const mapDispatchToProps = {
  getPhotoList: actions.getPhotoList,
  deletePhoto: actions.deletePhoto,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotoList);
