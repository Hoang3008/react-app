import {
  createAction
} from 'redux-actions';
import {
  identity
} from 'ramda';
import API from '../../../api/API.service';
import AsyncStorage from '@react-native-community/async-storage';
import {
  withKey
} from '../../../api/urls';

const actionType = 'PHOTO/DELETE_PHOTO';

// /PMPhotos/Filter?totalPages=10&pageIndex=1&FK_PMProjectID=117

const createTaskAPI = (item) => {
  return AsyncStorage.getItem('token').then((token) => {
    return API.delete(withKey('/photo'), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: {
        id: [
          item?.PMPhotoID
        ],
        projectId: item?.FK_PMProjectID
      }
    });    
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {
  meta
}) => {
  const {
    PMPhotoID
  } = meta;

  return {
    ...state,
    photoList: state.photoList.filter((item) => item.PMPhotoID !== PMPhotoID),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {
  action,
  reducer
};
