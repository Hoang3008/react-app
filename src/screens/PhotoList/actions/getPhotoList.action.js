import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {actionType as AddPhotoActionType} from '../../AddPhoto/actions/addPhoto.action';
import {actionType as UpdatePhotoActionType} from '../../EditPhoto/actions/updatePhoto.action';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_PHOTO_LIST';

// /PMPhotos/Filter?totalPages=10&pageIndex=1&FK_PMProjectID=117

const createTaskAPI = (projectId, page, size, keyword,userName, data, parentId) => {
  // const url = withKey(
  //   `/PMPhotos/Filter?totalPages=${size}&pageIndex=${page}&FK_PMProjectID=${projectId}&KeyWord=${keyword}&userName=${userName}`,
  // );
  // return API.get(url);

  const body = {
    pageSize: size,
    pageIndex: page,
    projectId: projectId,
    albumId: parentId && parentId
  };

  return AsyncStorage.getItem('token').then((token) => {
    return API.post(withKey(`/photo/search`), body, {
      headers: {Authorization: `Bearer ${token}`},
    });
  });
};

const action = createAction(actionType, createTaskAPI, (...param) => param);

const handleAPISuccess = (state, {payload, meta}) => {
  const [projectId, page, size, keyword] = meta;

  if (page === 1) {
    return {
      ...state,
      photoList: payload.data,
    };
  }

  return {
    ...state,
    photoList: [...state.photoList, ...payload.data],
  };
};

const handleAddPhotoSuccess = (state, {payload}) => {
  const resItem = payload.data || [];
  return {
    ...state,
    photoList: [...resItem, ...state.photoList],
  };
};

const handleUpdatePhotoSuccess = (state, {payload}) => {
  return {
    ...state,
    photoList: state.photoList.map((item) => {
      if (item.PMPhotoID === payload.data.PMPhotoID) {
        return {
          ...item,
          ...payload.data,
        };
      }

      return item;
    }),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
  [AddPhotoActionType]: {
    SUCCESS: handleAddPhotoSuccess,
  },
  [UpdatePhotoActionType]: {
    SUCCESS: handleUpdatePhotoSuccess,
  },
};

export default {action, reducer};
