import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {connect} from 'react-redux';

import Theme from '../Theme';
import {UniStatusBarBackground} from '../components';
import {SCREEN_WIDTH} from '../utils/DimensionUtils';
import {logout} from '../action/AuthActions';
import RouteNames from '../RouteNames';

class Settings extends Component {



  renderNavigation = () => {
    const {logout, navigation, ADUserFirstName, ADUserLastName, ADUserName} = this.props;
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <View>
            <Text style={styles.textHeader}>Settings</Text>
          </View>
        </View>
      </View>
    );
  };

  handleLogout = () => {
    const {logout, navigation} = this.props;

    logout();
    navigation.navigate(RouteNames.AuthStack);
  };

  render() {
    const {logout, navigation, ADUserFirstName, ADUserLastName, ADUserName} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundColor}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {this.renderNavigation()}
        <ScrollView
          style={{
            flex: 1,
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}>
            <View style={styles.info} >
            <Text style={[styles.infoChild, {width: '20%'}]}>About</Text>
            <View style={[styles.infoChild, {width: '80%'}]}>
              <View style={styles.infoUser}>
                  <Text style={{ color: '#9EA0A5' }} >Full Name:  {ADUserFirstName} {ADUserLastName}</Text>
                  <Text style={{ color: '#9EA0A5' }} >Email: {ADUserName}</Text>
              </View>
            </View>
            </View>
          <TouchableOpacity style={styles.itemContainer}>
            <Text style={styles.item}>Version {DeviceInfo.getVersion()}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.logoutContainer}
            onPress={this.handleLogout}>
            <Text style={styles.logout}>Logout</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingHorizontal: Theme.margin.m16,
  },
  info: {
    flexDirection: 'row',
    flex:1,
  },
  infoChild: {
    fontSize: 16,
    padding: 15,
    backgroundColor: '#e5e5e5',
  },
  infoUser: {
    flexDirection: 'column',
    flex:1,
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  header: {
    width: '100%',
    fontSize: 16,
    padding: 15,
    backgroundColor: '#e5e5e5',
  },
  itemContainer: {
    width: '100%',
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
  },
  item: {
    width: '100%',
    fontSize: 14,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 20,
  },
  logoutContainer: {
    width: '100%',
  },

  logout: {
    width: '100%',
    fontSize: 16,
    paddingTop: 15,
    paddingBottom: 15,
    textAlign: 'center',
    color: '#DD4B39',
  },
};


const mapStateToProps = ({auth: {user}}) => user;

export default connect(
  mapStateToProps,
  {logout},
)(Settings);
