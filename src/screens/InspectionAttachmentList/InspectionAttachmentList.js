import React, {useEffect, useRef} from 'react';
import {connect} from 'react-redux';
import {StatusBar, Text, TouchableOpacity, View} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';

import styles from './InspectionAttachmentList.styles';

import Theme from '../../Theme';
import {
  UniBottomSheet,
  UniImageButton,
  UniStatusBarBackground,
} from '../../components';
import {isEmpty, pathOr, propOr} from 'ramda';
import RouteNames from '../../RouteNames';
import {getFileName} from '../defect/DefectHelper';
import AttachmentList from './AttachmentList';
import PhotoList from './PhotoList';
import {actions as actionsOffline} from '../CommonProjectData/CommonProjectData.reducer';
import {OFFLINE_ACTION_TYPES} from '../../global/constants';

const TYPE = {
  ATTACHMENT: 0,
  IMAGE: 1,
};

const InspectionAttachmentList = (props) => {
  const {
    navigation,
    stepItem,
    count,
    inspectionId,
    addActionIntoQueue,
    user,
    type,
    attachmentList,
  } = props;

  const id = pathOr('', ['route', 'params', 'id'], props);
  const allowEdit = pathOr(true, ['route', 'params', 'allowEdit'], props);

  const bottomSheetRef = useRef();

  useEffect(() => {
    if (count === 0 && isEmpty(attachmentList)) {
      handleAddNewAttachment();
    }
  }, [stepItem, count, attachmentList]);

  const getUploadBody = () => ({
    AACreatedUser: user.ADUserName,
    FK_PMProjectInsStpInspID: stepItem.PMProjectInsStpInspID,
  });

  const handleAddNewAttachment = () => {
    if (!allowEdit) {
      return;
    }

    handleShowBottomSheet();
  };

  const handleShowBottomSheet = () => {
    bottomSheetRef.current.show(
      handleSelectedBottomSheet(type === TYPE.ATTACHMENT),
      type === TYPE.ATTACHMENT,
    );
  };

  const handleSelectedBottomSheet = (isAttachment) => (type) => {
    let options = {
      cropping: false,
      writeTempFile: false,
      compressImageMaxWidth: 1024,
      compressImageMaxHeight: 1024,
    };
    switch (type) {
      case 'camera':
        navigation.push(RouteNames.CaptureImage, {
          onSave: isAttachment
            ? handleUploadAttachmentPhoto
            : handleUploadPhoto,
        });
        break;
      case 'photos-device':
        ImagePicker.openPicker(options)
          .then((image) =>
            navigation.push(RouteNames.SketchDrawing, {
              imagePath: image.path,
              onSave: isAttachment
                ? handleUploadAttachmentPhoto
                : handleUploadPhoto,
            }),
          )
          .catch(() => {});
        break;
      case 'files-device':
        handlePickPDF();
        break;
      default:
        break;
    }
  };

  //http://unicons-api.rocez.com/user/login

  const handlePickPDF = () => {
    DocumentPicker.pick({
      type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
    }).then(handleUploadPDF);
  };

  const handleUploadPhoto = (path) => {
    const countKey = pathOr(
      'PMProjectInsStpInspDocCount',
      ['route', 'params', 'countKey'],
      props,
    );

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.ADD_ITP_WORKFLOW_PHOTO,
      data: {
        inspectionId,
        offlineId: Date.now(),
        body: getUploadBody(),
        image: {
          uri: path || '',
          filename: getFileName(path),
          type: 'image/png',
        },
        updateITPStepContent: {
          ...stepItem,
          isShowDetails: false,
          [countKey]: attachmentList.length + 1,
        },
      },
    });

    // uploadPhoto(getUploadBody(), {
    //   uri: path || '',
    //   filename: getFileName(path),
    //   type: 'image/png',
    // }).then(() => {
    //   updateWorkFlowStepContent(
    //     {
    //       ...stepItem,
    //       isShowDetails: false,
    //       [countKey]: attachmentList.length + 1,
    //     },
    //     false,
    //   );
    // });
  };

  const handleUploadAttachmentPhoto = (path) => {
    handleUploadPDF({
      uri: path || '',
      name: getFileName(path),
      type: 'image/png',
    });
  };

  const handleUploadPDF = (file) => {
    const countKey = pathOr(
      'PMProjectInsStpInspDocCount',
      ['route', 'params', 'countKey'],
      props,
    );

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.ADD_ITP_WORKFLOW_DOC,
      data: {
        inspectionId,
        offlineId: Date.now(),
        body: getUploadBody(),
        image: {
          uri: file.uri || '',
          filename: file.name,
          type: file.type,
        },
        updateITPStepContent: {
          ...stepItem,
          isShowDetails: false,
          [countKey]: attachmentList.length + 1,
        },
      },
    });

    // uploadDoc(getUploadBody(), {
    //   uri: file.uri || '',
    //   filename: file.name,
    //   type: file.type,
    // }).then(() => {
    //   updateWorkFlowStepContent(
    //     {
    //       ...stepItem,
    //       isShowDetails: false,
    //       [countKey]: attachmentList.length + 1,
    //     },
    //     false,
    //   );
    // });
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={{...styles.row, flex: 1}}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <View>
          <View style={styles.row}>
            <Text style={styles.textHeader} numberOfLines={1}>
              {type === TYPE.ATTACHMENT ? 'Attachments' : 'Photos'}
            </Text>
          </View>
        </View>
      </View>
      {allowEdit && (
        <TouchableOpacity
          onPress={handleAddNewAttachment}
          style={{paddingVertical: Theme.margin.m6}}>
          <Text style={{color: 'white', fontSize: Theme.fontSize.normal}}>
            Add
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}

      {type === TYPE.ATTACHMENT ? (
        <AttachmentList
          id={stepItem.PMProjectInsStpInspID}
          navigation={navigation}
          attachmentList={attachmentList}
        />
      ) : (
        <PhotoList
          id={stepItem.PMProjectInsStpInspID}
          navigation={navigation}
          attachmentList={attachmentList}
        />
      )}
      <UniBottomSheet key={id} ref={bottomSheetRef} />
    </View>
  );
};

const mapStateToProps = (
  {auth: {user}, inspectionDetail, inspectionAttachmentList},
  props,
) => {
  const inspectionFromProps = pathOr({}, ['route', 'params'], props);
  const inspectionId = inspectionFromProps.inspectionId;
  const id = inspectionFromProps.id;
  const countKey = pathOr(
    'PMProjectInsStpInspDocCount',
    ['route', 'params', 'countKey'],
    props,
  );
  const inspection = propOr({}, inspectionId, inspectionDetail);
  const acceptanceSteps = propOr([], 'acceptanceSteps', inspection);
  const stepItem = acceptanceSteps.find(
    ({PMProjectInsStpInspID}) => PMProjectInsStpInspID === id,
  );
  const type =
    countKey === 'PMProjectInsStpInspDocCount' ? TYPE.ATTACHMENT : TYPE.IMAGE;

  let attachmentList;
  if (type === TYPE.ATTACHMENT) {
    attachmentList = inspection.docs || [];
  } else {
    attachmentList = inspection.pics || [];
  }

  return {
    user,
    stepItem,
    inspectionId,
    count: propOr(0, countKey, stepItem),
    type,
    attachmentList: attachmentList.filter(
      (item) => item.FK_PMProjectInsStpInspID === id,
    ),
  };
};

const mapDispatchToProps = {
  addActionIntoQueue: actionsOffline.addActionIntoQueue,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionAttachmentList);
