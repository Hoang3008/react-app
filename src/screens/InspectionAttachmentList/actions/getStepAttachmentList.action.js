import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {actionType as uploadDocActionType} from '../../InspectionDetail/actions/uploadDoc.action';
import {actionType as deleteDocActionType} from '../../InspectionDetail/actions/deleteDoc.action';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_INSPECTION_STEP_ATTACHMENT_LIST';

// PMProjectInsStpInspDocs/GetAllDataByPMProjectInsStpInspID?FK_PMProjectInsStpInspID=360
// PMProjectInsStpInspPics/GetAllDataByPMProjectInsStpInspID?FK_PMProjectInsStpInspID=360
const createTaskAPI = (id) => {
  const url = withKey(
    `/PMProjectInsStpInspDocs/GetAllDataByPMProjectInsStpInspID?FK_PMProjectInsStpInspID=${id}`,
  );

  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    attachmentList: payload.data,
  };
};

const handleUploadAttachmentSuccess = (state, {payload}) => {
  return {
    ...state,
    attachmentList: [...state.attachmentList, payload.data],
  };
};

const handleDeleteSuccess = (state, {meta}) => {
  const {PMProjectInsStpInspDocID} = meta;
  return {
    ...state,
    attachmentList: state.attachmentList.filter(
      (item) => item.PMProjectInsStpInspDocID !== PMProjectInsStpInspDocID,
    ),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: (state) => ({...state, attachmentList: []}),
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
  [uploadDocActionType]: {
    SUCCESS: handleUploadAttachmentSuccess,
  },
  [deleteDocActionType]: {
    SUCCESS: handleDeleteSuccess,
  },
};

export default {action, reducer};
