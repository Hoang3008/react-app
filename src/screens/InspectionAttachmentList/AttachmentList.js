import {connect} from 'react-redux';
import React from 'react';
import {propOr} from 'ramda';
import {View, Text, TouchableOpacity, TouchableHighlight} from 'react-native';
import moment from 'moment';
import {SwipeListView} from 'react-native-swipe-list-view';

import {actions as detailActions} from '../InspectionDetail/InspectionDetail.reducer';
import Theme from '../../Theme';
import RouteNames from '../../RouteNames';
import {getInspectionAttachmentUrl} from '../../api/urls';

const AttachmentList = (props) => {
  const {
    stepItem,
    attachmentList,
    deleteDoc,
    updateWorkFlowStepContent,
    navigation,
  } = props;

  const handleDelete = (item) => () => {
    return deleteDoc(item).then(() => {
      return updateWorkFlowStepContent(
        {
          ...stepItem,
          isShowDetails: false,
          PMProjectInsStpInspDocCount: attachmentList.length - 1,
          projectId: PMProjectID,
          idItp: item?.PMProjectInsStpInspPicID
        },
        false,
      );
    });
  };

  const handleClickItem = (item, url) => () => {
    if (url.toLowerCase().endsWith('pdf')) {
      navigation.navigate(RouteNames.PDFFullScreen, {url: url});
      return;
    }

    navigation.navigate(RouteNames.ImageFullScreen, {
      item,
      url,
      onDelete: handleDelete(item),
    });
  };

  const renderItem = ({item}) => {
    const {PMProjectInsStpInspDocDesc, AACreatedDate, AACreatedUser} = item;
    return (
      <TouchableHighlight
        onPress={handleClickItem(
          item,
          getInspectionAttachmentUrl(PMProjectInsStpInspDocDesc),
        )}>
        <View style={styles.item}>
          <Text style={styles.title}>{PMProjectInsStpInspDocDesc}</Text>
          <Text style={styles.subText}>{AACreatedUser}</Text>
          <Text style={styles.subText}>
            {moment(AACreatedDate).format('YYYY-MM-DD hh:mm A')}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  const renderHiddenItem = ({item}) => {
    return (
      <View style={styles.rowBack}>
        <TouchableOpacity
          onPress={handleDelete(item)}
          style={{
            width: 100,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={styles.delete}>Delete</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SwipeListView
      style={{flex: 1, width: '100%'}}
      data={attachmentList}
      renderItem={renderItem}
      renderHiddenItem={renderHiddenItem}
      keyExtractor={(item) => item.PMProjectInsStpInspDocID}
      ItemSeparatorComponent={() => <View style={styles.divider} />}
      leftOpenValue={0}
      rightOpenValue={-100}
      closeOnRowBeginSwipe
      disableRightSwipe
    />
  );
};

const styles = {
  rowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingLeft: 15,
  },
  item: {
    backgroundColor: '#ffffff',
    display: 'flex',
    flexDirection: 'column',
    padding: 15,
  },
  title: {
    fontSize: 16,
    color: '#245894',
  },
  subText: {
    fontSize: 12,
    color: '#aaa',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m52,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  delete: {
    fontSize: 16,
    color: '#ffffff',
  },
};

const mapStateToProps = ({auth: {user, project}, inspectionDetail, co}, props) => {
  const {id} = props;

  const PMProjectID = propOr('', 'PMProjectID', project);
  const workflowContent = propOr([], 'workflowContent', inspectionDetail);
  const stepItem = workflowContent.find(
    ({PMProjectInsStpInspID}) => PMProjectInsStpInspID === id,
  );

  return {
    user,
    stepItem,
    PMProjectID
  };
};

const mapDispatchToProps = {
  updateWorkFlowStepContent: detailActions.updateWorkFlowStepContent,
  deleteDoc: detailActions.deleteDoc,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AttachmentList);
