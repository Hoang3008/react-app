import {connect} from 'react-redux';
import React, {useEffect} from 'react';
import {propOr} from 'ramda';
import {View, Text, Image, TouchableHighlight} from 'react-native';
import moment from 'moment';
import {FlatGrid} from 'react-native-super-grid';

import {actions} from './InspectionAttachmentList.reducer';
import {actions as detailActions} from '../InspectionDetail/InspectionDetail.reducer';
import Theme from '../../Theme';
import {getInspectionPhotoUrl} from '../../api/urls';
import RouteNames from '../../RouteNames';

const PhotoList = (props) => {
  const {
    stepItem,
    attachmentList,
    navigation,
    deletePhoto,
    updateWorkFlowStepContent,
    PMProjectID
  } = props;

  const handleDeletePhoto = (item) => () => {
    return deletePhoto(item).then(() => {
      return updateWorkFlowStepContent(
        {
          ...stepItem,
          isShowDetails: false,
          PMProjectInsStpInspPicCount: attachmentList.length - 1,
          projectId: PMProjectID,
          idItp: item?.PMProjectInsStpInspPicID
        },
        false,
      );
    });
  };

  const handleViewPhoto = (item) => () => {
    const {PMProjectInsStpInspPicDesc} = item;
    navigation.navigate(RouteNames.ImageFullScreen, {
      item,
      url: getInspectionPhotoUrl(PMProjectInsStpInspPicDesc),
      onDelete: handleDeletePhoto(item),
    });
  };

  const renderItem = ({item}) => {
    const {
      PMProjectInsStpInspPicDesc,
      AACreatedDate,
      AACreatedUser,
      PMProjectInsStpInspPicID,
      imageLocalPath,
    } = item;
    return (
      <TouchableHighlight onPress={handleViewPhoto(item)}>
        <View style={styles.item}>
          <Image
            resizeMode="contain"
            style={{height: 140, width: '100%'}}
            source={{
              uri: PMProjectInsStpInspPicID
                ? getInspectionPhotoUrl(PMProjectInsStpInspPicDesc)
                : imageLocalPath,
            }}
          />
          <Text style={[styles.subText, {marginTop: 10}]}>{AACreatedUser}</Text>
          <Text style={styles.subText}>
            {moment(AACreatedDate).format('YYYY-MM-DD hh:mm A')}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  return (
    <FlatGrid
      keyExtractor={(item) => item.PMProjectInsStpInspPicID || item.offlineId}
      style={{flex: 1, width: '100%'}}
      itemDimension={130}
      items={attachmentList}
      spacing={15}
      renderItem={renderItem}
    />
  );
};

const styles = {
  rowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingLeft: 15,
  },
  item: {
    backgroundColor: '#ffffff',
    display: 'flex',
    flexDirection: 'column',
    padding: 15,
  },
  title: {
    fontSize: 16,
    color: '#245894',
  },
  subText: {
    fontSize: 12,
    color: '#aaa',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m52,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  delete: {
    fontSize: 16,
    color: '#ffffff',
  },
};

const mapStateToProps = ({auth: {user, project}, inspectionDetail}, props) => {
  const {id} = props;

  const PMProjectID = propOr('', 'PMProjectID', project);
  const workflowContent = propOr([], 'workflowContent', inspectionDetail);
  const stepItem = workflowContent.find(
    ({PMProjectInsStpInspID}) => PMProjectInsStpInspID === id,
  );

  return {
    user,
    stepItem,
    PMProjectID
  };
};

const mapDispatchToProps = {
  updateWorkFlowStepContent: detailActions.updateWorkFlowStepContent,
  deletePhoto: detailActions.deletePhoto,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotoList);
