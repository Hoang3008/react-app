import typeToReducer from 'type-to-reducer';

import getStepAttachmentList from './actions/getStepAttachmentList.action';
import getStepPhotoList from './actions/getStepPhotoList.action';

const initialState = {
  attachmentList: [],
};

export const actions = {
  getStepAttachmentList: getStepAttachmentList.action,
  getStepPhotoList: getStepPhotoList.action,
};

export default typeToReducer(
  {
    ...getStepAttachmentList.reducer,
    ...getStepPhotoList.reducer,
  },
  initialState,
);
