import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

export const actionType = 'UPDATE_INSPECTION';

// https://apidev.rocez.com/PMProjectInsps/UpdateObject
//   Request Method: PUT

const createTaskAPI = (item) => {
  const projectId = item?.FK_PMProjectID;
  const itpId = item?.PMProjectInspID;
  const url = withKey(`/itp/v2/${projectId}/${itpId}`);
  return AsyncStorage.getItem('token').then((token) => {
    return API.put(url, item, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
