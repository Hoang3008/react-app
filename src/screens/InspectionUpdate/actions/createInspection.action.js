import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'CREATE_INSPECTION';

// https://apidev.rocez.com/PMProjectInsps/CreateObject
//   Request Method: POST

const createTaskAPI = (item) => {
  const url = withKey('/itp/v2');
  return AsyncStorage.getItem('token').then((token) => {
    return API.post(url, item, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

// AACreatedDate: "2020-07-20T02:33:18.000Z"
// AACreatedUser: "nga.nguyen@unicons.vn"
// AAUpdatedDate: "2020-07-20T02:33:18.080Z"
// AAUpdatedUser: "nga.nguyen@unicons.vn"
// FK_PMInspectPeopleID: 87
// FK_PMProjectApprovedPeopleID: 87
// FK_PMProjectCategoryID: 1
// FK_PMProjectID: 125
// FK_PMProjectLocationID: 50
// FK_PMProjectPeopleID: 87
// FK_PMWorkInstID: 18
// PMProjectInspDate: "2020-06-28T17:00:00.000Z"
// PMProjectInspDesc: ""
// PMProjectInspEstEndDate: "2020-06-29T17:00:00.000Z"
// PMProjectInspEstStartDate: "2020-06-28T17:00:00.000Z"
// PMProjectInspName: "cuong test 1"
// PMProjectInspNo: "no1"

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
