import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'DELETE_INSPECTION';

// https://apidev.rocez.com/PMProjectInsps/DeleteObject
//   Request Method: PUT

const createTaskAPI = (item) => {
  const url = withKey('/PMProjectInsps/DeleteObject');
  return AsyncStorage.getItem('token').then((token) => {
    return API.put(url, item, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
