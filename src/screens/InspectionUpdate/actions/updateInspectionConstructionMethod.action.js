import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'UPDATE_INSPECTION_CONSTRUCTION_METHOD';

// /itp/steps/from-construction-method
// {constructionMethodId: 8, itpId: 1074}

const createTaskAPI = (itpId, constructionMethodId) => {
  const url = withKey('/itp/steps/from-construction-method');
  return AsyncStorage.getItem('token').then((token) => {
    return API.post(
      url,
      {itpId, constructionMethodId},
      {
        headers: {
          Accept: 'application/json, text/plain, */*',
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
