import React, {useEffect, useState} from 'react';
import {
  Alert,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {isEmpty, pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';
import moment from 'moment';

import styles from './styles';

import {EMPTY, OFFLINE_ACTION_TYPES} from '../../global/constants';
import {actions as updateActions} from './InspectionUpdate.reducer';
import {actions as detailActions} from '../InspectionDetail/InspectionDetail.reducer';
import {actions as commonActions} from '../CommonProjectData/CommonProjectData.reducer';
import {actions as InspectionListActions} from '../InspectionList/InspectionList.reducer';
import Theme from '../../Theme';
import {
  UniButton,
  UniDatePicker,
  UniDropbox,
  UniImageButton,
  UniInput,
  UniStatusBarBackground,
} from '../../components';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import {createLogList} from '../../utils/Log.utils';

const InspectionUpdate = (props) => {
  const {
    navigation,
    PMProjectID,
    route,
    user,
    inspection,
    locations,
    categories,
    peopleInProject,
    constructionMethodList,
    deleteInspection,
    getConstructionMethodList,
    getInspectionList,
    createInspection,
    updateInspection,
    updateInspectionConstructionMethod,
    addActionIntoQueue,
  } = props;

  const onDelete = pathOr(() => {}, ['params', 'onDelete'], route);
  const [PMProjectInspNo, setPMProjectInspNo] = useState('');
  const [PMProjectInspName, setPMProjectInspName] = useState('');
  const [FK_PMProjectLocationID, setFK_PMProjectLocationID] = useState();
  const [FK_PMProjectCategoryID, setFK_PMProjectCategoryID] = useState();
  const [PMProjectInspDate, setPMProjectInspDate] = useState();
  const [PMProjectInspEstStartDate, setPMProjectInspEstStartDate] = useState();
  const [PMProjectInspEstEndDate, setPMProjectInspEstEndDate] = useState();
  const [FK_PMInspectPeopleID, setFK_PMInspectPeopleID] = useState();
  const [FK_PMWorkInstID, setFK_PMWorkInstID] = useState();
  const [FK_PMProjectPeopleID, setFK_PMProjectPeopleID] = useState();
  const [
    FK_PMProjectApprovedPeopleID,
    setFK_PMProjectApprovedPeopleID,
  ] = useState();
  const [PMProjectInspDesc, setPMProjectInspDesc] = useState('');

  useEffect(() => {
    if (isEmpty(inspection)) {
      return;
    }

    setPMProjectInspNo(inspection.PMProjectInspNo);
    setPMProjectInspName(inspection.PMProjectInspName);
    setFK_PMProjectLocationID(inspection.FK_PMProjectLocationID);
    setFK_PMProjectCategoryID(inspection.FK_PMProjectCategoryID);
    setPMProjectInspDate(inspection.PMProjectInspDate);
    setPMProjectInspEstStartDate(inspection.PMProjectInspEstStartDate);
    setPMProjectInspEstEndDate(inspection.PMProjectInspEstEndDate);
    setFK_PMInspectPeopleID(inspection.FK_PMInspectPeopleID);
    setFK_PMWorkInstID(inspection.FK_PMWorkInstID);
    setFK_PMProjectPeopleID(inspection.FK_PMProjectPeopleID);
    setFK_PMProjectApprovedPeopleID(inspection.FK_PMProjectApprovedPeopleID);
    setPMProjectInspDesc(inspection.PMProjectInspDesc);
  }, [inspection]);

  useEffect(() => {
    getConstructionMethodList(PMProjectID);
  }, [getConstructionMethodList, PMProjectID]);

  const onPressSave = () => {
    const item = {
      ...inspection,
      FK_PMProjectID: PMProjectID,
      PMProjectInspNo,
      PMProjectInspName,
      FK_PMProjectLocationID,
      FK_PMProjectCategoryID,
      PMProjectInspDate: moment(PMProjectInspDate).format('YYYY-MM-DD'),
      PMProjectInspEstStartDate: moment(PMProjectInspEstStartDate).format(
        'YYYY-MM-DD',
      ),
      PMProjectInspEstEndDate: moment(PMProjectInspEstEndDate).format(
        'YYYY-MM-DD',
      ),
      FK_PMInspectPeopleID,
      FK_PMWorkInstID,
      FK_PMProjectPeopleID,
      FK_PMProjectApprovedPeopleID,
      PMProjectInspDesc,
    };

    if (isEmpty(inspection)) {
      return createInspection({
        ...item,
        AACreatedDate: moment().format('YYYY-MM-DD'),
        AAUpdatedDate: moment().format('YYYY-MM-DD'),
        AACreatedUser: user.ADUserName,
        AAUpdatedUser: user.ADUserName,
      })
        .then((res) => {
          const PMProjectInspID = pathOr(
            '',
            ['action', 'payload', 'data', 'PMProjectInspID'],
            res,
          );
          return updateInspectionConstructionMethod(
            PMProjectInspID,
            FK_PMWorkInstID,
          );
        })
        .then(() => {
          getInspectionList(PMProjectID);
          onDelete();
          navigation.goBack();
        });
    }

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.UPDATE_ITP_OVERVIEW,
      data: item,
    });
    navigation.goBack();

    // updateInspection(item).then(() => {
    //   getInspectionList(PMProjectID);
    //   navigation.goBack();
    // });
  };

  const onPressDelete = () => {
    Alert.alert(
      'Delete',
      'Do you want to delete?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => {
            deleteInspection(inspection).then(() => {
              getInspectionList(PMProjectID);
              onDelete();
              navigation.goBack();
            });
          },
        },
      ],
      {cancelable: false},
    );
  };

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={navigation.goBack}
          />
          <View>
            <Text style={styles.textHeader}>Inspection Detail</Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="padding" enabled>
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {renderNavigation()}
        <ScrollView>
          <UniInput
            forceValueFromProp
            placeholder="Enter No."
            value={PMProjectInspNo}
            onChangeText={setPMProjectInspNo}
            containerStyle={styles.editText}
          />
          <UniInput
            forceValueFromProp
            placeholder="Enter Name"
            value={PMProjectInspName}
            onChangeText={setPMProjectInspName}
            containerStyle={styles.editText}
          />

          <UniDropbox
            label={'Category'}
            selectedKey="PPProjectDiscID"
            selectedValue={FK_PMProjectCategoryID}
            options={categories}
            pathLeft={'PPProjectDiscNo'}
            pathValue={'PPProjectDiscName'}
            onSelect={(no, {PPProjectDiscID}) => {
              setFK_PMProjectCategoryID(PPProjectDiscID);
              setFK_PMWorkInstID('');
            }}
          />

          <UniDropbox
            label={'Location'}
            selectedKey="PMProjectLocationID"
            selectedValue={FK_PMProjectLocationID}
            options={locations}
            pathLeft={'PMProjectLocationNo'}
            pathValue={'PMProjectLocationName'}
            onSelect={(no, {PMProjectLocationID}) =>
              setFK_PMProjectLocationID(PMProjectLocationID)
            }
          />

          <UniDatePicker
            label={'Date Published'}
            selectedValue={PMProjectInspDate}
            onSelect={setPMProjectInspDate}
          />

          <UniDatePicker
            label={'Start Date'}
            selectedValue={PMProjectInspEstStartDate}
            onSelect={setPMProjectInspEstStartDate}
          />

          <UniDatePicker
            label={'End Date'}
            selectedValue={PMProjectInspEstEndDate}
            onSelect={setPMProjectInspEstEndDate}
          />

          <UniDropbox
            label={'Inspector'}
            selectedKey="PMProjectPeopleID"
            selectedValue={FK_PMInspectPeopleID}
            options={peopleInProject}
            pathLeft={'shortName'}
            pathValue={'fullName'}
            onSelect={(no, {PMProjectPeopleID}) =>
              setFK_PMInspectPeopleID(PMProjectPeopleID)
            }
          />

          <UniDropbox
            label={'Workflow'}
            disabled={!isEmpty(inspection)}
            selectedKey="PMWorkInstID"
            selectedValue={FK_PMWorkInstID}
            options={(constructionMethodList || []).filter(
              (item) => item.FK_PMProjectCategoryID === FK_PMProjectCategoryID,
            )}
            pathLeft={'PMWorkInstNo'}
            pathValue={'PMWorkInstName'}
            onSelect={(no, {PMWorkInstID}) => setFK_PMWorkInstID(PMWorkInstID)}
          />

          <UniDropbox
            label={'Main Contact'}
            selectedKey="PMProjectPeopleID"
            selectedValue={FK_PMProjectPeopleID}
            options={peopleInProject}
            pathLeft={'shortName'}
            pathValue={'fullName'}
            onSelect={(no, {PMProjectPeopleID}) =>
              setFK_PMProjectPeopleID(PMProjectPeopleID)
            }
          />

          <UniDropbox
            label={'Signed By'}
            selectedKey="PMProjectPeopleID"
            selectedValue={FK_PMProjectApprovedPeopleID}
            options={peopleInProject}
            pathLeft={'shortName'}
            pathValue={'fullName'}
            onSelect={(no, {PMProjectPeopleID}) =>
              setFK_PMProjectApprovedPeopleID(PMProjectPeopleID)
            }
          />

          <UniInput
            forceValueFromProp
            numberOfLines={3}
            placeholder="Enter Description"
            value={PMProjectInspDesc}
            onChangeText={setPMProjectInspDesc}
            containerStyle={styles.editTextDescription}
          />

          <View style={{alignItems: 'flex-end', marginRight: Theme.margin.m16}}>
            {!isEmpty(inspection) && (
              <TouchableOpacity onPress={onPressDelete}>
                <Text
                  style={{
                    color: Theme.colors.textDangerous,
                    fontSize: Theme.fontSize.normal,
                    paddingVertical: Theme.margin.m8,
                  }}>
                  Delete
                </Text>
              </TouchableOpacity>
            )}
            <UniButton
              text={'Save'}
              onPress={onPressSave}
              containerStyle={styles.button}
            />
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingViewWrapper>
  );
};

const mapStateToProps = (
  {auth: {user, project, master}, commonData, inspectionList, inspectionDetail},
  props,
) => {
  const inspectionFromProps = pathOr({}, ['route', 'params'], props);
  const inspectionId = inspectionFromProps.PMProjectInspID;

  const inspects = propOr([], 'inspectionList', inspectionList);
  const inspection =
    inspects.find(({PMProjectInspID}) => PMProjectInspID === inspectionId) ||
    inspectionFromProps;

  const {PMProjectID} = project;

  const workflow = propOr([], 'workflow', inspectionDetail);

  return {
    user,
    project,
    PMProjectID,
    inspectionId,
    categories: master.categories,
    inspection: pathOr(
      inspection,
      [inspectionId, 'overview', 0],
      inspectionDetail,
    ),
    workflow: isEmpty(workflow) || !Array.isArray(workflow) ? [] : workflow,
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    constructionMethodList: inspectionDetail.constructionMethodList || [],
  };
};

const mapDispatchToProps = {
  addActionIntoQueue: commonActions.addActionIntoQueue,
  getWorkFlowByInspectionId: detailActions.getWorkFlowByInspectionId,
  updateWorkFlowStatus: detailActions.updateWorkFlowStatus,
  getProjectLocations: commonActions.getProjectLocations,
  getPeopleInProject: commonActions.getPeopleInProject,
  getInspectionList: InspectionListActions.getInspectionList,
  getConstructionMethodList: detailActions.getConstructionMethodList,
  deleteInspection: updateActions.deleteInspection,
  createInspection: updateActions.createInspection,
  updateInspection: updateActions.updateInspection,
  updateInspectionConstructionMethod:
    updateActions.updateInspectionConstructionMethod,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionUpdate);
