import typeToReducer from 'type-to-reducer';
import deleteInspection from './actions/deleteInspection.action';
import createInspection from './actions/createInspection.action';
import updateInspection from './actions/updateInspection.action';
import updateInspectionConstructionMethod from './actions/updateInspectionConstructionMethod.action';

const initialState = {};

export const actions = {
  deleteInspection: deleteInspection.action,
  createInspection: createInspection.action,
  updateInspection: updateInspection.action,
  updateInspectionConstructionMethod: updateInspectionConstructionMethod.action,
};

export default typeToReducer(
  {
    ...deleteInspection.reducer,
    ...createInspection.reducer,
    ...updateInspection.reducer,
    ...updateInspectionConstructionMethod.reducer,
  },
  initialState,
);
