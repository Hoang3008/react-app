import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Text, TouchableOpacity, View} from 'react-native';
import {isEmpty, propOr} from 'ramda';
import Icon from 'react-native-vector-icons/Fontisto';

import styles from './TodaySchedule.styles';
import {actions} from './ProjectOverviewDashboard.reducer';
import moment from 'moment';
import {PRIORITY_COLOR} from '../ScheduleList/ScheduleList';
import {useNavigation} from '@react-navigation/native';
import RouteNames from '../../RouteNames';

const MyOpenItemOverview = (props) => {
  const {todaySchedule, project, getTodaySchedule, token} = props;
  const navigation = useNavigation();
  const PMProjectID = propOr(0, 'PMProjectID', project);

  useEffect(() => {
    getTodaySchedule(PMProjectID, token);
  }, [getTodaySchedule, PMProjectID, token]);

  if (isEmpty(todaySchedule)) {
    return <View />;
  }

  const handleOpenSchedule = (item) => () => {
    navigation.push(RouteNames.ScheduleDetail, {PMTaskID: item.PMTaskID});
  };

  const renderRowItem = (item) => {
    const {
      PMTaskID,
      PMTaskName,
      PMTaskStartDate,
      PMTaskEndDate,
      PMTaskPriorityCombo,
      PMTaskCompletePct,
    } = item;

    return (
      <TouchableOpacity key={PMTaskID} onPress={handleOpenSchedule(item)}>
        <View style={styles.itemContainer}>
          <View
            style={[
              styles.priority,
              {
                backgroundColor:
                  PMTaskCompletePct === 100
                    ? PRIORITY_COLOR.Complete
                    : PRIORITY_COLOR[PMTaskPriorityCombo],
              },
            ]}
          />
          <View style={styles.itemMainContent}>
            <Text style={styles.itemText}>
              #{PMTaskID} {PMTaskName}
            </Text>
            <Text style={styles.itemDate}>
              {moment(PMTaskStartDate).format('M/D/YY')} -{' '}
              {moment(PMTaskEndDate).format('M/D/YY')}
            </Text>
          </View>
          <Text style={styles.itemPercentText}>{PMTaskCompletePct}%</Text>
          <Icon
            style={{marginRight: 25}}
            size={12}
            color={'#9EA0A5'}
            name="angle-right"
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View style={[styles.row, {alignItems: 'flex-end', marginBottom: 10}]}>
        <Text style={[styles.headerText, {marginBottom: 0}]}>Schedule</Text>
        <Text
          style={[
            styles.label,
            {color: '#999', marginLeft: 10, marginBottom: 2},
          ]}>
          Today's Activities
        </Text>
      </View>
      {todaySchedule.splice(0, 3).map(renderRowItem)}
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, projectOverviewDashboard}) => {
  return {
    user,
    token: user.token,
    project,
    todaySchedule: projectOverviewDashboard.todaySchedule,
  };
};

const mapDispatchToProps = {
  getTodaySchedule: actions.getTodaySchedule,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyOpenItemOverview);
