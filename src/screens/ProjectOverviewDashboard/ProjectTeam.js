import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Text, TouchableOpacity, View} from 'react-native';
import {isEmpty, pathOr, propOr} from 'ramda';

import styles from './ProjectTeam.styles';
import {actions} from './ProjectOverviewDashboard.reducer';

const MyOpenItemOverview = (props) => {
  const {projectTeam = [], project, getProjectTeam, token} = props;
  const [shouldShow, setShouldShow] = useState(false);

  const PMProjectID = propOr(0, 'PMProjectID', project);

  useEffect(() => {
    getProjectTeam(PMProjectID, token);
  }, [getProjectTeam, PMProjectID, token]);

  if (isEmpty(projectTeam)) {
    return <View />;
  }

  // PMProjectPeopleEmailAddress: "tan.nguyen@unicons.vn"
  // PMProjectPeopleFirstName: "Nguyễn Phi"
  // PMProjectPeopleID: 87
  // PMProjectPeopleInitals: ""
  // PMProjectPeopleInviteStatus: ""
  // PMProjectPeopleLastName: "Tân"

  const renderRowItem = (item) => {
    const {
      PMProjectPeopleID,
      PMProjectPeopleEmailAddress,
      PMProjectPeopleFirstName,
      PMProjectPeopleLastName,
    } = item;

    return (
      <TouchableOpacity key={PMProjectPeopleID}>
        <View style={styles.itemContainer}>
          <View style={styles.itemMainContent}>
            <Text style={styles.itemText}>
              {PMProjectPeopleFirstName} {PMProjectPeopleLastName}
            </Text>
            <Text style={styles.itemDate}>{PMProjectPeopleEmailAddress}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <View style={[styles.headerContainer]}>
        <Text style={[styles.headerText, {flex: 1}]}>Project Team</Text>
        <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
          <Text style={styles.headerButton}>
            {shouldShow ? 'HIDE' : 'SHOW'}
          </Text>
        </TouchableOpacity>
      </View>
      {shouldShow && projectTeam.map(renderRowItem)}
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, projectOverviewDashboard, commonData}) => {

  const userInProject = pathOr([],['currentProject','users'],commonData);

  return {
    user,
    token: user.token,
    project,
    projectTeam: userInProject,
  };
};

const mapDispatchToProps = {
  getProjectTeam: actions.getProjectTeam,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyOpenItemOverview);
