import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';

const actionType = 'GET_PROJECT_TEAM';

const createTaskAPI = (projectId, token) => {
  const url = withKey(
    // `/PMProjectPeoples/GetAllDataByProjectID?PMProjectID=${projectId}`,
    `/user/people/of-project/${projectId}`,
  );
  return API.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const action = createAction(actionType, createTaskAPI, (...param) => param);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    projectTeam: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
