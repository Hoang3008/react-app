import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';

const actionType = 'GET_MY_OPEN_ITEMS';

const createTaskAPI = (projectId, token) => {
  const url = withKey(`/dashboard/${projectId}/my-open-items`);
  return API.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const action = createAction(actionType, createTaskAPI, (...param) => param);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    myOpenItems: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
