import mainStyles from './styles';

export default {
  ...mainStyles,
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },

  row: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  circlePoint: {
    width: 16,
    height: 16,
    borderRadius: 8,
  },

  itemContainer: {
    width: '100%',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 15,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.5,
    borderBottomStyle: 'solid',
  },
  itemMainContent: {
    marginLeft: 10,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  itemText: {
    width: '100%',
    fontSize: 16,
    fontWeight: '600',
  },
  itemDate: {
    marginTop: 6,
    width: '100%',
    fontSize: 13,
    fontWeight: '400',
  },
};
