import mainStyles from './styles';

export default {
  ...mainStyles,
  row: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  circlePoint: {
    width: 16,
    height: 16,
    borderRadius: 8,
  },

  column: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  item: {
    flexDirection: 'row',
    width: 60,
  },
};
