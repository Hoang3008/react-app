import typeToReducer from 'type-to-reducer';
import getProjectOverview from './actions/getProjectOverview.action';
import getTodaySchedule from './actions/getTodaySchedule.action';
import getRecentPhotos from './actions/getRecentPhotos.action';
import getProjectTeam from './actions/getProjectTeam.action';
import getMyOpenItems from './actions/getMyOpenItems.action';

const initialState = {
  projectOverview: [],
  todaySchedule: [],
  recentPhotos: [],
  projectTeam: [],
  myOpenItems: [],
};

export const actions = {
  getProjectOverview: getProjectOverview.action,
  getTodaySchedule: getTodaySchedule.action,
  getRecentPhotos: getRecentPhotos.action,
  getProjectTeam: getProjectTeam.action,
  getMyOpenItems: getMyOpenItems.action,
};

export default typeToReducer(
  {
    ...getProjectOverview.reducer,
    ...getTodaySchedule.reducer,
    ...getRecentPhotos.reducer,
    ...getProjectTeam.reducer,
    ...getMyOpenItems.reducer,
  },
  initialState,
);
