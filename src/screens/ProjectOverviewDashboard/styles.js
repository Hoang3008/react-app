import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  headerContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  headerButton: {
    marginTop: 30,
    marginBottom: 10,
    marginRight: 15,
    fontWeight: '500',
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 14,
    lineHeight: 22,
    color: '#245894',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m52,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  sectionHeaderText: {
    flex: 1,
    paddingHorizontal: Theme.margin.m12,
    paddingBottom: Theme.margin.m6,
    paddingTop: Theme.margin.m22,
    color: Theme.colors.textNormal,
    backgroundColor: Theme.colors.backgroundHeader,
  },
  floatButton: {
    position: 'absolute',
    bottom: Theme.margin.m16,
    right: Theme.margin.m20,
    width: Theme.specifications.floatButtonSize,
    height: Theme.specifications.floatButtonSize,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#34AA44',
    borderRadius: Theme.specifications.floatButtonSize / 2,
    borderWidth: 0,
  },

  headerText: {
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 15,
    fontWeight: '600',
    fontSize: 20,
    lineHeight: 22,
    color: '#000',
  },

  text: {
    fontSize: 13,
    fontWeight: '400',
    color: '#333333',
  },

  label: {
    fontSize: 15,
    fontWeight: '400',
    color: '#333333',
  },
  mainRow: {
    marginLeft: 15,
    paddingBottom: 15,
    paddingTop: 10,
    paddingRight: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.5,
    borderBottomStyle: 'solid',
  },

  labelBar: {
    paddingBottom: 10,
    paddingTop: 10,
    paddingLeft: 15,
    paddingRight: 15,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.5,
    borderBottomStyle: 'solid',
  },
};
