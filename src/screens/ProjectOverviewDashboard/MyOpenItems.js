import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Text, TouchableOpacity, View} from 'react-native';
import {isEmpty, propOr} from 'ramda';
import Icon from 'react-native-vector-icons/Fontisto';

import styles from './TodaySchedule.styles';
import {actions} from './ProjectOverviewDashboard.reducer';
import moment from 'moment';
import {PRIORITY_COLOR} from '../ScheduleList/ScheduleList';
import {useNavigation} from '@react-navigation/native';
import RouteNames from '../../RouteNames';

const MyOpenItemOverview = (props) => {
  const {myOpenItems, project, getMyOpenItems, token} = props;
  const navigation = useNavigation();
  const PMProjectID = propOr(0, 'PMProjectID', project);

  useEffect(() => {
    getMyOpenItems(PMProjectID, token);
  }, [getMyOpenItems, PMProjectID, token]);

  if (isEmpty(myOpenItems)) {
    return <View />;
  }

  const handleOpenDetail = (item) => () => {
    const {PMTaskTypeCombo, PMTaskID} = item;

    switch (PMTaskTypeCombo) {
      case 'defect':
        navigation.push(RouteNames.DefectDetail, {
          defect: item,
          onDelete: () => {},
          onUpdate: () => {},
        });
        break;
      case 'task':
        navigation.push(RouteNames.TaskDetail, {PMTaskID});
        break;
      case 'itp':
        navigation.push(RouteNames.InspectionDetail, item);
        break;

      default:
        return;
    }
  };

  const renderRowItem = (item) => {
    const {
      PMTaskID,
      PMTaskName,
      PMTaskStartDate,
      PMTaskEndDate,
      PMTaskPriorityCombo,
      PMTaskCompletePct,
      PMTaskTypeCombo,
    } = item;

    return (
      <TouchableOpacity key={PMTaskID} onPress={handleOpenDetail(item)}>
        <View style={styles.itemContainer}>
          <View
            style={[
              styles.priority,
              {
                backgroundColor:
                  PMTaskCompletePct === 100
                    ? PRIORITY_COLOR.Complete
                    : PRIORITY_COLOR[PMTaskPriorityCombo],
              },
            ]}
          />
          <View style={styles.itemMainContent}>
            <Text style={styles.itemText}>
              [{PMTaskTypeCombo}] {PMTaskName}
            </Text>
            <Text style={styles.itemDate}>
              {moment(PMTaskStartDate).format('M/D/YY')} -{' '}
              {moment(PMTaskEndDate).format('M/D/YY')}
            </Text>
          </View>
          <Text style={styles.itemPercentText}>{PMTaskCompletePct}%</Text>
          <Icon
            style={{marginRight: 25}}
            size={12}
            color={'#9EA0A5'}
            name="angle-right"
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.headerText]}>My Open Items</Text>
      {myOpenItems.splice(0, 10).map(renderRowItem)}
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, projectOverviewDashboard}) => {
  return {
    user,
    token: user.token,
    project,
    myOpenItems: projectOverviewDashboard.myOpenItems,
  };
};

const mapDispatchToProps = {
  getMyOpenItems: actions.getMyOpenItems,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyOpenItemOverview);
