import mainStyles from './styles';

export default {
  ...mainStyles,
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },

  image: {
    height: 150,
    width: 150,
    marginRight: 15,
    borderRadius: 8,
  },
};
