import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {StatusBar, Text, View, ScrollView} from 'react-native';

import styles from './styles';
import {pathOr} from 'ramda';

import MyOpenItemOverview from './MyOpenItemOverview';
import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import TodaySchedule from './TodaySchedule';
import RecentPhotos from './RecentPhotos';
import ProjectTeam from './ProjectTeam';
import MyOpenItems from './MyOpenItems';

const ProjectOverviewDashboard = (props) => {
  const {navigation} = props;
  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <View>
            <View style={styles.row}>
              <Text style={styles.textHeader}>Dashboard</Text>
            </View>
          </View>
        </View>
        <View style={styles.row} />
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundWhite,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <ScrollView>
        <MyOpenItemOverview />
        <MyOpenItems />
        <TodaySchedule />
        <RecentPhotos />
        <ProjectTeam />
        <View style={{height: 30}} />
      </ScrollView>
    </View>
  );
};

const mapStateToProps = ({auth: {user, project, master}, drawing}) => {
  return {
    user,
    token: user.token,
    project,
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProjectOverviewDashboard);
