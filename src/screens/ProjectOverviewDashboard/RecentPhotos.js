import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Image, Text, TouchableOpacity, View, ScrollView} from 'react-native';
import {isEmpty, propOr} from 'ramda';
import {useNavigation} from '@react-navigation/native';

import styles from './RecentPhotos.styles';
import {actions} from './ProjectOverviewDashboard.reducer';
import {getPhotoUrl} from '../../api/urls';
import RouteNames from '../../RouteNames';

const RecentPhotos = (props) => {
  const {recentPhotos, project, getRecentPhotos, token} = props;
  const navigation = useNavigation();

  const PMProjectID = propOr(0, 'PMProjectID', project);

  useEffect(() => {
    getRecentPhotos(PMProjectID, token);
  }, [getRecentPhotos, PMProjectID, token]);

  if (isEmpty(recentPhotos)) {
    return <View />;
  }

  const handleViewPhoto = (item) => () => {
    const url = getPhotoUrl(item.PMPhotoServerPath);
    navigation.push(RouteNames.ImageFullScreen, {
      title: 'Photo',
      info: {
        information: '',
        uploadedBy: '',
        category: '',
        location: '',
      },
      url,
    });
  };

  const renderRowItem = (item) => {
    return (
      <TouchableOpacity key={item.PMPhotoID} onPress={handleViewPhoto(item)}>
        <Image
          resizeMode="cover"
          style={styles.image}
          source={{uri: getPhotoUrl(item.PMPhotoServerPath)}}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.headerText]}>Recent Photos</Text>
      <ScrollView horizontal style={{paddingBottom: 15}}>
        {recentPhotos.splice(0, 10).map(renderRowItem)}
      </ScrollView>
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, projectOverviewDashboard}) => {
  return {
    user,
    token: user.token,
    project,
    recentPhotos: projectOverviewDashboard.recentPhotos,
  };
};

const mapDispatchToProps = {
  getRecentPhotos: actions.getRecentPhotos,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RecentPhotos);
