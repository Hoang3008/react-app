import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import styles from './ProjectOverview.styles';
import Icon from 'react-native-vector-icons/Fontisto';
import ProjectOverviewBar from '../../components/ProjectOverviewBar/ProjectOverviewBar';

const OverdueColor = '#d33d3d';
const NextSevenDay = '#e8b83f';
const OverSevenDay = '#4dc489';

const ProjectOverview = (props) => {
  const {projectOverview, onOpenItemList} = props;

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>Project Overview</Text>
      <View style={styles.labelBar}>
        <View style={[styles.circlePoint, {backgroundColor: OverdueColor}]} />
        <Text style={[styles.text, {marginLeft: 5}]}>Overdue</Text>
        <View
          style={[
            styles.circlePoint,
            {backgroundColor: NextSevenDay, marginLeft: 15},
          ]}
        />
        <Text style={[styles.text, {marginLeft: 5}]}>Next 7 Days</Text>
        <View
          style={[
            styles.circlePoint,
            {backgroundColor: OverSevenDay, marginLeft: 15},
          ]}
        />
        <Text style={[styles.text, {marginLeft: 5}]}>Over 7 Days</Text>
      </View>

      {projectOverview.map((item) => {
        const {greaterThan7Days, name, next7Days, overdue, type} = item;

        const values = [
          {
            value: overdue,
            color: OverdueColor,
          },
          {
            value: next7Days,
            color: NextSevenDay,
          },
          {
            value: greaterThan7Days,
            color: OverSevenDay,
          },
        ];

        return (
          <TouchableOpacity key={name} onPress={onOpenItemList(type)}>
            <View style={styles.mainRow}>
              <View style={[styles.column, {marginRight: 15}]}>
                <View style={styles.row}>
                  <Text style={styles.label}>{name}</Text>
                  <Text style={[styles.label, {color: '#999', marginLeft: 10}]}>
                    {greaterThan7Days + next7Days + overdue} Item(s)
                  </Text>
                </View>
                <View
                  style={[
                    styles.row,
                    {
                      marginTop: 8,
                      justifyContent: 'space-between',
                    },
                  ]}>
                  <ProjectOverviewBar values={values} />
                </View>
              </View>
              <Icon size={12} color={'#9EA0A5'} name="angle-right" />
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default ProjectOverview;
