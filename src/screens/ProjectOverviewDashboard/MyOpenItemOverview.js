import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Text, TouchableOpacity, View} from 'react-native';
import {propOr} from 'ramda';
import Icon from 'react-native-vector-icons/Fontisto';

import styles from './MyOpenItemOverview.styles';
import {actions} from './ProjectOverviewDashboard.reducer';
import ProjectOverview from './ProjectOverview';
import {useNavigation} from '@react-navigation/native';
import RouteNames from '../../RouteNames';

const OverdueColor = '#d33d3d';
const NextSevenDay = '#e8b83f';
const OverSevenDay = '#4dc489';

const MyOpenItemOverview = (props) => {
  const {projectOverview, project, getProjectOverview, token} = props;
  const navigation = useNavigation();
  const PMProjectID = propOr(0, 'PMProjectID', project);

  useEffect(() => {
    getProjectOverview(PMProjectID, token);
  }, [getProjectOverview, PMProjectID, token]);

  const handleOpenItemList = (type) => () => {
    switch (type) {
      case 'defect':
        navigation.push(RouteNames.DefectMain);
        break;
      case 'schedule':
        navigation.push(RouteNames.ScheduleList);
        break;
      case 'task':
        navigation.push(RouteNames.TaskList);
        break;
      case 'itp':
        navigation.push(RouteNames.InspectionTabs);
        break;

      default:
        return;
    }
  };

  return (
    <View style={styles.container}>
      {/*<Text style={styles.headerText}>My Open Items</Text>*/}

      {/*<View style={styles.labelBar}>*/}
      {/*  <View style={[styles.circlePoint, {backgroundColor: OverdueColor}]} />*/}
      {/*  <Text style={[styles.text, {marginLeft: 5}]}>Overdue</Text>*/}
      {/*  <View*/}
      {/*    style={[*/}
      {/*      styles.circlePoint,*/}
      {/*      {backgroundColor: NextSevenDay, marginLeft: 15},*/}
      {/*    ]}*/}
      {/*  />*/}
      {/*  <Text style={[styles.text, {marginLeft: 5}]}>Next 7 Days</Text>*/}
      {/*  <View*/}
      {/*    style={[*/}
      {/*      styles.circlePoint,*/}
      {/*      {backgroundColor: OverSevenDay, marginLeft: 15},*/}
      {/*    ]}*/}
      {/*  />*/}
      {/*  <Text style={[styles.text, {marginLeft: 5}]}>Over 7 Days</Text>*/}
      {/*</View>*/}

      {/*{projectOverview.map((item) => {*/}
      {/*  const {greaterThan7Days, name, next7Days, overdue, type} = item;*/}
      {/*  return (*/}
      {/*    <TouchableOpacity key={name} onPress={handleOpenItemList(type)}>*/}
      {/*      <View style={styles.mainRow}>*/}
      {/*        <View style={styles.column}>*/}
      {/*          <View style={styles.row}>*/}
      {/*            <Text style={styles.label}>{name}</Text>*/}
      {/*            <Text style={[styles.label, {color: '#999', marginLeft: 10}]}>*/}
      {/*              {greaterThan7Days + next7Days + overdue} Item(s)*/}
      {/*            </Text>*/}
      {/*          </View>*/}
      {/*          <View*/}
      {/*            style={[*/}
      {/*              styles.row,*/}
      {/*              {*/}
      {/*                marginTop: 8,*/}
      {/*                justifyContent: 'space-between',*/}
      {/*                marginRight: 15,*/}
      {/*              },*/}
      {/*            ]}>*/}
      {/*            <View style={styles.item}>*/}
      {/*              <View*/}
      {/*                style={[*/}
      {/*                  styles.circlePoint,*/}
      {/*                  {backgroundColor: OverdueColor},*/}
      {/*                ]}*/}
      {/*              />*/}
      {/*              <Text style={[styles.text, {marginLeft: 5}]}>*/}
      {/*                {`${overdue}`}*/}
      {/*              </Text>*/}
      {/*            </View>*/}
      {/*            <View style={styles.item}>*/}
      {/*              <View*/}
      {/*                style={[*/}
      {/*                  styles.circlePoint,*/}
      {/*                  {backgroundColor: NextSevenDay},*/}
      {/*                ]}*/}
      {/*              />*/}
      {/*              <Text style={[styles.text, {marginLeft: 5}]}>*/}
      {/*                {next7Days}*/}
      {/*              </Text>*/}
      {/*            </View>*/}
      {/*            <View style={styles.item}>*/}
      {/*              <View*/}
      {/*                style={[*/}
      {/*                  styles.circlePoint,*/}
      {/*                  {backgroundColor: OverSevenDay},*/}
      {/*                ]}*/}
      {/*              />*/}
      {/*              <Text style={[styles.text, {marginLeft: 5}]}>*/}
      {/*                {greaterThan7Days}*/}
      {/*              </Text>*/}
      {/*            </View>*/}
      {/*          </View>*/}
      {/*        </View>*/}
      {/*        <Icon*/}
      {/*          style={{marginBottom: 0}}*/}
      {/*          size={12}*/}
      {/*          color={'#9EA0A5'}*/}
      {/*          name="angle-right"*/}
      {/*        />*/}
      {/*      </View>*/}
      {/*    </TouchableOpacity>*/}
      {/*  );*/}
      {/*})}*/}

      <ProjectOverview
        projectOverview={projectOverview}
        onOpenItemList={handleOpenItemList}
      />
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, projectOverviewDashboard}) => {
  return {
    user,
    token: user.token,
    project,
    projectOverview: projectOverviewDashboard.projectOverview,
  };
};

const mapDispatchToProps = {
  getProjectOverview: actions.getProjectOverview,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyOpenItemOverview);
