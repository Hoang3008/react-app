import mainStyles from './styles';

export default {
  ...mainStyles,
  container: {
    display: 'flex',
    flexDirection: 'column',
  },

  row: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  circlePoint: {
    width: 16,
    height: 16,
    borderRadius: 8,
  },
};
