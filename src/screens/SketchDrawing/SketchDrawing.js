import React, {useRef} from 'react';
import {StatusBar, StyleSheet, Text, View, Platform} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';
import RNSketchCanvas from '@terrylinla/react-native-sketch-canvas';

import Theme from '../../Theme';
import {UniButton, UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {pathOr} from 'ramda';

const URI_PREFIX = Platform.OS === 'ios' ? '' : 'file://';

const SketchDrawing = (props) => {
  const {navigation, route} = props;

  // const imagePath = pathOr('', ['state', 'params', 'imagePath'], navigation);
  // const onSave = pathOr(null, ['state', 'params', 'onSave'], navigation);

  const imagePath = pathOr('', ['params', 'imagePath'], route);
  const onSave = pathOr(null, ['params', 'onSave'], route);

  const canvasRef = useRef(null);

  const handleSave = () => {
    canvasRef.current.save();
  };

  const handleSketchSaved = (success, path, ...params) => {
    if (success) {
      saveImage(path);
      try {
        onSave(`${URI_PREFIX}${path}`);
      } catch (error) {}
      navigation.goBack();
    }
  };

  const saveImage = (path) => {
    CameraRoll.saveToCameraRoll(path);
  };

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => props.navigation && props.navigation.goBack()}
          />
          <Text style={styles.textHeader}>Sketch Drawing</Text>
        </View>
        <UniButton
          text={'Save'}
          onPress={handleSave}
          containerStyle={styles.button}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{flex: 1, flexDirection: 'row'}}>
        <RNSketchCanvas
          ref={canvasRef}
          onSketchSaved={handleSketchSaved}
          containerStyle={{backgroundColor: 'transparent', flex: 1}}
          canvasStyle={{backgroundColor: 'transparent', flex: 1}}
          defaultStrokeIndex={1}
          defaultStrokeWidth={5}
          localSourceImage={{
            filename: imagePath.replace('file://', ''),
            directory: '',
            mode: 'AspectFill',
          }}
          undoComponent={
            <View style={styles.functionButton}>
              <Text style={{color: 'white'}}>Undo</Text>
            </View>
          }
          clearComponent={
            <View style={styles.functionButton}>
              <Text style={{color: 'white'}}>Clear</Text>
            </View>
          }
          strokeComponent={(color) => (
            <View
              style={[{backgroundColor: color}, styles.strokeColorButton]}
            />
          )}
          strokeSelectedComponent={(color, index, changed) => {
            return (
              <View
                style={[
                  {backgroundColor: color, borderWidth: 2},
                  styles.strokeColorButton,
                ]}
              />
            );
          }}
          strokeWidthComponent={(w) => {
            return (
              <View style={styles.strokeWidthButton}>
                <View
                  style={{
                    backgroundColor: 'white',
                    marginHorizontal: 2.5,
                    width: Math.sqrt(w / 3) * 10,
                    height: Math.sqrt(w / 3) * 10,
                    borderRadius: (Math.sqrt(w / 3) * 10) / 2,
                  }}
                />
              </View>
            );
          }}

          savePreference={() => {
            return {
              folder: 'unicore',
              filename: String(Math.ceil(Math.random() * 100000000)),
              transparent: false,
              imageType: 'jpg',
              includeImage: true,
              includeText: false,
              cropToImageSize: true,
            };
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  strokeColorButton: {
    marginHorizontal: 2.5,
    marginVertical: 8,
    width: 30,
    height: 30,
    borderRadius: 15,
  },
  strokeWidthButton: {
    marginHorizontal: 2.5,
    marginVertical: 8,
    width: 30,
    height: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Theme.colors.accent,
  },
  functionButton: {
    marginHorizontal: 2.5,
    marginVertical: 8,
    height: 30,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.accent,
    backgroundColor: Theme.colors.accent,
  },
  button: {
    marginRight: Theme.margin.m15,
  },
  buttonContainerActive: {
    marginTop: Theme.margin.m10,
    padding: Theme.margin.m8,
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.accent,
    backgroundColor: Theme.colors.accent,
  },
  textButtonActive: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textWhite,
  },
});

export default SketchDrawing;
