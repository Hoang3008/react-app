import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {StatusBar, Text, View, Platform, TouchableOpacity} from 'react-native';
import {WebView} from 'react-native-webview';
import NetInfo from '@react-native-community/netinfo';
import {isEmpty, pathOr, propOr} from 'ramda';
import RNFS from 'react-native-fs';
import StaticServer from 'react-native-static-server';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import RouteNames from '../../RouteNames';
import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import {EMPTY, OFFLINE_ACTION_TYPES} from '../../global/constants';
import {getFileName} from '../defect/DefectHelper';
import {getDefaultDefect} from '../DefectDetail/DefectDetail';
import {requestDeleteTask} from '../../api/defect';
import Configs from '../../global/configs';
import IconOcticons from 'react-native-vector-icons/Octicons';


const WebViewInstanceObject = {};

const getWebViewInstanceByLink = (link, onMessage, onLoadEnd) => {
  if (!(link in WebViewInstanceObject)) {
    const webviewRef = React.createRef();
    const instance = (
      <WebView
        key={link}
        source={{
          uri: link,
        }}
        ref={webviewRef}
        style={{flex: 1}}
        domStorageEnabled
        javaScriptEnabled
        cacheEnabled
        onMessage={onMessage}
        onLoadEnd={onLoadEnd}
        onError={(syntheticEvent) => {
          const {nativeEvent} = syntheticEvent;
          console.warn('WebView error: ', nativeEvent);
        }}
      />
    );

    WebViewInstanceObject[link] = {
      id: Date.now(),
      firstTime: true,
      webviewRef,
      instance,
    };
  } else {
    WebViewInstanceObject[link].firstTime = false;
  }

  return WebViewInstanceObject[link];
};

const getPath = () => {
  return Platform.OS === 'android'
    ? RNFS.DocumentDirectoryPath + '/www'
    : RNFS.MainBundlePath + '/www';
};

const moveAndroidFiles = async () => {
  if (Platform.OS === 'android') {
    await RNFS.mkdir(RNFS.DocumentDirectoryPath + '/www');
    const files = [
      'www/Complete.svg',
      'www/Overdue.svg',
      'www/Priority1.svg',
      'www/Priority23.svg',
      'www/Verify.svg',
    ];
    await files.forEach(async (file) => {
      await RNFS.copyFileAssets(file, RNFS.DocumentDirectoryPath + '/' + file);
    });
  }
};

function DrawingComponent(props) {
  const {
    user,
    token,
    navigation,
    project,
    getDefectListByDrawingId,
    allDefects,
    addActionIntoQueue,
  } = props;
  const [showLoading, setShowLoading] = useState(true);
  const [localHostURL, setLocalHostURL] = useState('');
  const [imageData, setImageData] = useState(null);
  const [firstTime, setFirstTime] = useState(true);
  const [defects, setDefects] = useState([]);
  const [isSelected, setSelection] = useState(false);

  const [selectedCategories, setSelectedCategories] = useState([]);
  const [selectedPeople, setSelectedPeople] = useState([]);
  const [selectedPriority, setSelectedPriority] = useState([]);
  const [selectedLocation, setSelectedLocation] = useState([]);

  useEffect(() => {
    const filteredDefect = filterDefectByConditions();
    setDefects(filteredDefect);
  }, [
    allDefects,
    setDefects,
    selectedCategories,
    selectedPeople,
    selectedPriority,
    selectedLocation,
    isSelected,
  ]);

  const getPath = () => {
    return Platform.OS === 'android'
      ? RNFS.DocumentDirectoryPath + '/www'
      : RNFS.MainBundlePath + '/www';
  };

  const moveAndroidFiles = async () => {
    if (Platform.OS === 'android') {
      await RNFS.mkdir(RNFS.DocumentDirectoryPath + '/www');
      const files = [
        'www/Complete.svg',
        'www/Overdue.svg',
        'www/Priority1.svg',
        'www/Priority23.svg',
        'www/Verify.svg',
      ];
      await files.forEach(async (file) => {
        await RNFS.copyFileAssets(
          file,
          RNFS.DocumentDirectoryPath + '/' + file,
        );
      });
    }
  };

  const drawingId = pathOr('', ['route', 'params', 'PPDrawingID'], props);
  const time = pathOr(0, ['route', 'params', 'time'], props);
  const userId = user.ADUserID;

  const uri = `${Configs.drawingBasedURL}/projects/${
    project.PMProjectID
  }/drawing/${drawingId}/offline-mobile?userid=${userId}&token=${token}`;

  const filterDefectByConditions = (isFrist = false) => {
    const filteredDefect = allDefects.filter((item) => {
      const {
        FK_ADUserID,
        FK_ADAssignUserID,
        FK_PMProjectCategoryID,
        PMTaskPriorityCombo,
        FK_PMProjectLocationID,
      } = item;

      if (isFrist && (userId == FK_ADAssignUserID || userId == FK_ADUserID)) {
        return true;
      } else if (!isFrist) {
        if (
          !isEmpty(selectedCategories) &&
          !selectedCategories.includes(FK_PMProjectCategoryID)
        ) {
          return false;
        }

        if (
          !isEmpty(selectedPeople) &&
          !selectedPeople.includes(FK_ADAssignUserID) &&
          !selectedPeople.includes(FK_ADUserID)
        ) {
          return false;
        }

        if (
          !isEmpty(selectedPriority) &&
          !selectedPriority.includes(PMTaskPriorityCombo)
        ) {
          return false;
        }

        if (
          !isEmpty(selectedLocation) &&
          !selectedLocation.includes(FK_PMProjectLocationID)
        ) {
          return false;
        }

        //false : My defect
        // isFrist true: my defect
        if (
          !isSelected &&
          (userId == FK_ADAssignUserID || userId == FK_ADUserID)
        ) {
          return true;
        }

        if (isSelected) {
          return true;
        }
      }
    });

    return filteredDefect;
  };

  useEffect(() => {
    reload();
  }, [user, drawingId, defects]);

  //each click drawing will reset options filter
  useEffect(() => {
    setSelection(false);
    setSelectedCategories([]);
    setSelectedCategories([]);
    setSelectedPeople([]);
    setSelectedPriority([]);
    setSelectedLocation([]);
  }, [drawingId]);

  const handleMessage = (event) => {
    const eventData = JSON.parse(pathOr('{}', ['nativeEvent', 'data'], event));
    let projectId = project?.PMProjectID;

    console.log('OnMessage', eventData);

    const {type, data} = eventData;

    switch (type) {
      case 'MARKER_LONG_CLICK':
        captureImage(eventData);
        return;
      case 'MAP_LONG_CLICK':
        captureImage(eventData);
        return;
      case 'DELETE_MARKER':
        requestDeleteTask(data).then((res) => {
          getDefectListByDrawingId({user, drawingId, projectId}, token);
        });
        return;
      case 'MAP_READY_TO_LOADED':
        const allDefectData = {
          type: 'DEFECT_LIST',
          data: filterDefectByConditions(true),
        };
        webviewRef.current.postMessage(JSON.stringify(allDefectData));
        return;
      case 'MOVE_MARKER':
        addActionIntoQueue({
          type: OFFLINE_ACTION_TYPES.UPDATE_DEFECT,
          data: data,
        });
        return;
    }

    const lat = propOr(0, 'lat', data);
    const lng = propOr(0, 'lng', data);

    navigation.push(RouteNames.DefectDetail, {
      lat,
      lng,
      drawingId,
      drawingPage: true,
      onDelete: () => reload(),
      onUpdate: () => reload(),
    });
  };

  const {
    webviewRef,
    firstTime: firstTimeLoadWebview,
  } = getWebViewInstanceByLink(uri, handleMessage, () => setShowLoading(true));

  if (firstTimeLoadWebview && firstTimeLoadWebview !== firstTime) {
    setFirstTime(firstTimeLoadWebview);
  }

  useEffect(() => {
    NetInfo.fetch().then((state) => {
      const error = pathOr(
        'IDLE',
        ['current', 'state', 'viewState'],
        webviewRef,
      );

      const isError = error === 'ERROR';

      if (state.isConnected && webviewRef.current && isError) {
        setFirstTime(true);
        setShowLoading(true);
        webviewRef.current.reload();
      }
    });
  }, [time]);

  useEffect(() => {
    if (!imageData) {
      return;
    }

    const {lat, lng, path} = imageData;

    const defect = defects.find(
      ({PMTaskLat, PMTaskLng}) => PMTaskLat === lat && lng === PMTaskLng,
    );

    if (!defect) {
      return;
    }

    const image = {
      uri: path || '',
      filename: getFileName(path),
      type: 'image/png',
    };

    const {ADUserID} = user || {};
    const {PMTaskID, offlineId} = defect || {};

    const body = {AACreatedUser: ADUserID, FK_PMTaskID: PMTaskID || offlineId};

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.ADD_PHOTO,
      data: {body, image},
    });
    setImageData(null);
  }, [imageData, defects]);

  useEffect(() => {
    let server = new StaticServer(3000, getPath(), {
      localOnly: true,
      keepAlive: true,
    });
    moveAndroidFiles()
      .then(() => server.start())
      .then((url) => setLocalHostURL(url));

    return () => server.stop();
  }, []);

  useEffect(() => {
    if (isEmpty(defects) || showLoading || !localHostURL) {
      return;
    }

    const data = {
      type: 'DEFECT_LIST',
      data: defects,
    };

    if (webviewRef.current && !firstTime) {
      webviewRef.current.postMessage(JSON.stringify(data));
      return;
    }

    setTimeout(() => {
      if (!webviewRef.current) {
        return;
      }

      webviewRef.current.postMessage(JSON.stringify(data));
      setFirstTime(false);
    }, 3000);
  }, [defects, showLoading, localHostURL, webviewRef.current]);

  const reload = () => {
    if (!webviewRef.current) {
      return;
    }

    const data = {
      type: 'DEFECT_LIST',
      data: defects,
    };

    console.log(defects);

    webviewRef.current.postMessage(JSON.stringify(data));
  };

  const captureImage = (eventData) => {
    const {
      data: {lat, lng, ...rest},
    } = eventData;

    const restProps = rest || {};

    navigation.navigate(RouteNames.CaptureImage, {
      onSave: (path) => {
        const defect = {
          ...getDefaultDefect(user, project, lat, lng, drawingId),
          ...restProps,
          PMTaskID: undefined,
        };

        addActionIntoQueue({
          type: OFFLINE_ACTION_TYPES.CREATE_DEFECT,
          data: defect,
        });

        setImageData({
          path,
          lat: lat || restProps.PMTaskLat,
          lng: lng || restProps.PMTaskLng,
        });
      },
    });
  };

  //Click reload defects with drawingId
  const onClickReloadDefects = async () => {
    let projectId = project?.PMProjectID;
    let response = await getDefectListByDrawingId(
      {user, drawingId, projectId},
      token,
    );

    if (response) {
      reload();
    }
  };

  const renderNavigation = () => {
    const index = pathOr('', ['route', 'params', 'index'], props);
    const PPDrawingNo = pathOr('', ['route', 'params', 'PPDrawingNo'], props);
    const PPDrawingName = pathOr(
      '',
      ['route', 'params', 'PPDrawingName'],
      props,
    );
    const subtitle = `#${index + 1} | ${PPDrawingNo}`;

    return (
      <View style={styles.navigationBar}>
        <View style={{...styles.row, flex: 1}}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => navigation.goBack()}
          />
          <View>
            <Text style={styles.textHeaderSmall}>{subtitle}</Text>
            <View style={styles.row}>
              <Text style={styles.textHeader} numberOfLines={1}>
                {PPDrawingName}
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity onPress={() => onClickReloadDefects()}>
          <IconOcticons
            style={{marginLeft: 6, marginRight: 0}}
            size={24}
            color={'#fff'}
            name="sync"
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.push(RouteNames.DrawingFilter, {
              selectedCategories,
              selectedPeople,
              selectedPriority,
              selectedLocation,
              isSelected,
              onFilter: (item) => {
                const {
                  selectedCategories,
                  selectedPeople,
                  selectedPriority,
                  selectedLocation,
                  isSelected,
                } = item;

                setSelectedCategories(selectedCategories);
                setSelectedPeople(selectedPeople);
                setSelectedPriority(selectedPriority);
                setSelectedLocation(selectedLocation);
                setSelection(isSelected);
              },
            })
          }>
          <IconOcticons
            style={{marginLeft: 6, marginRight: 0}}
            size={26}
            color={'#fff'}
            name="settings"
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      {Object.keys(WebViewInstanceObject).map((key) => {
        const {instance} = WebViewInstanceObject[key];

        return (
          <View
            key={key}
            style={{display: key === uri ? undefined : 'none', flex: 1}}>
            {instance}
          </View>
        );
      })}
    </View>
  );
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
    marginRight: 60,
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m52,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  sectionHeaderText: {
    flex: 1,
    paddingHorizontal: Theme.margin.m12,
    paddingBottom: Theme.margin.m6,
    paddingTop: Theme.margin.m22,
    color: Theme.colors.textNormal,
    backgroundColor: Theme.colors.backgroundHeader,
  },
  floatButton: {
    position: 'absolute',
    bottom: Theme.margin.m16,
    right: Theme.margin.m20,
    width: Theme.specifications.floatButtonSize,
    height: Theme.specifications.floatButtonSize,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#34AA44',
    borderRadius: Theme.specifications.floatButtonSize / 2,
    borderWidth: 0,
  },
};

const mapStateToProps = ({auth: {user, project}, commonData}, props) => {
  const drawingId = pathOr('', ['route', 'params', 'PPDrawingID'], props);
  const PMProjectID = propOr('', 'PMProjectID', project);

  console.log('Syncing mapStateToProps in Drawing: ', project, user);

  const defectList = pathOr(
    EMPTY.ARRAY,
    ['defectListByDrawingId', drawingId],
    commonData,
  );
  const categoryList = pathOr(
    EMPTY.ARRAY,
    ['currentProject', 'categories'],
    commonData,
  );

  //filter defect from list all
  const allDefects = defectList.map((item) => ({
    ...item,
    PMProjectCategory: categoryList.find(
      ({PMProjectCategoryID}) =>
        PMProjectCategoryID === item.FK_PMProjectCategoryID,
    ),
  }));

  return {
    user,
    token: user.token,
    project,
    allDefects,
  };
};

const mapDispatchToProps = {
  getDefectListByDrawingId: actions.getDefectListByDrawingId,
  addActionIntoQueue: actions.addActionIntoQueue,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawingComponent);

// PMTasks/GetAllDataByPPDrawingIDAndUserID?PPDrawingID=317&ADUserID=1
