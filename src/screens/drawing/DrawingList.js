import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {
  StatusBar,
  ActivityIndicator,
  Text,
  View,
  TouchableOpacity,
  Image,
  SectionList,
} from 'react-native';
import {groupBy, pathOr, propOr} from 'ramda';
import {SearchBar} from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {getDrawingListAction} from '../../action/DrawingActions';
import RouteNames from '../../RouteNames';
import {actions as commonActions} from '../CommonProjectData/CommonProjectData.reducer';
import {actions} from '../NewDrawing/NewDrawing.reducer';

function DrawingListComponent(props) {
  const {
    token,
    shouldShowTitleBar = true,
    project,
    drawingList,
    getDrawingList,
    navigation,
    categories,
    updateDrawing,
    user,
    getDefectListByDrawingId,
    defectListByDrawingId,
  } = props;
  const [sortData, setSortData] = useState(props.sortData);
  const [filteredDrawingList, setFilteredDrawingList] = useState([]);
  const [showSearchBox, setShowSearchBox] = useState(props.showSearchBox);
  const [search, setSearch] = useState('');
  const time = pathOr(0, ['route', 'params', 'time'], props);

  const areaId = pathOr(undefined, ['route', 'params', 'areaId'], props);
  const PMProjectID = propOr(0, 'PMProjectID', project);

  useEffect(() => {
    setSortData(props.sortData);
    setShowSearchBox(props.showSearchBox);
  }, [props.showSearchBox, props.sortData]);

  useEffect(() => {
    const lowerCaseSearch = (search || '').toLowerCase();

    const filteredDrawingList = !search
      ? drawingList
      : drawingList.filter(({PPDrawingName}) =>
          PPDrawingName.toLowerCase().includes(lowerCaseSearch),
        );

    const result = groupBy(({FK_PPProjectDiscID}) => {
      const result = categories.find(
        ({PPProjectDiscID}) => PPProjectDiscID === FK_PPProjectDiscID,
      );

      if (!result) {
        return '';
      }

      return result['PPProjectDiscName'];
    }, filteredDrawingList);

    let listKeys = Object.keys(result);

    if (sortData) {
      listKeys = listKeys.sort((a1, b1) => {
        const a = (a1 || '').toLowerCase();
        const b = (b1 || '').toLowerCase();

        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }

        return 0;
      });
    }

    setFilteredDrawingList(
      listKeys.map((key) => {
        let data = result[key] || [];

        if (sortData) {
          data = data.sort((a, b) => {
            const aPPDrawingName = (a.PPDrawingName || '').toLowerCase();
            const bPPDrawingName = (b.PPDrawingName || '').toLowerCase();

            if (aPPDrawingName < bPPDrawingName) {
              return -1;
            }
            if (aPPDrawingName > bPPDrawingName) {
              return 1;
            }

            return 0;
          });
        }

        return {
          title: key,
          data,
        };
      }),
    );

    const unsubscribeList = {};

    drawingList.forEach((drawing) => {
      const {PPDrawingID, PPDrawingServerPath} = drawing;
      if (!PPDrawingServerPath) {
        unsubscribeList[PPDrawingID] = firestore()
          .doc(`pdf2tiles/${PPDrawingID}`)
          .onSnapshot((snapshot) => {
            const {status, url} = propOr({}, '_data', snapshot);

            if (status !== 'DONE') {
              return;
            }

            updateDrawingDone(drawing, url).then((response) => {
              const {data} = response;

              if (typeof data === 'string') {
                return;
              }

              loadDrawingList();

              unsubscribeList[PPDrawingID]();
              unsubscribeList[PPDrawingID] = undefined;
            });
          });
      }
    });

    return () => {
      Object.keys(unsubscribeList).forEach(
        (id) => unsubscribeList[id] && unsubscribeList[id](),
      );
    };
  }, [drawingList, categories, search, sortData]);

  useEffect(() => {
    loadDrawingList();
  }, [time]);

  const loadDrawingList = () => {
    if (PMProjectID) {
      getDrawingList(areaId, PMProjectID, token);
    }
  };

  const updateDrawingDone = (drawing, url) =>
    updateDrawing({
      ...drawing,
      PPDrawingServerPath: url,
      PPDrawingStatus: 'SUCCESS',
      PMProjectID,
    });

  const onPressNewDrawing = () => {
    navigation.push(RouteNames.NewDrawing, {
      areaId,
      onDone: () => loadDrawingList(),
    });
  };

  const handleItemClick = (item, index) => {
    const {user, project, token, defectListByDrawingId} = props;
    const projectId = project.PMProjectID;
    const drawingId = item?.PPDrawingID;
    const defectList = pathOr(undefined, [drawingId], defectListByDrawingId);
    navigation.navigate(RouteNames.DrawingScreen, {
      ...item,
      index,
      time: Date.now(),
    });
  };

  const toggleShowSearch = () => {
    setShowSearchBox(!showSearchBox);
    setSearch('');
  };

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m16,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => {
              // props.navigation.navigate(RouteNames.BottomTabs);
              props.navigation.goBack();
            }}
          />
          <Text style={styles.textHeader}>Drawing List</Text>
        </View>
        <View style={styles.row}>
          <TouchableOpacity onPress={() => setSortData(!sortData)}>
            <Image
              style={styles.navIcon}
              source={require('../../assets/img/ic_font.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={toggleShowSearch}>
            <Image
              style={{
                width: Theme.specifications.icSmall,
                height: Theme.specifications.icSmall,
              }}
              source={require('../../assets/img/ic_search.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const renderDrawingItem = ({item, index}) => {
    return (
      <TouchableOpacity
        disabled={item.PPDrawingStatus !== 'SUCCESS'}
        onPress={() => handleItemClick(item, index)}>
        <View
          style={{
            padding: 15,
            display: 'flex',
            flexDirection: 'column',
            shadowColor: '#555',
            backgroundColor: 'white',
            borderRadius: 5,
          }}>
          <Text
            style={{
              fontSize: 12,
              color: '#9EA0A5',
            }}>
            {`#${index + 1} | ${item.PPDrawingNo}`}
          </Text>
          <Text
            style={{
              marginTop: 7,
              fontSize: 16,
              color: '#3E3F42',
            }}>
            {item.PPDrawingName}
          </Text>

          {item.PPDrawingStatus !== 'SUCCESS' && (
            <ActivityIndicator
              style={{position: 'absolute', right: 10, top: 25}}
              size="small"
              color="#555"
            />
          )}
        </View>
      </TouchableOpacity>
    );
  };

  const renderSectionHeader = (title) => {
    return (
      <Text
        style={{
          paddingHorizontal: Theme.margin.m15,
          paddingBottom: Theme.margin.m6,
          paddingTop: Theme.margin.m22,
          color: Theme.colors.textNormal,
          backgroundColor: Theme.colors.backgroundHeader,
        }}>
        {title}
      </Text>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundColor}}>
      {shouldShowTitleBar && (
        <>
          <StatusBar
            backgroundColor={Theme.colors.primary}
            barStyle="light-content"
          />
          <UniStatusBarBackground />
          {renderNavigation()}
        </>
      )}
      {showSearchBox && (
        <SearchBar
          placeholder="Type Here..."
          value={search}
          onChangeText={setSearch}
          inputStyle={{
            color: Theme.colors.textNormal,
          }}
          containerStyle={{
            borderBottomColor: '#0000',
            backgroundColor: '#ffffff',
          }}
          inputContainerStyle={{
            backgroundColor: '#ddd',
          }}
        />
      )}
      <SectionList
        sections={filteredDrawingList}
        keyExtractor={(item, index) => item.PPDrawingID}
        renderItem={renderDrawingItem}
        renderSectionHeader={({section: {title}}) => renderSectionHeader(title)}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
      />
      <UniImageButton
        containerStyle={styles.floatButton}
        source={require('../../assets/img/ic_add.png')}
        onPress={onPressNewDrawing}
      />
    </View>
  );
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m52,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  sectionHeaderText: {
    flex: 1,
    paddingHorizontal: Theme.margin.m12,
    paddingBottom: Theme.margin.m6,
    paddingTop: Theme.margin.m22,
    color: Theme.colors.textNormal,
    backgroundColor: Theme.colors.backgroundHeader,
  },
  floatButton: {
    position: 'absolute',
    bottom: Theme.margin.m16,
    right: Theme.margin.m20,
    width: Theme.specifications.floatButtonSize,
    height: Theme.specifications.floatButtonSize,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#34AA44',
    borderRadius: Theme.specifications.floatButtonSize / 2,
    borderWidth: 0,
  },
};

const mapStateToProps = ({
  auth: {user, project, master},
  drawing,
  commonData,
}) => {
  return {
    user,
    token: user.token,
    project,
    drawingList: propOr([], 'drawingList', drawing),
    categories: propOr([], 'categories', master),
    defectListByDrawingId: propOr([], 'defectListByDrawingId', commonData),
  };
};

const mapDispatchToProps = {
  getDrawingList: getDrawingListAction,
  updateDrawing: actions.updateDrawing,
  getDefectListByDrawingId: commonActions.getDefectListByDrawingId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawingListComponent);
