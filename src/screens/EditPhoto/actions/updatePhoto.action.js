import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';
import {createFormData} from '../../../api/defect';
import AsyncStorage from '@react-native-community/async-storage';

export const actionType = 'PHOTOS/UPDATE_PHOTO';

// https://api.rocez.com/PMPhotos/UpdateObject
//   Request Method: PUT

const addPhotoAPI = (body) => {
  const photoId = body.PMPhotoID;
  const projectId = body.FK_PMProjectID;

  const data = {
    FK_PMProjectCategoryID: body?.FK_PMProjectCategoryID,
    FK_PMProjectLocationID: body?.FK_PMProjectLocationID,
    PMPhotoIsPrivate: false,
    PMPhotoDesc: body?.PMPhotoDesc
  };
  
  return AsyncStorage.getItem('token').then((token) => {
    return API.put(
      withKey(`/photo/${projectId}/${photoId}`),
      {
        ...data
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, addPhotoAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  console.log(payload);
  return state;
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
