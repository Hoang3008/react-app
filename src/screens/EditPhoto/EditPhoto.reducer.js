import typeToReducer from 'type-to-reducer';
import updatePhoto from './actions/updatePhoto.action';

const initialState = {};

export const actions = {
  updatePhoto: updatePhoto.action,
};

export default typeToReducer(
  {
    ...updatePhoto.reducer,
  },
  initialState,
);
