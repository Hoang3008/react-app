import React, {useEffect, useState, useRef} from 'react';
import {connect} from 'react-redux';
import {StatusBar, Text, View, ScrollView} from 'react-native';
import {pathOr, propOr} from 'ramda';
import MultiSelect from 'react-native-multiple-select';
import styles from './EditPhoto.styles';
import {actions} from './EditPhoto.reducer';
import Theme from '../../Theme';
import {
  UniButton,
  UniDropbox,
  UniImageButton,
  UniInput,
  UniStatusBarBackground,
  UniMultiSelect
} from '../../components';
import {EMPTY} from '../../global/constants';
import moment from 'moment';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import UpdateWather from './../../action/UpdateWather';

const EditPhoto = (props) => {
  const {categories, locations, navigation, updatePhoto, item, canChange, peopleInProject, projectId, taskId,} = props;
  const [information, setInformation] = useState('');
  const [categoryId, setCategoryId] = useState(0);
  const [locationId, setLocationId] = useState(0);
  const [selectedPriority, setSelectedPriority] = useState([]);
  const [selectedPeople, setSelectedPeople] = useState([]);

  const peopleRef = useRef(null);
  const dataPeople = [];


  useEffect(() => {
    setInformation(item.PMPhotoDesc);
    setCategoryId(item.FK_PMProjectCategoryID);
    setLocationId(item.FK_PMProjectLocationID);
  }, [item]);

  const handleSavePhoto = () => {
    updatePhoto({
      ...item,
      FK_PMProjectCategoryID: categoryId,
      FK_PMProjectLocationID: locationId,
      PMPhotoDesc: information,
    }).then(() => navigation.goBack());
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={styles.row}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <View>
          <Text style={styles.textHeader}>Edit Photo</Text>
        </View>
      </View>
    </View>
  );

  return (
    <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="padding" enabled>
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {renderNavigation()}
        <ScrollView>
          <UniInput
            numberOfLines={3}
            placeholder="Photo information"
            forceValueFromProp
            value={information}
            onChangeText={setInformation}
            containerStyle={styles.editTextDescription}
          />

          <UniDropbox
            label={'Category'}
            selectedKey="PMProjectCategoryID"
            selectedValue={categoryId}
            options={categories}
            pathLeft={'PMProjectCategoryNo'}
            pathValue={'PMProjectCategoryName'}
            onSelect={(no, {PMProjectCategoryID}) =>
              setCategoryId(PMProjectCategoryID)
            }
          />

          <UniDropbox
            label={'Location'}
            selectedKey="PMProjectLocationID"
            selectedValue={locationId}
            options={locations}
            pathLeft={'PMProjectLocationNo'}
            pathValue={'PMProjectLocationName'}
            onSelect={(no, {PMProjectLocationID}) =>
              setLocationId(PMProjectLocationID)
            }
          />
          {
          taskId &&  
          <UniMultiSelect 
              projectId={projectId}
              taskId={taskId}
              canChange={canChange}
              type={'photo'}
            />
            }
           
          <View style={[styles.sectionView, {marginTop: 15}]}>
            <View style={[styles.headerContainer]}>
              <Text style={[styles.headerText]}>General Information</Text>
            </View>
            <View style={styles.generalRow}>
              <Text style={styles.generalLeftView}>Uploaded By:</Text>
              <Text style={[styles.normalText, {color: '#245894'}]}>
                {item.AACreatedUser}
              </Text>
            </View>
            <View style={styles.generalRow}>
              <Text style={styles.generalLeftView}>Uploaded On:</Text>
              <Text style={[styles.normalText]}>
                {moment(item.AACreatedDate).format('MMM DD, YYYY at hh:mm:ss')}
              </Text>
            </View>
            <View style={styles.generalRow}>
              <Text style={styles.generalLeftView}>File Name:</Text>
              <Text style={[styles.normalText, {flex: 1}]}>
                {item.PMPhotoServerPath}
              </Text>
            </View>
          </View>

          <View
            style={{
              alignItems: 'flex-end',
              marginTop: 20,
              marginRight: Theme.margin.m16,
            }}>
            <UniButton
              text={'Save'}
              onPress={handleSavePhoto}
              containerStyle={styles.button}
              disabled={!props?.canChange}
            />
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingViewWrapper>
  );
};

const mapStateToProps = (
  {auth: {user, project}, commonData, photoList},
  props,
) => {
  const PMProjectID = propOr(0, 'PMProjectID', project);
  const item = pathOr(undefined, ['route', 'params', 'item'], props);

  const photos = photoList.photoList;
  const AACreatedUser = propOr('', 'AACreatedUser', item);

  const permissions = commonData.Permission.find(x => x.module === 'photo');
  const canChange =  item?.edit;

  return {
    user,
    project,
    categories: pathOr(EMPTY.ARRAY, ['currentProject', 'categories'], commonData),
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    item: photos.find(({PMPhotoID}) => PMPhotoID === item.PMPhotoID),
    canChange,
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    projectId:PMProjectID,
    taskId: item?.PMPhotoID
  };
};

const mapDispatchToProps = {
  updatePhoto: actions.updatePhoto,
  UpdateWather: UpdateWather.action
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditPhoto);
