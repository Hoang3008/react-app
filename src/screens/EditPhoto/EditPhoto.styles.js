import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textSectionHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNormal,
    fontWeight: '600',
  },
  priorityContainer: {
    width: Theme.specifications.icBig,
    height: Theme.specifications.icBig,
    marginHorizontal: Theme.margin.m5,
    borderRadius: Theme.specifications.icBig / 2,
    borderWidth: 1,
    borderColor: Theme.colors.border,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNormalSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
  },
  textSmallSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.small_12,
  },
  button: {
    marginLeft: Theme.margin.m15,
    marginVertical: Theme.margin.m16,
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },

  editTextDescription: {
    textAlignVertical: 'top',
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
    paddingTop: 10,
    height: 100,
  },
  imageContainer: {
    width: '100%',
    height: 200,
    backgroundColor: '#efefef',
  },
  image: {
    width: '100%',
    height: '100%',
  },
  sectionView: {
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },
  headerContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  normalText: {
    fontSize: 14,
    lineHeight: 18,
    color: '#3E3F42',
  },
  headerText: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    color: '#3E3F42',
  },
  generalRow: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    width: '100%',
    marginTop: 8,
  },
  generalLeftView: {
    width: 120,
  },
};
