import React, {useState} from 'react';
import {connect} from 'react-redux';
import {StatusBar, Text, Image, View, ScrollView} from 'react-native';
import {pathOr, propOr} from 'ramda';
import Carousel, {Pagination} from 'react-native-snap-carousel';

import styles from './AddPhoto.styles';
import {actions} from './AddPhoto.reducer';

import Theme from '../../Theme';
import {
  UniButton,
  UniDropbox,
  UniImageButton,
  UniStatusBarBackground,
  UniCheckbox,
  UniText
} from '../../components';
import {EMPTY} from '../../global/constants';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {getFileName} from '../defect/DefectHelper';

const AddPhoto = (props) => {
  const {project, categories, locations, navigation, addPhoto} = props;
  const [information, setInformation] = useState('');
  const [categoryId, setCategoryId] = useState(0);
  const [locationId, setLocationId] = useState(0);
  const [activeSlide, setActiveSlide] = useState(0);
  const [isSelected, setSelection] = useState(false);
  const [isAddingPhoto, setAddingPhoto] = useState(false);

  const images = pathOr([], ['route', 'params', 'images'], props);
  const folderId = pathOr([], ['route', 'params', 'folderId'], props);

  const uploadPhoto = (path, isSelected) => {
    const file = {
      uri: path || '',
      filename: getFileName(path),
      type: 'image/png',
    };

    return addPhoto(
      project.PMProjectID,
      {
        categoryId,
        locationId
        // PMPhotoDesc: information,
      },
      file,
      isSelected,
      folderId
    );
  };

  const handleAddPhoto = () => {
    setAddingPhoto(true);
    Promise.all(images.map(({path}) => uploadPhoto(path, isSelected))).then(() =>
      navigation.goBack(),
    ).catch((err) => {
      alert("Add more photo error");
      setAddingPhoto(false);
    });
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={styles.row}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <View>
          <Text style={styles.textHeader}>Add Photo</Text>
        </View>
      </View>
    </View>
  );

  const renderPhotoItem = ({item}) => {
    return (
      <View style={{flex: 1}}>
        <Image
          style={styles.image}
          resizeMode="contain"
          source={{uri: item.path}}
        />
      </View>
    );
  };

  const renderImages = () => {
    return (
      <View style={styles.imageContainer}>
        {images.length > 0 ? (
          <View style={[styles.imageContainer, {backgroundColor: '#efefef'}]}>
            <Carousel
              slideStyle={{
                width: SCREEN_WIDTH,
                backgroundColor: '#efefef',
              }}
              data={images}
              renderItem={renderPhotoItem}
              sliderWidth={SCREEN_WIDTH}
              itemWidth={SCREEN_WIDTH}
              inactiveSlideOpacity={1}
              inactiveSlideScale={1}
              onSnapToItem={setActiveSlide}
              useScrollView={true}
            />
            <Pagination
              dotsLength={images.length}
              activeDotIndex={activeSlide}
              containerStyle={{
                position: 'absolute',
                bottom: 0,
                left: 0,
                right: 0,
                paddingBottom: Theme.margin.m15,
              }}
              dotStyle={{
                width: Theme.margin.m10,
                height: Theme.margin.m10,
                borderRadius: 5,
                marginHorizontal: 4,
                backgroundColor: 'rgba(255, 255, 255, 0.92)',
              }}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
          </View>
        ) : null}
      </View>
    );
  };

  return (
    <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="padding" enabled>
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {renderNavigation()}
        <ScrollView>
          {renderImages()}
          {/*<UniInput*/}
          {/*  numberOfLines={3}*/}
          {/*  placeholder="Photo information"*/}
          {/*  initValue={''}*/}
          {/*  onChangeText={setInformation}*/}
          {/*  containerStyle={styles.editTextDescription}*/}
          {/*/>*/}
          <UniDropbox
            label={'Category'}
            selectedKey="PMProjectCategoryID"
            selectedValue={categoryId}
            options={categories}
            pathLeft={'PMProjectCategoryNo'}
            pathValue={'PMProjectCategoryName'}
            onSelect={(no, {PMProjectCategoryID}) =>
              setCategoryId(PMProjectCategoryID)
            }
          />
          <UniDropbox
            label={'Location'}
            selectedKey="PMProjectLocationID"
            selectedValue={locationId}
            options={locations}
            pathLeft={'PMProjectLocationNo'}
            pathValue={'PMProjectLocationName'}
            onSelect={(no, {PMProjectLocationID}) =>
              setLocationId(PMProjectLocationID)
            }
          />
          <View  style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: Theme.margin.m4,
          marginLeft: 15
        }}>
            <UniCheckbox
                checked={isSelected}
                onChangeValue={() => setSelection(!isSelected)}
                component="addPhoto"
              />
             <UniText containerStyle={{marginHorizontal: 6}}>Private</UniText>
         </View>
          <View style={{alignItems: 'flex-end', marginRight: Theme.margin.m16}}>
            <UniButton
              text={'Add'}
              onPress={handleAddPhoto}
              containerStyle={styles.button}
              disabled={isAddingPhoto}
            />
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingViewWrapper>
  );
};

const mapStateToProps = ({auth: {user, project}, commonData, photoList}) => {
  const PMProjectID = propOr(0, 'PMProjectID', project);
  // const {ADUserID} = user;

  return {
    user,
    project,
    categories: pathOr(EMPTY.ARRAY, ['currentProject', 'categories'], commonData),
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
  };
};

const mapDispatchToProps = {
  addPhoto: actions.addPhoto,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddPhoto);
