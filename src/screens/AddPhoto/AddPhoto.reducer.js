import typeToReducer from 'type-to-reducer';
import addPhoto from './actions/addPhoto.action';

const initialState = {};

export const actions = {
  addPhoto: addPhoto.action,
};

export default typeToReducer(
  {
    ...addPhoto.reducer,
  },
  initialState,
);
