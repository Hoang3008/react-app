import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, pathOr} from 'ramda';
import {createFormData} from '../../../api/defect';
import AsyncStorage from '@react-native-community/async-storage';

export const actionType = 'PHOTOS/ADD_PHOTO';

const addPhotoAPI = (projectId, body, image, isSelected, folderId) => {

  const numPrivate = isSelected ? 1 : 0;
  const formData = createFormData({}, image, 'files');
  const data = {
    private: numPrivate,
    urls: [formData]
  }

  return AsyncStorage.getItem('token').then((token) => {
    return API.post(withKey(`/file/${projectId}/uploads`), formData, {
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'multipart/form-data',
      },
    }).then((response) => {
      const urls = pathOr([], ['data', 'urls'], response);
      const data = {
        private: numPrivate,
        urls: urls,
        ...(folderId > 0) && {albumId: folderId},
      }
      return API.post(
        withKey(`/photo/${projectId}/uploads`),
        {...body, ...data},
        {
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        },
      );
    });
  });
};

// {
//   "categoryId":66,
//   "locationId":50,
//   "urls":[
//   "https://rocez-media-dev.s3.ap-southeast-1.amazonaws.com/projects/125/ViewDocument_1600834540962.jpeg"
// ]
// }

// https://apid.rocez.com/file/125/uploads

const action = createAction(actionType, addPhotoAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  return state;
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
