import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {View, Text, StatusBar} from 'react-native';
import {
  UniImageButton,
  UniInput,
  UniStatusBarBackground,
} from '../../components';
import Theme from '../../Theme';
import {scaledWidth, SCREEN_WIDTH} from '../../utils/DimensionUtils';
import UniButton from '../../components/UniButton';
import {actions} from './CreateDrawingArea.reducer';
import {isEmpty, pathOr, propOr} from 'ramda';

const CreateDrawingArea = (props) => {
  const {
    navigation,
    createDrawingArea,
    token,
    project,
    updateDrawingArea,
  } = props;

  const area = pathOr({}, ['route', 'params', 'area'], props);
  const parentId = pathOr({}, ['route', 'params', 'parentId'], props);

  const [no, setNo] = useState(area.No || '');
  const [name, setName] = useState(area.Name || '');
  const [description, setDescription] = useState(area.Description || '');

  const PMProjectID = propOr(0, 'PMProjectID', project);

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => navigation.goBack()}
          />
          <View>
            <Text style={styles.textHeader}>
              {!isEmpty(area) ? 'Update Folder' : 'Create new Folder'}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  const handleCreateProject = () => {
    if (!isEmpty(area)) {
      updateDrawingArea(
        area.ID,
        {
          description,
          name,
          no,
          projectId: PMProjectID,
          drawingAreaType: 2,
          ...(parentId > -1) && {parentId: parentId},
        },
        token,
      )
        .then((res) => {
          navigation.goBack();
        })
        .catch((error) => {});
      return;
    }

    createDrawingArea(
      {
        description,
        name,
        no,
        projectId: PMProjectID,
        drawingAreaType: 2,
        ...(parentId > -1) && {parentId: parentId},
      },
      token,
    )
      .then((res) => {
        navigation.goBack();
      })
      .catch((error) => {});
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundColor}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{marginTop: 22}}>
        <UniInput
          placeholder={'No'}
          initValue={no}
          onChangeText={setNo}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder={'Name'}
          initValue={name}
          onChangeText={setName}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder={'Description'}
          initValue={description}
          onChangeText={setDescription}
          containerStyle={styles.editText}
        />

        <UniButton
          text={!isEmpty(area) ? 'Update' : 'Create'}
          onPress={handleCreateProject}
          containerStyle={styles.button}
        />
      </View>
    </View>
  );
};

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  editText: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
  button: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
};

const mapStateToProps = ({auth: {user, project}}) => ({
  user,
  project,
  token: user.token,
});

const mapDispatchToProps = {
  createDrawingArea: actions.createDrawingArea,
  updateDrawingArea: actions.updateDrawingArea,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateDrawingArea);
