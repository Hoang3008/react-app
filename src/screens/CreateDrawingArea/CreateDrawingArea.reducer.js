import typeToReducer from 'type-to-reducer';
import createDrawingArea from './actions/createDrawingArea.action';
import updateDrawingArea from './actions/updateDrawingArea.action';

const initialState = {};

export const actions = {
  createDrawingArea: createDrawingArea.action,
  updateDrawingArea: updateDrawingArea.action,
};

export default typeToReducer(
  {
    ...createDrawingArea.reducer,
    ...updateDrawingArea.reducer,
  },
  initialState,
);
