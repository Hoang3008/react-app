import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';

export const actionType = 'UPDATE_DRAWING_AREA';

const createUserURL = (id) => withKey(`/drawing-area/${id}`);
const createUserAPI = (id, body, token) =>
  API.put(createUserURL(id), body, {
    headers: {Authorization: `Bearer ${token}`},
  });

const action = createAction(actionType, createUserAPI);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
