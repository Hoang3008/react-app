import React, {useEffect, useRef, useState} from 'react';
import {connect} from 'react-redux';
import {Animated, Image, TouchableOpacity} from 'react-native';
import {StatusBar, View} from 'react-native';
import ViewPager from '@react-native-community/viewpager';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import Tab from '../../components/ScheduleTab/ScheduleTab';
import styles from './PhotoList.style';
import PhotoList from '../PhotoList/PhotoList';
import PhotoArea from '../PhotoArea/PhotoArea';

const PhotoListTabs = (props) => {
  const {navigation} = props;
  const pagerRef = useRef();
  const [tab, setTab] = useState(0);
  const [sortData, setSortData] = useState(false);
  const [showSearchBox, setShowSearchBox] = useState(false);
  const groupDropDownRef = useRef(null);

  useEffect(() => {
    if (!pagerRef.current) {
      return;
    }
    pagerRef.current.setPage(tab);
  }, [tab]);

  const showGroupDropDown = () => {
    if (!groupDropDownRef.current) {
      return;
    }
    groupDropDownRef.current.show();
  };

  const position = new Animated.Value(tab);
  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={[styles.row, {flex: 1}]}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m8,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
      </View>

      <View
        style={{
          width: 150,
          height: 40,
          backgroundColor: Theme.colors.backgroundColor,
          flexDirection: 'row',
          marginLeft: 10,
          marginRight: 10,
          borderRadius: 6,
          padding: 3,
        }}>
        {['Albums', 'Photos'].map((route, index, array) => {
          const focusAnim = position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [0, 1, 0],
          });
          return (
            <Tab
              key={route}
              style={{flex: 1}}
              focusAnim={focusAnim}
              title={route}
              onPress={() => setTab(index)}
            />
          );
        })}
      </View>

      <View style={[styles.row, {flex: 1, justifyContent: 'flex-end'}]}>
        {(
          <>
            <TouchableOpacity onPress={() => setSortData(!sortData)}>
              <Image
                style={styles.navIcon}
                source={require('../../assets/img/ic_font.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setShowSearchBox(!showSearchBox)}>
              <Image
                style={styles.navIcon}
                source={require('../../assets/img/ic_search.png')}
              />
            </TouchableOpacity>
          </>
        )}
      </View>
    </View>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundWhite,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        {tab === 0 && <PhotoArea navigation={navigation} showSearchBox = {showSearchBox}/>}
        {tab === 1 && (
          <PhotoList
            navigation={navigation}
            shouldShowTitleBar={false}
            sortData={sortData}
            showSearchBox={showSearchBox}
            showHeader={false}
          />
        )}
      </View>
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, photoList}) => {
  return {};
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotoListTabs);
