import React, {Fragment} from 'react';
import {pathOr, propOr  } from 'ramda';
import {View} from 'react-native';
import {connect} from 'react-redux';

import AcceptanceStep from '../../components/AcceptanceStep/AcceptanceStep';
import RouteNames from '../../RouteNames';
import {OFFLINE_ACTION_TYPES} from '../../global/constants';
import {actions as actionsOffline} from '../CommonProjectData/CommonProjectData.reducer';

const WorkFlowStepContent = (props) => {
  const {
    user,
    allowEdit = true,
    workflowContent,
    // updateWorkFlowStepContent,
    navigation,
    inspectionId,
    addActionIntoQueue,
    permissions,
    inspection,
    peopleID
  } = props;

  const handleUpdateStatus = (item) => (newStatus) => {
    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.UPDATE_ITP_WORKFLOW_CONTENT_STATUS,
      data: {
        ...item,
        inspectionId,
        isShowDetails: false,
        PMProjectInsStpInspStatus: newStatus,
      },
    });
  };

  const handlePickAttachment = (item) => () => {
    const {PMProjectInsStpInspID} = item;
    navigation.push(RouteNames.InspectionAttachmentList, {
      id: PMProjectInsStpInspID,
      inspectionId,
      countKey: 'PMProjectInsStpInspDocCount',
      allowEdit: true,
    });
  };

  const handlePickPhoto = (item) => () => {
    const {PMProjectInsStpInspID} = item;
    navigation.push(RouteNames.InspectionAttachmentList, {
      id: PMProjectInsStpInspID,
      inspectionId,
      countKey: 'PMProjectInsStpInspPicCount',
      allowEdit: true,
    });
  };

  const handleOpenCommentList = (item) => () => {
    const {PMProjectInsStpInspID} = item;
    navigation.push(RouteNames.InspectionWorkflowComment, {
      inspectionId,
      id: PMProjectInsStpInspID,
    });
  };

  const handleOpenLogList = (item) => () => {
    const {PMProjectInsStpInspID} = item;
    navigation.push(RouteNames.InspectionWorkflowLog, {
      inspectionId,
      id: PMProjectInsStpInspID,
    });
  };

  return (
    <View
      style={{
        marginTop: 15,
        width: '100%',
        flexDirection: 'column',
        backgroundColor: '#F6F6F8',
        borderRadius: 5,
        paddingHorizontal: 3,
        paddingVertical: 10,
        overflow: 'hidden',
      }}>
      {workflowContent.map((item) => (
        <Fragment key={item.PMProjectInsStpInspID}>
          <AcceptanceStep
            key={item.PMProjectInsStpInspID}
            {...item}
            user={user}
            allowEdit={allowEdit}
            onClickItem={handleOpenCommentList(item)}
            onUpdateStatus={handleUpdateStatus(item)}
            onClickAttachment={handlePickAttachment(item)}
            onClickPhoto={handlePickPhoto(item)}
            onClickLog={handleOpenLogList(item)}
            permissions = {permissions}
            inspection = {inspection}
            peopleID = {peopleID || 0}
          />
          <View
            style={{
              width: '100%',
              height: 0.5,
              backgroundColor: '#efeff3',
            }}
          />
        </Fragment>
      ))}
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, inspectionDetail, commonData, inspectionList}, props) => {
  const {workflowId, inspectionId} = props;
  const acceptanceSteps = pathOr(
    [],
    [inspectionId, 'acceptanceSteps'],
    inspectionDetail,
  );

  const inspectionFromProps = pathOr({}, ['route', 'params'], props);
  const permissions = commonData.Permission.find(x => x.module === 'itp');
  
  const inspects = Array.isArray(propOr([], 'inspectionList', inspectionList))
  ? propOr([], 'inspectionList', inspectionList)
  : [];

  const peopleInProject =  pathOr([], ['currentProject', 'users'], commonData);

  const PMProjectPeopleID = peopleInProject.find(x => x.PMProjectPeopleEmailAddress === user.ADUserName)?.PMProjectPeopleID;

  const inspection =
  inspects.find(({PMProjectInspID}) => PMProjectInspID === inspectionId) ||
  inspectionFromProps;


  return {
    project,
    workflowContent:
      acceptanceSteps.filter(
        ({FK_PMProjectInsStpID}) => FK_PMProjectInsStpID === workflowId,
      ) || [],
    user,
    permissions,
    peopleID: PMProjectPeopleID,
    inspection: pathOr(
      inspection,
      [inspectionId, 'overview', 0],
      inspectionDetail,
    ),
  };
};

const mapDispatchToProps = {
  // updateWorkFlowStepContent: actions.updateWorkFlowStepContent,
  addActionIntoQueue: actionsOffline.addActionIntoQueue,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WorkFlowStepContent);
