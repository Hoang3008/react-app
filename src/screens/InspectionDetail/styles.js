import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    color: '#3E3F42',
  },
  subLabelText: {
    fontSize: 14,
    lineHeight: 18,
    color: '#9EA0A5',
  },
  normalText: {
    fontSize: 14,
    lineHeight: 18,
    color: '#3E3F42',
  },
  headerText: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    color: '#3E3F42',
  },
  headerContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerButton: {
    fontWeight: '500',
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 14,
    lineHeight: 22,
    color: '#245894',
  },
  generalRow: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginTop: 8,
  },
  generalLeftView: {
    width: 153,
  },
  sectionView: {
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },
  divider: {
    height: 0.5,
    marginTop: 20,
    backgroundColor: '#D1D7E6',
  },
  label: {
    fontSize: 14,
    fontWeight: '400',
    color: '#333333',
  },
};
