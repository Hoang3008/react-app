import {Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import styles from './styles';
import {isEmpty, pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';
import ProjectOverviewBar from '../../components/ProjectOverviewBar/ProjectOverviewBar';

const PROGRESS_COLOR = '#34aa44';
const BACKGROUND_COLOR = '#bdbdbd';
const FAILED_COLOR = '#e6482c';

const Overview = (props) => {
  const {total, passed, failed, open, finished} = props;
  const [shouldShow, setShouldShow] = useState(true);

  const renderItem = (name, progress, total, processColor = PROGRESS_COLOR) => {
    const values = [
      {
        value: progress,
        color: processColor,
      },
      {
        value: total - progress,
        color: BACKGROUND_COLOR,
      },
    ];

    return (
      <>
        <View style={[styles.row, {marginBottom: 6}]}>
          <Text style={[styles.label, {color: '#235894'}]}>{name}</Text>
          <Text style={[styles.label, {color: '#999', marginLeft: 3}]}>
            ({progress}/{total})
          </Text>
        </View>
        <ProjectOverviewBar style={{marginBottom: 15}} values={values} />
      </>
    );
  };

  return (
    <View style={[styles.sectionView, {marginTop: 15}]}>
      <View style={[styles.headerContainer]}>
        <Text style={[styles.headerText]}>Overview</Text>
        <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
          <Text style={styles.headerButton}>
            {shouldShow ? 'HIDE' : 'SHOW'}
          </Text>
        </TouchableOpacity>
      </View>
      {shouldShow && (
        <>
          {renderItem('Made', finished, total)}
          {renderItem('Passed', passed, total)}
          {renderItem('Failed', failed, total, FAILED_COLOR)}
          {renderItem('Open', open, total)}
        </>
      )}
    </View>
  );
};

const mapStateToProps = ({inspectionDetail}, props) => {
  const inspectionId = propOr(0, 'id', props);
  const workflow = pathOr([], [inspectionId, 'steps'], inspectionDetail);

  const passed = workflow.filter(
    ({OverviewStatus}) => OverviewStatus === 'Pass',
  ).length;

  const failed = workflow.filter(
    ({OverviewStatus}) => OverviewStatus === 'Fail',
  ).length;

  const open = workflow.filter(({OverviewStatus}) => OverviewStatus === 'Open')
    .length;

  return {
    total: workflow.length,
    passed,
    failed,
    open,
    finished: failed + passed,
  };
};

export default connect(
  mapStateToProps,
  {},
)(Overview);
