import {Text, TouchableOpacity, View} from 'react-native';
import {pathOr, propOr} from 'ramda';
import moment from 'moment';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {isNil} from 'ramda'
import styles from './styles';
import {EMPTY} from '../../global/constants';

export const GeneralInformation = (props) => {
  const {
    constructionMethodList,
    locations,
    peopleInProject,
    categories,
    inspection,
  } = props;
  const [shouldShow, setShouldShow] = useState(true);

  const {
    PMProjectInspDesc,
    PMProjectInspDate,
    PMProjectInspEstStartDate,
    PMProjectInspEstEndDate,
    FK_PMProjectLocationID,
    FK_PMInspectPeopleID,
    FK_PMProjectPeopleID,
    FK_PMProjectApprovedPeopleID,
    FK_PMProjectCategoryID,
    FK_PMWorkInstID,
  } = inspection;

  const constructionMethod = constructionMethodList.find(
    ({PMWorkInstID}) => FK_PMWorkInstID === PMWorkInstID,
  );

  const location = locations.find(
    ({PMProjectLocationID}) => PMProjectLocationID === FK_PMProjectLocationID,
  );

  const mainContact = peopleInProject.find(
    ({PMProjectPeopleID}) => FK_PMProjectPeopleID === PMProjectPeopleID,
  );

  const inspector = peopleInProject.find(
    ({PMProjectPeopleID}) => FK_PMInspectPeopleID === PMProjectPeopleID,
  );

  const signedBy = peopleInProject.find(
    ({PMProjectPeopleID}) => FK_PMProjectApprovedPeopleID === PMProjectPeopleID,
  );

  const category = categories.find(
    ({PPProjectDiscID}) => FK_PMProjectCategoryID === PPProjectDiscID,
  );

  return (
    <View style={[styles.sectionView, {marginTop: 15}]}>
      <View style={[styles.headerContainer]}>
        <Text style={[styles.headerText]}>General Information</Text>
        <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
          <Text style={styles.headerButton}>
            {shouldShow ? 'HIDE' : 'SHOW'}
          </Text>
        </TouchableOpacity>
      </View>

      {shouldShow && (
        <>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Category:</Text>
            <Text style={[styles.normalText, {color: '#245894'}]}>
              {propOr('', 'PPProjectDiscName', category)}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Construction Method:</Text>
            <Text style={[styles.normalText, {color: '#245894'}]}>
              {propOr('', 'PMWorkInstName', constructionMethod)}
            </Text>
          </View>

          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Location:</Text>
            <Text style={[styles.normalText, {color: '#245894'}]}>
              {propOr('', 'PMProjectLocationName', location)}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Date Published:</Text>
            <Text style={[styles.normalText]}>
              {!isNil(PMProjectInspDate) && moment(PMProjectInspDate).format('DD/MM/YYYY')}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Start Date:</Text>
            <Text style={[styles.normalText]}>
              {!isNil(PMProjectInspEstStartDate) && moment(PMProjectInspEstStartDate).format('DD/MM/YYYY')}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>End Date:</Text>
            <Text style={[styles.normalText]}>
              {!isNil(PMProjectInspEstEndDate) && moment(PMProjectInspEstEndDate).format('DD/MM/YYYY')}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Inspector:</Text>
            <Text style={[styles.normalText, {color: '#245894'}]}>
              {propOr('', 'PMProjectPeopleFirstName', inspector)}{' '}
              {propOr('', 'PMProjectPeopleLastName', inspector)}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Main contact:</Text>
            <Text style={[styles.normalText, {color: '#245894'}]}>
              {propOr('', 'PMProjectPeopleFirstName', mainContact)}{' '}
              {propOr('', 'PMProjectPeopleLastName', mainContact)}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Signed by:</Text>
            <Text style={[styles.normalText, {color: '#245894'}]}>
              {propOr('', 'PMProjectPeopleFirstName', signedBy)}{' '}
              {propOr('', 'PMProjectPeopleLastName', signedBy)}
            </Text>
          </View>
          <Text style={[styles.headerText, {marginTop: 25}]}>Description</Text>
          <Text style={[styles.normalText, {marginTop: 4}]}>
            {PMProjectInspDesc}
          </Text>
          <View style={styles.divider} />
        </>
      )}
    </View>
  );
};

const mapStateToProps = ({
  auth: {project, master},
  commonData,
  inspectionDetail,
}) => {
  const {PMProjectID} = project;

  return {
    categories: master ? master.categories : [],
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    constructionMethodList: inspectionDetail.constructionMethodList || [],
  };
};

export default connect(
  mapStateToProps,
  {},
)(GeneralInformation);
