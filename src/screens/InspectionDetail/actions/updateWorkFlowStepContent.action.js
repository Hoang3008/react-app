import {createAction} from 'redux-actions';
import {identity, omit, pathOr, propOr} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';
import {createLogAPI} from './createLog.action';

export const actionType = 'UPDATE_WORKFLOW_STEP_CONTENT';

export const updateITPStepAPI = (item, projectId, callLog = true) => {
  const data = {
    ...(item.PMProjectInsStpInspPicCount > -1) &&  {picCount: item.PMProjectInsStpInspPicCount},
    ...(item.PMProjectInsStpInspDocCount > -1) &&  {docCount: item.PMProjectInsStpInspDocCount},
    inspStatus: item?.PMProjectInsStpInspStatus
  }
  const url = withKey(`/itp/v2/instStepInst/${projectId}/${item?.PMProjectInsStpInspID}`);
  return AsyncStorage.getItem('token').then((token) => {
    return API.put(url, data, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((res) => {
      if (!callLog) {
        return res;
      }

      const payloadId = pathOr('', ['data', 'PMProjectInsStpInspID'], res);
      const status = pathOr('', ['data', 'PMProjectInsStpInspStatus'], res);
      let type = 6;
      if (status === 'Pass') {
        type = 7;
      } else if (status === 'Verify') {
        type = 8;
      }
      createLogAPI(token, payloadId, type);
      return res;
    });
  });
};

const action = createAction(actionType, updateITPStepAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const {inspectionId, PMProjectInsStpInspID} = meta;
  const currentItem = propOr({}, inspectionId, state);
  const acceptanceSteps = propOr([], 'acceptanceSteps', currentItem);

  return {
    ...state,
    [inspectionId]: {
      ...currentItem,
      acceptanceSteps: acceptanceSteps.map((item) => {
        if (item.PMProjectInsStpInspID === PMProjectInsStpInspID) {
          return {
            ...item,
            ...payload.data,
          };
        }

        return item;
      }),
    },
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
