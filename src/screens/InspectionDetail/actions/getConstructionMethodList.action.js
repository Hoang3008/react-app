import {createAction} from 'redux-actions';
import {identity, isEmpty} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_CONSTRUCTION_METHOD_LIST';

const createAPI = (projectID) => {  
  const url = withKey(`/itp/v2/workFlow?projectId=${projectID}`);

  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
      timeout: 120000,
    });
  });
};

const action = createAction(actionType, createAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    constructionMethodList: isEmpty(payload.data) ? [] : payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
