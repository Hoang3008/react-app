import {createAction} from 'redux-actions';
import {identity, pathOr, propOr} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';
import {createLogAPI} from './createLog.action';

export const actionType = 'DELETE_WORKFLOW_DOC';

const createAPI = (data) => {
  const url = withKey('/PMProjectInsStpInspDocs/DeleteObject');

  return AsyncStorage.getItem('token').then((token) => {
    return API.delete(url, {
      data,
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    }).then((res) => {
      const payloadId = propOr('', 'FK_PMProjectInsStpInspID', data);
      createLogAPI(token, payloadId, 2);
      return res;
    });
  });
};

const action = createAction(actionType, createAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  const payloadId = payload.data.FK_PMProjectInsStpInspID;
  const currentWorkflowContent = propOr([], 'workflowContent', state);

  return {
    ...state,
    workflowContent: currentWorkflowContent.map((item) => {
      if (item.PMProjectInsStpInspID === payloadId) {
        return {
          ...item,
          PMProjectInsStpInspDocCount:
            propOr(0, 'PMProjectInsStpInspDocCount', item) - 1,
        };
      }

      return item;
    }),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
