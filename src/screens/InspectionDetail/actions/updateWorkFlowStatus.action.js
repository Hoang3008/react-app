import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'UPDATE_WORKFLOW_STATUS';

const createTaskAPI = (id) => {
  const url = withKey('/PMProjectInsStps/UpdateStatus');
  return AsyncStorage.getItem('token').then((token) => {
    return API.put(
      url,
      {PMProjectInsStpID: id},
      {
        headers: {
          Accept: 'application/json, text/plain, */*',
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  const payloadId = payload.data.PMProjectInsStpID;
  const currentWorkflow = propOr([], 'workflow', state);

  const newState = {
    ...state,
    workflow: [
      ...currentWorkflow.map((item) => {
        if (item.PMProjectInsStpID === payloadId) {
          return {
            ...item,
            ...payload.data,
          };
        }

        return item;
      }),
    ],
  };

  return newState;
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
