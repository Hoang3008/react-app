import {createAction} from 'redux-actions';
import {identity, pathOr, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';
import {createLogAPI} from './createLog.action';

import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';
import {createFormData} from '../../../api/defect';
import {updateITPStepAPI} from './updateWorkFlowStepContent.action';

export const actionType = 'UPLOAD_WORKFLOW_PHOTO';

const createAPI = ({body, image, updateITPStepContent}, projectId) => {
  const file = image;
  const insStpInspId = propOr(undefined, 'PMProjectInsStpInspID', updateITPStepContent);
  const data = {
    "projectId": projectId,
   // "file": file,
    "insStpInspId": insStpInspId
  };

  const formData = createFormData(data, file);

  return AsyncStorage.getItem('token').then((token) => {
    return API.post(
      withKey('/itp/v2/pic'),
      formData,
      {
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${token}`,
        },
      },
    ).then((res) => {
      const payloadId = pathOr('', ['data', 'FK_PMProjectInsStpInspID'], res);
      createLogAPI(token, payloadId, 3);
      updateITPStepAPI(updateITPStepContent, false);
      return res;
    }).catch((err) => {
       alert(JSON.stringify(err));
     });;
  });
};

const action = createAction(actionType, createAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const {inspectionId, offlineId} = meta;

  const currentItem = propOr({}, inspectionId, state);
  const pics = currentItem.pics || [];

  return {
    ...state,
    [inspectionId]: {
      ...currentItem,
      pics: pics.map((item) => {
        if (item.offlineId === offlineId) {
          return payload.data;
        }

        return item;
      }),
    },
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
