import {createAction} from 'redux-actions';
import {identity, pathOr, propOr} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {createFormData} from '../../../api/defect';
import AsyncStorage from '@react-native-community/async-storage';
import {createLogAPI} from './createLog.action';


export const actionType = 'UPLOAD_WORKFLOW_DOC';

const createAPI = (body, projectId) => {
  const file = pathOr(undefined, ['image'], body);
  const insStpInspId = pathOr(undefined, ['body','FK_PMProjectInsStpInspID'], body);
  const data = {
    "projectId": projectId,
    //"file": file,
    "insStpInspId": insStpInspId
  };


  const formData = createFormData(data, file);
  return AsyncStorage.getItem('token').then((token) => {
    try {
      return API.post(
        withKey('/itp/v2/doc'),
        formData,
        {
          headers: {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${token}`,
          },
        },
      ).then((res) => {
        const payloadId = pathOr('', ['data', 'FK_PMProjectInsStpInspID'], res);
        createLogAPI(token, payloadId, 1);
        return res;
      }).catch((err) => {
        // alert(JSON.stringify(err));
      });
    } catch (error) {
     // alert(error);
    }
  });
};

const action = createAction(actionType, createAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const {inspectionId, offlineId} = meta;

  const currentItem = propOr({}, inspectionId, state);
  const docs = currentItem.docs || [];

  return {
    ...state,
    [inspectionId]: {
      ...currentItem,
      docs: docs.map((item) => {
        if (item.offlineId === offlineId) {
          return payload.data;
        }

        return item;
      }),
    },
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
