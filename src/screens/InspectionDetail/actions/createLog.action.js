import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';

// {"type":6,"insStpInspID":4616,"contents":"Change status from Open to Fail"}

const getLogContent = (type, value) => {
  let text = '';
  switch (type) {
    case 1:
      text = 'đã đính kèm tài liệu';
      break;
    case 2:
      text = 'đã xoá đính kèm tài liệu';
      break;
    case 3:
      text = 'đã đính kèm hình ảnh';
      break;
    case 4:
      text = 'đã xoá hình ảnh';
      break;
    case 5:
      text = `${value}`;
      break;
    case 6:
      text = 'đã xác nhận Failed';
      break;
    case 7:
      text = 'đã xác nhận Pass';
      break;
    case 8:
      text = 'đã xác nhận Verified';
      break;

    default:
      break;
  }

  return text;
};

export const createLogAPI = (token, insStpInspID, type, value) => {
  const data = {
    insStpInspID,
    type,
    contents: getLogContent(type, value),
  };

  return API.post(withKey('/itp/log/content-of-step'), data, {
    headers: {
      Accept: 'application/json, text/plain, */*',
      Authorization: `Bearer ${token}`,
    },
  });
};
