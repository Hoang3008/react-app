import {createAction} from 'redux-actions';
import {groupBy, identity, pathOr, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';
import API from '../../../api/API.service';
import {actionType as updateITPActionType} from '../../InspectionUpdate/actions/updateInspection.action';
import {actionType as offlineAddActionType} from '../../CommonProjectData/actions/addActionIntoQueue.action';

import {withKey} from '../../../api/urls';
import {OFFLINE_ACTION_TYPES} from '../../../global/constants';

const actionType = 'GET_ITP_DETAIL_BY_ID';

const createTaskAPI = (id) => {
  // https://apidev.rocez.com/itp/23/all
  const url = withKey(`/itp/${id}/all`);

  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
      timeout: 120000,
    });
  });
};
const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const id = meta;

  const constructionMethodSteps = groupBy(
    ({FK_ParentID}) => FK_ParentID || 0,
    pathOr([], ['data', 'constructionMethodSteps'], payload).sort(
      (a, b) => a.ListOrder - b.ListOrder,
    ),
  );

  // acceptanceSteps: (9) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]
  // activity: []
  // comment: []
  // constructionMethodSteps: (2) [{…}, {…}]
  // docs: []
  // overview: [{…}]
  // pics: []
  // steps: (7

  return {
    ...state,
    [id]: {
      ...payload.data,
      rawConstructionMethodSteps: pathOr(
        [],
        ['data', 'constructionMethodSteps'],
        payload,
      ),
      constructionMethodSteps: propOr([], 0, constructionMethodSteps).map(
        (item, index) => ({
          index,
          ID: item.ID,
          title: item.Description,
          data: propOr([], item.ID, constructionMethodSteps),
          ...item,
        }),
      ),
    },
  };
};

const handleUpdateITPSuccess = (state, {payload}) => {
  const {data} = payload;
  const {PMProjectInspID} = data;

  const currentItem = pathOr({}, [PMProjectInspID], state);
  const currentItemOverview = pathOr(
    {},
    [PMProjectInspID, 'overview', 0],
    state,
  );

  return {
    ...state,
    [PMProjectInspID]: {
      ...currentItem,
      overview: [
        {
          ...currentItemOverview,
          ...data,
        },
      ],
    },
  };
};

const handleAddActionIntoQueue = (state, {payload}) => {
  if (
    payload.type === OFFLINE_ACTION_TYPES.UPDATE_ITP_WORKFLOW_CONTENT_STATUS
  ) {
    const {data} = payload;
    const {inspectionId, FK_PMProjectInsStpID, PMProjectInsStpInspID} = data;
    const currentItem = propOr({}, inspectionId, state);
    const acceptanceSteps = propOr([], 'acceptanceSteps', currentItem).map(
      (item) => {
        if (item.PMProjectInsStpInspID === PMProjectInsStpInspID) {
          return {
            ...item,
            ...data,
            inspectionId: undefined,
          };
        }

        return item;
      },
    );
    const steps = propOr([], 'steps', currentItem);
    const acceptanceStepsOfStep =
      acceptanceSteps.filter(
        (item) => item.FK_PMProjectInsStpID === FK_PMProjectInsStpID,
      ) || [];

    let requiredStepCount = 0;
    let requiredStepPassCount = 0;
    let requiredStepFailCount = 0;
    let nonRequiredStepCount = 0;
    let nonRequiredStepPassCount = 0;
    let nonRequiredStepFailCount = 0;

    acceptanceStepsOfStep.forEach((item) => {
      if (item.PMProjectInsStpInspRequired) {
        requiredStepCount += 1;
        if (
          item.PMProjectInsStpInspStatus === 'Pass' &&
          !item.PMInsChklstInspVerifiedby
        ) {
          requiredStepPassCount += 1;
        }

        if (
          item.PMInsChklstInspVerifiedby &&
          item.PMProjectInsStpInspStatus === 'Verify'
        ) {
          requiredStepPassCount += 1;
        }

        if (item.PMProjectInsStpInspStatus === 'Fail') {
          requiredStepFailCount += 1;
        }
      } else {
        nonRequiredStepCount += 1;

        if (
          item.PMProjectInsStpInspStatus === 'Pass' &&
          !item.PMInsChklstInspVerifiedby
        ) {
          nonRequiredStepPassCount += 1;
        }

        if (
          item.PMInsChklstInspVerifiedby &&
          item.PMProjectInsStpInspStatus === 'Verify'
        ) {
          nonRequiredStepPassCount += 1;
        }

        if (item.PMProjectInsStpInspStatus === 'Fail') {
          nonRequiredStepFailCount += 1;
        }
      }
    });

    return {
      ...state,
      [inspectionId]: {
        ...currentItem,
        steps: steps.map((item) => {
          if (item.PMProjectInsStpID === FK_PMProjectInsStpID) {
            let OverviewStatus = 'In Progress';

            if (requiredStepCount > 0) {
              if (requiredStepFailCount > 0) {
                OverviewStatus = 'Fail';
              } else if (requiredStepPassCount === requiredStepCount) {
                OverviewStatus = 'Pass';
              }
            } else if (nonRequiredStepCount > 0) {
              if (nonRequiredStepFailCount > 0) {
                OverviewStatus = 'Fail';
              } else if (nonRequiredStepPassCount > 0) {
                OverviewStatus = 'Pass';
              }
            }

            return {
              ...item,
              OverviewStatus:
                OverviewStatus === 'Open' ? 'In Progress' : OverviewStatus,
            };
          }

          return item;
        }),
        acceptanceSteps,
      },
    };
  }

  if (payload.type === OFFLINE_ACTION_TYPES.ADD_ITP_WORKFLOW_DOC) {
    const {data} = payload;
    const {inspectionId, body, image, offlineId, updateITPStepContent} = data;
    const {FK_PMProjectInsStpInspID} = body;

    const currentItem = propOr({}, inspectionId, state);
    const acceptanceSteps = propOr([], 'acceptanceSteps', currentItem);
    const steps = propOr([], 'steps', currentItem);
    const docs = currentItem.docs || [];

    // OverviewStatus: "Pass"
    // PMProjectInsStpDesc: "Shopdrawing"
    // PMProjectInsStpID: 3107
    // PMProjectInsStpNo: "Shopdrawing"
    // Required: false
    //FK_PMProjectInsStpID

    // AACreatedDate: "2020-10-23T17:25:13.097Z"
    // AACreatedUser: "vuong.ho@gmail.com"
    // FK_PMProjectInsStpInspID: 4312
    // PMProjectInsStpInspPicDate: "2020-10-23T00:00:00.000Z"
    // PMProjectInsStpInspPicDesc: "banner.jpg"
    // PMProjectInsStpInspPicID: 2317
    // PMInsChklstInspVerifiedby
    return {
      ...state,
      [inspectionId]: {
        ...currentItem,
        steps: steps.map((item) => {
          if (
            item.PMProjectInsStpID === updateITPStepContent.FK_PMProjectInsStpID
          ) {
            return {
              ...item,
              OverviewStatus:
                item.OverviewStatus === 'Open'
                  ? 'In Progress'
                  : item.OverviewStatus,
            };
          }

          return item;
        }),
        acceptanceSteps: acceptanceSteps.map((item) => {
          if (item.PMProjectInsStpInspID === FK_PMProjectInsStpInspID) {
            return {
              ...item,
              ...updateITPStepContent,
            };
          }

          return item;
        }),
        docs: [
          ...docs,
          {
            ...body,
            offlineId,
            PMProjectInsStpInspPicDesc: image.filename,
            imageLocalPath: image.uri,
          },
        ],
      },
    };
  }

  if (payload.type === OFFLINE_ACTION_TYPES.ADD_ITP_WORKFLOW_PHOTO) {
    const {data} = payload;
    const {inspectionId, body, image, offlineId, updateITPStepContent} = data;
    const {FK_PMProjectInsStpInspID} = body;

    const currentItem = propOr({}, inspectionId, state);
    const acceptanceSteps = propOr([], 'acceptanceSteps', currentItem);
    const steps = propOr([], 'steps', currentItem);
    const pics = currentItem.pics || [];

    return {
      ...state,
      [inspectionId]: {
        ...currentItem,

        steps: steps.map((item) => {
          if (
            item.PMProjectInsStpID === updateITPStepContent.FK_PMProjectInsStpID
          ) {
            return {
              ...item,
              OverviewStatus:
                item.OverviewStatus === 'Open'
                  ? 'In Progress'
                  : item.OverviewStatus,
            };
          }

          return item;
        }),
        acceptanceSteps: acceptanceSteps.map((item) => {
          if (item.PMProjectInsStpInspID === FK_PMProjectInsStpInspID) {
            return {
              ...item,
              ...updateITPStepContent,
            };
          }

          return item;
        }),
        pics: [
          ...pics,
          {
            ...body,
            offlineId,
            PMProjectInsStpInspPicDesc: image.filename,
            imageLocalPath: image.uri,
          },
        ],
      },
    };
  }

  if (payload.type === OFFLINE_ACTION_TYPES.POST_ITP_WORKFLOW_COMMENT) {
    const {data} = payload;
    const {inspectionId} = data;

    const currentItem = propOr({}, inspectionId, state);
    const comment = currentItem.comment || [];

    return {
      ...state,
      [inspectionId]: {
        ...currentItem,
        comment: [...comment, data],
      },
    };
  }

  if (payload.type === OFFLINE_ACTION_TYPES.UPDATE_ITP_OVERVIEW) {
    const {data} = payload;
    const {PMProjectInspID} = data;

    const currentItem = pathOr({}, [PMProjectInspID], state);
    const currentItemOverview = pathOr(
      {},
      [PMProjectInspID, 'overview', 0],
      state,
    );
    return {
      ...state,
      [PMProjectInspID]: {
        ...currentItem,
        overview: [
          {
            ...currentItemOverview,
            ...data,
          },
        ],
      },
    };
  }

  return state;
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
  [updateITPActionType]: {
    SUCCESS: handleUpdateITPSuccess,
  },
  [offlineAddActionType]: handleAddActionIntoQueue,
};

export default {action, reducer};
