import {createAction} from 'redux-actions';
import {groupBy, identity, pathOr, propOr} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_WORKFLOW_STEP_CONTENT';

const createTaskAPI = (id) => {
  const url = withKey(`/itp/content-of-step/${id}`);

  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  const acceptanceSteps = pathOr([], ['data', 'acceptanceSteps'], payload);
  const constructionMethodSteps = groupBy(
    ({FK_ParentID}) => FK_ParentID || 0,
    pathOr([], ['data', 'constructionMethodSteps'], payload).sort(
      (a, b) => a.ListOrder - b.ListOrder,
    ),
  );

  return {
    ...state,
    workflowContent: acceptanceSteps,
    constructionMethodSteps: propOr([], 0, constructionMethodSteps).map(
      (item, index) => ({
        index,
        ID: item.ID,
        title: item.Description,
        data: propOr([], item.ID, constructionMethodSteps),
        ...item,
      }),
    ),
  };
};

// CreatedDate: "2020-06-30T02:17:09.327Z"
// CreatedUser: "vuong.ho@gmail.com"
// Description: "Trình phê duyệt CDT,TVGS"
// DocServerPath: ""
// FK_ITPStepID: 1495
// FK_ParentID: 0
// ID: 29
// ListOrder: 2
// PictureServerPath: ""
// Status: "Alive"
// UpdatedDate: "2020-06-30T02:17:09.327Z"
// UpdatedUser: "vuong.ho@gmail.com"

const reducer = {
  [actionType]: {
    BEGIN: (state) => ({...state, workflowContent: []}),
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
