import {createAction} from 'redux-actions';
import {identity, isEmpty} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_WORK_FLOW_BY_INSPECTION_ID';

const createTaskAPI = (projectId, ITPId) => {
  const url = withKey(`/itp/${ITPId}/steps`);

  // https://apidev.rocez.com/itp/23/steps

  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
      timeout: 120000,
    });
  });
};
const action = createAction(actionType, createTaskAPI, (...params) => params);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    workflow: isEmpty(payload.data) ? [] : payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: (state, {meta}) => {
      const clearOldData = meta[2];

      if (clearOldData) {
        return {
          ...state,
          workflow: [],
        };
      }

      return state;
    },
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
