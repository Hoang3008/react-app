import typeToReducer from 'type-to-reducer';

import getWorkFlowByInspectionId from './actions/getWorkFlowByInspectionId.action';
import getWorkFlowStepContent from './actions/getWorkFlowStepContent.action';
import updateWorkFlowStepContent from './actions/updateWorkFlowStepContent.action';
import uploadDoc from './actions/uploadDoc.action';
import uploadPhoto from './actions/uploadPhoto.action';
import deleteDoc from './actions/deleteDoc.action';
import deletePhoto from './actions/deletePhoto.action';
import updateWorkFlowStatus from './actions/updateWorkFlowStatus.action';
import getConstructionMethodList from './actions/getConstructionMethodList.action';
import getITPDetailByID from './actions/getITPDetailByID.action';

const initialState = {
  workflow: [],
  workflowContent: [],
  constructionMethodList: [],
};

export const actions = {
  getWorkFlowByInspectionId: getWorkFlowByInspectionId.action,
  getWorkFlowStepContent: getWorkFlowStepContent.action,
  updateWorkFlowStepContent: updateWorkFlowStepContent.action,
  uploadDoc: uploadDoc.action,
  uploadPhoto: uploadPhoto.action,
  deleteDoc: deleteDoc.action,
  deletePhoto: deletePhoto.action,
  updateWorkFlowStatus: updateWorkFlowStatus.action,
  getConstructionMethodList: getConstructionMethodList.action,
  getITPDetailByID: getITPDetailByID.action,
};

export default typeToReducer(
  {
    ...getWorkFlowByInspectionId.reducer,
    ...getWorkFlowStepContent.reducer,
    ...updateWorkFlowStepContent.reducer,
    ...uploadDoc.reducer,
    ...uploadPhoto.reducer,
    ...deleteDoc.reducer,
    ...deletePhoto.reducer,
    ...updateWorkFlowStatus.reducer,
    ...getConstructionMethodList.reducer,
    ...getITPDetailByID.reducer,
  },
  initialState,
);
