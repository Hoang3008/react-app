import React from 'react';
import {Image, SectionList, Text, TouchableHighlight, View} from 'react-native';
import {pathOr} from 'ramda';
import {connect} from 'react-redux';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {getConstructionMethodFileUrl} from '../../api/urls';
import RouteNames from '../../RouteNames';
import {isNil, split} from 'ramda';

const ConstructionMethodContent = (props) => {
  const {constructionMethodSteps, navigation} = props;

  const renderItem = ({index, item, section}) => {
    const {Description, PictureServerPath} = item;
    const fileURL = getConstructionMethodFileUrl(PictureServerPath);

    return (
      <View style={{paddingLeft: 30}}>
        <Text style={[styles.headerText, {flex: 1, marginTop: 15}]}>
          {section.index + 1}.{index + 1} {Description}
        </Text>
        <TouchableHighlight
          onPress={() => {
            navigation.push(RouteNames.ImageFullScreen, {
              title: Description,
              info: {
                information: '',
                uploadedBy: '',
                category: '',
                location: '',
              },
              url: fileURL,
            });
          }}>
          <Image
            style={{
              resizeMode: 'center',
              width: SCREEN_WIDTH / 3,
              height: SCREEN_WIDTH / 3,
              marginTop: 6,
            }}
            source={{uri: fileURL}}
          />
        </TouchableHighlight>
      </View>
    );
  };

  const renderImages= (item) => {
    const {
      section: {title, ListOrder, Description, PictureServerPath},
    } = item;
    const dataRender = !isNil(PictureServerPath) && split('|',PictureServerPath).map((image) => {
      const fileURL =  getConstructionMethodFileUrl(image);
      return (
        <TouchableHighlight
            onPress={() => {
              navigation.push(RouteNames.ImageFullScreen, {
                title: Description,
                info: {
                  information: '',
                  uploadedBy: '',
                  category: '',
                  location: '',
                },
                url: fileURL,
              });
            }}>
          <Image
            style={{
              resizeMode: 'center',
              width: SCREEN_WIDTH / 4,
              height: SCREEN_WIDTH / 4,
              marginTop: 6,
              marginRight: 3,
            }}
            source={{uri: fileURL}}
          />
        </TouchableHighlight>
      )
    })

    return (
      <>
       {dataRender}
      </>
    )
  }

  const renderSectionHeader = (item) => {
    const {
      section: {title, ListOrder, Description, PictureServerPath},
    } = item;
    const fileURL = getConstructionMethodFileUrl(PictureServerPath);

    return (
      <View>
        <Text
          style={[
            styles.headerText,
            {flex: 1, marginRight: 15, marginTop: 15},
          ]}>
          {ListOrder}. {title}
        </Text>
        {/* {!!PictureServerPath && (
          <TouchableHighlight
            onPress={() => {
              navigation.push(RouteNames.ImageFullScreen, {
                title: Description,
                info: {
                  information: '',
                  uploadedBy: '',
                  category: '',
                  location: '',
                },
                url: fileURL,
              });
            }}>
            <Image
              style={{
                resizeMode: 'center',
                width: SCREEN_WIDTH / 3,
                height: SCREEN_WIDTH / 3,
                marginTop: 6,
              }}
              source={{uri: fileURL}}
            />
          </TouchableHighlight>
          
        )} */}

        {!!PictureServerPath && (
          <View style={styles.flexImages}>
              {renderImages(item)}
          </View>
        )}
      </View>
      
    );
  };

  return (
    <View>
      <SectionList
        sections={constructionMethodSteps}
        keyExtractor={(item, index) => item.ID}
        renderItem={renderItem}
        renderSectionHeader={renderSectionHeader}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
      />
    </View>
  );
};

const styles = {
  headerText: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    color: '#3E3F42',
  },
  flexImages: {
    flexDirection:  'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    direction: 'inherit',
    flexWrap: 'wrap'
  }
};

const mapStateToProps = ({auth: {user, project}, inspectionDetail}, props) => {
  const {workflowId, inspectionId} = props;
  const constructionMethodSteps = pathOr(
    [],
    [inspectionId, 'constructionMethodSteps'],
    inspectionDetail,
  );

  return {
    user,
    project,
    constructionMethodSteps:
      constructionMethodSteps.filter(
        ({FK_ITPStepID}) => FK_ITPStepID === workflowId,
      ) || [],
  };
};

export default connect(
  mapStateToProps,
  null,
)(ConstructionMethodContent);
