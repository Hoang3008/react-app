import React, {useEffect, useState} from 'react';
import moment from 'moment';
import {connect} from 'react-redux';
import {propOr, pathOr, isEmpty, isNil} from 'ramda';
import {
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';

import styles from './styles';
import Overview from './Overview';
import {actions} from './InspectionDetail.reducer';
import GeneralInformation from './GeneralInformation';
import WorkFlowStepContent from './WorkFlowStepContent';
import ConstructionMethodContent from './ConstructionMethodContent';

import Theme from '../../Theme';
import RouteNames from '../../RouteNames';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import InspectionTabBar from '../../components/InspectionTabBar/InspectionTabBar';
import {actions as commonActions} from '../CommonProjectData/CommonProjectData.reducer';
import HorizontalScrollInspectionFlow from '../../components/HorizontalScrollInspectionFlow/HorizontalScrollInspectionFlow';

const InspectionDetail = (props) => {
  const {
    navigation,
    inspection,
    workflow,
    getProjectLocations,
    getPeopleInProject,
    getConstructionMethodList,
    user,
    project,
    inspectionId,
    getITPDetailByID,
  } = props;

  const {PMProjectID} = project;

  useEffect(() => {
    getITPDetailByID(inspectionId);
  }, [getITPDetailByID, inspectionId]);

  useEffect(() => {
    getConstructionMethodList(PMProjectID);
  }, [getConstructionMethodList, PMProjectID]);

  const {
    PMProjectInspNo,
    PMProjectInspName,
    PMProjectInspEstEndDate,
    AAStatus,
  } = inspection;

  const [activeIndex, setActiveIndex] = useState(0);
  const [tabIndex, setTabIndex] = useState(0);

  const completeIndexCount = (workflow || []).filter(({OverviewStatus}) => {
    return OverviewStatus === 'Pass';
  }).length;

  useEffect(() => {
    if (!workflow || isEmpty(workflow)) {
      setActiveIndex(0);
      return;
    }

    let newActiveIndex;

    if (activeIndex >= 0) {
      const {OverviewStatus} = workflow[activeIndex];

      newActiveIndex =
        activeIndex < workflow.length - 1 && OverviewStatus === 'Pass'
          ? activeIndex + 1
          : activeIndex;
    } else {
      newActiveIndex = workflow.findIndex(({OverviewStatus}) => {
        return OverviewStatus !== 'Pass';
      });
    }

    setActiveIndex(newActiveIndex < 0 ? 0 : newActiveIndex);
  }, [completeIndexCount]);

  useEffect(() => {
    getProjectLocations(PMProjectID);
    getPeopleInProject({PMProjectID, UserName: user.ADUserName});
  }, [
    user,
    PMProjectID,
    inspectionId,
    getPeopleInProject,
    getProjectLocations,
  ]);
  const allowEdit = true; // OverviewStatus !== 'Pass';

  const onPressUpdateDefect = () => {
    navigation.push(RouteNames.InspectionUpdate, {
      ...inspection,
      onDelete: () => navigation.goBack(),
    });
  };

  const renderNavigation = () =>
  {
    const { canChange } = props;
    return  (
        <View style={styles.navigationBar}>
          <View style={styles.row}>
            <UniImageButton
              containerStyle={{
                paddingHorizontal: Theme.margin.m16,
                paddingVertical: Theme.margin.m6,
              }}
              source={require('../../assets/img/ic_back.png')}
              onPress={() => navigation.goBack()}
            />
            <Text style={styles.textHeader}>Inspection Detail</Text>
          </View>
    
          {
            canChange &&
              <TouchableOpacity
              onPress={onPressUpdateDefect}
              style={{paddingVertical: Theme.margin.m6}}
              >
              <Text style={{color: 'white', fontSize: Theme.fontSize.normal}}>
                Edit
              </Text>
            </TouchableOpacity>
          }   
        </View>
      );
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}

      <ScrollView style={{backgroundColor: '#F6F6F8'}}>
        <View style={[styles.sectionView, {backgroundColor: '#F6F6F8'}]}>
          <Text
            style={[styles.title, {marginTop: 15}]}
            numberOfLines={2}
            ellipsizeMode="tail">
            #{PMProjectInspNo} - {PMProjectInspName}
          </Text>
          <View style={[styles.row, {marginTop: 4}]}>
            <Text style={[styles.subLabelText, {width: 67}]}>Due date:</Text>
            <Text style={[styles.subLabelText]}>
              {!isNil(PMProjectInspEstEndDate) && moment(PMProjectInspEstEndDate).format('DD/MM/YYYY')}
            </Text>
          </View>
          <View style={[styles.row, {marginTop: 4}]}>
            <Text style={[styles.subLabelText, {width: 67}]}>Status:</Text>
            <View
              style={{
                width: 8,
                height: 8,
                borderRadius: 4,
                backgroundColor: '#34AA44',
              }}
            />
            <Text style={[styles.normalText, {marginLeft: 6}]}>{AAStatus}</Text>
          </View>
        </View>

        <Overview id={inspectionId} />

        <GeneralInformation inspection={inspection} />

        <View
          style={[
            styles.sectionView,
            {marginTop: 15, paddingLeft: 0, paddingRight: 0},
          ]}>
          <View
            style={[
              styles.headerContainer,
              {marginTop: 15, paddingLeft: 15, paddingRight: 15},
            ]}>
            <Text style={[styles.headerText]}>Quy trình</Text>
            <TouchableOpacity
              onPress={() => {
                navigation.push(RouteNames.InspectionMethodList, {
                  inspectionId,
                  workflow,
                });
              }}>
              <Text style={styles.headerButton}>Hình ảnh mẫu</Text>
            </TouchableOpacity>
          </View>

          <HorizontalScrollInspectionFlow
            style={{marginTop: 20}}
            activeIndex={activeIndex}
            flowList={workflow}
            onChangeActiveIndex={setActiveIndex}
          />

          <View style={[styles.sectionView, {paddingBottom: 50}]}>
            <InspectionTabBar
              activeTab={tabIndex}
              onChangeTab={setTabIndex}
              style={{marginTop: 14}}
            />

            {tabIndex === 0 ? (
              <WorkFlowStepContent
                navigation={navigation}
                allowEdit={allowEdit}
                inspectionId={inspectionId}
                workflowId={pathOr(
                  0,
                  [activeIndex, 'PMProjectInsStpID'],
                  workflow,
                )}
              />
            ) : (
              <ConstructionMethodContent
                navigation={navigation}
                inspectionId={inspectionId}
                workflowId={pathOr(
                  0,
                  [activeIndex, 'PMProjectInsStpID'],
                  workflow,
                )}
              />
            )}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (
  {auth: {user, project, master}, commonData, inspectionList, inspectionDetail},
  props,
) => {
  const inspectionFromProps = pathOr({}, ['route', 'params'], props);
  const inspectionId = inspectionFromProps.PMProjectInspID;

  const inspects = Array.isArray(propOr([], 'inspectionList', inspectionList))
    ? propOr([], 'inspectionList', inspectionList)
    : [];

  const inspection =
    inspects.find(({PMProjectInspID}) => PMProjectInspID === inspectionId) ||
    inspectionFromProps;

  const permissions = commonData.Permission.find(x => x.module === 'itp');
  const canChange =   inspection?.AACreatedUser === user?.ADUserName || (permissions.permissions?.length > 0 && permissions.permissions[0] === 'CHANGE_ALL')  

  return {
    user,
    project,
    inspectionId,
    inspection: pathOr(
      inspection,
      [inspectionId, 'overview', 0],
      inspectionDetail,
    ),
    workflow: pathOr([], [inspectionId, 'steps'], inspectionDetail),
    canChange
  };
};

const mapDispatchToProps = {
  updateWorkFlowStatus: actions.updateWorkFlowStatus,
  getProjectLocations: commonActions.getProjectLocations,
  getPeopleInProject: commonActions.getPeopleInProject,
  getConstructionMethodList: actions.getConstructionMethodList,
  getITPDetailByID: actions.getITPDetailByID,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionDetail);
