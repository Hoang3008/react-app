import React, {Component} from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

import RouteNames from '../../RouteNames';
import UniButton from '../../components/UniButton';
import {UniInput, UniCheckbox, UniText} from '../../components';
import {scaledWidth} from '../../utils/DimensionUtils';
import LinkButton from '../../components/LinkButton';
import loginAction from './actions/login.action';
import {isEmpty, propOr} from 'ramda';
import {showAlert} from '../../utils/MessageUtils';
import configs from '../../global/configs';
import {DropDownWithListScreen} from '../SelectFromList/SelectFromListLogin';
import Configs from '../../global/configs';
import API from '../../api/API.service';
import envJSON from '../../env';
import {userAddDomain} from '../../action/DomainAction';


class AuthLogin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      onCloud: true,
      api: undefined,
      apiList: [],
    };
  }

  componentDidMount(): void {
    const {isLoggedIn, navigation} = this.props;
    AsyncStorage.getItem('apiObject').then((jsonStr) => {
      if (!jsonStr) {
        if (isLoggedIn) {
          navigation.navigate(RouteNames.SelectProjectScreen);
          return;
        }

        this.setState({
          onCloud: true,
          api: undefined,
        });
        return;
      }

      const api = JSON.parse(jsonStr);
      let url = api.api || '';
      if (url.endsWith('/')) {
        url = url.substring(0, url.length - 1);
      }
      Configs.apiBasedURL = url;
      Configs.drawingBasedURL = api.drawingBasedURL;

      if (isLoggedIn) {
        navigation.navigate(RouteNames.SelectProjectScreen);
        return;
      }

      this.setState({
        onCloud: true,
        api,
      });
    });

    const url = `${envJSON.apiBasedURL}/settings/onDemand`;
    API.get(url)
      .then((response) => {
        this.setState({
          apiList: response.data || [],
        });
      })
      .catch((error) => {
        console.log(222222222, error);
      });
  }

  onChangeUserName = (value) => {
    this.userName = value;
  };

  onChangePassword = (value) => {
    this.password = value;
  };

  onChangeDomain = (value) => {
    this.domain = value;
  };

  // substring the last character if contains '/'
  setConfigFromApi = (api) => {
    let url;

    url = api.api;
    Configs.apiBasedURL = url;
    Configs.drawingBasedURL = api.drawingBasedURL;
    AsyncStorage.setItem('apiObject', JSON.stringify(api));
  }

  onPressLogin = async () => {
    const {login, navigation, userAddDomain} = this.props;
    const {onCloud, api} = this.state;

    if (!onCloud && api) {
      this.setConfigFromApi(api);
    } else {
      Configs.apiBasedURL = envJSON.apiBasedURL;
      Configs.drawingBasedURL = envJSON.drawingBasedURL;
      AsyncStorage.setItem('apiObject', '');
    }

    try {
      const response = await login(this.userName, this.password);
      if (response && !isEmpty(response.data)) {
        navigation.navigate(RouteNames.SelectProjectScreen);
      } else {
        showAlert('The username or password is incorrect');
      }
    } catch (e) {
      showAlert('The username or password is incorrect');
    }
  };

  render() {
    const {navigation} = this.props;
    const {onCloud, api, apiList} = this.state;
    const logoIcon = configs.useRocezLoginIcon
      ? require('../../assets/img/logo-rocez.png')
      : require('../../assets/img/unicore.png');

    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          paddingTop: 44,
          backgroundColor: '#ffffff',
        }}>
        <Image style={styles.logo} source={logoIcon} />
        <UniInput
          placeholder="Email"
          initValue={this.userName}
          onChangeText={this.onChangeUserName}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder="Password"
          initValue={this.password}
          secureTextEntry={true}
          onChangeText={this.onChangePassword}
          containerStyle={styles.editText}
        />
        <View style={styles.row}>
          <UniCheckbox
            checked={onCloud}
            onChangeValue={() => this.setState({onCloud: !onCloud})}
          />
          <UniText containerStyle={{marginHorizontal: 6}}>On Cloud</UniText>
        </View>

        {!onCloud && (
          <View style={styles.row}>
            <DropDownWithListScreen
              containerStyle={{
                width: '100%',
                marginHorizontal: 0,
                marginVertical: 0,
              }}
              navigation={navigation}
              options={apiList}
              label={'Server'}
              idKey={'api'}
              noKey={'no'}
              nameKey={'name'}
              descriptionKey=""
              value={propOr('', 'api', api)}
              onSelect={(item) =>
                this.setState({
                  api: item,
                  onCloud: false,
                })
              }
            />
          </View>
        )}

        <UniButton
          text={'Login'}
          onPress={this.onPressLogin}
          containerStyle={styles.button}
        />

        <View
          style={{
            flex: 1,
            paddingBottom: 10,
            justifyContent: 'flex-end',
          }}>
          <LinkButton
            text="Create an account"
            onPress={() =>
              this.props.navigation.navigate(RouteNames.CreateAccount)
            }
          />
          <LinkButton
            text="Forgot password"
            onPress={() =>
              this.props.navigation.navigate(RouteNames.ForgotPassword)
            }
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: scaledWidth(175),
    height: scaledWidth(95),
    marginVertical: scaledWidth(60),
    resizeMode: 'contain',
  },
  editText: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
  button: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
  row: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
});

const mapStateToProps = ({auth: {user, isLoggedIn}, domains}) => ({
  user,
  isLoggedIn,
  domains,
});

const mapDispatchToProps = {
  login: loginAction,
  userAddDomain,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthLogin);
