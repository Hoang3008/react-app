import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';

const actionType = 'LOGIN';

const loginAPI = (username, password) => {
  const url = withKey('/user/login');
  return API.post(url, {username, password});
};

const action = createAction(actionType, loginAPI);

export default action;
