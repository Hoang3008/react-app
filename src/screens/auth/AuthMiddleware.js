import {propOr} from 'ramda';
import {Auth} from '../../action/type';

const AuthMiddleware = (store) => (next) => (action) => {
  const {type, payload} = action;
  if (type.endsWith('_FAILURE')) {
    const status = propOr(200, 'status', payload);
    if (status === 401) {
      store.dispatch({type: Auth.LOGOUT});
      return {};
    }
  }
  return next(action);
};

export default AuthMiddleware;
