import typeToReducer from 'type-to-reducer';
import getDrawingSubFolder from './actions/DrawingSubFolder.action';

const initialState = {
  drawingAreaList: [],
};

export const actions = {
    getDrawingSubFolder: getDrawingSubFolder.action,
};

export default typeToReducer(
  {
    ...getDrawingSubFolder.reducer,
  },
  initialState,
);
