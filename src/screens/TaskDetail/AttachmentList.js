import {connect} from 'react-redux';
import React, {useEffect} from 'react';
import {pathOr, propOr} from 'ramda';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
} from 'react-native';
import moment from 'moment';
import {SwipeListView} from 'react-native-swipe-list-view';

import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import RouteNames from '../../RouteNames';
import {getTaskDocumentFileUrl} from '../../api/urls';
import {EMPTY} from '../../global/constants';

const AttachmentList = (props) => {
  const {
    id,
    user,
    getTaskDocumentList,
    attachmentList,
    navigation,
    peopleInProject,
    deleteAttachment,
  } = props;

  useEffect(() => {
    if (!id) {
      return;
    }
    getTaskDocumentList(id);
  }, [id, getTaskDocumentList]);

  const handleDelete = (item) => () => {
    Alert.alert(
      'Delete',
      'Do you want to delete this attachment?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => deleteAttachment(item),
        },
      ],
      {cancelable: false},
    );
  };

  const handleClickItem = (url, fileName, item) => () => {
    const fileNameLowerCase = fileName.toLowerCase();

    if (fileNameLowerCase.endsWith('pdf')) {
      navigation.navigate(RouteNames.PDFFullScreen, {url: url});
    } else {
      navigation.navigate(RouteNames.ImageFullScreen, {
        title: 'Task Photo',
        item,
        url: url,
      });
    }
  };

  const renderItem = ({item}) => {
    const {PMTaskDocName, AACreatedDate, AACreatedUser} = item;

    const createdUser =
      AACreatedUser === `${user.ADUserID}`
        ? user
        : peopleInProject.find(
            ({ADUserID, PMProjectPeopleID, ADUserName}) =>
              AACreatedUser === `${PMProjectPeopleID}` ||
              AACreatedUser === `${ADUserID}` ||
              AACreatedUser === ADUserName,
          );

    return (
      <TouchableHighlight
        onPress={handleClickItem(
          getTaskDocumentFileUrl({fileName: PMTaskDocName}),
          PMTaskDocName,
          item,
        )}>
        <View style={styles.item}>
          <Text style={styles.title}>{PMTaskDocName}</Text>
          <Text style={styles.subText}>
            {propOr('', 'fullName', createdUser) ||
              `${propOr('', 'ADUserFirstName', createdUser)} ${propOr(
                '',
                'ADUserLastName',
                createdUser,
              )}`}
          </Text>
          <Text style={styles.subText}>
            {moment(AACreatedDate).format('YYYY-MM-DD hh:mm A')}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  const renderHiddenItem = ({item}) => {
    return (
      <View style={styles.rowBack}>
        <TouchableOpacity
          onPress={handleDelete(item)}
          style={{
            width: 100,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={styles.delete}>Delete</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SwipeListView
      style={{width: '100%'}}
      data={attachmentList}
      renderItem={renderItem}
      renderHiddenItem={renderHiddenItem}
      keyExtractor={(item) => item.PMTaskDocID}
      ItemSeparatorComponent={() => <View style={styles.divider} />}
      leftOpenValue={0}
      rightOpenValue={-100}
      closeOnRowBeginSwipe
      disableRightSwipe
      scrollEnabled={false}
      showsVerticalScrollIndicator={false}
      recalculateHiddenLayout={true}
    />
  );
};

const mapStateToProps = ({auth: {user, project}, commonData}, props) => {
  const {id} = props;
  const allDocuments = pathOr([], ['photos', id], commonData);
  const {PMProjectID} = project;

  return {
    user,
    attachmentList: allDocuments,
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
  };
};

const styles = {
  rowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingLeft: 15,
  },
  item: {
    backgroundColor: '#ffffff',
    display: 'flex',
    flexDirection: 'column',
    paddingVertical: 15,
    paddingHorizontal: 15,
  },
  title: {
    fontSize: 16,
    color: '#245894',
  },
  subText: {
    fontSize: 12,
    color: '#aaa',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  delete: {
    fontSize: 16,
    color: '#ffffff',
  },
};

const mapDispatchToProps = {
  getTaskDocumentList: actions.getTaskDocumentList,
  deleteAttachment: actions.deletePhoto,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AttachmentList);
