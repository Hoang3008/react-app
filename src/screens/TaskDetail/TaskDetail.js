import React from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  Animated,
  TextInput,
  Keyboard,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import DocumentPicker from 'react-native-document-picker';
import moment from 'moment';
import {all, isEmpty, pathOr, propOr} from 'ramda';

import Theme from '../../Theme';
import {
  UniStatusBarBackground,
  UniImageButton,
  UniBottomSheet,
} from '../../components';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {
  getTaskCommentFileUrl,
  getUserAvatarUrlByUsername,
} from '../../api/urls';
import {
  getFileName,
  filterStatusResponseList,
  isCommentSystemType,
} from '../defect/DefectHelper';
import RouteNames from '../../RouteNames';
import {EMPTY, OFFLINE_ACTION_TYPES} from '../../global/constants';
import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import {actions as ListActions} from '../TaskList/TaskList.reducer';
import styles from './TaskDetail.styles';
import {PRIORITY_COLOR} from '../ScheduleList/ScheduleList';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import AttachmentList from './AttachmentList';
import getTaskList from '../TaskList/actions/getTaskList.action';

export const getDefaultTask = (user, project) => {
  const {PMProjectID} = project;
  const {ADUserID, ADUserName} = user;
  const date = new Date();
  const fromTask = {};

  return {
    AACreatedUser: ADUserName,
    AAUpdatedUser: ADUserName,
    PMTaskNo: 0,
    PMTaskName: 'Enter title',
    PMTaskDesc: 'Enter description',
    FK_ADUserID: ADUserID,
    PMTaskLat: 0,
    PMTaskLng: 0,
    FK_PPDrawingID: 0,
    FK_PMProjectID: PMProjectID,
    FK_ADAssignUserID: propOr(ADUserID, 'FK_ADAssignUserID', fromTask),
    PMTaskStartDate: propOr(date.toISOString(), 'PMTaskStartDate', fromTask),
    PMTaskEndDate: propOr(date.toISOString(), 'PMTaskEndDate', fromTask),
    PMTaskCompleteCheck: false,
    PMTaskLocation: '',
    PMTaskManPower: 0,
    PMTaskCost: 0,
    PMTaskTags: '',
    PMTaskTypeCombo: 'task',
    PMTaskPriorityCombo: 'Priority1',
    FK_PMProjectCategoryID: 0,
    FK_PMProjectLocationID: 0,
    FK_PMTaskGroupID: 0,
  };
};

class TaskDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectingDocumentForComment: false,
      showAllComments: false,
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    const {
      task,
      project,
      addActionIntoQueue,
      getTaskList,
      getTaskDetail,
    } = this.props;
    const PMProjectID = propOr(0, 'PMProjectID', project);
    const taskId = pathOr(0, ['route', 'params', 'PMTaskID'], this.props);

    if (!isEmpty(task) || taskId) {
      if (isEmpty(task)) {
        getTaskList(PMProjectID);
      }

      this.getDetailData(taskId);
    } else {
      addActionIntoQueue({
        type: OFFLINE_ACTION_TYPES.CREATE_DEFECT,
        data: this.getDefaultTaskWrapper(),
      });
    }
  }

  getDefaultTaskWrapper = () => {
    const {user, project} = this.props;
    return getDefaultTask(user, project);
  };

  getDetailData = (PMTaskID) => {
    const {getTaskCommentList, getTaskDetail} = this.props;
    getTaskCommentList(PMTaskID);
    getTaskDetail(PMTaskID);
  };

  onPressUpdateTask = () => {
    const {navigation, task} = this.props;

    navigation.navigate(RouteNames.TaskUpdate, {
      task,
      onDelete: () => {
        this.props.route &&
          this.props.route.params &&
          this.props.route.params.onDelete &&
          this.props.route.params.onDelete();
        this.props.navigation.goBack();
      },
      onUpdate: (task) => {
        this.props.route &&
          this.props.route.params &&
          this.props.route.params.onUpdate &&
          this.props.route.params.onUpdate();
        this.getData();
        this.setState({task});
      },
    });
  };

  onPressAddAttachment = (item) => {
    this.onCreateTaskDocument(item);
  };

  onCreateTaskDocument = (image) => {
    const {addActionIntoQueue, task, user} = this.props;
    const {ADUserID} = user || {};
    const {PMTaskID, offlineId} = task || {};
    const body = {AACreatedUser: ADUserID, FK_PMTaskID: PMTaskID || offlineId};

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.ADD_PHOTO,
      data: {body, image},
    });
  };

  onChangeComment = (value) => {
    this.inputingComment = value;
  };

  onPressSendComment = async () => {
    if (
      (!this.inputingComment || this.inputingComment.trim().length === 0) &&
      !this.state.selectingDocumentForComment
    ) {
      return;
    }

    const {user, task, addActionIntoQueue} = this.props;
    const {selectingDocumentForComment} = this.state;

    const {ADUserName} = user || {};
    const {PMTaskID, offlineId} = task || {};

    const body = {
      AACreatedUser: `${ADUserName}`,
      AAUpdatedUser: `${ADUserName}`,
      ADUserName: ADUserName,
      PMTaskCommentUserName: ADUserName,
      FK_PMTaskID: PMTaskID || offlineId,
      PMTaskCommentDesc:
        this.inputingComment && this.inputingComment.length > 0
          ? this.inputingComment
          : '',
      PMTaskCommentType: 'USER_INPUT',
    };

    if (this.inputingComment && this.inputingComment.length > 0) {
      body.PMTaskCommentDesc = this.inputingComment;
    }

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.ADD_COMMENT,
      data: {body, image: selectingDocumentForComment},
    });

    this.textInputComment && this.textInputComment.clear();
    this.inputingComment = '';
    this.inputingComment = '';
    Keyboard.dismiss();
    this.setState({selectingDocumentForComment: false}, () => {
      this.scrollView && this.scrollView.scrollToEnd({animated: true});
    });
  };

  showBottomSheetFromAttachment = () => {
    this.bottomSheet &&
      this.bottomSheet.show(
        this.handleSelectBottomMenu(this.onPressAddAttachment),
        true,
      );
  };

  showBottomSheetFromComment = () => {
    this.bottomSheet &&
      this.bottomSheet.show(
        this.handleSelectBottomMenu(this.onPressSelectedAttachForComment),
        true,
      );
  };

  handleSelectBottomMenu = (callback) => (item) => {
    const options = {
      cropping: false,
      writeTempFile: false,
      compressImageMaxWidth: 1024,
      compressImageMaxHeight: 1024,
    };

    switch (item) {
      case 'camera':
        this.props.navigation.push(RouteNames.CaptureImage, {
          onSave: (path) => {
            callback({
              uri: path || '',
              filename: getFileName(path),
              type: 'image/png',
            });
          },
        });
        break;
      case 'photos-device':
        ImagePicker.openPicker(options)
          .then((image) =>
            this.props.navigation.push(RouteNames.SketchDrawing, {
              imagePath: image.path,
              onSave: (path) => {
                callback({
                  uri: path || '',
                  filename: getFileName(path),
                  type: 'image/png',
                });
              },
            }),
          )
          .catch(() => {});
        break;
      case 'files-device':
        DocumentPicker.pick({
          type: [DocumentPicker.types.pdf],
        }).then((file) => {
          callback({
            uri: file.uri || '',
            filename: file.name,
            type: file.type,
          });
        });
        break;
    }
  };

  onPressSelectedAttachForComment = (item) => {
    this.setState({
      selectingDocumentForComment: item,
    });
  };

  renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={{...styles.row, flex: 1}}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() =>
              this.props.navigation && this.props.navigation.goBack()
            }
          />
          <View>
            <View style={styles.row}>
              <Text style={styles.textHeader} numberOfLines={1}>
                Task Detail
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          onPress={this.onPressUpdateTask}
          style={{paddingVertical: Theme.margin.m6}}>
          <Text style={{color: 'white', fontSize: Theme.fontSize.normal}}>
            Edit
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  onPressPhoto = (item, url) => {
    const {navigation} = this.props;

    navigation.navigate(RouteNames.ImageFullScreen, {
      item,
      url: url,
    });
  };

  onPressPDF = (url) => {
    const {navigation} = this.props;
    navigation.navigate(RouteNames.PDFFullScreen, {url: url});
  };

  renderCommentsItem = (item = {}) => {
    if (isEmpty(item)) {
      return <View />;
    }

    let {
      AACreatedUser,
      AAUpdatedDate,
      PMTaskCommentServerPath,
      PMTaskCommentDesc,
      PMTaskCommentUserName,
      tempImage,
    } = item;

    const PMTaskCommentContentType = propOr(
      '',
      'PMTaskCommentContentType',
      item,
    );

    const isTempFile = !PMTaskCommentServerPath && tempImage;
    const fileURL = isTempFile
      ? pathOr('', ['tempImage', 'uri'], item)
      : getTaskCommentFileUrl({fileName: PMTaskCommentServerPath});
    const fileType = isTempFile
      ? pathOr('', ['tempImage', 'type'], item)
      : PMTaskCommentContentType;

    const {user} = this.props;

    let avatar = getUserAvatarUrlByUsername(AACreatedUser);
    let name = PMTaskCommentUserName;
    const date = AAUpdatedDate ? new Date(AAUpdatedDate) : null;

    const isSystem = isCommentSystemType(item);

    const showDate = !date
      ? 'Not synced yet'
      : `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} at ${
          (date.getHours() % 12 || 12) < 10 ? '0' : ''
        }${date.getHours() % 12 || 12}:${
          date.getMinutes() < 10 ? '0' : ''
        }${date.getMinutes()} ${date.getHours() >= 12 ? 'PM' : 'AM'}`;

    if (isSystem) {
      return (
        <View style={{paddingVertical: 8}}>
          <View
            style={{
              ...styles.row,
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
            }}>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
              }}>
              {showDate}
            </Text>
            <Text
              style={{
                paddingVertical: Theme.margin.m2,
                fontSize: Theme.fontSize.normal,
                color: Theme.colors.textNormal,
              }}>
              {name === '1' ? 'System' : name}
            </Text>
          </View>
          <View
            style={{
              alignItems: 'center',
            }}>
            {PMTaskCommentDesc && PMTaskCommentDesc.length > 0 ? (
              <Text
                style={{
                  color: Theme.colors.textNormal,
                  fontStyle: 'italic',
                  fontSize: 12,
                }}>
                {PMTaskCommentDesc}
              </Text>
            ) : null}
          </View>
        </View>
      );
    }

    if (
      `${user.ADUserID}` === AACreatedUser ||
      user.ADUserName === AACreatedUser
    ) {
      return (
        <View style={{paddingVertical: 8}}>
          <View
            style={{
              ...styles.row,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              display: 'flex',
            }}>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
                textAlign: 'right',
              }}>
              {showDate}
            </Text>
          </View>
          <View
            style={{
              paddingLeft:
                Theme.specifications.commentAvatarSize + Theme.margin.m12,
              alignItems: 'flex-end',
            }}>
            {PMTaskCommentDesc && PMTaskCommentDesc.length > 0 ? (
              <Text
                style={{
                  marginTop: Theme.margin.m6,
                  paddingVertical: Theme.margin.m8,
                  paddingHorizontal: Theme.margin.m10,
                  backgroundColor: '#3483F6',
                  borderRadius: 10,
                  borderWidth: 0,
                  color: '#fff',
                  fontStyle: isSystem ? 'italic' : 'normal',
                  fontSize: isSystem ? 12 : 14,
                }}>
                {PMTaskCommentDesc}
              </Text>
            ) : null}
            {fileURL && fileType.includes('image') ? (
              <TouchableOpacity
                onPress={() =>
                  isTempFile ? true : this.onPressPhoto(null, fileURL)
                }>
                <Image
                  style={{
                    resizeMode: 'center',
                    width: SCREEN_WIDTH / 3,
                    height: SCREEN_WIDTH / 3,
                    marginTop: 6,
                  }}
                  source={{uri: fileURL}}
                />
              </TouchableOpacity>
            ) : fileType.toLowerCase().includes('pdf') ? (
              <TouchableOpacity
                onPress={() => (isTempFile ? true : this.onPressPDF(fileURL))}>
                <Text
                  style={{
                    maxWidth: 280,
                    padding: 10,
                    borderRadius: 4,
                    borderWidth: 1,
                    borderColor: '#999',
                    color: '#3173ff',
                  }}>
                  {fileURL}
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      );
    }

    return (
      <View style={{paddingVertical: 8}}>
        <View style={styles.row}>
          <ImageBackground
            style={{
              resizeMode: 'center',
              width: Theme.specifications.commentAvatarSize,
              height: Theme.specifications.commentAvatarSize,
              borderRadius: Theme.specifications.commentAvatarSize / 2,
            }}
            source={require('../../assets/img/default_avatar.png')}>
            <Image
              style={{
                resizeMode: 'center',
                width: Theme.specifications.commentAvatarSize,
                height: Theme.specifications.commentAvatarSize,
                borderRadius: Theme.specifications.commentAvatarSize / 2,
              }}
              source={{uri: avatar}}
            />
          </ImageBackground>
          <View style={{marginLeft: Theme.margin.m12}}>
            <Text
              style={{
                paddingVertical: Theme.margin.m2,
                fontSize: Theme.fontSize.normal,
                color: Theme.colors.textNormal,
                fontWeight: 'bold',
              }}>
              {name}
            </Text>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
              }}>
              {showDate}s
            </Text>
          </View>
        </View>
        <View
          style={{
            paddingLeft:
              Theme.specifications.commentAvatarSize + Theme.margin.m12,
            alignItems: 'flex-start',
          }}>
          {PMTaskCommentDesc && PMTaskCommentDesc.length > 0 ? (
            <Text
              style={{
                marginTop: Theme.margin.m6,
                paddingVertical: Theme.margin.m8,
                paddingHorizontal: Theme.margin.m10,
                backgroundColor: Theme.colors.backgroundComment,
                borderRadius: 10,
                borderWidth: 0,
                fontStyle: isSystem ? 'italic' : 'normal',
                fontSize: isSystem ? 12 : 14,
              }}>
              {PMTaskCommentDesc}
            </Text>
          ) : null}
          {fileURL && fileType.includes('image') ? (
            <TouchableOpacity
              onPress={() =>
                isTempFile ? true : this.onPressPhoto(null, fileURL)
              }>
              <Image
                style={{
                  resizeMode: 'center',
                  width: SCREEN_WIDTH / 3,
                  height: SCREEN_WIDTH / 3,
                  marginTop: 6,
                }}
                source={{uri: fileURL}}
              />
            </TouchableOpacity>
          ) : fileType.toLowerCase().includes('pdf') ? (
            <TouchableOpacity
              onPress={() => (isTempFile ? true : this.onPressPDF(fileURL))}>
              <Text
                style={{
                  maxWidth: 280,
                  padding: 10,
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: '#999',
                  color: '#3173ff',
                }}>
                {PMTaskCommentServerPath}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    );
  };

  renderComments = () => {
    const {showAllComments} = this.state;
    const {allComments} = this.props;

    const showComments = filterStatusResponseList(allComments, showAllComments);

    return (
      <View
        style={{
          padding: Theme.margin.m15,
          backgroundColor: 'white',
          marginBottom: Theme.specifications.inputCommentContainerHeight,
        }}>
        <View
          style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 20,
          }}>
          <Text style={styles.headerText}>Comments:</Text>
          <View style={{flex: 1}} />
          <TouchableOpacity
            style={{
              marginLeft: 20,
              maxWidth: 150,
              paddingRight: Theme.margin.m12,
              paddingVertical: Theme.margin.m4,
            }}
            onPress={() => this.setState({showAllComments: !showAllComments})}>
            <Image
              style={{
                width: Theme.specifications.checkBoxSize,
                height: Theme.specifications.checkBoxSize,
              }}
              source={
                showAllComments
                  ? require('../../assets/img/ic_checkbox_active.png')
                  : require('../../assets/img/ic_checkbox_inactive.png')
              }
            />
          </TouchableOpacity>
          <Text
            style={{
              color: Theme.colors.textNormal,
              fontSize: Theme.fontSize.normal,
            }}>
            Show history
          </Text>
        </View>
        <FlatList
          scrollEnabled={false}
          showsVerticalScrollIndicator={false}
          data={showComments}
          renderItem={({item, index}) => this.renderCommentsItem(item, index)}
          keyExtractor={(item, index) => 'comment_' + index}
        />
      </View>
    );
  };

  renderInputComment = () => {
    const {selectingDocumentForComment} = this.state;
    return (
      <Animated.View
        style={[
          styles.inputCommentContainer,
          {
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
          },
        ]}>
        {selectingDocumentForComment ? (
          <View
            style={{
              paddingBottom: Theme.margin.m12,
              paddingTop: Theme.margin.m6,
              paddingHorizontal: Theme.margin.m6,
            }}>
            {selectingDocumentForComment.filename &&
            selectingDocumentForComment.filename
              .toLowerCase()
              .endsWith('pdf') ? (
              <Text
                style={{
                  maxWidth: 280,
                  padding: 10,
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: '#999',
                  color: '#3173ff',
                }}>
                {selectingDocumentForComment.filename}
              </Text>
            ) : (
              <Image
                style={{
                  resizeMode: 'contain',
                  width: SCREEN_WIDTH / 3,
                  height: SCREEN_WIDTH / 3,
                  borderRadius: Theme.margin.m8,
                }}
                source={{uri: this.state.selectingDocumentForComment.uri}}
              />
            )}
            <TouchableOpacity
              style={{position: 'absolute', right: 0, top: 0}}
              onPress={() =>
                this.setState({selectingDocumentForComment: false})
              }>
              <Image
                style={{width: 20, height: 20}}
                source={require('../../assets/img/ic_close.png')}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={this.showBottomSheetFromComment}
            style={{
              paddingVertical: Theme.margin.m6,
              paddingHorizontal: Theme.margin.m10,
            }}>
            <Image
              style={{width: Theme.margin.m20, height: Theme.margin.m20}}
              source={require('../../assets/img/ic_attach.png')}
            />
          </TouchableOpacity>
          <TextInput
            ref={(ref) => (this.textInputComment = ref)}
            style={styles.inputComment}
            multiline
            onChangeText={this.onChangeComment}
          />
          <TouchableOpacity
            onPress={this.onPressSendComment}
            style={styles.buttonSendComment}>
            <Text style={{color: 'white', fontSize: Theme.fontSize.normal}}>
              Share
            </Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    );
  };

  renderBasicInfo = () => {
    const {
      PMTaskName,
      PMTaskID,
      PMTaskStartDate,
      PMTaskPriorityCombo,
      PMTaskCompletePct,
    } = this.props.task || {};
    return (
      <View style={[styles.sectionView]}>
        <Text
          style={[styles.title, {marginTop: 15}]}
          numberOfLines={2}
          ellipsizeMode="tail">
          #{PMTaskID} - {PMTaskName}
        </Text>
        <View style={[styles.row, {marginTop: 4}]}>
          <Text style={[styles.subLabelText, {width: 80}]}>Due date:</Text>
          <Text style={[styles.normalText]}>
            {moment(PMTaskStartDate).format('DD/MM/YYYY HH:mm')}
          </Text>
        </View>
        <View style={[styles.row, {marginTop: 4}]}>
          <Text style={[styles.subLabelText, {width: 80}]}>Priority:</Text>
          <View
            style={{
              width: 8,
              height: 8,
              borderRadius: 4,
              backgroundColor:
                PMTaskCompletePct === 100
                  ? PRIORITY_COLOR.Complete
                  : PRIORITY_COLOR[PMTaskPriorityCombo],
            }}
          />
          <Text style={[styles.normalText, {marginLeft: 6}]}>
            {PMTaskPriorityCombo}
          </Text>
        </View>
      </View>
    );
  };

  renderGeneralInfo = () => {
    const {peopleInProject, task} = this.props;
    const {FK_ADAssignUserID, PMTaskDesc, PMTaskCompletePct} = task || {};

    const assignContact = peopleInProject.find(
      ({PMProjectPeopleID}) => FK_ADAssignUserID === PMProjectPeopleID,
    );
    return (
      <>
        <View style={[styles.sectionView, {marginTop: 15}]}>
          <View style={[styles.headerContainer]}>
            <Text style={[styles.headerText]}>General Information</Text>
          </View>

          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Assign to:</Text>
            <Text style={[styles.normalText, {color: '#245894'}]}>
              {propOr('', 'fullName', assignContact)}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Percent Complete:</Text>
            <Text style={[styles.normalText]}>{PMTaskCompletePct}%</Text>
          </View>
        </View>
        <View style={[styles.sectionView, {marginTop: 25}]}>
          <Text style={styles.headerText}>Description</Text>
          <Text style={[styles.normalText, {marginTop: 4}]}>
            {PMTaskDesc || 'N/A'}
          </Text>
        </View>
      </>
    );
  };

  renderAttachments = () => {
    const {task, navigation} = this.props;
    const {PMTaskID} = task;
    return (
      <>
        <View style={[styles.sectionView, {marginTop: 15}]}>
          <View style={[styles.headerContainer]}>
            <Text style={[styles.headerText, {flex: 1}]}>Attachments</Text>
            <TouchableOpacity onPress={this.showBottomSheetFromAttachment}>
              <Text
                style={[styles.headerText, {color: '#245894', fontSize: 28}]}>
                +
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <AttachmentList id={PMTaskID} navigation={navigation} />
      </>
    );
  };

  render() {
    return (
      <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="height" enabled>
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <StatusBar
            backgroundColor={Theme.colors.primary}
            barStyle="light-content"
          />
          <UniStatusBarBackground />
          {this.renderNavigation()}
          <ScrollView
            showsVerticalScrollIndicator={false}
            ref={(ref) => (this.scrollView = ref)}>
            {this.renderBasicInfo()}
            <View style={styles.divider} />
            {this.renderGeneralInfo()}
            <View style={styles.divider} />
            {this.renderAttachments()}
            <View style={styles.divider} />
            {this.renderComments()}
          </ScrollView>
          {this.renderInputComment()}
          <UniBottomSheet ref={(ref) => (this.bottomSheet = ref)} />
        </View>
      </KeyboardAvoidingViewWrapper>
    );
  }
}

const mapStateToProps = (
  {auth: {user, project}, commonData, taskList},
  props,
) => {
  const taskId = pathOr(0, ['route', 'params', 'PMTaskID'], props);
  const allComments = pathOr([], ['comments', taskId], commonData);
  const {PMProjectID} = project;
  const task =  propOr(EMPTY.ARRAY, 'taskList', taskList).find(
    ({PMTaskID}) => PMTaskID === taskId,
  ) || {};

  return {
    user,
    project,
    allComments,
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    taskList: propOr(EMPTY.ARRAY, 'taskList', taskList),
    task:
      propOr(EMPTY.ARRAY, 'taskList', taskList).find(
        ({PMTaskID}) => PMTaskID === taskId,
      ) || {},
  };
};

const mapDispatchToProps = {
  addActionIntoQueue: actions.addActionIntoQueue,
  getTaskCommentList: actions.getTaskCommentList,
  getTaskDetail: ListActions.getTaskDetail,
  getTaskList: ListActions.getTaskList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TaskDetail);
