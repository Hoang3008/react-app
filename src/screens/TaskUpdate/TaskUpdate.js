import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {pathOr, propOr} from 'ramda';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import styles from './TaskUpdate.styles';

import {
  UniImageButton,
  UniStatusBarBackground,
  UniDropbox,
  UniInput,
  UniButton,
  UniDatePicker,
  UniBottomSheet,
  UniMultiSelect
} from '../../components';
import Theme from '../../Theme';
import {showAlert} from '../../utils/MessageUtils';
import {requestDeleteTask} from '../../api/defect';
import {getFileName, PRIORITY} from '../defect/DefectHelper';
import {EMPTY, OFFLINE_ACTION_TYPES} from '../../global/constants';
import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import {getDefaultTask} from '../TaskDetail/TaskDetail';
import RouteNames from '../../RouteNames';
import ImagePicker from 'react-native-image-crop-picker';
import DocumentPicker from 'react-native-document-picker';
import {createLogList} from '../../utils/LogTaskSchedule.utils';


class TaskUpdate extends React.Component {
  constructor(props) {
    super(props);
    let task = pathOr(undefined, ['route', 'params', 'task'], this.props);

    this.inputingTitle = task ? task.PMTaskName : '';
    let userId = pathOr(undefined, ['user', 'ADUserID'], this.props);
    let permissions = this.props.modulePermissions.find(x => x.module === 'task');
    this.state = {
      task,
      isNewTask: !!!task,
    };
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {allTasks} = this.props;

    if (prevProps.allTasks !== allTasks) {
      const lat = pathOr(undefined, ['route', 'params', 'lat'], this.props);
      const lng = pathOr(undefined, ['route', 'params', 'lng'], this.props);

      if (lat === undefined || lng === undefined) {
        return;
      }

      const taskFromMemory = allTasks.find(
        ({PMTaskLat, PMTaskLng}) => PMTaskLat === lat && lng === PMTaskLng,
      );

      if (taskFromMemory) {
        this.setState({
          task: {
            ...taskFromMemory,
            ...this.state.task,
          },
        });
      }
    }
  }

  componentWillUnmount(): void {
    this.props.route &&
      this.props.route.params &&
      this.props.route.params.onGoBack &&
      this.props.route.params.onGoBack();
  }

  componentDidMount(): void {
    const {getPeopleInProject, project, user} = this.props;
    const {PMProjectID} = project;
    getPeopleInProject({PMProjectID, UserName: user.ADUserName});

    if (!this.props.master) {
      showAlert('Alert', 'Cannot get Master data!', () => {
        this.props.navigation.goBack();
      });
    }
    if (!this.state.task) {
      this.setState({
        task: getDefaultTask(user, project),
      });
    }
  }

  onPressPriority = (value) => {
    this.setState({
      task: {...this.state.task, PMTaskPriorityCombo: value},
    });
  };

  onChangeDesc = (value) => {
    this.setState({
      task: {...this.state.task, PMTaskDesc: value},
    });
  };

  onChangePercent = (value) => {
    let numberValue = parseInt(value, 10);

    if (numberValue < 0 || isNaN(numberValue)) {
      numberValue = 0;
    } else if (numberValue > 100) {
      numberValue = 100;
    }

    this.setState({
      task: {...this.state.task, PMTaskCompletePct: numberValue},
    });
  };

  onPressDelete = () => {
    requestDeleteTask(this.state.task).then((response) => {
      if (response) {
        this.props.route &&
          this.props.route.params &&
          this.props.route.params.onDelete &&
          this.props.route.params.onDelete();
        this.props.navigation.goBack();
      }
    });
  };

  onPressSave = () => {
    const {addActionIntoQueue, navigation, peopleInProject} = this.props;
    const {attachment} = this.state;
    this.setState(
      {task: {...this.state.task, PMTaskName: this.inputingTitle}},
      () => {
        const PMTaskID =
          pathOr(0, ['task', 'PMTaskID'], this.state) ||
          pathOr(0, ['task', 'offlineId'], this.state);

        if (!PMTaskID) {
          addActionIntoQueue({
            type: OFFLINE_ACTION_TYPES.CREATE_DEFECT,
            data: this.state.task,
            attachment,
          });
          navigation.goBack();
        } else {
          let currentData = pathOr(
            undefined,
            ['route', 'params', 'task'],
            this.props,
          );
          addActionIntoQueue({
            type: OFFLINE_ACTION_TYPES.UPDATE_DEFECT,
            data: this.state.task,
            currentData,
            logs: createLogList(currentData, this.state.task, {
              peopleInProject
            }),
          });
          navigation.goBack();
        }
      },
    );
  };

  renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() =>
              this.props.navigation && this.props.navigation.goBack()
            }
          />
          <View>
            <Text style={styles.textHeader}>Task Detail</Text>
          </View>
        </View>
      </View>
    );
  };

  renderPriority = (key, value, color, currentPriority = '', short) => {
    let active = currentPriority.toLowerCase() === key.toLowerCase();
    return (
      <TouchableOpacity
        style={{alignItems: 'center'}}
        onPress={() => this.onPressPriority(key)}>
        <View
          style={[
            styles.priorityContainer,
            active ? {backgroundColor: color, borderWidth: 0} : {},
          ]}>
          <Text
            style={[styles.textNormalSecond, active ? {color: 'white'} : {}]}>
            {short}
          </Text>
        </View>
        <Text
          style={[
            styles.textSmallSecond,
            active ? {color: Theme.colors.textNormal} : {},
          ]}>
          {value}
        </Text>
      </TouchableOpacity>
    );
  };

  renderSelectPriority = () => {
    let {PMTaskPriorityCombo} = this.state.task;
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          backgroundColor: Theme.colors.backgroundHeader,
          paddingTop: Theme.margin.m12,
          paddingBottom: Theme.margin.m10,
          paddingHorizontal: Theme.margin.m15,
        }}>
        {this.renderPriority(
          'Priority1',
          'Priority 1',
          '#FFEB33',
          PMTaskPriorityCombo,
          'P1',
        )}
        {this.renderPriority(
          'Priority2',
          'Priority 2',
          '#f6ab2f',
          PMTaskPriorityCombo,
          'P2',
        )}
        {this.renderPriority(
          'Priority3',
          'Priority 3',
          '#c2cc06',
          PMTaskPriorityCombo,
          'P3',
        )}
        {this.renderPriority(
          'Complete',
          'Complete',
          '#62E787',
          PMTaskPriorityCombo,
          '✓',
        )}
        {this.renderPriority(
          'Verify',
          'Verify',
          '#519D59',
          PMTaskPriorityCombo,
          '✓',
        )}
      </View>
    );
  };

  onChangeUserName = (value) => {
    this.inputingTitle = value;
  };

  canDeleteTask = () => {
    const {user} = this.props;
    const {task} = this.state;

    if (!task.PMTaskID) {
      return false;
    }

    const priority = propOr('', 'priority', task);

    return (
      task.AACreatedUser === user.ADUserName &&
      priority !== PRIORITY.COMPLETE &&
      priority !== PRIORITY.VERIFY
    );
  };

  onPressAddAttachment = (attachment) => {
    this.setState({
      attachment,
    });
  };

  showBottomSheetFromAttachment = () => {
    this.bottomSheet &&
      this.bottomSheet.show(
        this.handleSelectBottomMenu(this.onPressAddAttachment),
        true,
      );
  };

  handleSelectBottomMenu = (callback) => (item) => {
    const options = {
      cropping: false,
      writeTempFile: false,
      compressImageMaxWidth: 1024,
      compressImageMaxHeight: 1024,
    };

    switch (item) {
      case 'camera':
        this.props.navigation.push(RouteNames.CaptureImage, {
          onSave: (path) => {
            callback({
              uri: path || '',
              filename: getFileName(path),
              type: 'image/png',
            });
          },
        });
        break;
      case 'photos-device':
        ImagePicker.openPicker(options)
          .then((image) =>
            this.props.navigation.push(RouteNames.SketchDrawing, {
              imagePath: image.path,
              onSave: (path) => {
                callback({
                  uri: path || '',
                  filename: getFileName(path),
                  type: 'image/png',
                });
              },
            }),
          )
          .catch(() => {});
        break;
      case 'files-device':
        DocumentPicker.pick({
          type: [DocumentPicker.types.pdf],
        }).then((file) => {
          callback({
            uri: file.uri || '',
            filename: file.name,
            type: file.type,
          });
        });
        break;
    }
  };

  renderAttachmentItem = () => {
    const {attachment} = this.state;

    if (!attachment) {
      return null;
    }

    const {filename, type} = attachment;

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={[styles.item, {flex: 1}]}>
          <Text style={styles.title}>{filename}</Text>
          <Text style={styles.subText}>{type}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.setState({attachment: null});
          }}>
          <View style={{padding: 10}}>
            <MaterialIcons size={20} color="#3E3F42" name="delete" />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  renderAttachments = () => {
    const {task, attachment} = this.state;

    if (task && task.PMTaskID) {
      return null;
    }

    return (
      <>
        <View style={[styles.sectionView, {marginTop: 15}]}>
          <View style={[styles.headerContainer]}>
            <Text style={[styles.headerText, {flex: 1}]}>Attachments</Text>
            {!attachment && (
              <TouchableOpacity onPress={this.showBottomSheetFromAttachment}>
                <Text
                  style={[styles.headerText, {color: '#245894', fontSize: 28}]}>
                  +
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </>
    );
  };

  render() {
    const {peopleInProject, canChange, projectId} = this.props;
    const {task} = this.state;

    if (!task) {
      return <View />;
    }

    return (
      <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="padding" enabled>
        <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
          <StatusBar
            backgroundColor={Theme.colors.primary}
            barStyle="light-content"
          />
          <UniStatusBarBackground />
          {this.renderNavigation()}
          <ScrollView>
            {this.renderSelectPriority()}

            <UniInput
              placeholder="Enter title"
              autoSelectAllWhenTextIs="Enter title"
              initValue={propOr('Enter title', 'PMTaskName', task)}
              onChangeText={this.onChangeUserName}
              containerStyle={styles.editText}
            />

            <UniDropbox
              label={'Assignee'}
              selectedValue={propOr('', 'FK_ADAssignUserID', task)}
              options={peopleInProject}
              selectedKey="PMProjectPeopleID"
              pathLeft={'shortName'}
              pathValue={'fullName'}
              onSelect={(item, {PMProjectPeopleID}) =>
                this.setState({
                  task: {
                    ...task,
                    FK_ADAssignUserID: PMProjectPeopleID,
                  },
                })
              }
            />

            <UniDatePicker
              label={'Due date'}
              mode={'datetime'}
              selectedValue={propOr(
                new Date().toISOString(),
                'PMTaskStartDate',
                task,
              )}
              onSelect={(item) =>
                this.setState({
                  task: {...task, PMTaskStartDate: item},
                })
              }
            />

            <View style={styles.percentContainer}>
              <Text
                style={[
                  styles.textNormalSecond,
                  {marginLeft: Theme.margin.m14, flex: 0},
                ]}>
                Percent Completed
              </Text>
              <UniInput
                keyboardType="number-pad"
                forceValueFromProp
                value={`${propOr(0, 'PMTaskCompletePct', task)}`}
                initValue={`${propOr(0, 'PMTaskCompletePct', task)}`}
                onChangeText={this.onChangePercent}
                containerStyle={{
                  ...styles.editText,
                  marginHorizontal: 0,
                  marginBottom: 0,
                  marginTop: 0,
                  textAlign: 'right',
                  borderColor: 'transparent',
                  flex: 1,
                }}
              />
            </View>

            {
          task?.PMTaskID &&  
          <UniMultiSelect 
              projectId={projectId}
              taskId={task?.PMTaskID}
              canChange={canChange}
              type={'task'}
            />
            }

            <UniInput
              numberOfLines={3}
              autoSelectAllWhenTextIs="Enter Description"
              initValue={propOr('', 'PMTaskDesc', task)}
              onChangeText={this.onChangeDesc}
              containerStyle={styles.editTextDescription}
            />

            {this.renderAttachments()}
            {this.renderAttachmentItem()}

            <View
              style={{alignItems: 'flex-end', marginRight: Theme.margin.m16}}>
              {this.canDeleteTask() && (
                <TouchableOpacity onPress={this.onPressDelete} disabled={!canChange}>
                  <Text
                    style={{
                      color: !canChange ? Theme.colors.black : Theme.colors.textDangerous,
                      fontSize: Theme.fontSize.normal,
                      paddingVertical: Theme.margin.m8,
                    }}>
                    Delete Task
                  </Text>
                </TouchableOpacity>
              )}
              { (canChange || this.state.isNewTask) && 
                 <UniButton
                 text={'Save'}
                 onPress={this.onPressSave}
                 containerStyle={[styles.button, {marginTop: 40}]}
               />
              }
              <UniBottomSheet ref={(ref) => (this.bottomSheet = ref)} />
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingViewWrapper>
    );
  }
}

const mapStateToProps = ({
  auth: {user, master, project},
  commonData,
  taskList,
},props) => {
  const modulePermissions = pathOr([],['Permission'],commonData);
  const PMProjectID = propOr(0, 'PMProjectID', project);
  const item = pathOr(undefined, ['route', 'params', 'task'], props);
  const canChange =  item?.edit;

  return {
    user,
    master,
    project,
    allTasks: taskList.taskList,
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    modulePermissions,
    projectId:PMProjectID,
    taskId: item?.PMtaskID,
    canChange
  };
};

const mapDispatchToProps = {
  addActionIntoQueue: actions.addActionIntoQueue,
  getPeopleInProject: actions.getPeopleInProject,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TaskUpdate);

// https://apidev.rocez.com/PMTasks/CreateObject
//   Request Method: POST
