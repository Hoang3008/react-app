import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textSectionHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNormal,
    fontWeight: '600',
  },
  priorityContainer: {
    width: Theme.specifications.icBig,
    height: Theme.specifications.icBig,
    marginHorizontal: Theme.margin.m5,
    borderRadius: Theme.specifications.icBig / 2,
    borderWidth: 1,
    borderColor: Theme.colors.border,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNormalSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
  },
  textSmallSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.small_12,
  },
  button: {
    marginLeft: Theme.margin.m15,
    marginVertical: Theme.margin.m16,
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },
  editTextDescription: {
    textAlignVertical: 'top',
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
    paddingTop: 10,
    height: 100,
  },
  headerText: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    color: '#3E3F42',
  },
  percentContainer: {
    marginHorizontal: Theme.margin.m15,
    marginVertical: Theme.margin.m8,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.border,
    borderRadius: Theme.margin.m4,
    paddingVertical: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  sectionView: {
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },
  item: {
    backgroundColor: '#ffffff',
    display: 'flex',
    flexDirection: 'column',
    paddingVertical: 15,
    paddingHorizontal: 15,
  },
  title: {
    fontSize: 16,
    color: '#245894',
  },
  subText: {
    fontSize: 12,
    color: '#aaa',
  },
};
