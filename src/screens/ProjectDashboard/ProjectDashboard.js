import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, Platform} from 'react-native';
import React, {useEffect} from 'react';
import {pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

import DashboardMain from '../dashboard/DashboardMain';
import DrawingTabs from '../DrawingTabs/DrawingTabs';
import Notifications from '../NotificationList/NotificationList';
import NotificationTab from '../NotificationList/NotificationTab';
import Dashboard from '../ProjectOverviewDashboard/ProjectOverviewDashboard';
import Settings from '../Settings';
import Theme from '../../Theme';
import IconWithBadge from '../../components/IconWithBadge/IconWithBadge';
import {getDrawingListAction} from '../../action/DrawingActions';
import {readNotificationAction} from '../NotificationList/actions/readNotification.action';
import {handleClickNotification} from '../../utils/NotificationUtils';
import AsyncStorage from '@react-native-community/async-storage';
import {PERMISSION_TYPE} from '../../global/constants';
import {actions} from '../CommonProjectData/CommonProjectData.reducer';


const TabNames = {
  dashboard: 'Tools',
  create: 'Drawing',
  camera: 'Dashboard',
  notification: 'Notifications',
  settings: 'Settings',
};

const Tab = createBottomTabNavigator();

const ProjectDashboard = (props) => {
  const {
    notificationCount,
    navigation,
    route,
    getDrawingList,
    PMProjectID,
    token,
    readNotification,
    modulePermissions,
    getDefectCodeList,
    getProjectCategories,
    getProjectLocations,
    getPeopleInProject,
    getAllPermissionsByProject,
    user
  } = props;

  const time = pathOr(0, ['params', 'time'], route);
  const project = pathOr(0, ['params', 'project'], route);

  useEffect(() => {
    const notification = pathOr(undefined, ['params', 'notification'], route);
    handleClickNotification(navigation, readNotification)(notification);
  }, [time]);

  useEffect(() => {
    // When change project will call api 
    Promise.all([
      getDefectCodeList({projectId: PMProjectID}, token),
      getProjectLocations(PMProjectID),
      getProjectCategories(PMProjectID),
      getPeopleInProject({
        PMProjectID: PMProjectID,
        UserName: user.ADUserName,
      }),
      getAllPermissionsByProject(PMProjectID),
    ])
    .then((data) => {
      const permissions = pathOr({},[4,'value','data','modulePermissions'],data);
      const permissionsDefect = 
          permissions.find((item) => item.module === PERMISSION_TYPE.MODULE_DEFECT && item.permissions != PERMISSION_TYPE.TYPE_NONE);

          console.log(permissionsDefect);
       AsyncStorage.setItem('modulePermissions',permissionsDefect?.permissions[0]);
    });
  }, [project]);

  useEffect(() => {
    loadDrawingList();
  }, []);

  const loadDrawingList = () => {
    if (PMProjectID) {
      getDrawingList(undefined, PMProjectID, token);
    }
  };

  return (
    <Tab.Navigator
      tabBarOptions={{
        safeAreaInset: {bottom: 'never', top: 'never'},
        activeBackgroundColor: Theme.colors.bottomNavbar,
        inactiveBackgroundColor: Theme.colors.bottomNavbar,
        activeTintColor: Theme.colors.activeTint,
        inactiveTintColor: Theme.colors.inactiveTint,
        tabStyle: {
          maxHeight: 60,
        },
        style: {
          paddingTop: Platform.OS === 'android' ? 10 : 0,
          paddingBottom: 0,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: Platform.OS === 'android' ? 'center' : 'flex-start',
          alignItems: Platform.OS === 'android' ? 'center' : 'flex-start',
          borderTopColor: 'transparent',
          maxHeight: Theme.specifications.bottomNavbarHeight,
          height: Theme.specifications.bottomNavbarHeight,
          backgroundColor: Theme.colors.bottomNavbar,
          elevation: 4,
          shadowColor: '#2f2f2f',
          shadowOpacity: 0.2,
          shadowRadius: 3,
        },
      }}>
      <Tab.Screen
        name={TabNames.dashboard}
        component={DashboardMain}
        options={{
          tabBarLabel: TabNames.dashboard,
          tabBarIcon: ({focused}) => (
            <Image
              style={{width: 28, height: 28, padding: 0}}
              resizeMode={'contain'}
              source={
                focused
                  ? require('../../assets/img/ic_tab_tool.png')
                  : require('../../assets/img/ic_tab_tool_inactive.png')
              }
            />
          ),
        }}
      />
      <Tab.Screen
        name={TabNames.camera}
        component={Dashboard}
        options={{
          tabBarLabel: TabNames.camera,
          tabBarIcon: ({focused}) =>
            focused ? (
              <SimpleLineIcons size={28} color="#245894" name="speedometer" />
            ) : (
              <SimpleLineIcons size={28} color="#B5C0DB" name="speedometer" />
            ),
        }}
      />
      <Tab.Screen
        name={TabNames.create}
        component={DrawingTabs}
        options={{
          tabBarLabel: TabNames.create,
          tabBarIcon: ({focused}) => (
            <Image
              style={{width: 28, height: 28, padding: 0}}
              resizeMode={'contain'}
              source={
                focused
                  ? require('../../assets/img/ic_tab_drawing.png')
                  : require('../../assets/img/ic_tab_drawing_inactive.png')
              }
            />
          ),
        }}
      />
      <Tab.Screen
        name={TabNames.notification}
        component={NotificationTab}
        options={{
          tabBarLabel: TabNames.notification,
          tabBarIcon: ({focused}) => (
            <IconWithBadge
              name="bell"
              badgeCount={notificationCount}
              color={
                focused ? Theme.colors.activeTint : Theme.colors.inactiveTint
              }
              size={40}
            />
          ),
        }}
      />
      <Tab.Screen
        name={TabNames.settings}
        component={Settings}
        options={{
          tabBarLabel: TabNames.settings,
          tabBarIcon: ({focused}) => (
            <Image
              style={{width: 28, height: 28, padding: 0}}
              resizeMode={'contain'}
              source={
                focused
                  ? require('../../assets/img/ic_tab_setting.png')
                  : require('../../assets/img/ic_tab_setting_inactive.png')
              }
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const mapStateToProps = ({auth: {user, project}, commonData}) => {
  const PMProjectID = propOr(0, 'PMProjectID', project);
  const modulePermissions = pathOr([],['Permission'],commonData);


  const notificationCount = pathOr(
    0,
    ['projectNotificationCount', PMProjectID],
    commonData,
  );

  return {
    user,
    project,
    PMProjectID,
    token: user.token,
    notificationCount,
    modulePermissions
    // notificationList: notificationList.notificationList.filter(
    //   ({projectId}) => projectId === PMProjectID,
    // ),
  };
};

const mapDispatchToProps = {
  getDrawingList: getDrawingListAction,
  readNotification: readNotificationAction,
  getDefectCodeList: actions.getDefectCodeList,
  getProjectCategories: actions.getProjectCategories,
  getProjectLocations: actions.getProjectLocations,
  getPeopleInProject: actions.getPeopleInProject,
  getAllPermissionsByProject: actions.getAllPermissionsByProject,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProjectDashboard);
