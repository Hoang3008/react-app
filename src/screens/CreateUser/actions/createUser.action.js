import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';

const actionType = 'CREATE_USER';

const createUserURL = () => withKey('/ADUsers/CreateObject');
const createUserAPI = (userInfo) => API.post(createUserURL(), userInfo);

const action = createAction(actionType, createUserAPI);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
