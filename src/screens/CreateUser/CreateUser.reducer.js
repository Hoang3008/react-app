import typeToReducer from 'type-to-reducer';
import createUser from './actions/createUser.action';

const initialState = {};

export const actions = {
  createUser: createUser.action,
};

export default typeToReducer(
  {
    ...createUser.reducer,
  },
  initialState,
);
