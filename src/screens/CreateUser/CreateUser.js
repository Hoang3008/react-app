import React, {useState} from 'react';
import {connect} from 'react-redux';
import {View, Text, StatusBar} from 'react-native';
import {
  UniImageButton,
  UniInput,
  UniStatusBarBackground,
} from '../../components';
import Theme from '../../Theme';
import {scaledWidth, SCREEN_WIDTH} from '../../utils/DimensionUtils';
import UniButton from '../../components/UniButton';
import {actions} from './CreateUser.reducer';

const CreateUser = (props) => {
  const {navigation, createUser} = props;
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => navigation.goBack()}
          />
          <View>
            <Text style={styles.textHeader}>Create your account</Text>
          </View>
        </View>
      </View>
    );
  };

  const handleCreateAccount = () => {
    createUser({
      ADUserName: email ? email.toLowerCase() : '',
      ADPassword: password,
      ADUserFirstName: firstName,
      ADUserLastName: lastName,
    })
      .then(() => navigation.goBack())
      .catch((error) => {});
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundColor}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{marginTop: 22}}>
        <UniInput
          placeholder={'First name'}
          initValue={firstName}
          onChangeText={setFirstName}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder={'Last name'}
          initValue={lastName}
          onChangeText={setLastName}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder={'Email'}
          initValue={email}
          onChangeText={setEmail}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder={'Password'}
          initValue={password}
          secureTextEntry={true}
          onChangeText={setPassword}
          containerStyle={styles.editText}
        />

        <UniButton
          text={'Create Account'}
          onPress={handleCreateAccount}
          containerStyle={styles.button}
        />
      </View>
    </View>
  );
};

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  editText: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
  button: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
};

const mapStateToProps = ({auth: {project}}) => ({project});

const mapDispatchToProps = {
  createUser: actions.createUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateUser);
