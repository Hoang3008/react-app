import {createAction} from 'redux-actions';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';
import API from '../../../api/API.service';
import {identity} from 'ramda';

const actionType = 'READ_NOTIFICATION';

const readAPI = (id) => {
  const url = withKey(`/notification/${id}/read`);
  return AsyncStorage.getItem('token').then((token) => {
    return API.patch(
      url,
      {},
      {
        headers: {
          Accept: 'application/json, text/plain, */*',
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, readAPI, identity);

const updateNotificationList = (state, {meta}) => {
  const notificationList = {...(state.notifications || {})};
  Object.keys(notificationList).forEach((key) => {
    const notifications = notificationList[key].map((item) => {
      if (item.id !== meta) {
        return item;
      }

      return {
        ...item,
        read: true,
      };
    });

    notificationList[key] = notifications;
  });

  return {
    ...state,
    notifications: notificationList,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: updateNotificationList,
    FAILURE: (state, action) => {
      return state;
    },
  },
};

export default {action, reducer};
