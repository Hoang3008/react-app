import {createAction} from 'redux-actions';

const actionType = 'UPDATE_NOTIFICATION_LIST';

const action = createAction(actionType);

const updateNotificationList = (state, {payload}) => {
  const notificationList = payload;
  return {
    ...state,
    notificationList: notificationList,
  };
};

const reducer = {
  [actionType]: updateNotificationList,
};

export default {action, reducer};

// body: "Enter description"
// drawingId: 319
// projectId: 117
// taskId: 2262
// title: "Task Notify test (2262) is assigned to you"
// type: "ASSIGNED_TASK"
// updatedDate: "2020-05-07T03:00:24.521Z"
