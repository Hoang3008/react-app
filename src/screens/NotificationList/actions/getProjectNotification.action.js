import {createAction} from 'redux-actions';
import {identity, isEmpty, propOr} from 'ramda';
import firestore from '@react-native-firebase/firestore';
import {filterNotificatios} from '../../../utils/FilterNotifications.utils'

const actionType = 'GET_ALL_PROJECT_NOTIFICATION';

const getAPI = (userId, projectId) => {
  if (!projectId) {
    return Promise.resolve([]);
  }

  const collectionPath = `users/${userId}/notifications`;
  const handler = firestore().collection(collectionPath);

  return handler
    .where('projectId', '==', projectId)
    .limit(100)
    .orderBy('updatedDate', 'desc')
    .get({source: 'server'})
    .then((result) => {
      const docs = result._docs || [];
      return {
        id: projectId,
        docs: (docs || []).map(({id, _data}) => ({
          id,
          ..._data,
        })),
      };
    });
};

const action = createAction(actionType, getAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  const {id, docs} = payload;
  let notificatiosDefect = [],
  notificationsTask = [],
  notificationsSchedule = [],
  notificationsMeeting = [],
  notificationsITP = [];


  filterNotificatios(docs, 
    notificatiosDefect, 
    notificationsTask, 
    notificationsSchedule, 
    notificationsMeeting, 
    notificationsITP);



  return {
    ...state,
    notifications: {
      ...propOr({}, 'notifications', state),
      [id]: docs || [],
    },
    notificatiosDefect: notificatiosDefect,
    notificationsTask: notificationsTask,
    notificationsSchedule: notificationsSchedule,
    notificationsMeeting: notificationsMeeting,
    notificationsITP: notificationsITP
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
