import React, {useEffect} from 'react';
import {FlatList, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';
import moment from 'moment';

import styles from './styles';
import {actions} from './NotificationList.reducer';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import {actions as CommonProjectDataActions} from '../CommonProjectData/CommonProjectData.reducer';
import {EMPTY} from '../../global/constants';
import {handleClickNotification} from '../../utils/NotificationUtils';

const NotificationMeeting = (props) => {
  const {
    user,
    project,
    notificationList,
    navigation,
    readNotification,
    count,
    getProjectNotification,
    getAllProjectNotificationUnreadCount,
    defectCurrent,
    getDefectListByDrawingId,
    notificationsITP
  } = props;
  const todayMoment = moment();

  const ADUserID = propOr('', 'ADUserID', user);
  const PMProjectID = propOr('', 'PMProjectID', project);

  useEffect(() => {
    getProjectNotification(ADUserID, PMProjectID);
  }, [count, getProjectNotification, ADUserID, PMProjectID]);

  const handleRefreshNotificationList = () =>
    getAllProjectNotificationUnreadCount(user.ADUserID, [PMProjectID]);

  const renderItem = ({item}) => {
    const {body, title, read, updatedDate} = item;
    const {defectCurrent, user, project, getDefectListByDrawingId} = props;

    const updatedMoment = moment(updatedDate);
    const diff = todayMoment.diff(updatedMoment, 'hours');
    const timeLabel =
      diff < 24
        ? updatedMoment.fromNow()
        : updatedMoment.format('YYYY-MM-DD hh:mm:ss');

    return (
      <TouchableOpacity
        onPress={() =>
          handleClickNotification(
            navigation,
            readNotification,
            defectCurrent,
            user,
            project,
            getDefectListByDrawingId,
            handleRefreshNotificationList,
          )(item)
        }>
        <View
          style={{
            paddingVertical: 10,
            paddingHorizontal: 15,
            display: 'flex',
            flexDirection: 'column',
            shadowColor: '#555',
            backgroundColor: 'white',
            borderRadius: 5,
          }}>
          <Text
            style={{
              marginTop: 7,
              fontSize: 16,
              color: '#3E3F42',
              fontWeight: read ? 'normal' : 'bold',
            }}>
            {title}
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: '#9EA0A5',
              fontWeight: read ? 'normal' : 'bold',
            }}>
            {body}
          </Text>
          <Text
            style={{
              marginTop: 6,
              fontSize: 12,
              color: '#9EA0A5',
            }}>
            {timeLabel}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        <FlatList
          keyExtractor={(item, index) => item.updatedDate}
          renderItem={renderItem}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          data={notificationsITP}
        />
      </View>
    </View>
  );
};

const mapStateToProps = ({
  auth: {user, project},
  commonData,
  notificationList,
}) => {
  const defectCurrent = propOr({}, 'defectListByDrawingId', commonData);
  const PMProjectID = propOr(0, 'PMProjectID', project);

  const notifications = pathOr(
    EMPTY.ARRAY,
    ['notifications', PMProjectID],
    notificationList,
  );

  const notificationsITP = pathOr(
    EMPTY.ARRAY,
    ['notificationsITP'],
    notificationList,
  );


  const count = pathOr(
    0,
    ['projectNotificationCount', PMProjectID],
    commonData,
  );

  return {
    user,
    project,
    count,
    notificationsITP,
    notificationList: (Array.isArray(notifications) ? notifications : []).sort(
      (b, a) => {
        if (a.updatedDate < b.updatedDate) {
          return -1;
        }
        if (a.updatedDate > b.updatedDate) {
          return 1;
        }

        return 0;
      },
    ),
    defectCurrent
  };
};

const mapDispatchToProps = {
  getProjectNotification: actions.getProjectNotification,
  readNotification: actions.readNotification,
  getAllProjectNotificationUnreadCount:
    CommonProjectDataActions.getAllProjectNotificationUnreadCount,
  getDefectListByDrawingId: CommonProjectDataActions.getDefectListByDrawingId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationMeeting);
