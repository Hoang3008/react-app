import React, {useEffect, useRef, useState} from 'react';
import {connect} from 'react-redux';
import {Animated, Image, TouchableOpacity} from 'react-native';
import {StatusBar, View} from 'react-native';
import ViewPager from '@react-native-community/viewpager';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import Tab from '../../components/ScheduleTab/ScheduleTab';
import styles from './styles';
import DrawingList from '../drawing/DrawingList';
import NotificationDefect from '../NotificationList/NotificationsDefect';
import NotificationsTask from '../NotificationList/NotificationsTask';
import NotificationSchedule from '../NotificationList/NotificationSchedule';
import NotificationMeeting from '../NotificationList/NotificationMeeting';
import NotificationITP from '../NotificationList/NotificationITP';


const NotificationTab = (props) => {
  const {navigation} = props;
  const pagerRef = useRef();
  const [tab, setTab] = useState(0);
  const [sortData, setSortData] = useState(false);
  const [showSearchBox, setShowSearchBox] = useState(false);
  const groupDropDownRef = useRef(null);

  useEffect(() => {
    if (!pagerRef.current) {
      return;
    }
    pagerRef.current.setPage(tab);
  }, [tab]);

  const showGroupDropDown = () => {
    if (!groupDropDownRef.current) {
      return;
    }
    groupDropDownRef.current.show();
  };

  const position = new Animated.Value(tab);
  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={[styles.row, {flex: 1}]}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m8,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
      </View>

      <View
        style={{
          width: 330,
          height: 40,
          backgroundColor: Theme.colors.backgroundColor,
          flexDirection: 'row',
          marginLeft: 10,
          marginRight: 10,
          borderRadius: 6,
          padding: 3,
        }}>
        {['Defect', 'Task','Schedule','Meeting','ITP'].map((route, index) => {
          const focusAnim = position.interpolate({
            inputRange: [0, 1, 2, 3, 4],
            outputRange: [0, 1, 2, 3, 4]
          });
          return (
            <Tab
              key={route}
              style={{flex: 1}}
              focusAnim={focusAnim}
              title={route}
              onPress={() => setTab(index)}
            />
          );
        })}
      </View>

      <View style={[styles.row, {flex: 1, justifyContent: 'flex-end'}]}>
        {tab === 1 && (
          <>
            <TouchableOpacity onPress={() => setSortData(!sortData)}>
              <Image
                style={styles.navIcon}
                source={require('../../assets/img/ic_font.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setShowSearchBox(!showSearchBox)}>
              <Image
                style={styles.navIcon}
                source={require('../../assets/img/ic_search.png')}
              />
            </TouchableOpacity>
          </>
        )}
      </View>
    </View>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundWhite,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        {tab === 0 && <NotificationDefect navigation={navigation} />}
        {tab === 1 && (
          <NotificationsTask navigation={navigation}/>
        )}
        {tab === 2 && (
          <NotificationSchedule navigation={navigation}/>
        )}
        {tab === 3 && (
          <NotificationMeeting navigation={navigation}/>
        )}
         {tab === 4 && (
          <NotificationITP navigation={navigation}/>
        )}
      </View>
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, photoList}) => {
  return {};
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationTab);
