import typeToReducer from 'type-to-reducer';
import updateNotificationList from './actions/updateNotificationList.action';
import readNotification from './actions/readNotification.action';
import getProjectNotification from './actions/getProjectNotification.action';

const initialState = {
  notifications: [],
  notificatiosDefect: [],
  notificationsTask: [],
  notificationsSchedule: [],
  notificationsMeeting: [],
  notificationsITP: []
};

export const actions = {
  updateNotificationList: updateNotificationList.action,
  readNotification: readNotification.action,
  getProjectNotification: getProjectNotification.action,
};

export default typeToReducer(
  {
    ...updateNotificationList.reducer,
    ...readNotification.reducer,
    ...getProjectNotification.reducer,
  },
  initialState,
);
