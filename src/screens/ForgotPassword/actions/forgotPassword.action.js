import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';

const actionType = 'FORGOT_PASSWORD';

const changePasswordURL = () => withKey('/ADUsers/ChangePassword');
const changePasswordAPI = (userInfo) => API.put(changePasswordURL(), userInfo);

const action = createAction(actionType, changePasswordAPI);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
