import React, {useState} from 'react';
import {connect} from 'react-redux';
import {View, Text, StatusBar} from 'react-native';
import {
  UniImageButton,
  UniInput,
  UniStatusBarBackground,
} from '../../components';
import Theme from '../../Theme';
import {scaledWidth, SCREEN_WIDTH} from '../../utils/DimensionUtils';
import UniButton from '../../components/UniButton';
import {actions} from './ForgotPassword.reducer';
import {isEmpty, pathOr} from 'ramda';
import {showAlert} from '../../utils/MessageUtils';

const ForgotPassword = (props) => {
  const {navigation, forgotPassword} = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() => navigation.goBack()}
          />
          <View>
            <Text style={styles.textHeader}>Create your account</Text>
          </View>
        </View>
      </View>
    );
  };

  const handleChangePassword = () => {
    forgotPassword({
      ADUserName: email ? email.toLowerCase() : '',
      ADOldPassword: password,
      ADNewPassword: newPassword,
    })
      .then((response) => {
        const data = pathOr({}, ['value', 'data'], response);
        if (!isEmpty(data)) {
          navigation.goBack();
        } else {
          showAlert('The current password is not correct!');
        }
      })
      .catch((error) => {});
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundColor}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{marginTop: 22}}>
        <UniInput
          placeholder={'Email'}
          initValue={email}
          onChangeText={setEmail}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder={'Current Password'}
          initValue={password}
          secureTextEntry={true}
          onChangeText={setPassword}
          containerStyle={styles.editText}
        />
        <UniInput
          placeholder={'New Password'}
          initValue={newPassword}
          secureTextEntry={true}
          onChangeText={setNewPassword}
          containerStyle={styles.editText}
        />

        <UniButton
          text={'Save'}
          onPress={handleChangePassword}
          containerStyle={styles.button}
        />
      </View>
    </View>
  );
};

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  editText: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
  button: {
    marginHorizontal: scaledWidth(30),
    marginVertical: 8,
  },
};

const mapStateToProps = ({auth: {project}}) => ({project});

const mapDispatchToProps = {
  forgotPassword: actions.forgotPassword,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPassword);
