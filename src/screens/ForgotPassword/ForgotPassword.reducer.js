import typeToReducer from 'type-to-reducer';
import forgotPassword from './actions/forgotPassword.action';

const initialState = {};

export const actions = {
  forgotPassword: forgotPassword.action,
};

export default typeToReducer(
  {
    ...forgotPassword.reducer,
  },
  initialState,
);
