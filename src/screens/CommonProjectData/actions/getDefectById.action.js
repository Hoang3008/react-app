import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_DEFECT_DETAIL';

const getAPIURL = (id, PMProjectID) => withKey(`/task/${id}`);
const getAPI = (id, PMProjectID) =>
  AsyncStorage.getItem('token').then((token) => {
    return API.get(getAPIURL(id, PMProjectID), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });

const action = createAction(actionType, getAPI);

const handleSuccess = (state, {payload}) => {
  const defect = propOr({}, 'data', payload);
  const {PMTaskID, FK_PPDrawingID} = defect;
  const defectList = propOr({}, 'defectList', state);
  const defectListByDrawing = propOr({}, 'defectListByDrawingId', state);

  // const rawDefectList = propOr([], 'rawDefectList', state); TODO
  const defectListByDrawingId = propOr([], FK_PPDrawingID, defectList);
  const defectListByDrawingIdAllProject = propOr([], FK_PPDrawingID, defectListByDrawing);

   

  return {
    ...state,
    defectList: {
      ...defectList,
      [FK_PPDrawingID]: defectListByDrawingId.map((item) => {
        if (item.PMTaskID === PMTaskID) {
          return defect;
        }

        return item;
      }),
    },
    defectListByDrawingId: {
      ...defectListByDrawing,
      [FK_PPDrawingID]: defectListByDrawingIdAllProject.map((item) => {
        if (item.PMTaskID === PMTaskID) {
          return defect;
        }

        return item;
      }),
    }
    // rawDefectList: rawDefectList.map((item) => {
    //   if (item.PMTaskID === PMTaskID) {
    //     return defect;
    //   }

    //   return item;
    // }), TODO
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleSuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
