import {propOr} from 'ramda';
import {actionType as updateITPActionType} from '../../InspectionUpdate/actions/updateInspection.action';
import {actionType as postITPWorkflowCommentActionType} from '../../InspectionWorkflowComment/actions/sendComment.action';
import {actionType as addITPWorkflowPhotoActionType} from '../../InspectionDetail/actions/uploadPhoto.action';
import {actionType as addITPWorkflowDocActionType} from '../../InspectionDetail/actions/uploadDoc.action';
import {actionType as updateWorkFlowStepContentActionType} from '../../InspectionDetail/actions/updateWorkFlowStepContent.action';
import {OFFLINE_ACTION_TYPES} from '../../../global/constants';

const handleAPISuccess = (state, {meta}) => {
  const {offlineId} = meta;
  const offlineQueue = propOr([], 'offlineQueue', state);

  return {
    ...state,
    offlineQueue: offlineQueue.filter(
      (item) => item.data.offlineId !== offlineId,
    ),
  };
};

const handleAPIFailed = (state, {meta}) => {
  const offlineQueue = propOr([], 'offlineQueue', state);
  const {offlineId} = meta;

  return {
    ...state,
    offlineQueue: offlineQueue
      .filter(
        (item) =>
          item.type === OFFLINE_ACTION_TYPES.ADD_PHOTO ||
          item.data.PMTaskName !== undefined,
      )
      .map((item) => {
        if (item.data.offlineId === offlineId) {
          return {
            ...item,
            data: {
              ...item.data,
              syncing: false,
            },
          };
        }

        return item;
      }),
  };
};

const reducer = {
  [updateITPActionType]: {
    SUCCESS: handleAPISuccess,
    FAILURE: handleAPIFailed,
  },
  [postITPWorkflowCommentActionType]: {
    SUCCESS: handleAPISuccess,
    FAILURE: handleAPIFailed,
  },
  [addITPWorkflowPhotoActionType]: {
    SUCCESS: handleAPISuccess,
    FAILURE: handleAPIFailed,
  },
  [addITPWorkflowDocActionType]: {
    SUCCESS: handleAPISuccess,
    FAILURE: handleAPIFailed,
  },
  [updateWorkFlowStepContentActionType]: {
    SUCCESS: handleAPISuccess,
    FAILURE: handleAPIFailed,
  },
};

export default {reducer};
