import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_PROJECT_LOCATIONS';

const getProjectLocationsURL = (projectId) =>
  // withKey(
  //   `/PMProjectLocations/GetAllDataByPMProjectID?FK_PMProjectID=${projectId}`,
  // );
   withKey(
    `/project/location?appFlag=1&projectId=${projectId}`,
  );
const getProjectLocationsAPI = (PMProjectID) => {
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(getProjectLocationsURL(PMProjectID), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getProjectLocationsAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const locations = propOr([], 'data', payload);
  const currentProject = propOr({}, 'currentProject', state);
  return {
    ...state,
    // [meta]: {
    //   ...currentProject,
    //   locations,
    // },
    currentProject : {
      ...currentProject,
      locations,
    }
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
