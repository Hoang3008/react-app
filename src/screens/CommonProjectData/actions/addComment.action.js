import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, omit, propOr} from 'ramda';
import {createFormData} from '../../../api/defect';

const actionType = 'ADD_COMMENT';

const config = (token) => ({
  headers: {
    Authorization: `Bearer ${token}`,
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'multipart/form-data',
  },
  timeout: 120000,
});

const createCommentAPI = (comment, token) => {
  const {body, image} = comment;

  const formBody = {
    taskCommentDesc: body.PMTaskCommentDesc,
    taskCommentType: body.PMTaskCommentType,
  };

  const formData = createFormData(formBody, image);

  console.log('start sent comment: ', body.FK_PMTaskID, formData, token);

  return API.post(
    withKey(`/comment/task/${body.FK_PMTaskID}`),
    formData,
    config(token),
  );
};

const action = createAction(actionType, createCommentAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  console.log('sync comment done', payload, meta);

  if (typeof payload.data === 'string') {
    return handleFailed(state, {payload, meta});
  }

  const {
    offlineId,
    body: {FK_PMTaskID},
  } = meta;

  const offlineQueue = propOr([], 'offlineQueue', state);
  const comments = propOr({}, 'comments', state);
  const commentList = propOr([], FK_PMTaskID, comments);

  return {
    ...state,
    comments: {
      ...omit([offlineId], comments),
      [FK_PMTaskID]: commentList.map((item) => {
        if (item.offlineId === offlineId) {
          return {
            ...item,
            ...payload.data,
            syncing: false,
          };
        }

        return item;
      }),
    },
    offlineQueue: offlineQueue.filter(
      (item) => item.data.offlineId !== offlineId,
    ),
  };
};

const handleStart = (state, {meta}) => {
  const {offlineId, body, image} = meta;
  const {FK_PMTaskID} = body;

  const offlineQueue = propOr([], 'offlineQueue', state);
  const comments = propOr({}, 'comments', state);
  const commentList = propOr([], FK_PMTaskID, comments);

  const isExisted = commentList.find(
    ({offlineId}) => offlineId === meta.offlineId,
  );

  return {
    ...state,
    comments: {
      ...comments,
      [FK_PMTaskID]: isExisted
        ? commentList
        : [...commentList, {...body, tempImage: image, offlineId}],
    },
    offlineQueue: offlineQueue.map((item) => {
      if (item.data.offlineId === offlineId) {
        return {
          ...item,
          data: {
            ...item.data,
            syncing: true,
          },
        };
      }

      return item;
    }),
  };
};

const handleFailed = (state, {payload, meta}) => {
  const error = propOr('', 'response', payload);

  console.log('sync comment failed', error, meta);

  const offlineQueue = propOr([], 'offlineQueue', state);
  const {offlineId} = meta;

  const newOfflineQueue =
    error.status === 400 || error.status === 404
      ? offlineQueue.filter((item) => item.data.offlineId !== offlineId)
      : offlineQueue.map((item) => {
          if (item.data.offlineId === offlineId) {
            return {
              ...item,
              data: {
                ...item.data,
                syncing: false,
              },
            };
          }

          return item;
        });

  return {
    ...state,
    offlineQueue: newOfflineQueue,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: handleStart,
    SUCCESS: handleAPISuccess,
    FAILURE: handleFailed,
  },
};

export default {action, reducer};
