import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_PROJECT_CATEGORIES';

const getProjectCategoriesURL = (projectId) =>
  // withKey(
  //   `/PMProjectCategorys/GetAllDataByPMProjectID?FK_PMProjectID=${projectId}`,
  // );
  withKey(
    `/settings/projectCatalogue/list/${projectId}?appFlag=1`,
  );
const getProjectCategoriesAPI = (PMProjectID) => {
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(getProjectCategoriesURL(PMProjectID), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getProjectCategoriesAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const categories = propOr([], 'data', payload);
  const currentProject = propOr({}, 'currentProject', state);
  return {
    ...state,
    // [meta]: {
    //   ...currentProject,
    //   categories,
    // },
    currentProject : {
      ...currentProject,
      categories,
    }
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
