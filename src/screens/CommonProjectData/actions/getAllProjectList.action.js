import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';
import {Auth} from '../../../action/type';

const actionType = 'GET_ALL_PROJECT_LIST';

const getAPI = (userName) => {
  return AsyncStorage.getItem('token').then((token) => {
    const url = `${withKey('/project/list')}`;

    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    projectList: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
  [Auth.LOGOUT]: {
    projectList: [],
  },
  ['LOGIN_SUCCESS']: {
    projectList: [],
  },
};

export default {action, reducer};
