import {createAction} from 'redux-actions';
import {identity, pathOr, propOr} from 'ramda';
import {OFFLINE_ACTION_TYPES} from '../../../global/constants';

export const actionType = 'ADD_CREATE_TASK_ACTION_INTO_QUEUE';

const action = createAction(actionType, identity);

const handleAPISuccess = (state, {payload}) => {
  const offlineQueue = propOr([], 'offlineQueue', state);
  const defectList = propOr({}, 'defectList', state);
  const defectListByDrawing = propOr({}, 'defectListByDrawingId', state);
  const drawingId = pathOr(0, ['data', 'FK_PPDrawingID'], payload);
  const defectListByDrawingId = propOr([], drawingId, defectList);
  const defectListByDrawingIdAllProject = propOr([], drawingId, defectListByDrawing);

  if (
    payload.type === OFFLINE_ACTION_TYPES.POST_ITP_WORKFLOW_COMMENT ||
    payload.type === OFFLINE_ACTION_TYPES.ADD_ITP_WORKFLOW_PHOTO ||
    payload.type === OFFLINE_ACTION_TYPES.ADD_ITP_WORKFLOW_DOC ||
    payload.type === OFFLINE_ACTION_TYPES.UPDATE_ITP_WORKFLOW_CONTENT_STATUS
  ) {
    return {
      ...state,
      offlineQueue: [
        ...offlineQueue,
        {
          ...payload,
          data: {
            offlineId: Date.now(),
            syncing: false,
            ...payload.data,
          },
        },
      ],
    };
  }

  if (payload.type === OFFLINE_ACTION_TYPES.UPDATE_ITP_OVERVIEW) {
    const isExisted = offlineQueue.find(
      (item) => item.data.PMProjectInspID === payload.data.PMProjectInspID,
    );

    return {
      ...state,
      offlineQueue: isExisted
        ? offlineQueue.map((item) => {
            if (item.data.PMProjectInspID === payload.data.PMProjectInspID) {
              return {
                ...item,
                data: {
                  ...item.data,
                  ...payload.data,
                },
              };
            }

            return item;
          })
        : [
            ...offlineQueue,
            {
              ...payload,
              data: {
                ...payload.data,
                offlineId: Date.now(),
                syncing: false,
              },
            },
          ],
    };
  }

  if (payload.type === OFFLINE_ACTION_TYPES.UPDATE_DEFECT) {
    if (!payload.data.PMTaskID) {
      return {
        ...state,
        defectList: {
          ...defectList,
          [drawingId]: defectListByDrawingId.map((item) => {
            if (item.offlineId === payload.data.offlineId) {
              return {
                ...item,
                ...payload.data,
              };
            }

            return item;
          }),
        },
        defectListByDrawingId: {
          ...defectListByDrawing,
          [drawingId]: defectListByDrawingIdAllProject.map((item) => {
            if (item.offlineId === payload.data.offlineId) {
              return {
                ...item,
                ...payload.data,
              };
            }

            return item;
          }),
        },
        offlineQueue: offlineQueue.map((item) => {
          if (item.data.offlineId === payload.data.offlineId) {
            return {
              ...item,
              data: {
                ...item.data,
                ...payload.data,
              },
            };
          }

          return item;
        }),
      };
    } else {
      const isExisted = offlineQueue.find(
        (item) => item.data.PMTaskID === payload.data.PMTaskID,
      );

      return {
        ...state,
        defectList: {
          ...defectList,
          [drawingId]: defectListByDrawingId.map((item) => {
            if (item.PMTaskID === payload.data.PMTaskID) {
              return {
                ...item,
                ...payload.data,
              };
            }

            return item;
          }),
        },
        defectListByDrawingId: {
          ...defectListByDrawing,
          [drawingId]: defectListByDrawingIdAllProject.map((item) => {
            if (item.PMTaskID === payload.data.PMTaskID) {
              return {
                ...item,
                ...payload.data,
              };
            }

            return item;
          }),
        },
        offlineQueue: isExisted
          ? offlineQueue.map((item) => {
              if (item.data.PMTaskID === payload.data.PMTaskID) {
                return {
                  ...item,
                  data: {
                    ...item.data,
                    ...payload.data,
                  },
                };
              }

              return item;
            })
          : [
              ...offlineQueue,
              {
                ...payload,
                data: {
                  ...payload.data,
                  offlineId: Date.now(),
                  syncing: false,
                },
              },
            ],
      };
    }
  }

  if (payload.type === OFFLINE_ACTION_TYPES.ADD_PHOTO) {
    const offlineId = Date.now();
    const {body, image} = payload.data;
    const FK_PMTaskID = propOr('', 'FK_PMTaskID', body);
    const photos = propOr({}, 'photos', state);
    const photoList = propOr([], FK_PMTaskID, photos);

    return {
      ...state,
      photos: {
        ...photos,
        [FK_PMTaskID]: [...photoList, {...body, tempImage: image, offlineId}],
      },
      offlineQueue: [
        ...offlineQueue,
        {
          ...payload,
          data: {
            ...payload.data,
            offlineId,
            syncing: false,
          },
        },
      ],
    };
  }

  if (payload.type === OFFLINE_ACTION_TYPES.CREATE_DEFECT) {
    const offlineId = Date.now();
    const offlineQueue = propOr([], 'offlineQueue', state);

    return {
      ...state,
      defectList: {
        ...defectList,
        [drawingId]: [...defectListByDrawingId, {...payload.data, offlineId}],
      },
      defectListByDrawingId: {
        ...defectListByDrawing,
        [drawingId]: [...defectListByDrawingIdAllProject, {...payload.data, offlineId}],
      },
      offlineQueue: [
        ...offlineQueue,
        {
          ...payload,
          data: {
            ...payload.data,
            offlineId,
            syncing: false,
          },
        },
      ],
    };
  }

  if (payload.type === OFFLINE_ACTION_TYPES.ADD_COMMENT) {
    const offlineId = Date.now();
    const {body, image} = payload.data;
    const FK_PMTaskID = propOr('', 'FK_PMTaskID', body);
    const comments = propOr({}, 'comments', state);
    const commentList = propOr([], FK_PMTaskID, comments);

    return {
      ...state,
      comments: {
        ...comments,
        [FK_PMTaskID]: [...commentList, {...body, tempImage: image, offlineId}],
      },
      offlineQueue: [
        ...offlineQueue,
        {
          ...payload,
          data: {
            ...payload.data,
            offlineId,
            syncing: false,
          },
        },
      ],
    };
  }

  return {
    ...state,
    offlineQueue: [
      ...offlineQueue,
      {
        ...payload,
        data: {
          ...payload.data,
          offlineId: Date.now(),
          syncing: false,
        },
      },
    ],
  };
};

const reducer = {
  [actionType]: handleAPISuccess,
};

export default {action, reducer};
