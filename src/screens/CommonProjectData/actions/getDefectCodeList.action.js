import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, propOr} from 'ramda';

const actionType = 'GET_DEFECT_CODE_LIST';

const getDefectCodeListAPI = ({projectId}, token) => {
  const url = withKey('/defect-code/search');
  return API.post(
    url,
    {
      projectId: projectId,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const action = createAction(actionType, getDefectCodeListAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  const defectCodeList = propOr([], 'data', payload);
  return {
    ...state,
    defectCodeList,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
