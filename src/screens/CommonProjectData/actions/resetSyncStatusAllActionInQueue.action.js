import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';

const actionType = 'RESET_STATUS_ALL_ACTION_IN_QUEUE';

const action = createAction(actionType, identity);

const handleAPISuccess = state => {
  const offlineQueue = propOr([], 'offlineQueue', state);
  return {
    ...state,
    offlineQueue: offlineQueue
      .filter(item => {
        if (item.type === 'CREATE_DEFECT' && item.PMTaskName === undefined) {
          return false;
        }
        return true;
      })
      .map(item => ({
        ...item,
        data: {
          ...item.data,
          syncing: false,
        },
      })),
  };
};

const reducer = {
  [actionType]: handleAPISuccess,
};

export default {action, reducer};
