import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, pathOr, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_PERMISSIONS_IN_PROJECT';

// https://apidev.rocez.com/user/people/of-project/125

const getPermissionsInProjectAPI = (PMProjectID) => {
  return AsyncStorage.getItem('token').then((token) => {
    const url = withKey(`/user/my-permissions/of-project/${PMProjectID}`);
    return API.get(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getPermissionsInProjectAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
    const permisstionsList = propOr([], 'data', payload);

    const modulePermissions =  pathOr([], ['modulePermissions'], permisstionsList);

  return {
    ...state,
    Permission: modulePermissions
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
