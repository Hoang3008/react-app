import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, pathOr, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_PEOPLE_IN_PROJECT';

// https://apidev.rocez.com/user/people/of-project/125

const getPeopleInProjectAPI = ({PMProjectID}) => {
  return AsyncStorage.getItem('token').then((token) => {
    const url = withKey(`/user/people/of-project/${PMProjectID}`);
    return API.get(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getPeopleInProjectAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const {PMProjectID} = meta;
  const currentProject = propOr({}, 'currentProject', state);

  const allUsers = pathOr([], ['master', 'assigners'], state);

  const users = propOr([], 'data', payload).map((item) => {
    const firstName = propOr('', 'PMProjectPeopleFirstName', item);
    const lastName = propOr('', 'PMProjectPeopleLastName', item);
    const ADUserID = propOr('', 'ADUserID', item); 

    // const email = propOr('', 'PMProjectPeopleEmailAddress', item).toLowerCase(); TODO

    // const {ADUserID} =
    //   allUsers.find(
    //     ({ADUserName = ''}) => ADUserName && ADUserName.toLowerCase() === email,
    //   ) || {}; TODO

    return {
      ...item,
      ADUserID: ADUserID || null,
      shortName: `${firstName.substring(0, 1)}${lastName.substring(0, 1)}`,
      fullName: `${firstName} ${lastName}`,
    };
  });

  return {
    ...state,
    // [PMProjectID]: {
    //   ...currentProject,
    //   users,
    // },
    currentProject: {
      ...currentProject,
      users,
    },
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
