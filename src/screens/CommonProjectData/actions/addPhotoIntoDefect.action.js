import {createAction} from 'redux-actions';
import AsyncStorage from '@react-native-community/async-storage';
import API from '../../../api/API.service';

import {getCreateTaskDocumentUrl} from '../../../api/urls';
import {identity, isEmpty, omit, propOr} from 'ramda';
import {createFormData} from '../../../api/defect';

const actionType = 'ADD_PHOTO_INTO_DEFECT';

export const addPhotoIntoDefectAPI = ({body, image}, token, projectId) => {
  const formData = createFormData(body, image);
  const taskId = body?.FK_PMTaskID;

  return AsyncStorage.getItem('token').then((token) => {
    return API.post(getCreateTaskDocumentUrl(taskId, projectId), formData, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      timeout: 120000,
    });
  });
};

const action = createAction(actionType, addPhotoIntoDefectAPI, identity);

// PMTaskDocID: 49
// AAStatus: "Alive"
// AACreatedUser: "1"
// AAUpdatedUser: "1"
// AACreatedDate: "2019-11-27T13:44:33.990Z"
// AAUpdatedDate: "2019-11-27T13:44:33.990Z"
// AASelected: false
// PMTaskDocNo: ""
// PMTaskDocName: "XÂY TÔ-Ô CHỜ KHÔNG ĐẠT.jpg"
// PMTaskDocDesc: ""
// PMTaskDocDate: "2019-11-27T13:44:33.990Z"
// FK_PMTaskID: 136

const handleAPISuccess = (state, {payload, meta}) => {
  console.log('sync photo done', payload, meta);

  if (typeof payload.data === 'string') {
    // || isEmpty(payload.data)) {
    return handleFailed(state, {payload, meta});
  }

  const {
    offlineId,
    body: {FK_PMTaskID},
  } = meta;

  const offlineQueue = propOr([], 'offlineQueue', state);
  const photos = propOr({}, 'photos', state);
  const photoList = propOr([], FK_PMTaskID, photos);

  return {
    ...state,
    photos: {
      ...omit([offlineId], photos),
      [FK_PMTaskID]: photoList.map((item) => {
        if (item.offlineId === offlineId) {
          return {
            ...item,
            ...payload.data,
            syncing: false,
          };
        }

        return item;
      }),
    },
    offlineQueue: offlineQueue.filter(
      (item) => item.data.offlineId !== offlineId,
    ),
  };
};

const handleStart = (state, {meta}) => {
  const {offlineId, body, image} = meta;
  const {FK_PMTaskID} = body;

  const offlineQueue = propOr([], 'offlineQueue', state);
  const photos = propOr({}, 'photos', state);
  const photoList = propOr([], FK_PMTaskID, photos);

  const isExisted = photoList.find(
    ({offlineId}) => offlineId === meta.offlineId,
  );

  return {
    ...state,
    photos: {
      ...photos,
      [FK_PMTaskID]: isExisted
        ? photoList
        : [...photoList, {...body, tempImage: image, offlineId}],
    },
    offlineQueue: offlineQueue.map((item) => {
      if (item.data.offlineId === offlineId) {
        return {
          ...item,
          data: {
            ...item.data,
            syncing: true,
          },
        };
      }

      return item;
    }),
  };
};

const handleFailed = (state, {payload, meta}) => {
  console.log('sync photo failed', payload, meta, state);

  const error = propOr('', 'data', payload);

  const offlineQueue = propOr([], 'offlineQueue', state);
  const {offlineId} = meta;

  const newOfflineQueue =
    !isEmpty(error) && error.includes('PMTaskID')
      ? offlineQueue.filter((item) => item.data.offlineId !== offlineId)
      : offlineQueue.map((item) => {
          if (item.data.offlineId === offlineId) {
            return {
              ...item,
              data: {
                ...item.data,
                syncing: false,
              },
            };
          }

          return item;
        });

  return {
    ...state,
    offlineQueue: newOfflineQueue,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: handleStart,
    SUCCESS: handleAPISuccess,
    FAILURE: handleFailed,
  },
};

export default {action, reducer};
