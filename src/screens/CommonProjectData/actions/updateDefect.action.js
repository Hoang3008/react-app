import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, isEmpty, omit, pathOr, propOr} from 'ramda';
import {OFFLINE_ACTION_TYPES} from '../../../global/constants';
import AsyncStorage from '@react-native-community/async-storage';
import {callAssignTask, callAssignDefect} from './createDefect.action';

export const actionType = 'UPDATE_TASK';

export const createLog = (token, taskId, log) => {
  const url = withKey(`/comment/task/${taskId}`);

  return API.post(url, log, {
    headers: {
      Accept: 'application/json, text/plain, */*',
      Authorization: `Bearer ${token}`,
    },
  });
};

export const changeStatus = (token, taskId, status) => {
  // "Verify" "Complete"
  if (status !== 'Verify' && status !== 'Complete') {
    return;
  }
  const url = withKey(`/task/${taskId}/change-status`);

  return API.patch(
    url,
    {taskId, status},
    {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const updateTaskAPI = (defect, currentDefect, logs) => {
  const data = omit(['offlineId', 'syncing'], defect);
  const projectId = propOr(undefined, 'FK_PMProjectID', defect);
  const taskId = propOr(undefined, 'PMTaskID', defect);

  return AsyncStorage.getItem('token').then((token) => {
    return API.put(withKey(`/task/v2/${projectId}/${taskId}`), data, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then(async (result) => {
      const {PMTaskID, FK_ADAssignUserID, priority, PMTaskTypeCombo} =
        defect || {};

      if (
        currentDefect &&
        FK_ADAssignUserID !== currentDefect.FK_ADAssignUserID
      ) {
        if (PMTaskTypeCombo === 'task') {
          callAssignTask(token, PMTaskID, FK_ADAssignUserID);
        } else if (PMTaskTypeCombo === 'defect') {
          callAssignDefect(token, PMTaskID, FK_ADAssignUserID);
        }
      }

      if (currentDefect && priority !== currentDefect.priority) {
        changeStatus(token, PMTaskID, priority);
      }

      if (logs && !isEmpty(logs)) {
        for (let i = 0; i < logs.length; i++) {
          const log = logs[i];
          await createLog(token, PMTaskID, log);
        }
      }

      return result;
    });
  });
};

const action = createAction(actionType, updateTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  if (typeof payload.data === 'string') {
    return handleFailed(state, {payload, meta});
  }

  const offlineQueue = propOr([], 'offlineQueue', state);
  const drawingId = pathOr('', ['data', 'FK_PPDrawingID'], payload);
  const defectList = propOr({}, 'defectList', state);
  const defectListByDrawingId = pathOr([], ['defectList', drawingId], state);
  const rawDefectList = propOr([], 'rawDefectList', state);

  const {offlineId} = meta;

  const PMTaskID = pathOr('', ['data', 'PMTaskID'], payload);
  let photos = propOr({}, 'photos', state);

  if (!(PMTaskID in photos)) {
    const photoList = propOr([], offlineId, photos);
    photos = {
      ...omit([offlineId], photos),
      [PMTaskID]: photoList.map((item) => ({
        ...item,
        FK_PMTaskID: PMTaskID,
      })),
    };
  }

  const newState = {
    ...state,
    photos,
    defectList: {
      ...defectList,
      [drawingId]: defectListByDrawingId.map((item) => {
        if (item.PMTaskID === PMTaskID || item.offlineId === offlineId) {
          return {
            ...item,
            ...payload.data,
            syncing: false,
          };
        }

        return item;
      }),
    },
    rawDefectList: rawDefectList.map((item) => {
      if (item.PMTaskID === PMTaskID || item.offlineId === offlineId) {
        return {
          ...item,
          ...payload.data,
          syncing: false,
        };
      }

      return item;
    }),
    offlineQueue: offlineQueue
      .filter((item) => item.data.offlineId !== offlineId)
      .map((item) => {
        if (pathOr('', ['data', 'body', 'FK_PMTaskID'], item) === offlineId) {
          return {
            ...item,
            data: {
              ...item.data,
              body: {
                ...item.data.body,
                FK_PMTaskID: PMTaskID,
              },
            },
          };
        }

        return item;
      }),
  };

  return newState;
};

const handleStart = (state, {meta}) => {
  const offlineQueue = propOr([], 'offlineQueue', state);
  const {offlineId, FK_PPDrawingID} = meta;
  const defectList = propOr({}, 'defectList', state);
  const defectListByDrawing = propOr({}, 'defectListByDrawingId', state);
  const defectListByDrawingId = pathOr(
    [],
    ['defectList', FK_PPDrawingID],
    state,
  );

  const defectListByDrawingIdAllProject = pathOr(
    [],
    ['defectListByDrawingId', FK_PPDrawingID],
    state,
  );

  


  const isExisted = defectListByDrawingId.find(
    ({offlineId}) => offlineId === meta.offlineId,
  );

  return {
    ...state,
    defectList: {
      ...defectList,
      [FK_PPDrawingID]: isExisted
      ? [...defectListByDrawingId, meta]
      : defectListByDrawingId,
    },
    defectListByDrawingId: {
      ...defectListByDrawing,
      [FK_PPDrawingID]: isExisted
        ? [...defectListByDrawingIdAllProject, meta]
        : defectListByDrawingIdAllProject,
    },
    offlineQueue: offlineQueue.map((item) => {
      if (item.data.offlineId === offlineId) {
        return {
          ...item,
          data: {
            ...item.data,
            syncing: true,
          },
        };
      }

      return item;
    }),
  };
};

const handleFailed = (state, {payload, meta}) => {
  const offlineQueue = propOr([], 'offlineQueue', state);
  const {offlineId} = meta;
  return {
    ...state,
    offlineQueue: offlineQueue
      .filter(
        (item) =>
          item.type === OFFLINE_ACTION_TYPES.ADD_PHOTO ||
          item.data.PMTaskName !== undefined,
      )
      .map((item) => {
        if (item.data.offlineId === offlineId) {
          return {
            ...item,
            data: {
              ...item.data,
              syncing: false,
            },
          };
        }

        return item;
      }),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: handleStart,
    SUCCESS: handleAPISuccess,
    FAILURE: handleFailed,
  },
};

export default {action, reducer};
