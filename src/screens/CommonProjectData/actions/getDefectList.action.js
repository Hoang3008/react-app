import {createAction} from 'redux-actions';
import API from '../../../api/API.service';
import {groupBy, identity, propOr} from 'ramda';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_DEFECT_LIST';

const getDefectListAPI = ({userId, projectId}, user) => {
  return AsyncStorage.getItem('token').then((token) => {
    const url = withKey('/defect/search');

    const type = propOr('', 'ADUserType', user);
    const userIdProps = {userId};
    return API.post(
      url,
      {
        categoryIds: [],
        locationIds: [],
        peopleIds: [],
        projectId: projectId,
        statuses: [],
        ...userIdProps,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, getDefectListAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  const defectRes = propOr([], 'data', payload);
  const defectList = groupBy(({FK_PPDrawingID}) => FK_PPDrawingID)(
    defectRes,
  );

  return {
    ...state,
    defectList
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
