import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';

const actionType = 'REMOVE_CREATE_TASK_ACTION_INTO_QUEUE';

const action = createAction(actionType, identity);

const handleAPISuccess = (state, {payload}) => {
  const offlineQueue = propOr([], 'offlineQueue', state);
  const offlineId = propOr('', 'offlineId', payload);
  return {
    ...state,
    offlineQueue: offlineQueue.filter(
      item => item.data.offlineId !== offlineId,
    ),
  };
};

const reducer = {
  [actionType]: handleAPISuccess,
};

export default {action, reducer};
