import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity, propOr, type} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';


const actionType = 'GET_TASK_COMMENT_LIST';

const getCommentListAPI = (taskId, PMProjectID) => {
  return AsyncStorage.getItem('token').then((token) => {

    //Fix issue undefined taskId when offline
    if (!taskId || typeof(taskId) == undefined) {
      return;
    }
    
    return API.get(withKey(`/comment/task/${taskId}`), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).catch((err) => {
      alert("Data can't access, The reason can data deleted or you haven't permitted on the data");
    });
  });
};

const action = createAction(actionType, getCommentListAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const documentList = propOr([], 'data', payload);
  const comments = propOr({}, 'comments', state);

  const commentListInState = propOr([], meta, comments);
  const notSyncedCommentList = commentListInState.filter(
    (item) => !item.PMTaskCommentID,
  );

  return {
    ...state,
    comments: {
      ...comments,
      [meta]: [...documentList, ...notSyncedCommentList],
    },
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
