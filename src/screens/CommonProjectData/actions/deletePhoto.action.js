import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {getDeleteDocumentUrl} from '../../../api/urls';
import {identity, omit, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'DELETE_PHOTO';

const deletePhotoAPI = (photo) => {
  return AsyncStorage.getItem('token').then((token) => {
    return API.put(
      withKey(`/task/doc/${projectId}/${photoId}`),
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, deletePhotoAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  console.log('delete photo done', payload, meta);

  const {PMTaskDocID, FK_PMTaskID, offlineId = 0} = meta;

  const offlineQueue = propOr([], 'offlineQueue', state);
  const photos = propOr({}, 'photos', state);
  const photoList = propOr([], FK_PMTaskID, photos);

  const newState = {
    ...state,
    photos: {
      ...omit([PMTaskDocID], photos),
      [FK_PMTaskID]: photoList.filter(
        (item) =>
          item.offlineId !== offlineId && item.PMTaskDocID !== PMTaskDocID,
      ),
    },
    offlineQueue: offlineQueue.filter(
      (item) => item.data.offlineId !== offlineId,
    ),
  };

  return newState;
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
