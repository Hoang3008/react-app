import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {getCreateTaskUrl, withKey} from '../../../api/urls';
import {identity, omit, pathOr, propOr} from 'ramda';
import {OFFLINE_ACTION_TYPES} from '../../../global/constants';
import AsyncStorage from '@react-native-community/async-storage';
import {addPhotoIntoDefectAPI} from './addPhotoIntoDefect.action';

export const actionType = 'CREATE_TASK';

export const callAssignTask = (token, taskId, userId) => {
  const url = withKey(`/task/${taskId}/assign-to-people/${userId}`);

  // const url = withKey(`/task/${taskId}/assign-to/${userId}`); // defect

  return API.patch(
    url,
    {},
    {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

export const callAssignDefect = (token, taskId, userId) => {
  // const url = withKey(`/task/${taskId}/assign-to-people/${userId}`);
  const url = withKey(`/task/${taskId}/assign-to/${userId}`); // defect

  return API.patch(
    url,
    {},
    {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const createTaskAPI = (defect, attachment) => {
  console.log('createTaskAPI ', defect, attachment);
  const url = withKey(`/task/v2`); // defect
  const data = omit(['offlineId', 'syncing'], defect);
  return AsyncStorage.getItem('token').then((token) => {
    return API.post(url, data, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    }).then((result) => {
      const {PMTaskID, FK_ADAssignUserID, AACreatedUser, PMTaskTypeCombo} =
        result.data || {};

      if (PMTaskTypeCombo === 'task') {
        callAssignTask(token, PMTaskID, FK_ADAssignUserID);
      } else if (PMTaskTypeCombo === 'defect') {
        callAssignDefect(token, PMTaskID, FK_ADAssignUserID);
      }

      if (attachment) {
        addPhotoIntoDefectAPI({
          body: {
            AACreatedUser,
            FK_PMTaskID: PMTaskID
          },
          image: attachment,
        });
      }

      return result;
    });
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  if (typeof payload.data === 'string') {
    return handleFailed(state, {payload, meta});
  }

  const offlineQueue = propOr([], 'offlineQueue', state);
  const drawingId = pathOr('', ['data', 'FK_PPDrawingID'], payload);
  const defectList = propOr({}, 'defectList', state);
  const defectListByDrawing = propOr({}, 'defectListByDrawingId', state);
  const defectListByDrawingId = pathOr([], ['defectListByDrawingId', drawingId], state);
  const {offlineId} = meta;

  const PMTaskID = pathOr('', ['data', 'PMTaskID'], payload);

  const photos = propOr({}, 'photos', state);
  const photoList = propOr([], offlineId, photos);

  const comments = propOr({}, 'comments', state);
  const commentList = propOr([], offlineId, comments);

  console.log('sync create defect done: ', payload, meta, state);

  return {
    ...state,
    photos: {
      ...omit([offlineId], photos),
      [PMTaskID]: photoList.map((item) => ({
        ...item,
        FK_PMTaskID: PMTaskID,
      })),
    },
    comments: {
      ...omit([offlineId], comments),
      [PMTaskID]: commentList.map((item) => ({
        ...item,
        FK_PMTaskID: PMTaskID,
      })),
    },
    defectList: {
      ...defectList,
      [drawingId]: defectListByDrawingId.map((item) => {
        if (item.offlineId === offlineId) {
          return {
            ...item,
            ...payload.data,
            syncing: false,
          };
        }

        return item;
      }),
    },
    defectListByDrawingId: {
      ...defectListByDrawing,
      [drawingId]: defectListByDrawingId.map((item) => {
        if (item.offlineId === offlineId) {
          return {
            ...item,
            ...payload.data,
            syncing: false,
          };
        }

        return item;
      }),
    },
    offlineQueue: offlineQueue
      .filter((item) => item.data.offlineId !== offlineId)
      .map((item) => {
        if (pathOr('', ['data', 'body', 'FK_PMTaskID'], item) === offlineId) {
          return {
            ...item,
            data: {
              ...item.data,
              body: {
                ...item.data.body,
                FK_PMTaskID: PMTaskID,
              },
            },
          };
        }

        return item;
      }),
  };
};

const handleStart = (state, {meta}) => {
  const offlineQueue = propOr([], 'offlineQueue', state);
  const {offlineId, FK_PPDrawingID} = meta;
  const drawingId = FK_PPDrawingID;
  const defectList = propOr({}, 'defectList', state);
  const defectListByDrawing = propOr({}, 'defectListByDrawingId', state);
  const defectListByDrawingId = pathOr([], ['defectList', drawingId], state);
  const defectListByDrawingIdAllProject = pathOr([], ['defectListByDrawingId', drawingId], state);


  const isExisted = defectListByDrawingId.find(
    ({offlineId}) => offlineId === meta.offlineId,
  );

  return {
    ...state,
    defectList: {
      ...defectList,
      [drawingId]: isExisted
        ? defectListByDrawingId
        : [...defectListByDrawingId, meta],
    },
    defectListByDrawingId: {
      ...defectListByDrawing,
      [drawingId]: isExisted
        ? defectListByDrawingIdAllProject
        : [...defectListByDrawingIdAllProject, meta],
    },
    offlineQueue: offlineQueue.map((item) => {
      if (item.data.offlineId === offlineId) {
        return {
          ...item,
          data: {
            ...item.data,
            syncing: true,
          },
        };
      }

      return item;
    }),
  };
};

const handleFailed = (state, {payload, meta}) => {
  const offlineQueue = propOr([], 'offlineQueue', state);
  const {offlineId} = meta;

  console.log('handleFailed ', payload);

  const newState = {
    ...state,
    offlineQueue: offlineQueue
      .filter(
        (item) =>
          item.type === OFFLINE_ACTION_TYPES.ADD_PHOTO ||
          item.data.PMTaskName !== undefined,
      )
      .map((item) => {
        if (item.data.offlineId === offlineId) {
          return {
            ...item,
            data: {
              ...item.data,
              syncing: false,
            },
          };
        }

        return item;
      }),
  };

  return newState;
};

const reducer = {
  [actionType]: {
    BEGIN: handleStart,
    SUCCESS: handleAPISuccess,
    FAILURE: handleFailed,
  },
};

export default {action, reducer};
