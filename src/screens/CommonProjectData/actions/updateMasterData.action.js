import {Auth} from '../../../action/type';

const handleAPISuccess = (state, {payload}) => ({
  ...state,
  master: payload,
});

const reducer = {
  [Auth.MASTER_DATA_UPDATED]: handleAPISuccess,
};

export default {reducer};
