import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {getAllTaskDocByTaskIdUrl} from '../../../api/urls';
import {identity, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';
import {withKey} from '../../../api/urls';
const actionType = 'GET_TASK_DOCUMENT_LIST';

const getDefectCodeListAPI = (taskId, PMProjectID) => {
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(withKey(`/task/doc/${taskId}`), {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }).catch((err) => {
    alert("Data can't access, The reason can data deleted or you haven't permitted on the data");
  });
};

const action = createAction(actionType, getDefectCodeListAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const documentList = propOr([], 'data', payload);
  const photos = propOr({}, 'photos', state);
  const photoListInState = propOr([], meta, photos);
  const notSyncedPhotoList = photoListInState.filter(
    (item) => !item.PMTaskDocID,
  );

  return {
    ...state,
    photos: {
      ...photos,
      [meta]: [...documentList, ...notSyncedPhotoList],
    },
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
