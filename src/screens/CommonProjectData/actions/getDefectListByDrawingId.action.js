import {createAction} from 'redux-actions';
import API from '../../../api/API.service';
import {identity, propOr} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';
import {withKey} from '../../../api/urls';
import {PERMISSION_TYPE} from '../../../global/constants';

const actionType = 'GET_DEFECT_LIST_BY_DRAWING_ID';

const getDefectListByDrawingIdAPI = ({user, drawingId, projectId}, token) => {
  return AsyncStorage.getItem('modulePermissions').then((modulePermissions) => {
    let userId = user.ADUserID;
    const url = withKey('/defect/search');
    const userIdProps =
      modulePermissions === PERMISSION_TYPE.PERMISSION_CHANGEALL
        ? {}
        : {userId};

    return API.post(
      url,
      {
        categoryIds: [],
        locationIds: [],
        peopleIds: [],
        projectId: projectId,
        // userId: userId,
        drawingId: drawingId,
        statuses: [],
        ...userIdProps,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, getDefectListByDrawingIdAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  const data = propOr([], 'data', payload);
  const defectListByDrawingId = propOr([], 'defectListByDrawingId', state);
  const drawingId = meta.drawingId;
  const defectData = [...data];
  if (defectListByDrawingId.length > 0) {
    defectData = defectListByDrawingId.map((item) => {
      if (item.drawingId === drawingId) {
        return data;
      }

      return item;
    })
  };

  return {
    ...state,
    defectListByDrawingId: {
      ...defectListByDrawingId,
      [drawingId]: defectData
    },
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
