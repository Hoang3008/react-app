import {createAction} from 'redux-actions';
import {identity, isEmpty, propOr} from 'ramda';
import firestore from '@react-native-firebase/firestore';

const actionType = 'GET_ALL_PROJECT_NOTIFICATION_COUNT';

const getAPI = (userId, projectIds = []) => {
  if (!projectIds || isEmpty(projectIds)) {
    return Promise.resolve({});
  }

  const loadProjectIds = projectIds.filter((item) => !!item);

  const collectionPath = `users/${userId}/notifications`;
  const handler = firestore().collection(collectionPath);

  return Promise.all([
    ...loadProjectIds.map((id, index) => {
      try {
        return handler
          .where('projectId', '==', id)
          .where('read', '==', false)
          .limit(100)
          .get({source: 'server'})
          .then((result) => {
            const docs = result._docs || [];
            return {
              id,
              count: docs.length,
            };
          });
      } catch (e) {
        return Promise.resolve([]);
      }
    }),
  ]);
};

const action = createAction(actionType, getAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  const newProjectNotificationCount = {};
  const data = payload || [];

  data.forEach(({id, count}) => (newProjectNotificationCount[id] = count));

  return {
    ...state,
    projectNotificationCount: {
      ...propOr({}, 'projectNotificationCount', state),
      ...newProjectNotificationCount,
    },
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
