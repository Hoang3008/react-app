import typeToReducer from 'type-to-reducer';
import getDefectCodeList from './actions/getDefectCodeList.action';
import getDefectList from './actions/getDefectList.action';
import getProjectCategories from './actions/getProjectCategories.action';
import getProjectLocations from './actions/getProjectLocations.action';
import getPeopleInProject from './actions/getPeopleInProject.action';
import updateMasterData from './actions/updateMasterData.action';
import createDefect from './actions/createDefect.action';
import addPhotoIntoDefect from './actions/addPhotoIntoDefect.action';
import addActionIntoQueue from './actions/addActionIntoQueue.action';
import getTaskDocumentList from './actions/getTaskDocumentList.action';
import removeActionOutOfQueue from './actions/removeActionOutOfQueue.action';
import resetSyncStatusAllActionInQueue from './actions/resetSyncStatusAllActionInQueue.action';
import updateDefect from './actions/updateDefect.action';
import getDefectListByDrawingId from './actions/getDefectListByDrawingId.action';
import deletePhoto from './actions/deletePhoto.action';
import addComment from './actions/addComment.action';
import getTaskCommentList from './actions/getTaskCommentList.action';
import getAllProjectList from './actions/getAllProjectList.action';
import getAllProjectNotificationUnreadCount from './actions/getAllProjectNotificationUnreadCount.action';
import getDefectById from './actions/getDefectById.action';
import updateITPQueue from './actions/updateITPQueue.action';
import getPermissionsByProject from './actions/getPermissionsByProject.action';

const initialState = {
  projectList: [],
  offlineQueue: [],
  photos: {},
  comments: {},
  notifications: {},
};

export const actions = {
  getDefectCodeList: getDefectCodeList.action,
  getDefectList: getDefectList.action,
  getDefectById: getDefectById.action,
  getProjectCategories: getProjectCategories.action,
  getProjectLocations: getProjectLocations.action,
  getPeopleInProject: getPeopleInProject.action,
  createDefect: createDefect.action,
  addActionIntoQueue: addActionIntoQueue.action,
  addPhotoIntoDefect: addPhotoIntoDefect.action,
  getTaskDocumentList: getTaskDocumentList.action,
  removeActionOutOfQueue: removeActionOutOfQueue.action,
  resetSyncStatusAllActionInQueue: resetSyncStatusAllActionInQueue.action,
  updateDefect: updateDefect.action,
  getDefectListByDrawingId: getDefectListByDrawingId.action,
  deletePhoto: deletePhoto.action,
  addComment: addComment.action,
  getTaskCommentList: getTaskCommentList.action,
  getAllProjectList: getAllProjectList.action,
  getAllProjectNotificationUnreadCount:
    getAllProjectNotificationUnreadCount.action,
  getAllPermissionsByProject:
    getPermissionsByProject.action,
};

export default typeToReducer(
  {
    ...getDefectCodeList.reducer,
    ...getProjectCategories.reducer,
    ...getProjectLocations.reducer,
    ...getPeopleInProject.reducer,
    ...updateMasterData.reducer,
    ...getDefectList.reducer,
    ...createDefect.reducer,
    ...addActionIntoQueue.reducer,
    ...addPhotoIntoDefect.reducer,
    ...getTaskDocumentList.reducer,
    ...removeActionOutOfQueue.reducer,
    ...resetSyncStatusAllActionInQueue.reducer,
    ...updateDefect.reducer,
    ...getDefectListByDrawingId.reducer,
    ...deletePhoto.reducer,
    ...addComment.reducer,
    ...getTaskCommentList.reducer,
    ...getAllProjectList.reducer,
    ...getAllProjectNotificationUnreadCount.reducer,
    ...getDefectById.reducer,
    ...updateITPQueue.reducer,
    ...getPermissionsByProject.reducer
  },
  initialState,
);
