import {ScrollView, StatusBar, Text, View} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import MultiSelect from 'react-native-multiple-select';
import {identity, pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';
import {UniCheckbox, UniText} from '../../components';

import styles from './styles';

import Theme from '../../Theme';
import {UniButton, UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import {EMPTY} from '../../global/constants';


const priorityList = [
  {
    key: 'Priority1',
    value: 'Priority 1',
  },
  {
    key: 'Priority2',
    value: 'Priority 2',
  },
  {
    key: 'Priority3',
    value: 'Priority 3',
  },
  {
    key: 'Complete',
    value: 'Complete',
  },
  {
    key: 'Verify',
    value: 'Verify',
  },
];

const DrawingFilter = (props) => {
  const {navigation, categories, peopleInProject, locations} = props;

  const onFilter = pathOr(identity, ['route', 'params', 'onFilter'], props);

  const prevCategories = pathOr(
    [],
    ['route', 'params', 'selectedCategories'],
    props,
  );
  const prevPeople = pathOr([], ['route', 'params', 'selectedPeople'], props);
  const prevPriority = pathOr(
    [],
    ['route', 'params', 'selectedPriority'],
    props,
  );
  const prevLocation = pathOr(
    [],
    ['route', 'params', 'selectedLocation'],
    props,
  );

  const prevIsSelected = pathOr(
    [],
    ['route', 'params', 'isSelected'],
    props,
  );

  const [selectedCategories, setSelectedCategories] = useState([]);
  const [selectedPeople, setSelectedPeople] = useState([]);
  const [selectedPriority, setSelectedPriority] = useState([]);
  const [selectedLocation, setSelectedLocation] = useState([]);
  const [isSelected, setSelection] = useState(false);


  const categoryRef = useRef(null);
  const peopleRef = useRef(null);
  const priorityRef = useRef(null);
  const locationRef = useRef(null);

  useEffect(() => {
    setSelectedLocation(prevLocation);
    setSelectedPeople(prevPeople);
    setSelectedCategories(prevCategories);
    setSelectedPriority(prevPriority);
    setSelectedPriority(prevPriority);
    setSelection(prevIsSelected)
  }, [prevCategories, prevPeople, prevPriority, prevLocation, prevIsSelected]);

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={navigation.goBack}
          />
          <View>
            <View style={styles.row}>
              <Text style={styles.textHeader}>Filter</Text>
            </View>
          </View>
        </View>
        <View style={styles.row} />
      </View>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}

      <ScrollView style={{padding: 15}}>
        <MultiSelect
          styleInputGroup={{
            padding: 10,
            paddingLeft: 10,
            paddingRight: 5,
          }}
          styleDropdownMenuSubsection={{
            borderRadius: Theme.margin.m4,
            borderColor: Theme.colors.border,
            borderWidth: 1,
            padding: 10,
            paddingLeft: 10,
            paddingRight: 5,
            height: 50,
          }}
          styleListContainer={{maxHeight: 150}}
          ref={categoryRef}
          hideTags
          items={categories}
          uniqueKey="PMProjectCategoryID"
          onSelectedItemsChange={setSelectedCategories}
          selectedItems={selectedCategories}
          selectText="Select Categories"
          searchInputPlaceholderText="Search categories..."
          onChangeInput={(text) => console.log(text)}
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
          tagTextColor="#333"
          selectedItemTextColor="#000"
          selectedItemIconColor="#aaa"
          itemTextColor="#000"
          displayKey="PMProjectCategoryName"
          searchInputStyle={{color: '#CCC'}}
          submitButtonColor={Theme.colors.primary}
          submitButtonText="Save"
        />
        {categoryRef.current && (
          <View>
            {categoryRef.current.getSelectedItemsExt(selectedCategories)}
          </View>
        )}

        <View style={{height: 20}} />

        <MultiSelect
          styleInputGroup={{
            padding: 10,
            paddingLeft: 10,
            paddingRight: 5,
          }}
          styleDropdownMenuSubsection={{
            borderRadius: Theme.margin.m4,
            borderColor: Theme.colors.border,
            borderWidth: 1,
            padding: 10,
            paddingLeft: 10,
            paddingRight: 5,
            height: 50,
          }}
          styleListContainer={{maxHeight: 150}}
          ref={peopleRef}
          hideTags
          items={peopleInProject}
          uniqueKey="ADUserID" // "
          onSelectedItemsChange={setSelectedPeople}
          selectedItems={selectedPeople}
          selectText="Select Assignees"
          searchInputPlaceholderText="Search Assignees..."
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
          tagTextColor="#333"
          selectedItemTextColor="#000"
          selectedItemIconColor="#aaa"
          itemTextColor="#000"
          displayKey="fullName"
          searchInputStyle={{color: '#CCC'}}
          submitButtonColor={Theme.colors.primary}
          submitButtonText="Save"
        />
        {peopleRef.current && (
          <View>{peopleRef.current.getSelectedItemsExt(selectedPeople)}</View>
        )}

        <View style={{height: 20}} />

        <MultiSelect
          styleInputGroup={{
            padding: 10,
            paddingLeft: 10,
            paddingRight: 5,
          }}
          styleDropdownMenuSubsection={{
            borderRadius: Theme.margin.m4,
            borderColor: Theme.colors.border,
            borderWidth: 1,
            padding: 10,
            paddingLeft: 10,
            paddingRight: 5,
            height: 50,
          }}
          styleListContainer={{maxHeight: 150}}
          ref={priorityRef}
          hideTags
          items={priorityList}
          uniqueKey="key"
          onSelectedItemsChange={setSelectedPriority}
          selectedItems={selectedPriority}
          selectText="Select Priorities"
          searchInputPlaceholderText="Search Priorities..."
          onChangeInput={(text) => console.log(text)}
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
          tagTextColor="#333"
          selectedItemTextColor="#000"
          selectedItemIconColor="#aaa"
          itemTextColor="#000"
          displayKey="value"
          searchInputStyle={{color: '#CCC'}}
          submitButtonColor={Theme.colors.primary}
          submitButtonText="Save"
        />
        {priorityRef.current && (
          <View>
            {priorityRef.current.getSelectedItemsExt(selectedPriority)}
          </View>
        )}

        <View style={{height: 20}} />

        <MultiSelect
          styleInputGroup={{
            padding: 10,
            paddingLeft: 10,
            paddingRight: 5,
          }}
          styleDropdownMenuSubsection={{
            borderRadius: Theme.margin.m4,
            borderColor: Theme.colors.border,
            borderWidth: 1,
            padding: 10,
            paddingLeft: 10,
            paddingRight: 5,
            height: 50,
          }}
          styleListContainer={{maxHeight: 150}}
          ref={locationRef}
          hideTags
          items={locations}
          uniqueKey="PMProjectLocationID" // PMProjectPeopleID"
          onSelectedItemsChange={setSelectedLocation}
          selectedItems={selectedLocation}
          selectText="Select Locations"
          searchInputPlaceholderText="Search Locations..."
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
          tagTextColor="#333"
          selectedItemTextColor="#000"
          selectedItemIconColor="#aaa"
          itemTextColor="#000"
          displayKey="PMProjectLocationName"
          searchInputStyle={{color: '#CCC'}}
          submitButtonColor={Theme.colors.primary}
          submitButtonText="Save"
        />

        <View style={styles.row}>
            <UniCheckbox
                checked={isSelected}
                onChangeValue={setSelection}
                component="drawingFilter"
              />
             <UniText containerStyle={{marginHorizontal: 6}}>All Defects</UniText>
         </View>

         <View style={{height: 20}} />

        {locationRef.current && (
          <View>
            {locationRef.current.getSelectedItemsExt(selectedLocation)}
          </View>
        )}

        <View
          style={{
            flexDirection: 'row',
            marginTop: 30,
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 30,
          }}>
          <UniButton
            text={'Save'}
            onPress={() => {
              onFilter({
                selectedCategories,
                selectedPeople,
                selectedPriority,
                selectedLocation,
                isSelected,
              });
              navigation.goBack();
            }}
            containerStyle={{maxWidth: 80, marginRight: 8}}
          />

          <UniButton
            text={'Reset'}
            onPress={() => {
              setSelectedPriority([]);
              setSelectedLocation([]);
              setSelectedCategories([]);
              setSelectedPeople([]);
              setSelection(false);
            }}
            containerStyle={{
              marginLeft: 8,
              maxWidth: 80,
              backgroundColor: '#aaa',
            }}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => {
  const {
    auth: {project},
    commonData,
  } = state;
  const {PMProjectID} = project;

  return {
    categories: pathOr(EMPTY.ARRAY, ['currentProject', 'categories'], commonData),
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawingFilter);
