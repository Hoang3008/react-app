import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import moment from 'moment';
import {pathOr, propOr} from 'ramda';

import styles from './ScheduleUpdate.styles';

import {
  UniImageButton,
  UniStatusBarBackground,
  UniDropbox,
  UniInput,
  UniButton,
  UniDatePicker,
  UniMultiSelect
} from '../../components';
import Theme from '../../Theme';
import {showAlert} from '../../utils/MessageUtils';
import {requestDeleteTask} from '../../api/defect';
import {PRIORITY} from '../defect/DefectHelper';
import {EMPTY, OFFLINE_ACTION_TYPES} from '../../global/constants';
import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import {getDefaultSchedule} from '../ScheduleDetail/ScheduleDetail';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import {createLogList} from '../../utils/LogTaskSchedule.utils';
class ScheduleUpdate extends React.Component {
  constructor(props) {
    super(props);
    let schedule = pathOr(
      undefined,
      ['route', 'params', 'schedule'],
      this.props,
    );

    this.inputingTitle = schedule ? schedule.PMTaskName : '';
    let userId = pathOr(undefined, ['user', 'ADUserID'], this.props);
    let permissions = this.props.modulePermissions.find(x => x.module === 'schedule');
    this.state = {
      schedule,
      isNewSchedule: !!!schedule,
      canChange: userId === schedule?.FK_ADAssignUserID || (permissions.permissions?.length > 0 && permissions.permissions[0] === 'CHANGE_ALL')
    };
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {allSchedules} = this.props;

    if (prevProps.allSchedules !== allSchedules) {
      const lat = pathOr(0, ['route', 'params', 'lat'], this.props);
      const lng = pathOr(0, ['route', 'params', 'lng'], this.props);
      const scheduleFromMemory = allSchedules.find(
        ({PMTaskLat, PMTaskLng}) => PMTaskLat === lat && lng === PMTaskLng,
      );

      if (scheduleFromMemory) {
        this.setState({
          schedule: {
            ...scheduleFromMemory,
            ...this.state.schedule,
          },
        });
      }
    }
  }

  componentWillUnmount(): void {
    this.props.route &&
      this.props.route.params &&
      this.props.route.params.onGoBack &&
      this.props.route.params.onGoBack();
  }

  componentDidMount(): void {
    const {getPeopleInProject, project, user} = this.props;
    const {PMProjectID} = project;
    getPeopleInProject({PMProjectID, UserName: user.ADUserName});

    if (!this.props.master) {
      showAlert('Alert', 'Cannot get Master data!', () => {
        this.props.navigation.goBack();
      });
    }
    if (!this.state.schedule) {
      this.setState({
        schedule: getDefaultSchedule(user, project),
      });
    }
  }

  onPressPriority = (value) => {
    this.setState({
      schedule: {...this.state.schedule, PMTaskPriorityCombo: value},
    });
  };

  onChangeDesc = (value) => {
    this.setState({
      schedule: {...this.state.schedule, PMTaskDesc: value},
    });
  };

  onChangePercent = (value) => {
    let numberValue = parseInt(value, 10);

    if (numberValue < 0 || isNaN(numberValue)) {
      numberValue = 0;
    } else if (numberValue > 100) {
      numberValue = 100;
    }

    this.setState({
      schedule: {...this.state.schedule, PMTaskCompletePct: numberValue},
    });
  };

  onPressDelete = () => {
    requestDeleteTask(this.state.schedule).then((response) => {
      if (response) {
        this.props.route &&
          this.props.route.params &&
          this.props.route.params.onDelete &&
          this.props.route.params.onDelete();
        this.props.navigation.goBack();
      }
    });
  };

  onPressSave = () => {
    const {addActionIntoQueue, navigation, peopleInProject} = this.props;
    this.setState(
      {schedule: {...this.state.schedule, PMTaskName: this.inputingTitle}},
      () => {
        const PMTaskID =
          pathOr(0, ['schedule', 'PMTaskID'], this.state) ||
          pathOr(0, ['schedule', 'offlineId'], this.state);

        if (!PMTaskID) {
          addActionIntoQueue({
            type: OFFLINE_ACTION_TYPES.CREATE_DEFECT,
            data: this.state.schedule,
          });
          navigation.goBack();
        } else {
          let currentData = pathOr(
            undefined,
            ['route', 'params', 'schedule'],
            this.props,
          );
          addActionIntoQueue({
            type: OFFLINE_ACTION_TYPES.UPDATE_DEFECT,
            data: this.state.schedule,
            currentData,
            logs: createLogList(currentData, this.state.schedule, {
              peopleInProject
            }),
          });
          navigation.goBack();
        }
      },
    );
  };

  renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() =>
              this.props.navigation && this.props.navigation.goBack()
            }
          />
          <View>
            <Text style={styles.textHeader}>Schedule Details</Text>
          </View>
        </View>
      </View>
    );
  };

  renderPriority = (key, value, color, currentPriority = '', short) => {
    let active = currentPriority.toLowerCase() === key.toLowerCase();
    return (
      <TouchableOpacity
        style={{alignItems: 'center'}}
        onPress={() => this.onPressPriority(key)}>
        <View
          style={[
            styles.priorityContainer,
            active ? {backgroundColor: color, borderWidth: 0} : {},
          ]}>
          <Text
            style={[styles.textNormalSecond, active ? {color: 'white'} : {}]}>
            {short}
          </Text>
        </View>
        <Text
          style={[
            styles.textSmallSecond,
            active ? {color: Theme.colors.textNormal} : {},
          ]}>
          {value}
        </Text>
      </TouchableOpacity>
    );
  };

  renderSelectPriority = () => {
    let {PMTaskPriorityCombo} = this.state.schedule;
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          backgroundColor: Theme.colors.backgroundHeader,
          paddingTop: Theme.margin.m12,
          paddingBottom: Theme.margin.m10,
          paddingHorizontal: Theme.margin.m15,
        }}>
        {this.renderPriority(
          'Priority1',
          'Priority 1',
          '#FFEB33',
          PMTaskPriorityCombo,
          'P1',
        )}
        {this.renderPriority(
          'Priority2',
          'Priority 2',
          '#f6ab2f',
          PMTaskPriorityCombo,
          'P2',
        )}
        {this.renderPriority(
          'Priority3',
          'Priority 3',
          '#c2cc06',
          PMTaskPriorityCombo,
          'P3',
        )}
      </View>
    );
  };

  onChangeUserName = (value) => {
    this.inputingTitle = value;
  };

  canDeleteSchedule = () => {
    const {user} = this.props;
    const {schedule} = this.state;

    if (!schedule.PMTaskID) {
      return false;
    }

    const priority = propOr('', 'priority', schedule);

    return (
      schedule.AACreatedUser === user.ADUserName &&
      priority !== PRIORITY.COMPLETE &&
      priority !== PRIORITY.VERIFY
    );
  };

  render() {
    const {peopleInProject, PMProjectID, canChange} = this.props;
    const {schedule} = this.state;

    if (!schedule) {
      return <View />;
    }

    return (
      <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="padding" enabled>
        <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
          <StatusBar
            backgroundColor={Theme.colors.primary}
            barStyle="light-content"
          />
          <UniStatusBarBackground />
          {this.renderNavigation()}
          <ScrollView>
            {this.renderSelectPriority()}

            <UniInput
              placeholder="Enter title"
              autoSelectAllWhenTextIs="Enter title"
              initValue={propOr('Enter title', 'PMTaskName', schedule)}
              onChangeText={this.onChangeUserName}
              containerStyle={styles.editText}
            />

            <UniDropbox
              label={'Assignee'}
              selectedValue={propOr('', 'FK_ADAssignUserID', schedule)}
              options={peopleInProject}
              selectedKey="ADUserID"
              pathLeft={'shortName'}
              pathValue={'fullName'}
              onSelect={(item, {ADUserID}) =>
                this.setState({
                  schedule: {
                    ...schedule,
                    FK_ADAssignUserID: ADUserID,
                  },
                })
              }
            />

            <UniDatePicker
              label={'Start date'}
              selectedValue={propOr(
                new Date().toISOString(),
                'PMTaskStartDate',
                schedule,
              )}
              onSelect={(item) =>
                this.setState({
                  schedule: {...schedule, PMTaskStartDate: item},
                })
              }
            />

            <UniDatePicker
              label={'End date'}
              selectedValue={propOr(
                new Date().toISOString(),
                'PMTaskEndDate',
                schedule,
              )}
              onSelect={(item) => {
                if (moment(item).isSameOrBefore(schedule.PMTaskStartDate)) {
                  return;
                }

                this.setState({
                  schedule: {...schedule, PMTaskEndDate: item},
                });
              }}
            />

            <View style={styles.percentContainer}>
              <Text
                style={[
                  styles.textNormalSecond,
                  {marginLeft: Theme.margin.m14, flex: 0},
                ]}>
                Percent Completed
              </Text>
              <UniInput
                keyboardType="number-pad"
                forceValueFromProp
                value={`${propOr(0, 'PMTaskCompletePct', schedule)}`}
                initValue={`${propOr(0, 'PMTaskCompletePct', schedule)}`}
                onChangeText={this.onChangePercent}
                containerStyle={{
                  ...styles.editText,
                  marginHorizontal: 0,
                  marginBottom: 0,
                  marginTop: 0,
                  textAlign: 'right',
                  borderColor: 'transparent',
                  flex: 1,
                }}
              />
            </View>

            {
          schedule?.PMTaskID &&  
          <UniMultiSelect 
              projectId={PMProjectID}
              taskId={schedule?.PMTaskID}
              canChange={canChange}
              type={'schedule'}
            />
            }

            <UniInput
              numberOfLines={3}
              autoSelectAllWhenTextIs="Enter Description"
              initValue={propOr('', 'PMTaskDesc', schedule)}
              onChangeText={this.onChangeDesc}
              containerStyle={styles.editTextDescription}
            />

            <View
              style={{alignItems: 'flex-end', marginRight: Theme.margin.m16}}>
              {this.canDeleteSchedule() && (
                <TouchableOpacity onPress={this.onPressDelete} disabled={!this.state.canChange}>
                  <Text
                    style={{
                      color: !this.state.canChange ? Theme.colors.black : Theme.colors.textDangerous,
                      fontSize: Theme.fontSize.normal,
                      paddingVertical: Theme.margin.m8,
                    }}>
                    Delete Schedule
                  </Text>
                </TouchableOpacity>
              )}
              { ( canChange || this.state.isNewSchedule ) && 
                      <UniButton
                      text={'Save'}
                      onPress={this.onPressSave}
                      containerStyle={styles.button}
                    />
                    }
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingViewWrapper>
    );
  }
}

const mapStateToProps = ({
  auth: {user, master, project},
  commonData,
  scheduleList,
}, props) => {
  const {PMProjectID} = project;
  const modulePermissions = pathOr([],['Permission'],commonData);
  const item = pathOr(undefined, ['route', 'params', 'schedule'], props);
  const canChange =  item?.edit;


  return {
    user,
    master,
    project,
    allSchedules: scheduleList.scheduleList,
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    modulePermissions,
    canChange,
    PMProjectID
  };
};

const mapDispatchToProps = {
  addActionIntoQueue: actions.addActionIntoQueue,
  getPeopleInProject: actions.getPeopleInProject,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScheduleUpdate);
