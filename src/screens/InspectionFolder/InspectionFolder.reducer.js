import typeToReducer from 'type-to-reducer';
import getInspectionFolderList from './actions/getInspectionFolderList.action';
import deleteInspectionFolder from './actions/deleteInspectionFolder.action';

const initialState = {
  inspectionFolderList: [],
};

export const actions = {
  getInspectionFolderList: getInspectionFolderList.action,
  deleteInspectionFolder: deleteInspectionFolder.action,
};

export default typeToReducer(
  {
    ...getInspectionFolderList.reducer,
    ...deleteInspectionFolder.reducer,
  },
  initialState,
);
