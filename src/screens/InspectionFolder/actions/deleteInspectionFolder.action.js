import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';

export const actionType = 'DELETE_INSPECTION_FOLDER';

const createURL = (id) => withKey(`/itp-group/${id}`);
const deleteAPI = (id, token) =>
  API.delete(createURL(id), {
    headers: {Authorization: `Bearer ${token}`},
  });

const action = createAction(actionType, deleteAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
    inspectionFolderList: state.inspectionFolderList.filter(
      (item) => item.ID !== meta,
    ),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};

// https://apidev.rocez.com/itp-group/1012
//   Request Method: DELETE
