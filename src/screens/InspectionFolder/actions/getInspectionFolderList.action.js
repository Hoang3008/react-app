import {createAction} from 'redux-actions';
import {identity} from 'ramda';

import {actionType as createFolderActionType} from '../../NewInspectionFolder/actions/createInspectionFolder.action';
import {actionType as updateFolderActionType} from '../../NewInspectionFolder/actions/updateInspectionFolder.action';

import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';

const actionType = 'GET_INSPECTION_FOLDER_LIST';

// https://apidev.rocez.com/itp-group/of-project/125
// Request Method: GET

const createTaskAPI = (token, projectId) => {
  const url = withKey(`/itp-group/of-project/${projectId}`);
  return API.get(url, {headers: {Authorization: `Bearer ${token}`}});
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    inspectionFolderList: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
