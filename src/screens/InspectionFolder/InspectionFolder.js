import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {SwipeListView} from 'react-native-swipe-list-view';
import {
  Alert,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';

import {actions} from './InspectionFolder.reducer';
import styles from './InspectionFolder.styles';
import {SearchBar} from 'react-native-elements';
import Theme from '../../Theme';
import UniImageButton from '../../components/UniImageButton';
import {pathOr, propOr} from 'ramda';
import RouteNames from '../../RouteNames';

const InspectionFolder = (props) => {
  const {
    token,
    navigation,
    inspectionFolderList,
    getInspectionFolderList,
    project,
    deleteInspectionFolder,
    showSearchBox
  } = props;

  const [searchText, setSearchText] = useState('');
  const [filterInspectionAreaList, setFilterInspectionAreaList] = useState([]);

  const PMProjectID = propOr(0, 'PMProjectID', project);

  const setFilterData = () => {
    return inspectionFolderList.filter((item => {
        if (item.Name.includes(searchText)) {
          return true;
        }
    }));
  };

  useEffect(() => {
    const listData = setFilterData();
    setFilterInspectionAreaList(listData);
  }, [inspectionFolderList, searchText]);

  useEffect(() => {
    getInspectionFolderList(token, PMProjectID);
  }, [getInspectionFolderList, PMProjectID]);

  const handleItemClick = (item, index) => {
    if (item.types === 0) {
      navigation.navigate(RouteNames.InspectionList, {
        folderId: item.ID,
      });
    } else {
      navigation.navigate(RouteNames.InspectionSubFolder, {
        folderId: item.ID,
        name: item.Name,
      });
    }
  };

  const onPressNewTask = () => {
    navigation.push(RouteNames.NewInspectionFolder, {
      callback: () => getInspectionFolderList(token, PMProjectID),
    });
  };

  const handleDelete = (item) => () => {
    Alert.alert(
      'Delete',
      'Do you want to delete this folder?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => {
            deleteInspectionFolder(item.ID, token);
          },
        },
      ],
      {cancelable: false},
    );
  };

  const handleEdit = (item) => () => {
    navigation.push(RouteNames.NewInspectionFolder, {
      folder: item,
      callback: () => getInspectionFolderList(token, PMProjectID),
    });
  };

  const renderItem = ({item}) => {
    // CreatedDate: "2020-05-24T04:28:50.160Z"
    // Description: "00 Construction Drawing Set-BP02  Construction Drawing Set "
    // ID: 2
    // Name: "Construction Drawing Set"
    // No: "01"
    // TotalDrawings: 1
    // UpdatedDate: "2020-05-24T04:28:50.160Z"
    const {Name} = item;

    return (
      <TouchableHighlight onPress={() => handleItemClick(item)}>
        <View
          style={{
            padding: 15,
            display: 'flex',
            flexDirection: 'row',
            shadowColor: '#555',
            backgroundColor: 'white',
            alignItems: 'center',
          }}>
          <Image
            style={styles.icon}
            source={require('../../assets/img/ico_folder.png')}
          />
          <Text
            style={{
              flex: 1,
              marginLeft: 15,
              fontSize: 16,
              color: '#3E3F42',
            }}>
            {Name}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  const renderHiddenItem = ({item}) => {
    return (
      <View style={styles.rowBack}>
        <TouchableOpacity
          onPress={handleDelete(item)}
          style={{
            width: 100,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={styles.delete}>Delete</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleEdit(item)}
          style={{
            width: 100,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'green',
          }}>
          <Text style={styles.delete}>Edit</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
      { showSearchBox && (
          <SearchBar
          returnKeyLabel="search"
          placeholder="Type Here..."
          value={searchText}
          onChangeText={setSearchText}
          inputStyle={{
            color: Theme.colors.textNormal,
          }}
          containerStyle={{
            borderTopColor: 'transparent',
            borderBottomColor: 'transparent',
            backgroundColor: '#ffffff',
          }}
          inputContainerStyle={{
            backgroundColor: '#ddd',
          }}
        />
        )
      }
        <SwipeListView
          style={{width: '100%'}}
          data={filterInspectionAreaList}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          keyExtractor={(item) => item.ID}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          leftOpenValue={0}
          rightOpenValue={-200}
          closeOnRowBeginSwipe
          disableRightSwipe
          scrollEnabled={true}
          showsVerticalScrollIndicator={true}
          recalculateHiddenLayout={true}
        />
      </View>
      <UniImageButton
        containerStyle={styles.floatButton}
        source={require('../../assets/img/ic_add.png')}
        onPress={onPressNewTask}
      />
    </View>
  );
};

const mapStateToProps = ({
  auth: {user, project},
  commonData,
  inspectionFolder,
}) => {
  const PMProjectID = propOr(0, 'PMProjectID', project);

  return {
    user,
    project,
    token: user.token,
    categories: pathOr([], ['currentProject', 'categories'], commonData),
    inspectionFolderList: inspectionFolder.inspectionFolderList,
  };
};

const mapDispatchToProps = {
  getInspectionFolderList: actions.getInspectionFolderList,
  deleteInspectionFolder: actions.deleteInspectionFolder,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionFolder);
