import {createAction} from 'redux-actions';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import {identity} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'POST_CHECKLIST';

// http://api.gmc.solutions/PMTaskItems/CreateObjectByList

const postCheckListURL = () => withKey('/PMTaskItems/CreateObjectByList');
const postCheckListAPI = (checklistItems) =>
  AsyncStorage.getItem('token').then((token) => {
    const data = {
      projectId: checklistItems && checklistItems[0].projectId,
      items: checklistItems.map(item => item.items[0]),
      taskId: checklistItems && checklistItems[0].taskId
    }
    return API.post(withKey('/task/item'),data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });

const action = createAction(actionType, postCheckListAPI);

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: identity,
    FAILURE: identity,
  },
};

export default {action, reducer};
