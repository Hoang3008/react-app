import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    marginRight: 60,
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textSectionHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNormal,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonContainer: {
    padding: Theme.margin.m8,
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.border,
    alignItems: 'center',
  },
  textButton: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textPrimary,
  },
  buttonContainerActive: {
    padding: Theme.margin.m8,
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.accent,
    backgroundColor: Theme.colors.accent,
  },
  textButtonActive: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textWhite,
  },
  inputCommentContainer: {
    width: SCREEN_WIDTH,
    minHeight: Theme.specifications.inputCommentContainerHeight,
    alignItems: 'center',
    paddingVertical: Theme.margin.m12,
    paddingHorizontal: Theme.margin.m15,
    backgroundColor: 'white',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.1,
    shadowRadius: Theme.margin.m6,
    borderColor: Theme.colors.border,
    borderWidth: 0.5,
    // elevation: Theme.margin.m10,
  },
  inputComment: {
    flex: 1,
    minHeight: Theme.margin.m36,
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textNormal,
    marginHorizontal: Theme.margin.m12,
    paddingHorizontal: Theme.margin.m12,
    paddingVertical: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderRadius: Theme.specifications.editTextNormalBorder,
    borderColor: Theme.colors.border,
  },
  buttonSendComment: {
    backgroundColor: Theme.colors.primary,
    height: Theme.margin.m36,
    justifyContent: 'center',
    paddingHorizontal: Theme.margin.m14,
    borderRadius: Theme.margin.m4,
  },
};
