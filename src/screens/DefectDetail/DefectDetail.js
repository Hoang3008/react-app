import React from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  Animated,
  TextInput,
  Keyboard,
  ScrollView,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import moment from 'moment';
import DocumentPicker from 'react-native-document-picker';

import styles from './styles';

import Theme from '../../Theme';
import {
  UniStatusBarBackground,
  UniImageButton,
  UniBottomSheet,
} from '../../components';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {
  requestGetAllTaskItemByTaskId,
  requestUpdateTaskItem,
} from '../../api/defect';
import {
  getTaskCommentFileUrl,
  getTaskDocumentFileUrl,
  getUserAvatarUrlByUsername,
} from '../../api/urls';
import {
  getFileName,
  filterStatusResponseList,
  isCommentSystemType,
} from '../defect/DefectHelper';
import RouteNames from '../../RouteNames';
import {equals, isEmpty, pathOr, propOr} from 'ramda';
import postCheckList from './actions/postChecklist.action';
import NewChecklistItemModal from '../../components/NewChecklistItemModal';
import {EMPTY, OFFLINE_ACTION_TYPES} from '../../global/constants';
import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import BeforeAfter from '../../components/BeforeAfter/BeforeAfter';

export const getDefaultDefect = (user, project, lat, lng, drawingId) => {
  const {PMProjectID} = project;
  const {ADUserID, ADUserName} = user;
  const date = new Date();
  const fromDefect = {};

  return {
    AACreatedUser: ADUserName,
    AAUpdatedUser: ADUserName,
    PMTaskNo: 0,
    PMTaskName: 'Enter title',
    PMTaskDesc: 'Enter description',
    FK_ADUserID: ADUserID,
    PMTaskLat: lat,
    PMTaskLng: lng,
    FK_PPDrawingID: drawingId,
    FK_PMProjectID: PMProjectID,
    FK_ADAssignUserID: propOr(ADUserID, 'FK_ADAssignUserID', fromDefect),
    PMTaskStartDate: propOr(date.toISOString(), 'PMTaskStartDate', fromDefect),
    PMTaskEndDate: propOr(date.toISOString(), 'PMTaskEndDate', fromDefect),
    PMTaskCompleteCheck: false,
    PMTaskLocation: '',
    PMTaskManPower: 0,
    PMTaskCost: 0,
    PMTaskTags: '',
    PMTaskTypeCombo: 'defect',
    PMTaskPriorityCombo: 'Priority1',
    FK_PMProjectCategoryID: propOr(0, 'FK_PMProjectCategoryID', fromDefect),
    FK_PMProjectLocationID: propOr(0, 'FK_PMProjectLocationID', fromDefect),
    FK_PMTaskGroupID: 0,
  };
};

class DefectDetail extends React.Component {
  constructor(props) {
    super(props);
    const lat = pathOr(0, ['route', 'params', 'lat'], this.props);
    const lng = pathOr(0, ['route', 'params', 'lng'], this.props);

    this.state = {
      lat,
      lng,
      allItems: [],
      selectingDocumentForComment: false,
      activeSlide: 0,
      showNewChecklistItemModal: false,
      showImageModal: false,
      imageUrl: '',
      showAllComments: false,
    };
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {defectList, defect, getTaskCommentList} = this.props;
    const {lat, lng} = this.state;

    if (prevProps.defectList !== defectList) {
      const defectFromMemory = defectList.find(
        ({PMTaskLat, PMTaskLng}) => PMTaskLat === lat && lng === PMTaskLng,
      );

      if (defectFromMemory) {
        this.setState({
          defect: defectFromMemory,
        });
      }
    }

    const id = pathOr('', ['defect', 'PMTaskID'], this.props);
    const prevId = pathOr('', ['defect', 'PMTaskID'], prevProps);

    // if (id !== prevId) {
    //   this.getData();
    // }

    if (!equals(defect, prevProps.defect)) {
      getTaskCommentList(defect.PMTaskID);
    }
  }

  getData() {
    const {defect = {}, addActionIntoQueue, route} = this.props;
    const PMTaskID = pathOr(
      defect.PMTaskID,
      ['params', 'defect', 'PMTaskID'],
      route,
    );
    const PMProjectID = pathOr(
      defect.FK_PMProjectID,
      ['params', 'defect', 'FK_PMProjectID'],
      route,
    );

    if (PMTaskID) {
      this.getDetailData(PMTaskID, PMProjectID);
    } else {
      addActionIntoQueue({
        type: OFFLINE_ACTION_TYPES.CREATE_DEFECT,
        data: this.getDefaultDefectWrapper(),
      });
    }
  }

  getDefaultDefectWrapper = () => {
    const {user, project} = this.props;

    const lat = pathOr(0, ['route', 'params', 'lat'], this.props);
    const lng = pathOr(0, ['route', 'params', 'lng'], this.props);
    const drawingId = pathOr(0, ['route', 'params', 'drawingId'], this.props);

    return getDefaultDefect(user, project, lat, lng, drawingId);
  };

  getDetailData = (PMTaskID, PMProjectID) => {
    const {getTaskCommentList, getDefectById} = this.props;
    this.loadDocuments(PMTaskID, PMProjectID);
    this.loadTaskItems(PMTaskID, PMProjectID);
    getTaskCommentList(PMTaskID, PMProjectID);
    getDefectById(PMTaskID, PMProjectID);
  };

  loadDocuments = (PMTaskID, PMProjectID) => {
    if (!PMTaskID) {
      return;
    }

    const {getTaskDocumentList} = this.props;
    getTaskDocumentList(PMTaskID);
  };

  loadTaskItems = (PMTaskID, PMProjectID) => {
    requestGetAllTaskItemByTaskId({taskId: PMTaskID, projectId: PMProjectID})
      .then((response) => {
        if (response && Array.isArray(response.data)) {
          this.setState({allItems: response.data});
        }
      })
      .catch((error) => {});
  };

  onPressUpdateDefect = () => {
    const {lat, lng} = this.state;
    const {navigation, defect} = this.props;
    const drawingId = pathOr(0, ['route', 'params', 'drawingId'], this.props);

    navigation.navigate(RouteNames.DefectUpdate, {
      lat,
      lng,
      drawingId,
      defect,
      onGoBack: () => this.refresh,
      onDelete: () => {
        this.props.route &&
          this.props.route.params &&
          this.props.route.params.onDelete &&
          this.props.route.params.onDelete();
        this.props.navigation.goBack();
      },
      onUpdate: (defect) => {
        this.props.route &&
          this.props.route.params &&
          this.props.route.params.onUpdate &&
          this.props.route.params.onUpdate();
        this.getData();
        this.setState({defect});
        console.log('vao onUpdate');
      },
    });
  };

  onPressAddDocumentPhoto = () => {
    this.bottomSheet &&
      this.bottomSheet.show(this.onPressSelectedDocument, false);
  };

  onPressSelectedDocument = (item) => {
    let options = {
      cropping: false,
      writeTempFile: false,
      compressImageMaxWidth: 1024,
      compressImageMaxHeight: 1024,
    };
    switch (item) {
      case 'camera':
        this.props.navigation.push(RouteNames.CaptureImage, {
          onSave: (path) => {
            this.onCreateTaskDocument({
              uri: path || '',
              filename: getFileName(path),
              type: 'image/png',
            });
          },
        });
        break;
      case 'photos-device':
        ImagePicker.openPicker(options)
          .then((image) =>
            this.props.navigation.push(RouteNames.SketchDrawing, {
              imagePath: image.path,
              onSave: (path) => {
                this.onCreateTaskDocument({
                  uri: path || '',
                  filename: getFileName(path),
                  type: 'image/png',
                });
              },
            }),
          )
          .catch((e) => {});
        break;
      default:
        break;
    }
  };

  onCreateTaskDocument = (image) => {
    const {addActionIntoQueue, defect, user} = this.props;
    const {ADUserID} = user || {};
    const {PMTaskID, offlineId} = defect || {};
    const body = {AACreatedUser: ADUserID, FK_PMTaskID: PMTaskID || offlineId};

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.ADD_PHOTO,
      data: {body, image},
    });
  };

  onPressUpdateItemCompleteStatus = (item) => {
    let {allItems} = this.state;
    for (let i = 0; i < allItems.length; i++) {
      if (allItems[i].PMTaskItemID === item.PMTaskItemID) {
        allItems[i].PMTaskItemCompleteCheck = !allItems[i]
          .PMTaskItemCompleteCheck;
        requestUpdateTaskItem(allItems[i]).then((response) => {
          if (response && response.data) {
            this.props.route &&
              this.props.route.params &&
              this.props.route.params.onUpdate &&
              this.props.route.params.onUpdate();
          }
        });
        let newList = Object.assign([], allItems);
        this.setState({allItems: newList});
      }
    }
  };

  onChangeComment = (value) => {
    this.inputingComment = value;
  };

  onPressSendComment = async () => {
    if (
      (!this.inputingComment || this.inputingComment.trim().length === 0) &&
      !this.state.selectingDocumentForComment
    ) {
      return;
    }

    const {user, defect, addActionIntoQueue} = this.props;
    const {selectingDocumentForComment} = this.state;

    const {ADUserName, ADUserID} = user || {};
    const {PMTaskID, offlineId} = defect || {};

    const body = {
      AACreatedUser: `${ADUserName}`,
      AAUpdatedUser: `${ADUserName}`,
      ADUserName: ADUserName,
      PMTaskCommentUserName: ADUserName,
      FK_PMTaskID: PMTaskID || offlineId,
      PMTaskCommentDesc:
        this.inputingComment && this.inputingComment.length > 0
          ? this.inputingComment
          : '',
      PMTaskCommentType: 'USER_INPUT',
    };

    if (this.inputingComment && this.inputingComment.length > 0) {
      body.PMTaskCommentDesc = this.inputingComment;
    }

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.ADD_COMMENT,
      data: {body, image: selectingDocumentForComment},
    });

    this.textInputComment && this.textInputComment.clear();
    this.inputingComment = '';
    Keyboard.dismiss();
    this.setState({selectingDocumentForComment: false}, () => {
      this.scrollView && this.scrollView.scrollToEnd({animated: true});
    });
  };

  onPressCommentAttach = () => {
    this.bottomSheet &&
      this.bottomSheet.show(this.onPressSelectedAttachForComment, true);
  };

  onPressSelectedAttachForComment = (item) => {
    let options = {
      cropping: false,
      writeTempFile: false,
      compressImageMaxWidth: 1024,
      compressImageMaxHeight: 1024,
    };

    switch (item) {
      case 'camera':
        this.props.navigation.push(RouteNames.CaptureImage, {
          onSave: (path) => {
            this.setState({
              selectingDocumentForComment: {
                uri: path || '',
                filename: getFileName(path),
                type: 'image/png',
              },
            });
          },
        });
        break;
      case 'photos-device':
        ImagePicker.openPicker(options)
          .then((image) =>
            this.props.navigation.push(RouteNames.SketchDrawing, {
              imagePath: image.path,
              onSave: (path) => {
                this.setState({
                  selectingDocumentForComment: {
                    uri: path || '',
                    filename: getFileName(path),
                    type: 'image/png',
                  },
                });
              },
            }),
          )
          .catch((e) => {});
        break;
      case 'files-device':
        DocumentPicker.pick({
          type: [DocumentPicker.types.pdf],
        }).then((file) => {
          this.setState({
            selectingDocumentForComment: {
              uri: file.uri || '',
              filename: file.name,
              type: file.type,
            },
          });
        });
        break;
    }
  };

  renderNavigation = () => {
    const {peopleInProject} = this.props;
    const {PMTaskName, PMTaskID, AACreatedUser} = this.props.defect || {};

    const user = peopleInProject.find(
      ({PMProjectPeopleEmailAddress}) =>
        AACreatedUser === PMProjectPeopleEmailAddress,
    );

    return (
      <View style={styles.navigationBar}>
        <View style={{...styles.row, flex: 1}}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() =>
              this.props.navigation && this.props.navigation.goBack()
            }
          />
          <View>
            <Text style={styles.textHeaderSmall}>{`#${PMTaskID} - ${propOr(
              '',
              'fullName',
              user,
            )}`}</Text>
            <View style={styles.row}>
              <Text style={styles.textHeader} numberOfLines={1}>
                {PMTaskName}
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          onPress={this.onPressUpdateDefect}
          style={{
            height: '100%',
            paddingVertical: Theme.margin.m6,
            paddingHorizontal: 10,
            marginRight: 6,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white', fontSize: Theme.fontSize.normal}}>
            Edit
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderTopButton = () => {
    const {drawing, allDocuments} = this.props;
    const drawingName = propOr('', 'PPDrawingName', drawing);

    return (
      <View
        style={{
          backgroundColor: 'white',
          paddingHorizontal: Theme.margin.m15,
          paddingTop: Theme.margin.m15,
          flexDirection: 'row',
          justifyContent: 'flex-end',
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
          }}>
          <Text style={styles.textSectionHeader}>Drawing:</Text>
          <Text>{drawingName}</Text>
        </View>
        {allDocuments.length < 2 && (
          <TouchableOpacity style={styles.buttonSendComment}>
            <Text
              style={{color: 'white', fontSize: Theme.fontSize.normal}}
              onPress={this.onPressAddDocumentPhoto}>
              Photos
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  onPressPhoto = (item, url) => {
    const {navigation} = this.props;

    navigation.navigate(RouteNames.ImageFullScreen, {
      item,
      url: url,
    });
  };

  onPressPDF = (url) => {
    const {navigation} = this.props;
    navigation.navigate(RouteNames.PDFFullScreen, {url: url});
  };

  handlePostChecklist = (checklistItems) => {
    const {
      postCheckList,
      user: {ADUserName},
      defect,
      project
    } = this.props;

    postCheckList(
      checklistItems.map((item) => ({
        projectId: project?.PMProjectID,
        taskId: defect.PMTaskID,
        items: [
          {
            content: item.PMTaskItemDesc,
            checked: item.PMTaskItemCompleteCheck
          }
        ]
      })),
    ).then(() => {
      this.getData();
    });
  };

  handlePostChecklistItemFromModal = (value, checked) => {
    this.handlePostChecklist([
      {
        PMTaskItemCompleteCheck: checked,
        PMTaskItemDesc: value,
      },
    ]);
  };

  renderDocumentPhotoItem = ({item, index}) => {
    let imageWidth = SCREEN_WIDTH - Theme.margin.m15 * 2;
    let url = item.PMTaskDocName
      ? getTaskDocumentFileUrl({fileName: item.PMTaskDocName})
      : pathOr('', ['tempImage', 'uri'], item);

    return (
      <View style={{flex: 1}}>
        <TouchableOpacity onPress={() => this.onPressPhoto(item, url)}>
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            style={{
              marginTop: Theme.margin.m15,
              width: imageWidth,
              height: (imageWidth * 10) / 16,
            }}
            source={{
              uri: url,
              priority: FastImage.priority.normal,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  renderDocument = () => {
    const {allDocuments} = this.props;

    return (
      <View
        style={{
          paddingHorizontal: Theme.margin.m15,
          paddingBottom: Theme.margin.m15,
        }}>
        <BeforeAfter data={allDocuments} onPressPhoto={this.onPressPhoto} />
      </View>
    );
  };

  renderTaskItemsItem = (item = {}) => {
    let {PMTaskItemDesc, PMTaskItemCompleteCheck} = item;
    return (
      <View style={[styles.row, {paddingVertical: Theme.margin.m8}]}>
        <TouchableOpacity
          style={{
            paddingRight: Theme.margin.m12,
            paddingVertical: Theme.margin.m4,
          }}
          onPress={() => this.onPressUpdateItemCompleteStatus(item)}>
          <Image
            style={{
              width: Theme.specifications.checkBoxSize,
              height: Theme.specifications.checkBoxSize,
            }}
            source={
              PMTaskItemCompleteCheck
                ? require('../../assets/img/ic_checkbox_active.png')
                : require('../../assets/img/ic_checkbox_inactive.png')
            }
          />
        </TouchableOpacity>
        <Text
          style={{
            flex: 1,
            color: Theme.colors.textNormal,
            fontSize: Theme.fontSize.normal,
          }}>
          {PMTaskItemDesc}
        </Text>
      </View>
    );
  };

  renderTaskItems = () => {
    const {navigation} = this.props;
    return (
      <View
        style={{
          padding: Theme.margin.m15,
          backgroundColor: 'white',
        }}>
        <Text style={styles.textSectionHeader}>Checklist:</Text>
        <FlatList
          scrollEnabled={false}
          data={this.state.allItems}
          extraData={this.state}
          renderItem={({item, index}) => this.renderTaskItemsItem(item, index)}
          keyExtractor={(item, index) => 'item_' + index}
          showsVerticalScrollIndicator={false}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginLeft: Theme.margin.m30,
            marginTop: Theme.margin.m10,
          }}>
          <TouchableOpacity
            style={[styles.buttonContainer, {flex: 1}]}
            onPress={() => this.setState({showNewChecklistItemModal: true})}>
            <Text style={styles.textButton}>+ New item</Text>
          </TouchableOpacity>
          <View style={{width: Theme.margin.m15}} />
          <TouchableOpacity
            style={[styles.buttonContainer, {flex: 1}]}
            onPress={() =>
              navigation.push(RouteNames.NewChecklistScreen, {
                onPostChecklist: this.handlePostChecklist,
              })
            }>
            <Text style={styles.textButton}>+ Checklist</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderCommentsItem = (item = {}) => {
    if (isEmpty(item)) {
      return <View />;
    }

    let {
      AACreatedUser,
      AAUpdatedDate,
      PMTaskCommentServerPath,
      PMTaskCommentDesc,
      PMTaskCommentUserName,
      tempImage,
    } = item;

    const PMTaskCommentContentType = propOr(
      '',
      'PMTaskCommentContentType',
      item,
    );

    const isTempFile = !PMTaskCommentServerPath && tempImage;

    const fileURL = isTempFile
      ? pathOr('', ['tempImage', 'uri'], item)
      : getTaskCommentFileUrl({fileName: PMTaskCommentServerPath});

    const fileType = isTempFile
      ? pathOr('', ['tempImage', 'type'], item)
      : PMTaskCommentContentType;

    const {user} = this.props;

    let avatar = getUserAvatarUrlByUsername(AACreatedUser);
    let name = PMTaskCommentUserName;
    const date = AAUpdatedDate ? new Date(AAUpdatedDate) : null;

    const isSystem = isCommentSystemType(item);

    const showDate = !date
      ? 'Not synced yet'
      : `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} at ${
          (date.getHours() % 12 || 12) < 10 ? '0' : ''
        }${date.getHours() % 12 || 12}:${
          date.getMinutes() < 10 ? '0' : ''
        }${date.getMinutes()} ${date.getHours() >= 12 ? 'PM' : 'AM'}`;

    if (isSystem) {
      return (
        <View style={{paddingVertical: 8}}>
          <View
            style={{
              ...styles.row,
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
            }}>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
              }}>
              {showDate}
            </Text>
            <Text
              style={{
                paddingVertical: Theme.margin.m2,
                fontSize: Theme.fontSize.normal,
                color: Theme.colors.textNormal,
              }}>
              {name === '1' ? 'System' : name}
            </Text>
          </View>
          <View
            style={{
              alignItems: 'center',
            }}>
            {PMTaskCommentDesc && PMTaskCommentDesc.length > 0 ? (
              <Text
                style={{
                  color: Theme.colors.textNormal,
                  fontStyle: 'italic',
                  fontSize: 12,
                }}>
                {PMTaskCommentDesc}
              </Text>
            ) : null}
          </View>
        </View>
      );
    }

    if (
      `${user.ADUserID}` === AACreatedUser ||
      user.ADUserName === AACreatedUser
    ) {
      return (
        <View style={{paddingVertical: 8}}>
          <View
            style={{
              ...styles.row,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              display: 'flex',
            }}>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
                textAlign: 'right',
              }}>
              {showDate}
            </Text>
          </View>
          <View
            style={{
              paddingLeft:
                Theme.specifications.commentAvatarSize + Theme.margin.m12,
              alignItems: 'flex-end',
            }}>
            {PMTaskCommentDesc && PMTaskCommentDesc.length > 0 ? (
              <Text
                style={{
                  marginTop: Theme.margin.m6,
                  paddingVertical: Theme.margin.m8,
                  paddingHorizontal: Theme.margin.m10,
                  backgroundColor: '#3483F6',
                  borderRadius: 10,
                  borderWidth: 0,
                  color: '#fff',
                  fontStyle: isSystem ? 'italic' : 'normal',
                  fontSize: isSystem ? 12 : 14,
                }}>
                {PMTaskCommentDesc}
              </Text>
            ) : null}
            {fileURL && fileType.includes('image') ? (
              <TouchableOpacity
                onPress={() =>
                  isTempFile ? true : this.onPressPhoto(null, fileURL)
                }>
                <Image
                  style={{
                    resizeMode: 'center',
                    width: SCREEN_WIDTH / 3,
                    height: SCREEN_WIDTH / 3,
                    marginTop: 6,
                  }}
                  source={{uri: fileURL}}
                />
              </TouchableOpacity>
            ) : fileType.toLowerCase().includes('pdf') ? (
              <TouchableOpacity
                onPress={() => (isTempFile ? true : this.onPressPDF(fileURL))}>
                <Text
                  style={{
                    maxWidth: 280,
                    padding: 10,
                    borderRadius: 4,
                    borderWidth: 1,
                    borderColor: '#999',
                    color: '#3173ff',
                  }}>
                  {fileURL}
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      );
    }

    return (
      <View style={{paddingVertical: 8}}>
        <View style={styles.row}>
          <ImageBackground
            style={{
              resizeMode: 'center',
              width: Theme.specifications.commentAvatarSize,
              height: Theme.specifications.commentAvatarSize,
              borderRadius: Theme.specifications.commentAvatarSize / 2,
            }}
            source={require('../../assets/img/default_avatar.png')}>
            <Image
              style={{
                resizeMode: 'center',
                width: Theme.specifications.commentAvatarSize,
                height: Theme.specifications.commentAvatarSize,
                borderRadius: Theme.specifications.commentAvatarSize / 2,
              }}
              source={{uri: avatar}}
            />
          </ImageBackground>
          <View style={{marginLeft: Theme.margin.m12}}>
            <Text
              style={{
                paddingVertical: Theme.margin.m2,
                fontSize: Theme.fontSize.normal,
                color: Theme.colors.textNormal,
                fontWeight: 'bold',
              }}>
              {name}
            </Text>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
              }}>
              {showDate}s
            </Text>
          </View>
        </View>
        <View
          style={{
            paddingLeft:
              Theme.specifications.commentAvatarSize + Theme.margin.m12,
            alignItems: 'flex-start',
          }}>
          {PMTaskCommentDesc && PMTaskCommentDesc.length > 0 ? (
            <Text
              style={{
                marginTop: Theme.margin.m6,
                paddingVertical: Theme.margin.m8,
                paddingHorizontal: Theme.margin.m10,
                backgroundColor: Theme.colors.backgroundComment,
                borderRadius: 10,
                borderWidth: 0,
                fontStyle: isSystem ? 'italic' : 'normal',
                fontSize: isSystem ? 12 : 14,
              }}>
              {PMTaskCommentDesc}
            </Text>
          ) : null}
          {fileURL && fileType.includes('image') ? (
            <TouchableOpacity
              onPress={() =>
                isTempFile ? true : this.onPressPhoto(null, fileURL)
              }>
              <Image
                style={{
                  resizeMode: 'center',
                  width: SCREEN_WIDTH / 3,
                  height: SCREEN_WIDTH / 3,
                  marginTop: 6,
                }}
                source={{uri: fileURL}}
              />
            </TouchableOpacity>
          ) : fileType.toLowerCase().includes('pdf') ? (
            <TouchableOpacity
              onPress={() => (isTempFile ? true : this.onPressPDF(fileURL))}>
              <Text
                style={{
                  maxWidth: 280,
                  padding: 10,
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: '#999',
                  color: '#3173ff',
                }}>
                {PMTaskCommentServerPath}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    );
  };

  renderComments = () => {
    const {showAllComments} = this.state;
    const {allComments} = this.props;

    const showComments = filterStatusResponseList(allComments, showAllComments);

    return (
      <View
        style={{
          padding: Theme.margin.m15,
          backgroundColor: 'white',
          marginBottom: Theme.specifications.inputCommentContainerHeight,
        }}>
        <View
          style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 20,
          }}>
          <Text style={styles.textSectionHeader}>Comments:</Text>
          <View style={{flex: 1}} />
          <TouchableOpacity
            style={{
              marginLeft: 20,
              maxWidth: 150,
              paddingRight: Theme.margin.m12,
              paddingVertical: Theme.margin.m4,
            }}
            onPress={() => this.setState({showAllComments: !showAllComments})}>
            <Image
              style={{
                width: Theme.specifications.checkBoxSize,
                height: Theme.specifications.checkBoxSize,
              }}
              source={
                showAllComments
                  ? require('../../assets/img/ic_checkbox_active.png')
                  : require('../../assets/img/ic_checkbox_inactive.png')
              }
            />
          </TouchableOpacity>
          <Text
            style={{
              color: Theme.colors.textNormal,
              fontSize: Theme.fontSize.normal,
            }}>
            Show history
          </Text>
        </View>
        <FlatList
          scrollEnabled={false}
          data={showComments}
          extraData={this.state}
          renderItem={({item, index}) => this.renderCommentsItem(item, index)}
          keyExtractor={(item, index) => 'comment_' + index}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  };

  renderInputComment = () => {
    const {selectingDocumentForComment} = this.state;
    return (
      <Animated.View
        style={[
          styles.inputCommentContainer,
          {
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
          },
        ]}>
        {selectingDocumentForComment ? (
          <View
            style={{
              paddingBottom: Theme.margin.m12,
              paddingTop: Theme.margin.m6,
              paddingHorizontal: Theme.margin.m6,
            }}>
            {selectingDocumentForComment.filename &&
            selectingDocumentForComment.filename
              .toLowerCase()
              .endsWith('pdf') ? (
              <Text
                style={{
                  maxWidth: 280,
                  padding: 10,
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: '#999',
                  color: '#3173ff',
                }}>
                {selectingDocumentForComment.filename}
              </Text>
            ) : (
              <Image
                style={{
                  resizeMode: 'contain',
                  width: SCREEN_WIDTH / 3,
                  height: SCREEN_WIDTH / 3,
                  borderRadius: Theme.margin.m8,
                }}
                source={{uri: this.state.selectingDocumentForComment.uri}}
              />
            )}
            <TouchableOpacity
              style={{position: 'absolute', right: 0, top: 0}}
              onPress={() =>
                this.setState({selectingDocumentForComment: false})
              }>
              <Image
                style={{width: 20, height: 20}}
                source={require('../../assets/img/ic_close.png')}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={this.onPressCommentAttach}
            style={{
              paddingVertical: Theme.margin.m6,
              paddingHorizontal: Theme.margin.m10,
            }}>
            <Image
              style={{width: Theme.margin.m20, height: Theme.margin.m20}}
              source={require('../../assets/img/ic_attach.png')}
            />
          </TouchableOpacity>
          <TextInput
            ref={(ref) => (this.textInputComment = ref)}
            style={styles.inputComment}
            multiline
            onChangeText={this.onChangeComment}
          />
          <TouchableOpacity
            onPress={this.onPressSendComment}
            style={styles.buttonSendComment}>
            <Text style={{color: 'white', fontSize: Theme.fontSize.normal}}>
              Share
            </Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    );
  };

  render() {
    const {showNewChecklistItemModal} = this.state;
    return (
      <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="height" enabled>
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <StatusBar
            backgroundColor={Theme.colors.primary}
            barStyle="light-content"
          />
          <UniStatusBarBackground />
          {this.renderNavigation()}
          <ScrollView
            showsVerticalScrollIndicator={false}
            ref={(ref) => (this.scrollView = ref)}>
            {this.renderTopButton()}
            {this.renderDocument()}
            {this.renderTaskItems()}
            {this.renderComments()}
          </ScrollView>
          {this.renderInputComment()}
          <UniBottomSheet ref={(ref) => (this.bottomSheet = ref)} />
          <NewChecklistItemModal
            show={showNewChecklistItemModal}
            onPostChecklist={this.handlePostChecklistItemFromModal}
            onDismiss={() => this.setState({showNewChecklistItemModal: false})}
          />
        </View>
      </KeyboardAvoidingViewWrapper>
    );
  }
}

const mapStateToProps = (
  {auth: {user, project}, commonData, drawing},
  props,
) => {
  const lat = pathOr(0, ['route', 'params', 'lat'], props);
  const lng = pathOr(0, ['route', 'params', 'lng'], props);
  const drawingPage = pathOr(false, ['route', 'params', 'drawingPage'], props);
  let defect = pathOr({}, ['route', 'params', 'defect'], props);
  const drawingId =
    pathOr(0, ['route', 'params', 'drawingId'], props) ||
    (defect.FK_PPDrawingID || 0);
  // if defect detail is loaded from the drawing page, data will be taken from defectListByDrawingId
  const defectList = drawingPage ?
      pathOr(EMPTY.ARRAY, ['defectListByDrawingId', drawingId], commonData) :
      pathOr(EMPTY.ARRAY, ['defectList', drawingId], commonData);
  const drawingList = propOr([], 'drawingList', drawing);
  // const {PMProjectID} = project; TODO

  if (!isEmpty(drawingId) && !isEmpty(defect)) {
    defect =
      defectList.find(({PMTaskID}) => PMTaskID === defect.PMTaskID) || {};
  } else if (lat !== 0 && lng !== 0) {
    defect =
      defectList.find(
        ({PMTaskLat, PMTaskLng}) =>
          PMTaskLat.toFixed(12) === lat.toFixed(12) &&
          lng.toFixed(12) === PMTaskLng.toFixed(12),
      ) || {};
  }

  const taskId = propOr(defect.offlineId || '', 'PMTaskID', defect);
  const allDocuments = pathOr([], ['photos', taskId], commonData);
  const allComments = pathOr([], ['comments', taskId], commonData);

  return {
    user,
    project,
    allDocuments,
    allComments,
    defectList,
    defect,
    drawing: drawingList.find(({PPDrawingID}) => PPDrawingID === drawingId),
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
  };
};

const mapDispatchToProps = {
  postCheckList: postCheckList.action,
  addActionIntoQueue: actions.addActionIntoQueue,
  getTaskDocumentList: actions.getTaskDocumentList,
  getTaskCommentList: actions.getTaskCommentList,
  getDefectById: actions.getDefectById,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DefectDetail);
