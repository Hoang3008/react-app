import Theme from '../../Theme';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    marginLeft: Theme.margin.m52,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  sectionHeaderText: {
    flex: 1,
    paddingHorizontal: Theme.margin.m12,
    paddingBottom: Theme.margin.m6,
    paddingTop: Theme.margin.m22,
    color: Theme.colors.textNormal,
    backgroundColor: Theme.colors.backgroundHeader,
  },
  floatButton: {
    position: 'absolute',
    bottom: Theme.margin.m16,
    right: Theme.margin.m20,
    width: Theme.specifications.floatButtonSize,
    height: Theme.specifications.floatButtonSize,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#34AA44',
    borderRadius: Theme.specifications.floatButtonSize / 2,
    borderWidth: 0,
  },

  delete: {
    fontSize: 16,
    color: '#ffffff',
  },
  icon: {
    resizeMode: 'contain',
    width: 48,
    height: 48,
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingLeft: 15,
  },
};
