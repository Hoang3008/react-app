import typeToReducer from 'type-to-reducer';
import getInspectionSubFolderList from './actions/getInspectionSubFolderList.action';
import deleteInspectionSubFolder from './actions/deleteInspectionSubFolder.action';

const initialState = {
  inspectionFolderList: [],
};

export const actions = {
  getInspectionSubFolderList: getInspectionSubFolderList.action,
  deleteInspectionSubFolder: deleteInspectionSubFolder.action,
};

export default typeToReducer(
  {
    ...getInspectionSubFolderList.reducer,
    ...deleteInspectionSubFolder.reducer,
  },
  initialState,
);
