import {createAction} from 'redux-actions';
import {identity} from 'ramda';

import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';

const actionType = 'GET_INSPECTION_SUB_FOLDER_LIST';

// https://apidev.rocez.com/itp-group/of-project/125
// Request Method: GET
// https://apid.rocez.com/itp-group/of-project/1174?parentId=13

const createTaskAPI = (token, projectId, parentId) => {
  const url = withKey(
    `/itp-group/of-project/${projectId}?parentId=${parentId}`,
  );
  return API.get(url, {headers: {Authorization: `Bearer ${token}`}});
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    inspectionFolderList: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
