import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonContainerActive: {
    padding: Theme.margin.m8,
    borderRadius: Theme.margin.m4,
    borderWidth: Theme.margin.m1,
    borderColor: Theme.colors.accent,
    backgroundColor: Theme.colors.accent,
  },
  textButtonActive: {
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textWhite,
  },
  textValue: {
    textAlign: 'left',
    color: Theme.colors.textNormal,
    fontSize: Theme.fontSize.normal,
    marginRight: Theme.margin.m10,
    marginLeft: Theme.margin.m5,
    paddingVertical: Theme.margin.m4,
  },
  textNo: {
    textAlign: 'left',
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.small_12,
    marginRight: Theme.margin.m10,
    marginLeft: Theme.margin.m5,
    paddingVertical: Theme.margin.m4,
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },
};
