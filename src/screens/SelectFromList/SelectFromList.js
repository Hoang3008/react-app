import React, {useState, useEffect} from 'react';
import {View, Text, StatusBar, FlatList, TouchableOpacity} from 'react-native';
import {pathOr} from 'ramda';

import styles from './styles';

import Theme from '../../Theme';
import {
  UniStatusBarBackground,
  UniImageButton,
  UniInput,
} from '../../components';
import {EMPTY} from '../../global/constants';
import RouteNames from '../../RouteNames';
import DropDown from '../../components/DropDown/DropDown';

const SelectFromList = (props) => {
  const {navigation, route} = props;
  const [dataFromState, setDataFromState] = useState([]);
  const [filterValue, setFilterValue] = useState('');

  const data = pathOr(EMPTY.ARRAY, ['params', 'options'], route);
  const idKey = pathOr(EMPTY.STRING, ['params', 'idKey'], route);
  const nameKey = pathOr(EMPTY.STRING, ['params', 'nameKey'], route);
  const noKey = pathOr(EMPTY.STRING, ['params', 'noKey'], route);
  const descriptionKey = pathOr(
    EMPTY.STRING,
    ['params', 'descriptionKey'],
    route,
  );
  const label = pathOr(EMPTY.STRING, ['params', 'label'], route);

  useEffect(() => {
    const filteredData = data.filter((item) => {
      const id = item[idKey] || '';

      if (!id) {
        return false;
      }

      const name = item[nameKey] || '';
      const no = item[noKey] || '';

      if (!filterValue) {
        return true;
      }

      return (
        (no && no.toLowerCase().includes(filterValue.toLowerCase())) ||
        (name && name.toLowerCase().includes(filterValue.toLowerCase()))
      );
    });
    setDataFromState(filteredData);
  }, [data, filterValue, nameKey, noKey]);

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={navigation.goBack}
          />
          <View>
            <View style={styles.row}>
              <Text style={styles.textHeader}>Select {label}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  const renderRow = ({item}) => {
    const {
      route: {
        params: {onSelect},
      },
    } = props;
    const name = item[nameKey] || '';
    const no = item[noKey] || '';
    const description = item[descriptionKey] || '';

    return (
      <TouchableOpacity
        onPress={() => {
          onSelect(item);
          navigation.goBack();
        }}>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'flex-start',
            paddingVertical: Theme.margin.m4,
            borderColor: '#aaa',
            borderBottomWidth: 1,
            padding: 10,
          }}>
          <Text style={styles.textNo}>{no}</Text>
          <Text style={styles.textValue}>{name}</Text>
          {!!description && <Text style={styles.textNo}>{description}</Text>}
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}

      <UniInput
        initValue={filterValue}
        onChangeText={setFilterValue}
        containerStyle={styles.editText}
        placeholder="Type to search"
      />

      <FlatList
        style={{flex: 1}}
        data={dataFromState}
        renderItem={renderRow}
        keyExtractor={(item) => `${item[idKey]}`}
      />
    </View>
  );
};

export const showSelectFromList = (navigation, config) => {
  navigation.push(RouteNames.SelectFromList, config);
};

export const DropDownWithListScreen = (props) => {
  // const {label, options, idKey, nameKey, noKey, descriptionKey} = props;
  const {navigation, ...restProps} = props;

  const handleShowSelectScreen = () => {
    showSelectFromList(navigation, {
      ...restProps,
    });
  };

  return <DropDown {...restProps} onShowDropdown={handleShowSelectScreen} />;
};

export default SelectFromList;
