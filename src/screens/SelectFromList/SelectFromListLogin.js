import React, {useState, useEffect} from 'react';
import {View, Text, StatusBar, FlatList, TouchableOpacity, Alert} from 'react-native';
import {pathOr} from 'ramda';
import {connect} from 'react-redux';
import styles from './styles';

import Theme from '../../Theme';
import {
  UniStatusBarBackground,
  UniImageButton,
  UniInputBtn
} from '../../components';
import {EMPTY} from '../../global/constants';
import RouteNames from '../../RouteNames';
import DropDown from '../../components/DropDown/DropDown';
import {userAddDomain, userDeleteDomain} from '../../action/DomainAction';
import { isEmpty } from 'lodash';


const SelectFromListLogin = (props) => {
  const {
    domains,
    userAddDomain,
    userDeleteDomain
  } = props;
  const {navigation, route} = props;
  const [dataFromState, setDataFromState] = useState([]);
  const [filterValue, setFilterValue] = useState('');

  const data = pathOr(EMPTY.ARRAY, ['params', 'options'], route);
  const idKey = pathOr(EMPTY.STRING, ['params', 'idKey'], route);
  const nameKey = pathOr(EMPTY.STRING, ['params', 'nameKey'], route);
  const noKey = pathOr(EMPTY.STRING, ['params', 'noKey'], route);
  const descriptionKey = pathOr(
    EMPTY.STRING,
    ['params', 'descriptionKey'],
    route,
  );
  const label = pathOr(EMPTY.STRING, ['params', 'label'], route);

  useEffect(() => {
    const filteredData = data.filter((item) => {
      const id = item[idKey] || '';

      if (!id) {
        return false;
      }

      const name = item[nameKey] || '';
      const no = item[noKey] || '';

      if (!filterValue) {
        return true;
      }

      return (
        (no && no.toLowerCase().includes(filterValue.toLowerCase())) ||
        (name && name.toLowerCase().includes(filterValue.toLowerCase()))
      );
    });
    setDataFromState(filteredData);
  }, [data, nameKey, noKey]);

  const onAddMoreDomain = async () => {
    let res,
        item
    if (!isEmpty(filterValue) && filterValue != '') {
      item =  data.find((value) => {
        if (value.key === filterValue)
            return value
      });

      if (!item) {
        alert('Domain not exist');
        return;
      }

      res = await userAddDomain(item);

      if (res) {
        setFilterValue('')
        alert('Add domain success');
      }
    }
  };


  const onDeleteDomain = (val) => {
    let dataDelete = val?.payload,
        filteredDeleteData,
        res;
    
    if (dataDelete) {
      filteredDeleteData = data.filter((item) => {
        const {
          key
        } = item;

        if (
          !isEmpty(dataDelete) &&
          key === (dataDelete?.key)         
          ) {
          userDeleteDomain(item);
          return false;
        }

        return true;
      });
    }
  };

  const renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={navigation.goBack}
          />
          <View>
            <View style={styles.row}>
              <Text style={styles.textHeader}>Select {label}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  const renderRow = ({item}) => {
    const {
      route: {
        params: {onSelect},
      },
    } = props;
    const name = item.payload[nameKey] || '';
    const no = item.payload[noKey] || '';
    const description = item.payload[descriptionKey] || '';

    const handlerLongClick = () =>
      Alert.alert(
        "",
        "Do you want delete this domain?",
        [
          { text: "OK", onPress: () => {
             onDeleteDomain(item);
          }},
          { text: "Cancel", onPress: () => console.log("Cancel Pressed") },
        ]
  );

    return (
      <TouchableOpacity
          onLongPress={handlerLongClick}
          onPress={() => {
            onSelect(item.payload);
            navigation.goBack();
          }}
          //Here is the trick
          activeOpacity={0.6}
          style={styles.buttonStyle}>
           <View
          style={{
            flexDirection: 'column',
            alignItems: 'flex-start',
            paddingVertical: Theme.margin.m4,
            borderColor: '#aaa',
            borderBottomWidth: 1,
            padding: 10,
          }}>
          <Text style={styles.textNo}>{no}</Text>
          <Text style={styles.textValue}>{name}</Text>
          {!!description && <Text style={styles.textNo}>{description}</Text>}
        </View>
        </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}

      <UniInputBtn
        initValue={filterValue}
        onChangeText={setFilterValue}
        onPress={onAddMoreDomain}
        containerStyle={styles.editText}
        placeholder="Type to search"
      />

      <FlatList
        style={{flex: 1}}
        data={domains.domains}
        renderItem={renderRow}
        keyExtractor={(item) => `${item.payload[idKey]}`}
      />
    </View>
  );
};

export const showSelectFromList = (navigation, config) => {
  navigation.push(RouteNames.SelectFromListLogin, config);
};

export const DropDownWithListScreen = (props) => {
  // const {label, options, idKey, nameKey, noKey, descriptionKey} = props;
  const {navigation, ...restProps} = props;

  const handleShowSelectScreen = () => {
    showSelectFromList(navigation, {
      ...restProps,
    });
  };

  return <DropDown {...restProps} onShowDropdown={handleShowSelectScreen} />;
};

const mapStateToProps = ({auth: {user, isLoggedIn}, domains}) => {
  return {
    user,
    isLoggedIn,
    domains,
  };
};

const mapDispatchToProps = {
  userAddDomain,
  userDeleteDomain,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectFromListLogin);