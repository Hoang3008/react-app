import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';
import {actionType as createPhotoAreaActionType } from '../../CreatePhotoArea/actions/createPhotoArea.action'
const actionType = 'GET_PHOTO_AREA_LIST';

// https://apidev.rocez.com/drawing-area?projectId=
// type = 2 lay tat ca folder return type = 0 || 1 || 2
// types = 0 trong do chi chua drawing
// types = 1 trong do chi chua folder
// type = 2 trong do k co j ca
const createTaskAPI = ({projectId, parentId = -1}, token) => {

  const baseUrl = `/album?projectId=${projectId}`;

  if (parentId != -1) {
    baseUrl = `/drawing-area?projectId=${projectId}&parentId=${parentId}`;
  }

  const url = withKey(baseUrl);
  return API.get(url, {headers: {Authorization: `Bearer ${token}`}});
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {

  return {
    ...state,
    photoAreaList: payload.data.children,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
  [createPhotoAreaActionType]: {
    SUCCESS: (state, {payload, meta}) => {
      const data = {};

      // if (meta?.parentId) {
      //   return {...state ,photoSubFolder: [...state.photoSubFolder, payload.data] };
      // }else {
      //   return {...state ,photoAreaList: [...state.photoAreaList, payload.data]  };
      // }
      return {...state ,photoAreaList: [...state.photoAreaList, payload.data]  };
  }},
};

export default {action, reducer};
