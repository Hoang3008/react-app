import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {SwipeListView} from 'react-native-swipe-list-view';
import {
  Alert,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';

import {actions} from './PhotoArea.reducer';
import styles from './PhotoArea.styles';
import {SearchBar} from 'react-native-elements';
import Theme from '../../Theme';
import UniImageButton from '../../components/UniImageButton';
import {pathOr, propOr} from 'ramda';
import RouteNames from '../../RouteNames';
import { isEmpty } from 'ramda';

const DrawingArea = (props) => {
  const {
    token,
    navigation,
    photoAreaList,
    getPhotoAreaList,
    project,
    showSearchBox
  } = props;


  const [searchText, setSearchText] = useState('');
  const [filterPhotoAreaList, setFilterPhotoAreaList] = useState([]);

  const PMProjectID = propOr(0, 'PMProjectID', project);

  const setFilterData = () => {
    return photoAreaList.filter((item => {
        if (item.name.includes(searchText)) {
          return true;
        }
    }));
  };

  useEffect(() => {
    getPhotoAreaList({projectId:PMProjectID},token);
  }, [getPhotoAreaList, PMProjectID]);

  useEffect(() => {
    const listData = setFilterData();
    setFilterPhotoAreaList(listData);
  }, [photoAreaList, searchText]);

  const handleItemClick = (item, index) => {
    if (item.types === 0) {
      navigation.navigate(RouteNames.PhotoList, {
        alb: item.id,
        time: Date.now(),
        folderId: item.id,
      });
    } else {
      navigation.navigate(RouteNames.PhotoSubFolder, {
        folderId: item.id,
        name: item.name,
      });
    }
  };

  const onPressNewTask = () => {
    navigation.push(RouteNames.CreatePhotoArea, {
      parentId: -1
    });
  };

  const handleDelete = (item) => () => {
    Alert.alert(
      'Delete',
      'Do you want to delete this folder?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => {
            deleteDrawingArea(item.ID, token);
          },
        },
      ],
      {cancelable: false},
    );
  };

  const handleEdit = (item) => () => {
    navigation.push(RouteNames.CreateDrawingArea, {area: item});
  };

  const renderItem = ({item}) => {
    const {name} = item;
    return (
      <TouchableHighlight onPress={() => handleItemClick(item)}>
        <View
          style={{
            padding: 15,
            display: 'flex',
            flexDirection: 'row',
            shadowColor: '#555',
            backgroundColor: 'white',
            alignItems: 'center',
          }}>
          <Image
            style={styles.icon}
            source={require('../../assets/img/ico_folder.png')}
          />
          <Text
            style={{
              flex: 1,
              marginLeft: 15,
              fontSize: 16,
              color: '#3E3F42',
            }}>
            {name}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  const renderHiddenItem = ({item}) => {
    return (
      <View style={styles.rowBack}>
        <TouchableOpacity
          onPress={handleDelete(item)}
          style={{
            width: 100,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={styles.delete}>Delete</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleEdit(item)}
          style={{
            width: 100,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'green',
          }}>
          <Text style={styles.delete}>Edit</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
      { showSearchBox && (
          <SearchBar
          returnKeyLabel="search"
          placeholder="Type Here..."
          value={searchText}
          onChangeText={setSearchText}
          inputStyle={{
            color: Theme.colors.textNormal,
          }}
          containerStyle={{
            borderTopColor: 'transparent',
            borderBottomColor: 'transparent',
            backgroundColor: '#ffffff',
          }}
          inputContainerStyle={{
            backgroundColor: '#ddd',
          }}
        />
        )
      }
        <SwipeListView
          style={{width: '100%'}}
          data={filterPhotoAreaList}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          keyExtractor={(item) => item.ID}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          leftOpenValue={0}
          rightOpenValue={-200}
          closeOnRowBeginSwipe
          disableRightSwipe
          scrollEnabled={true}
          showsVerticalScrollIndicator={true}
          recalculateHiddenLayout={true}
        />
      </View>
      <UniImageButton
        containerStyle={styles.floatButton}
        source={require('../../assets/img/ic_add.png')}
        onPress={onPressNewTask}
      />
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, commonData, photoAreaList}) => {
  const PMProjectID = propOr(0, 'PMProjectID', project);
  const areaList = pathOr([],['photoAreaList'], photoAreaList);

  return {
    user,
    project,
    token: user.token,
    categories: pathOr([], ['currentProject', 'categories'], commonData),
    photoAreaList: areaList
  };
};

const mapDispatchToProps = {
    getPhotoAreaList: actions.getPhotoAreaList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawingArea);
