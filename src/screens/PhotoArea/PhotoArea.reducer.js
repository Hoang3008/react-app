import typeToReducer from 'type-to-reducer';
import getPhotoAreaList from './actions/getPhotoAreaList.action';

const initialState = {
  photoAreaList: [],
};

export const actions = {
  getPhotoAreaList: getPhotoAreaList.action,
};

export default typeToReducer(
  {
    ...getPhotoAreaList.reducer,
  },
  initialState,
);
