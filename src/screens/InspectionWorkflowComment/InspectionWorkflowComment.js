import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import moment from 'moment';
import {pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';

import styles from './InspectionWorkflowComment.styles';
import {actions} from './InspectionWorkflowComment.reducer';

import Theme from '../../Theme';
import {getUserAvatarUrlByUsername} from '../../api/urls';
import {UniImageButton, UniStatusBarBackground} from '../../components';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import {OFFLINE_ACTION_TYPES} from '../../global/constants';
import {actions as actionsOffline} from '../CommonProjectData/CommonProjectData.reducer';

const InspectionWorkflowComment = (props) => {
  const {
    workflowStep,
    navigation,
    commentList = [],
    user,
    addActionIntoQueue,
    inspectionId,
  } = props;

  const [comment, setComment] = useState('');

  const {PMProjectInsStpInspID} = workflowStep;

  const handleSendComment = () => {
    const item = {
      inspectionId,
      PMProjectInsStpInspCommentDate: new Date().toISOString(),
      PMProjectInsStpInspCommentDesc: comment,
      AACreatedDate: new Date().toISOString(),
      AACreatedUser: user.ADUserName,
      FK_PMProjectInsStpInspID: PMProjectInsStpInspID,
    };

    addActionIntoQueue({
      type: OFFLINE_ACTION_TYPES.POST_ITP_WORKFLOW_COMMENT,
      data: item,
    });

    setComment('');
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={{...styles.row, flex: 1}}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <View>
          <View style={styles.row}>
            <Text style={styles.textHeader} numberOfLines={1}>
              Comment #{PMProjectInsStpInspID}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );

  const renderCommentItem = ({item}) => {
    // PMProjectInsStpInspCommentID: 17
    // PMProjectInsStpInspCommentDate: "2020-04-07T00:00:00.000Z"
    // PMProjectInsStpInspCommentDesc: "Kiểm tra khu vực niêm phong ổn"
    // AACreatedDate: "2020-04-07T11:18:17.387Z"
    // AACreatedUser: "vuong.ho@gmail.com"
    // FK_PMProjectInsStpInspID: 368
    const {
      AACreatedUser,
      PMProjectInsStpInspCommentDesc,
      PMProjectInsStpInspCommentDate,
    } = item;

    const showDate = moment(PMProjectInsStpInspCommentDate).format(
      'YYYY/MM/DD hh:mm A',
    );

    const avatar = getUserAvatarUrlByUsername(AACreatedUser);
    const name = AACreatedUser;

    if (
      user.ADUserName === AACreatedUser ||
      `${user.ADUserID}` === AACreatedUser
    ) {
      return (
        <View style={{padding: 15}}>
          <View
            style={{
              ...styles.row,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              display: 'flex',
            }}>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
                textAlign: 'right',
              }}>
              {showDate}
            </Text>
          </View>
          <View
            style={{
              paddingLeft:
                Theme.specifications.commentAvatarSize + Theme.margin.m12,
              alignItems: 'flex-end',
            }}>
            {PMProjectInsStpInspCommentDesc &&
            PMProjectInsStpInspCommentDesc.length > 0 ? (
              <Text
                style={{
                  marginTop: Theme.margin.m6,
                  paddingVertical: Theme.margin.m8,
                  paddingHorizontal: Theme.margin.m10,
                  backgroundColor: '#3483F6',
                  borderRadius: 10,
                  borderWidth: 0,
                  color: '#fff',
                  fontStyle: 'normal',
                  fontSize: 14,
                }}>
                {PMProjectInsStpInspCommentDesc}
              </Text>
            ) : null}
          </View>
        </View>
      );
    }

    return (
      <View style={{padding: 15}}>
        <View style={styles.row}>
          <ImageBackground
            style={{
              resizeMode: 'center',
              width: Theme.specifications.commentAvatarSize,
              height: Theme.specifications.commentAvatarSize,
              borderRadius: Theme.specifications.commentAvatarSize / 2,
            }}
            source={require('../../assets/img/default_avatar.png')}>
            <Image
              style={{
                resizeMode: 'center',
                width: Theme.specifications.commentAvatarSize,
                height: Theme.specifications.commentAvatarSize,
                borderRadius: Theme.specifications.commentAvatarSize / 2,
              }}
              source={{uri: avatar}}
            />
          </ImageBackground>
          <View style={{marginLeft: Theme.margin.m12}}>
            <Text
              style={{
                paddingVertical: Theme.margin.m2,
                fontSize: Theme.fontSize.normal,
                color: Theme.colors.textNormal,
                fontWeight: 'bold',
              }}>
              {name}
            </Text>
            <Text
              style={{
                fontSize: Theme.fontSize.small_12,
                color: Theme.colors.textSecond,
              }}>
              {showDate}s
            </Text>
          </View>
        </View>
        <View
          style={{
            paddingLeft:
              Theme.specifications.commentAvatarSize + Theme.margin.m12,
            alignItems: 'flex-start',
          }}>
          {PMProjectInsStpInspCommentDesc &&
          PMProjectInsStpInspCommentDesc.length > 0 ? (
            <Text
              style={{
                marginTop: Theme.margin.m6,
                paddingVertical: Theme.margin.m8,
                paddingHorizontal: Theme.margin.m10,
                backgroundColor: Theme.colors.backgroundComment,
                borderRadius: 10,
                borderWidth: 0,
                fontStyle: 'normal',
                fontSize: 14,
              }}>
              {PMProjectInsStpInspCommentDesc}
            </Text>
          ) : null}
        </View>
      </View>
    );
  };

  return (
    <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="height" enabled>
      <View
        style={{
          flex: 1,
          backgroundColor: Theme.colors.backgroundColor,
          display: 'flex',
          flexDirection: 'column',
        }}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {renderNavigation()}

        <FlatList
          style={{flex: 1}}
          scrollEnabled={true}
          data={commentList}
          renderItem={renderCommentItem}
          keyExtractor={(item) => `${item.PMProjectInsStpInspCommentID}`}
          showsVerticalScrollIndicator={false}
        />

        <View style={styles.inputCommentContainer}>
          <TextInput
            placeholder="Type your comment here..."
            value={comment}
            style={styles.inputComment}
            multiline
            onChangeText={setComment}
          />
          <TouchableOpacity
            onPress={handleSendComment}
            style={styles.buttonSendComment}>
            <Text style={{color: 'white', fontSize: Theme.fontSize.normal}}>
              Send
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAvoidingViewWrapper>
  );
};

const mapStateToProps = (
  {
    auth: {user, project},
    commonData,
    inspectionList,
    inspectionDetail,
    inspectionWorkflowComment,
  },
  props,
) => {
  const id = pathOr('', ['route', 'params', 'id'], props);
  const inspectionId = pathOr('', ['route', 'params', 'inspectionId'], props);

  const inspection = propOr({}, inspectionId, inspectionDetail);
  const acceptanceSteps = propOr([], 'acceptanceSteps', inspection);

  return {
    user,
    project,
    inspectionId,
    commentList:
      propOr([], 'comment', inspection).filter(
        ({FK_PMProjectInsStpInspID}) => id === FK_PMProjectInsStpInspID,
      ) || [],
    workflowStep:
      acceptanceSteps.find(
        ({PMProjectInsStpInspID}) => id === PMProjectInsStpInspID,
      ) || {},
  };
};

const mapDispatchToProps = {
  sendComment: actions.sendComment,
  addActionIntoQueue: actionsOffline.addActionIntoQueue,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionWorkflowComment);
