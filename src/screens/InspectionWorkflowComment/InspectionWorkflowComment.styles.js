import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputCommentContainer: {
    display: 'flex',
    flexDirection: 'row',
    backdropColor: '#ff0000',
    width: SCREEN_WIDTH,
    minHeight: Theme.specifications.inputCommentContainerHeight,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingVertical: Theme.margin.m12,
    paddingHorizontal: Theme.margin.m15,
    backgroundColor: 'white',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.1,
    shadowRadius: Theme.margin.m6,
    borderColor: Theme.colors.border,
    borderWidth: 0.5,
  },
  inputComment: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: Theme.margin.m36,
    fontSize: Theme.fontSize.normal,
    color: Theme.colors.textNormal,
    paddingHorizontal: Theme.margin.m12,
    paddingTop: 8,
    borderWidth: Theme.margin.m1,
    borderRadius: Theme.specifications.editTextNormalBorder,
    borderColor: Theme.colors.border,
  },
  buttonSendComment: {
    alignSelf: 'flex-start',
    maxHeight: 55,
    height: '100%',
    marginLeft: 15,
    backgroundColor: Theme.colors.primary,
    justifyContent: 'center',
    paddingHorizontal: Theme.margin.m14,
    borderRadius: Theme.margin.m4,
  },
};
