import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_WORKFLOW_COMMENT_LIST';

const createTaskAPI = (id) => {
  const url = withKey(
    `/PMProjectInsStpInspComments/GetAllDataByPMProjectInsStpInspID?FK_PMProjectInsStpInspID=${id}`,
  );

  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};
// PMProjectInsStpInspComments/GetAllDataByPMProjectInsStpInspID?FK_PMProjectInsStpInspID=368

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    commentList: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
