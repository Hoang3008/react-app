import {createAction} from 'redux-actions';
import {identity, omit, pathOr, propOr} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';
import {createLogAPI} from '../../InspectionDetail/actions/createLog.action';

export const actionType = 'SEND_WORKFLOW_COMMENT';

const createTaskAPI = (item, projectId) => {
  const url = withKey('/itp/v2/comment');

  const data = {
    "projectId":projectId,
    "description": item?.PMProjectInsStpInspCommentDesc,
    "insStpInspId": propOr(undefined, 'FK_PMProjectInsStpInspID', item)
  };

  return AsyncStorage.getItem('token').then((token) => {
    return API.post(url, data, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((res) => {
      // AACreatedDate: "2020-09-28T04:10:23.297Z"
      // AACreatedUser: "itp1@unicons.vn"
      // FK_PMProjectInsStpInspID: 4618
      // PMProjectInsStpInspCommentDate: "2020-09-28T04:10:23.297Z"
      // PMProjectInsStpInspCommentDesc: "Comment 3"
      // PMProjectInsStpInspCommentID: 3077
      const payloadId = pathOr('', ['data', 'FK_PMProjectInsStpInspID'], res);
      const value = pathOr('', ['data', 'PMProjectInsStpInspCommentDesc'], res);
      createLogAPI(token, payloadId, 5, value);
      return res;
    });
  });
};
// api.rocez.com/PMProjectInsStpInspComments/CreateObject

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    commentList: [...state.commentList, payload.data],
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
