import typeToReducer from 'type-to-reducer';

import getWorkflowCommentList from './actions/getWorkflowCommentList.action';
import sendComment from './actions/sendComment.action';

const initialState = {
  commentList: [],
};

export const actions = {
  getWorkflowCommentList: getWorkflowCommentList.action,
  sendComment: sendComment.action,
};

export default typeToReducer(
  {
    ...getWorkflowCommentList.reducer,
    ...sendComment.reducer,
  },
  initialState,
);
