import {createAction} from 'redux-actions';
import {identity} from 'ramda';

import {actionType as createDrawingAreaActionType} from '../../CreateDrawingArea/actions/createDrawingArea.action';
import {actionType as updateDrawingAreaActionType} from '../../CreateDrawingArea/actions/updateDrawingArea.action';

import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';

const actionType = 'GET_DRAWING_AREA_LIST';

// https://apidev.rocez.com/drawing-area?projectId=
// type = 2 lay tat ca folder return type = 0 || 1 || 2
// types = 0 trong do chi chua drawing
// types = 1 trong do chi chua folder
// type = 2 trong do k co j ca
const createTaskAPI = ({projectId, parentId = -1}, token) => {

  const baseUrl = `/drawing-area?projectId=${projectId}&drawingAreaType=2`;

  if (parentId != -1) {
    baseUrl = `/drawing-area?projectId=${projectId}&parentId=${parentId}`;
  }

  const url = withKey(baseUrl);
  return API.get(url, {headers: {Authorization: `Bearer ${token}`}});
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {

  return {
    ...state,
    drawingAreaList: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
  [createDrawingAreaActionType]: {
    SUCCESS: (state, {payload}) => ({
      ...state,
      drawingAreaList: [...state.drawingAreaList, payload.data],
    }),
  },
  [updateDrawingAreaActionType]: {
    SUCCESS: (state, {payload}) => ({
      ...state,
      drawingAreaList: state.drawingAreaList.map((item) => {
        if (item.ID === payload.data.ID) {
          return payload.data;
        }

        return item;
      }),
    }),
  },
};

export default {action, reducer};
