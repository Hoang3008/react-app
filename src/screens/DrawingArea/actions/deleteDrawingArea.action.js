import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';

export const actionType = 'DELETE_DRAWING_AREA';

const createUserURL = (id) => withKey(`/drawing-area/${id}`);
const deleteUserAPI = (id, token) =>
  API.delete(createUserURL(id), {
    headers: {Authorization: `Bearer ${token}`},
  });

const action = createAction(actionType, deleteUserAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
    drawingAreaList: state.drawingAreaList.filter((item) => item.ID !== meta),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
