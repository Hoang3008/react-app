import typeToReducer from 'type-to-reducer';
import getDrawingAreaList from './actions/getDrawingAreaList.action';
import deleteDrawingArea from './actions/deleteDrawingArea.action';

const initialState = {
  drawingAreaList: [],
};

export const actions = {
  getDrawingAreaList: getDrawingAreaList.action,
  deleteDrawingArea: deleteDrawingArea.action,
};

export default typeToReducer(
  {
    ...getDrawingAreaList.reducer,
    ...deleteDrawingArea.reducer,
  },
  initialState,
);
