import React, {PureComponent} from 'react';
import {Image, StyleSheet, TouchableOpacity, View, Platform, PermissionsAndroid} from 'react-native';
import {RNCamera} from 'react-native-camera';
import CameraIcon from '../../assets/img/icon_camera_shutter.png';
import CloseIcon from '../../assets/img/icon_close_without_background.png';
import flashOnIcon from '../../assets/img/icon-flash-on.png';
import flashOffIcon from '../../assets/img/icon-flash-off.png';
import torchOffIcon from '../../assets/img/icon-torch-off.png';
import torchOnIcon from '../../assets/img/icon-torch-on.png';
import RouteNames from '../../RouteNames';
import {pathOr} from 'ramda';
import Torch from 'react-native-torch';


class CaptureImage extends PureComponent {

  takePicture = async () => {
    if (this.camera) {
      const options = {
        quality: 1,
        base64: false,
        pauseAfterCapture: true,
        fixOrientation: true,
      };
      const data = await this.camera.takePictureAsync(options);

      const shouldGoToDrawing = pathOr(
        true,
        ['route', 'params', 'shouldGoToDrawing'],
        this.props,
      );

      if (shouldGoToDrawing) {
        this.props.navigation.replace(RouteNames.SketchDrawing, {
          imagePath: data.uri,
          onSave: this.props.route.params.onSave,
        });
      } else {
        this.props.navigation.goBack();
        this.props.route.params.onSave(data.uri);
      }
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      offFlash: true,
      modeFlash: RNCamera.Constants.FlashMode.off,
      offTorch: true,
      torchON: false
    };
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {offFlash, modeFlash} = this.state;

    if (offFlash) {
      this.setState({
        modeFlash : RNCamera.Constants.FlashMode.off
      });
    }else {
      this.setState({
        modeFlash : RNCamera.Constants.FlashMode.on
      });
    }
    
  }

  toggleTorch(){
    if(this.camera){
      this.setState({
        torchON: !this.state.torchON,
        modeFlash : RNCamera.Constants.FlashMode.off,
      })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={(ref) => {
            this.camera = ref;
          }}
          autoFocus
          captureAudio={false}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={this.state.torchON ? RNCamera.Constants.FlashMode.torch: this.state.modeFlash}
          useNativeZoom={true}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
        <TouchableOpacity
            onPress={() => {
              this.setState({
                offFlash: !this.state.offFlash,
                offTorch: this.state.offTorch ? this.state.offTorch: this.state.offFlash
              })
            }}
            style={styles.flash}>
            <Image
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                width: 40,
                height: 40,
                resizeMode: 'contain',
              }}
              source={this.state.offFlash ? flashOffIcon : flashOnIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.toggleTorch();
              this.setState({
                modeFlash : RNCamera.Constants.FlashMode.off,
                offTorch: !this.state.offTorch,
                offFlash: this.state.offFlash ? this.state.offFlash: this.state.offTorch
              });
            }}
            style={styles.torch}>
            <Image
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                width: 40,
                height: 40,
                resizeMode: 'contain',
              }}
              source={this.state.offTorch ? torchOffIcon : torchOnIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={styles.capture}>
            <Image
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                width: 70,
                height: 70,
                resizeMode: 'contain',
              }}
              source={CameraIcon}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}
            style={styles.close}>
            <Image
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                width: 40,
                height: 40,
                resizeMode: 'contain',
              }}
              source={CloseIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    alignSelf: 'center',
    margin: 20,
  },
  close: {
    position: 'absolute',
    right: 20,
    bottom: 34,
  },
  flash: {
    position: 'absolute',
    left: 20,
    bottom: 34,
  },
  torch: {
    position: 'absolute',
    left: 80,
    bottom: 34,
  },
});

export default CaptureImage;
