import React from 'react';
import {
  View,
  StatusBar,
  Image,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {connect} from 'react-redux';
import IconFoundation from 'react-native-vector-icons/Foundation';
import IconEvilIcons from 'react-native-vector-icons/EvilIcons';
import Share from 'react-native-share';

import {UniImageButton, UniStatusBarBackground} from '../components';
import Theme from '../Theme';
import {SCREEN_WIDTH, SCREEN_HEIGHT} from '../utils/DimensionUtils';
import {actions} from './CommonProjectData/CommonProjectData.reducer';
import {pathOr, propOr} from 'ramda';
import {EMPTY} from '../global/constants';
import { SliderBox } from "react-native-image-slider-box";
import {actions as PhotoReducer} from './PhotoList/PhotoList.reducer';
import RouteNames from '../RouteNames';


class ImageFullScreen extends React.Component {
  constructor(props) {
    const dataImages = pathOr([], ['route', 'params', 'dataImages'], props);
    const canChange = props;
    super(props);
    this.state = {
      itemImage: dataImages && dataImages[0],
      canDelete: dataImages && dataImages[0]?.edit
    }
  }

  renderNavigation = () => {
    const {user} = this.props;
    const item = pathOr(undefined, ['route', 'params', 'item'], this.props);
    const title = pathOr('', ['route', 'params', 'title'], this.props);
    const onEdit = pathOr(undefined, ['route', 'params', 'onEdit'], this.props);
    const onShowInfo = pathOr(
      undefined,
      ['route', 'params', 'onShowInfo'],
      this.props,
    );
    const { canDelete } = this.state;

    const {ADUserID, ADUserName, ADUserType} = user;
    const AACreatedUser = propOr('', 'AACreatedUser', item);

    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../assets/img/ic_back.png')}
            onPress={() =>
              this.props.navigation && this.props.navigation.goBack()
            }
          />
          <View>
            <Text style={styles.textHeader}>{title || 'Defect Photo'}</Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {canDelete && (
            <TouchableOpacity
              onPress={this.deletePhoto}
              style={styles.navIconContainer}>
              <Image
                style={styles.navIcon}
                source={require('../assets/img/icon_delete.png')}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  deletePhoto = () => {
    const {deletePhoto, navigation} = this.props;
    const item = pathOr(undefined, ['route', 'params', 'item'], this.props);
    const onDelete = pathOr(
      undefined,
      ['route', 'params', 'onDelete'],
      this.props,
    );

    Alert.alert(
      'Delete',
      'Do you want to delete this photo?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => {
            if (onDelete) {
              onDelete().then(() => {
                navigation.goBack();
              });
              return;
            }

            deletePhoto(item).then(() => {
              navigation.goBack();
            });
          },
        },
      ],
      {cancelable: false},
    );
  };

  handleShare = () => {
    let defect = pathOr(undefined, ['route', 'params', 'url'], this.props);
    const options = {
      url: defect,
      content: defect,
      type: 'image/png',
    };
    Share.open(options)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        err && console.log(err);
      });
  };

  renderInfo = () => {
    const info = pathOr(undefined, ['route', 'params', 'info'], this.props);
    const onShowInfo = pathOr(
      undefined,
      ['route', 'params', 'onShowInfo'],
      this.props,
    );

    const onShowComment = pathOr(
      () => {},
      ['route', 'params', 'onShowComment'],
      this.props,
    );

    if (!onShowInfo || !info) {
      return null;
    }

    return (
      <View style={styles.infoContainer}>
        <TouchableOpacity
          onPress={this.handleShare}
          style={styles.navIconContainer}>
          <IconEvilIcons size={26} color="#f66" name="share-apple" />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleClick} style={styles.navIconContainer}>
          <IconFoundation size={26} color="#f66" name="info" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={onShowComment}
          style={styles.navIconContainer}>
          <IconEvilIcons size={26} color="#f66" name="comment" />
        </TouchableOpacity>
      </View>
    );
  };

  handleClick = () => {
    const { navigation } = this.props;
    const { itemImage } = this.state;
    navigation.push(RouteNames.EditPhoto, {item:itemImage });
  };

  render() {
    let defect = pathOr(undefined, ['route', 'params', 'url'], this.props);
    let reduced = pathOr([], ['route', 'params', 'reduced'], this.props);
    const dataImages = pathOr([], ['route', 'params', 'dataImages'], this.props);
    const images = [
      defect,
      ...reduced
    ]
    let height =
      SCREEN_HEIGHT -
      Theme.specifications.navigationBarHeight -
      getStatusBarHeight();

    return (
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {this.renderNavigation()}
        <View style={{flex: 1, position: 'relative'}}>
          <ImageZoom
            cropWidth={SCREEN_WIDTH}
            cropHeight={height}
            imageWidth={SCREEN_WIDTH}
            imageHeight={height}>
            <SliderBox
             resizeMode={'contain'}
              images={images}
              dataImages={dataImages}
              sliderBoxHeight = {height}
              currentImageEmitter={item =>
                this.setState({
                  itemImage: item,
                  canDelete: item?.edit
                })
             }
              >
            </SliderBox>
          </ImageZoom> 
        </View>
        {this.renderInfo()}
      </View>
    );
  }
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIconContainer: {
    margin: 0,
    paddingLeft: 8,
    paddingRight: 8,
  },
  navIcon: {
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textSectionHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNormal,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  priorityContainer: {
    width: Theme.specifications.icBig,
    height: Theme.specifications.icBig,
    marginHorizontal: Theme.margin.m5,
    borderRadius: Theme.specifications.icBig / 2,
    borderWidth: 1,
    borderColor: Theme.colors.border,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNormalSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
  },
  textSmallSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.small_12,
  },
  button: {
    marginLeft: Theme.margin.m15,
    marginVertical: Theme.margin.m16,
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },
  infoContainer: {
    backgroundColor: '#efefef',
    width: '100%',
    height: 50,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
};

const mapStateToProps = ({auth: {user}, commonData}) => {

  const permissions = commonData.Permission.find(x => x.module === 'photo');
  const canChange =  permissions.permissions?.length > 0 && permissions.permissions[0] === 'CHANGE_ALL'; 


  return {
    user,
    canChange
  };
};

const mapDispatchToProps = {
  deletePhoto: actions.deletePhoto,
  getPhotoList: PhotoReducer.getPhotoList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ImageFullScreen);
