import {createAction} from 'redux-actions';
import {identity, omit} from 'ramda';
import AsyncStorage from '@react-native-community/async-storage';

import API from '../../../api/API.service';
import {withKey} from '../../../api/urls';

export const actionType = 'GET_CONSTRUCTION_METHOD_LIST';

// https://apid.rocez.com/itp/construction-method/4899
export const getListAPI = (itpId) => {
  const url = withKey(`/itp/construction-method/${itpId}`);
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getListAPI, identity);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
    list: payload.data.imgConsMethod || [],
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
