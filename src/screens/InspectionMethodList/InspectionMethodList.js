import React, {useRef, useState} from 'react';
import {connect} from 'react-redux';
import {pathOr} from 'ramda';
import {StatusBar, Text, TouchableOpacity, View} from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import {actions} from './InspectionMethodList.reducer';

import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import UniImageButton from '../../components/UniImageButton';
import ConstructionMethodContent from '../InspectionDetail/ConstructionMethodContent';
import RouteNames from '../../RouteNames';
import {
  getConstructionMethodFileUrl,
  getConstructionMethodPDFFileUrl,
} from '../../api/urls';

const InspectionMethodList = (props) => {
  const {navigation} = props;
  const [activeIndex, setActiveIndex] = useState(0);
  const inspectionId = pathOr('', ['route', 'params', 'inspectionId'], props);
  const workflow = pathOr([], ['route', 'params', 'workflow'], props);

  const docStep = pathOr('', [activeIndex, 'docStep'], workflow);
  const picStep = pathOr('', [activeIndex, 'picStep'], workflow);

  const pagerRef = useRef();

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={[styles.row, {flex: 1}]}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <Text style={styles.textHeader} numberOfLines={1}>
          {pathOr('', [activeIndex, 'PMProjectInsStpDesc'], workflow)}
        </Text>
      </View>
      {!!picStep && (
        <TouchableOpacity
          onPress={() => {
            navigation.push(RouteNames.ImageFullScreen, {
              title: pathOr('', [activeIndex, 'PMProjectInsStpDesc'], workflow),
              info: {
                information: '',
                uploadedBy: '',
                category: '',
                location: '',
              },
              url: getConstructionMethodPDFFileUrl(picStep),
            });
          }}
          style={{
            paddingVertical: Theme.margin.m6,
            paddingHorizontal: Theme.margin.m6,
          }}>
          <IconFontAwesome size={20} color="white" name="file-image-o" />
        </TouchableOpacity>
      )}
      {!!docStep && (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate(RouteNames.PDFFullScreen, {
              url: getConstructionMethodPDFFileUrl(docStep),
            });
          }}
          style={{
            paddingVertical: Theme.margin.m6,
            paddingHorizontal: Theme.margin.m6,
          }}>
          <IconAntDesign size={20} color="white" name="pdffile1" />
        </TouchableOpacity>
      )}
    </View>
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundColor,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}

      <View style={{flex: 1}}>
        <ViewPager
          ref={pagerRef}
          initialPage={0}
          style={{flex: 1}}
          scrollEnabled={false}
          onPageSelected={(event) => {
            const {
              nativeEvent: {position},
            } = event;
            setActiveIndex(position);
          }}>
          {workflow.map(({PMProjectInsStpID}) => (
            <View key={PMProjectInsStpID} style={{padding: 15}}>
              <ConstructionMethodContent
                navigation={navigation}
                inspectionId={inspectionId}
                workflowId={PMProjectInsStpID}
              />
            </View>
          ))}
        </ViewPager>

        <Text
          style={{
            position: 'absolute',
            bottom: 6,
            left: 0,
            right: 0,
            textAlign: 'center',
          }}>
          {activeIndex + 1}/{workflow.length}
        </Text>
      </View>

      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          disabled={activeIndex === 0}
          style={{
            height: 46,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() =>
            pagerRef.current.setPageWithoutAnimation(activeIndex - 1)
          }>
          <Text
            style={[
              styles.headerButton,
              {opacity: activeIndex === 0 ? 0.6 : 1},
            ]}>
            Bước trước
          </Text>
        </TouchableOpacity>
        <View style={{height: 26, width: 1, backgroundColor: '#aaa'}} />
        <TouchableOpacity
          disabled={activeIndex >= workflow.length - 1}
          style={{
            height: 46,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() =>
            pagerRef.current.setPageWithoutAnimation(activeIndex + 1)
          }>
          <Text
            style={[
              styles.headerButton,
              {opacity: activeIndex >= workflow.length - 1 ? 0.6 : 1},
            ]}>
            Bước sau
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  getConstructionMethodList: actions.getConstructionMethodList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InspectionMethodList);

// https://api.rocez.com/PMWorkInsStepDtls/ViewDocument?file=1179/z2057593237085_6b5f971b0d4bd92421a6da0eec4705b4.jpg
// https://api.rocez.com/PMWorkInsStepDtls/ViewDocument?file=1179/z2057591029847_4e1d46f63b372f33bd26d97337e14be6.jpg

// "https://api.rocez.com/PMWorkInsStepDtls/ViewDocument?file=1179/z2057591029847_4e1d46f63b372f33bd26d97337e14be6.jpg"
// "https://api.rocez.com/PMWorkInstSteps/ViewDocument?file=1179/z2057591029847_4e1d46f63b372f33bd26d97337e14be6.jpg"
