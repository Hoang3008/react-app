import typeToReducer from 'type-to-reducer';

import getConstructionMethodList from './actions/getConstructionMethodList.action';

const initialState = {
  list: [],
  workflowContent: [],
  constructionMethodList: [],
};

export const actions = {
  getConstructionMethodList: getConstructionMethodList.action,
};

export default typeToReducer(
  {
    ...getConstructionMethodList.reducer,
  },
  initialState,
);
