import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import RouteNames from '../RouteNames';

export default class Splash extends Component {
  componentDidMount() {
    this.props.navigation.navigate(RouteNames.AuthLogin);
  }

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>splash screen</Text>
        <Button
          title={'Next'}
          onPress={() => this.props.navigation.navigate(RouteNames.AuthLogin)}
        />
      </View>
    );
  }
}
