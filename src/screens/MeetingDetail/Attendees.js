/* eslint-disable react-native/no-inline-styles */
import {Text, TouchableOpacity, View} from 'react-native';
import styles from './MeetingDetail.styles';
import React, {useEffect, useState} from 'react';
import {pathOr} from 'ramda';
import {EMPTY} from '../../global/constants';
import {actions} from './MeetingDetail.reducer';
import {connect} from 'react-redux';

const Attendees = (props) => {
  const {detail, getMeetingPeoples, people, peopleInProject, PMProjectID} = props;
  const [shouldShow, setShouldShow] = useState(false);
  const {PMProjectMeetingID} = detail;

  useEffect(() => {
    getMeetingPeoples(PMProjectMeetingID, PMProjectID);
  }, [PMProjectMeetingID, getMeetingPeoples]);

  // AACreatedDate: "2020-07-29T16:24:28.830Z"
  // AACreatedUser: "vuong.ho@gmail.com"
  // AAStatus: "Alive"
  // AAUpdatedDate: "2020-07-30T04:28:07.503Z"
  // AAUpdatedUser: "vuong.ho@gmail.com"
  // FK_PMProjectMeetingID: 1066
  // FK_PMProjectPeopleID: 88
  // PMProjectMeetingPeopleID: 2103
  // PMProjectMeetingPeopleIsAbscent: false
  // PMProjectMeetingPeopleIsDistributed: false

  return (
    <View style={[styles.sectionView, {marginTop: 15}]}>
      <View style={[styles.headerContainer]}>
        <Text style={[styles.headerText]}>Attendees</Text>
        <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
          <Text style={styles.headerButton}>
            {shouldShow ? 'HIDE' : 'SHOW'}
          </Text>
        </TouchableOpacity>
      </View>
      {shouldShow && (
        <>
          {people.map((item, index) => {
            const {
              FK_PMProjectPeopleID,
              PMProjectMeetingPeopleIsAbscent,
              PMProjectMeetingPeopleIsDistributed,
            } = item;

            const peopleFullObject = peopleInProject.find(
              ({PMProjectPeopleID}) =>
                PMProjectPeopleID === FK_PMProjectPeopleID,
            );

            return (
              <>
                {index > 0 && (
                  <View style={{...styles.divider, marginTop: 0}} />
                )}
                <View style={styles.item} key={FK_PMProjectPeopleID}>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#245894',
                    }}>
                    {peopleFullObject
                      ? peopleFullObject.fullName
                      : FK_PMProjectPeopleID}
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      color: PMProjectMeetingPeopleIsAbscent ? '#f00' : '#333',
                    }}>
                    Absent: {PMProjectMeetingPeopleIsAbscent ? 'Yes' : 'No'}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: PMProjectMeetingPeopleIsDistributed
                        ? '#34aa44'
                        : '#333',
                    }}>
                    Distributed:{' '}
                    {PMProjectMeetingPeopleIsDistributed ? 'Yes' : 'No'}
                  </Text>
                </View>
              </>
            );
          })}
        </>
      )}
    </View>
  );
};

const mapStateToProps = (
  {auth: {user, project}, commonData, meetingDetail},
  props,
) => {
  const {PMProjectID} = project;

  return {
    user,
    project,
    peopleInProject: pathOr(
      EMPTY.ARRAY,
      ['currentProject', 'users'],
      commonData,
    ),
    people: meetingDetail.people,
    PMProjectID: PMProjectID,
  };
};

const mapDispatchToProps = {
  getMeetingPeoples: actions.getMeetingPeoples,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Attendees);
