import {pathOr} from 'ramda';
import {connect} from 'react-redux';
import React, {useEffect} from 'react';
import {ScrollView, StatusBar, Text, View} from 'react-native';
import moment from 'moment';

import styles from './MeetingDetail.styles';
import {actions} from './MeetingDetail.reducer';

import {EMPTY} from '../../global/constants';
import KeyboardAvoidingViewWrapper from '../../components/KeyboardAvoidingViewWrapper';
import Theme from '../../Theme';
import {UniStatusBarBackground} from '../../components';
import TitleHeader from '../../components/TitleHeader/TitleHeader';
import {statusColor} from '../MeetingList/MeetingList';
import GeneralInformation from './GeneralInformation';
import AttachmentList from './AttachmentList';
import Attendees from './Attendees';
import Agenda from './Agenda';

const MeetingDetail = (props) => {
  const {navigation, getMeetingDetail, detail, getMeetingTypes, types, PMProjectID} = props;

  const id = pathOr(0, ['route', 'params', 'ID'], props);

  useEffect(() => {
    getMeetingDetail(id, PMProjectID);
    getMeetingTypes(PMProjectID);
  }, [id, getMeetingDetail, getMeetingTypes]);

  // AACreatedDate: "2020-08-06T07:56:31.483Z"
  // AACreatedUser: "manhdev3@gmail.com"
  // AAStatus: "Alive"
  // AAUpdatedDate: "2020-08-06T07:56:31.483Z"
  // AAUpdatedUser: "manhdev3@gmail.com"
  // FK_PMMeetingTypeID: 1
  // FK_PMProjectID: 125
  // PMProjectMeetingDate: "2020-12-02T00:00:00.000Z"
  // PMProjectMeetingDesc: ""
  // PMProjectMeetingDocServerpath: "https://rocez-media-dev.s3.ap-southeast-1.amazonaws.com/projects/125/test_1596700591332.pdf"
  // PMProjectMeetingEndTime: "2020-12-03T16:23:00.000Z"
  // PMProjectMeetingID: 3073
  // PMProjectMeetingLocation: "213"
  // PMProjectMeetingName: "23"
  // PMProjectMeetingNo: "123"
  // PMProjectMeetingOverview: "23"
  // PMProjectMeetingStartTime: "2020-12-02T17:02:00.000Z"
  // PMProjectMeetingStatus: "New"
  // PMProjectMeetingSubject: "23"

  // AACreatedDate: "2020-04-19T23:27:43.713Z"
  // AACreatedUser: ""
  // AAStatus: "Alive"
  // AAUpdatedDate: "2020-04-19T23:27:43.713Z"
  // AAUpdatedUser: ""
  // PMMeetingTypeDesc: "Private meeting"
  // PMMeetingTypeID: 1
  // PMMeetingTypeName: "Private meeting"
  // PMMeetingTypeNo: "PM"

  const {
    PMProjectMeetingID,
    PMProjectMeetingNo,
    PMProjectMeetingName,
    PMProjectMeetingDate,
    PMProjectMeetingStartTime,
    PMProjectMeetingEndTime,
    PMProjectMeetingStatus,
  } = detail;

  const renderBasicInfo = () => {
    return (
      <View style={[styles.sectionView]}>
        <Text
          style={[styles.title, {marginTop: 15}]}
          numberOfLines={2}
          ellipsizeMode="tail">
          #{PMProjectMeetingID} [{PMProjectMeetingNo}] - {PMProjectMeetingName}
        </Text>
        <View style={[styles.row, {marginTop: 4}]}>
          <Text style={[styles.subLabelText, {width: 80}]}>Date time:</Text>
          <Text style={[styles.normalText]}>
            {moment(PMProjectMeetingDate).format('DD/MM/YYYY')}{' '}
            {moment(PMProjectMeetingStartTime).format('hh:mm a')} -{' '}
            {moment(PMProjectMeetingEndTime).format('hh:mm a')}
          </Text>
        </View>
        <View style={[styles.row, {marginTop: 4}]}>
          <Text style={[styles.subLabelText, {width: 80}]}>Status:</Text>
          <View
            style={{
              width: 8,
              height: 8,
              borderRadius: 4,
              backgroundColor: statusColor[PMProjectMeetingStatus],
            }}
          />
          <Text style={[styles.normalText, {marginLeft: 6}]}>
            {PMProjectMeetingStatus}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <KeyboardAvoidingViewWrapper style={{flex: 1}} behavior="height" enabled>
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        <TitleHeader title="Meeting Detail" onBack={navigation.goBack} />
        <ScrollView showsVerticalScrollIndicator={false}>
          {renderBasicInfo()}
          <View style={styles.divider} />
          <GeneralInformation detail={detail} types={types} />
          <View style={styles.divider} />
          <AttachmentList detail={detail} navigation={navigation} />
          <View style={styles.divider} />
          <Attendees detail={detail} />
          <View style={styles.divider} />
          <Agenda detail={detail} />
        </ScrollView>
      </View>
    </KeyboardAvoidingViewWrapper>
  );
};

const mapStateToProps = (
  {auth: {user, project}, commonData, meetingDetail},
  props,
) => {
  const {PMProjectID} = project;

  return {
    user,
    project,
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    detail: meetingDetail.detail,
    types: meetingDetail.types,
    PMProjectID: PMProjectID
  };
};

const mapDispatchToProps = {
  getMeetingDetail: actions.getMeetingDetail,
  getMeetingTypes: actions.getMeetingTypes,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MeetingDetail);
