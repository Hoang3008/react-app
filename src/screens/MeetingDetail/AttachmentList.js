import {connect} from 'react-redux';
import React, {useState} from 'react';
import {pathOr} from 'ramda';
import {View, Text, TouchableOpacity, TouchableHighlight} from 'react-native';
import {SwipeListView} from 'react-native-swipe-list-view';

import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import RouteNames from '../../RouteNames';
import {EMPTY} from '../../global/constants';
import styles from './MeetingDetail.styles';
import {getPhotoUrl} from '../../api/urls';

const AttachmentList = (props) => {
  const {detail, navigation} = props;
  const [shouldShow, setShouldShow] = useState(true);

  const {PMProjectMeetingDocServerpath} = detail;

  const data = PMProjectMeetingDocServerpath
    ? PMProjectMeetingDocServerpath.split(',')
    : [];

  const handleClickItem = (url, fileName) => () => {
    const fileNameLowerCase = fileName.toLowerCase();

    if (fileNameLowerCase.endsWith('pdf')) {
      navigation.navigate(RouteNames.PDFFullScreen, {url: url});
    } else {
      navigation.navigate(RouteNames.ImageFullScreen, {
        title: 'Meeting Photo',
        url: getPhotoUrl(url),
      });
    }
  };

  const renderItem = ({item}) => {
    const path = item;
    const arr = item.split('/');
    const name = arr[arr.length - 1];

    return (
      <TouchableHighlight onPress={handleClickItem(path, name)}>
        <View style={styles.item}>
          <Text
            style={{
              fontSize: 16,
              color: '#245894',
            }}>
            {name}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  const renderHiddenItem = ({item}) => {
    return <View style={styles.rowBack} />;
  };

  return (
    <View style={[styles.sectionView, {marginTop: 15}]}>
      <View style={[styles.headerContainer]}>
        <Text style={[styles.headerText]}>Attachments</Text>
        <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
          <Text style={styles.headerButton}>
            {shouldShow ? 'HIDE' : 'SHOW'}
          </Text>
        </TouchableOpacity>
      </View>
      {shouldShow && (
        <SwipeListView
          style={{width: '100%'}}
          data={data}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          keyExtractor={(item) => item}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          leftOpenValue={0}
          rightOpenValue={-100}
          closeOnRowBeginSwipe
          disableRightSwipe
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
          recalculateHiddenLayout={true}
        />
      )}
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, commonData}, props) => {
  const {id} = props;
  const allDocuments = pathOr([], ['photos', id], commonData);
  const {PMProjectID} = project;

  return {
    user,
    attachmentList: allDocuments,
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
  };
};

const mapDispatchToProps = {
  getTaskDocumentList: actions.getTaskDocumentList,
  deleteAttachment: actions.deletePhoto,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AttachmentList);
