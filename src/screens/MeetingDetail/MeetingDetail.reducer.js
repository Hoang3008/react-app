import typeToReducer from 'type-to-reducer';
import getMeetingDetail from './actions/getMeetingDetail.action';
import getMeetingTypes from './actions/getMeetingTypes.action';
import getMeetingPeoples from './actions/getMeetingPeoples.action';
import getMeetingAgenda from './actions/getMeetingAgenda.action';
import getMeetingCatalogue from './actions/getMeetingCatalogue.action';

const initialState = {
  detail: {},
  types: [],
  people: [],
  agendas: [],
  category: [],
};

export const actions = {
  getMeetingDetail: getMeetingDetail.action,
  getMeetingTypes: getMeetingTypes.action,
  getMeetingPeoples: getMeetingPeoples.action,
  getMeetingAgenda: getMeetingAgenda.action,
  getMeetingCatalogue: getMeetingCatalogue.action,
};

export default typeToReducer(
  {
    ...getMeetingDetail.reducer,
    ...getMeetingTypes.reducer,
    ...getMeetingPeoples.reducer,
    ...getMeetingAgenda.reducer,
    ...getMeetingCatalogue.reducer,
  },
  initialState,
);
