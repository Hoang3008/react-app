import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';
import API from '../../../api/API.service';

import {actionType as AddPhotoActionType} from '../../AddPhoto/actions/addPhoto.action';
import {actionType as UpdatePhotoActionType} from '../../EditPhoto/actions/updatePhoto.action';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_MEETING_TYPES';

// https://apidev.rocez.com/PMMeetingTypes/GetAllObjects

const getListAPI = (PMProjectID) => {
  // const url = withKey('/PMMeetingTypes/GetAllObjects');
   const url = withKey(`/settings/meeting-type?appFlag=1&projectId=${PMProjectID}`);
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getListAPI, (...param) => param);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
    types: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
