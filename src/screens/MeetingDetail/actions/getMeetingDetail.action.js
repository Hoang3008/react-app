import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';
import API from '../../../api/API.service';

import {actionType as AddPhotoActionType} from '../../AddPhoto/actions/addPhoto.action';
import {actionType as UpdatePhotoActionType} from '../../EditPhoto/actions/updatePhoto.action';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_MEETING_DETAIL';

// https://apidev.rocez.com/PMProjectMeetings/GetObjectByID?PMProjectMeetingID=1066

const getListAPI = (meetingId, projectId) => {
  // const url = withKey(
  //   `/PMProjectMeetings/GetObjectByID?PMProjectMeetingID=${id}`,
  // );

  const url = withKey(
    `/meeting/${meetingId}/of-project/${projectId}`,
  );
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getListAPI, (...param) => param);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
    detail: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
