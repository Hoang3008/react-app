import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';

import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_MEETING_CATALOGUE';

//apid.rocez.com/meeting/meeting-catalogue

const getListAPI = ({PMProjectID}) => {
  const url = withKey(`/meeting/meeting-catalogue?projectId=${PMProjectID}`);
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getListAPI, (...param) => param);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    category: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
