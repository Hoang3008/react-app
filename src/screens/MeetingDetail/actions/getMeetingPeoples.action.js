import {createAction} from 'redux-actions';
import {identity} from 'ramda';
import API from '../../../api/API.service';


import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_MEETING_PEOPLE';

// https://apidev.rocez.com/PMProjectMeetingPeoples/GetDataByPMProjectMeetingID?FK_PMProjectMeetingID=1066

const getListAPI = (meetingId, projectId) => {
  // const url = withKey(
  //   `/PMProjectMeetingPeoples/GetDataByPMProjectMeetingID?FK_PMProjectMeetingID=${id}`,
  // );

  const url = withKey(
    `/meeting/${meetingId}/people/of-project/${projectId}`,
  );
  return AsyncStorage.getItem('token').then((token) => {
    return API.get(url, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        Authorization: `Bearer ${token}`,
      },
    });
  });
};

const action = createAction(actionType, getListAPI, (...param) => param);

const handleAPISuccess = (state, {payload, meta}) => {
  return {
    ...state,
    people: payload.data,
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
};

export default {action, reducer};
