import {Text, TouchableOpacity, View} from 'react-native';
import styles from './MeetingDetail.styles';
import {WebView} from 'react-native-webview';
import React, {useState} from 'react';
import {propOr} from 'ramda';

const GeneralInformation = (props) => {
  const {detail, types} = props;
  const [shouldShow, setShouldShow] = useState(true);
  const {
    PMProjectMeetingSubject,
    PMProjectMeetingLocation,
    PMProjectMeetingOverview,
    FK_PMMeetingTypeID,
  } = detail;

  const meetingType = (types || []).find(
    ({PMMeetingTypeID}) => `${PMMeetingTypeID}` === `${FK_PMMeetingTypeID}`,
  );

  return (
    <View style={[styles.sectionView, {marginTop: 15}]}>
      <View style={[styles.headerContainer]}>
        <Text style={[styles.headerText]}>General Information</Text>
        <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
          <Text style={styles.headerButton}>
            {shouldShow ? 'HIDE' : 'SHOW'}
          </Text>
        </TouchableOpacity>
      </View>
      {shouldShow && (
        <>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Subject:</Text>
            <Text style={[styles.normalText]}>{PMProjectMeetingSubject}</Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Meeting type:</Text>
            <Text style={[styles.normalText]}>
              {propOr('meetingType', 'PMMeetingTypeName', meetingType)}
            </Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Location:</Text>
            <Text style={[styles.normalText]}>{PMProjectMeetingLocation}</Text>
          </View>
          <View style={styles.generalRow}>
            <Text style={styles.generalLeftView}>Overview:</Text>
          </View>

          <WebView
            style={{
              height: 170,
              width: '100%',
              marginTop: 6,
              backgroundColor: '#aaa',
            }}
            originWhitelist={['*']}
            source={{
              html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body style="background-color: #eee">${PMProjectMeetingOverview}</body></html>`,
            }}
          />
        </>
      )}
    </View>
  );
};

export default GeneralInformation;
