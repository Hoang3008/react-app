/* eslint-disable react-native/no-inline-styles */
import {Text, TouchableOpacity, View} from 'react-native';
import styles from './MeetingDetail.styles';
import React, {useEffect, useState} from 'react';
import {pathOr, propOr} from 'ramda';
import {connect} from 'react-redux';
import moment from 'moment';

import {actions} from './MeetingDetail.reducer';
import {EMPTY} from '../../global/constants';

const Agenda = (props) => {
  const {
    detail,
    getMeetingAgenda,
    peopleInProject,
    agendas,
    getMeetingCatalogue,
    category,
    PMProjectID,
  } = props;
  const [shouldShow, setShouldShow] = useState(true);
  const {FK_PMProjectID, PMProjectMeetingID} = detail;

  useEffect(() => {
    getMeetingCatalogue({PMProjectID: PMProjectID});
  }, [getMeetingCatalogue]);

  useEffect(() => {
    getMeetingAgenda(FK_PMProjectID, PMProjectMeetingID);
  }, [getMeetingAgenda, FK_PMProjectID, PMProjectMeetingID]);

  return (
    <View style={[styles.sectionView, {marginTop: 15}]}>
      <View style={[styles.headerContainer]}>
        <Text style={[styles.headerText]}>Agenda</Text>
        <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
          <Text style={styles.headerButton}>
            {shouldShow ? 'HIDE' : 'SHOW'}
          </Text>
        </TouchableOpacity>
      </View>
      {shouldShow && (
        <>
          {agendas.map((item, index) => {
            const {
              PMProjectMeetingAgendaID,
              PMProjectMeetingAgendaNo,
              FK_PMProjectAssignedPeopleID,
              PMProjectMeetingAgendaTitle,
              PMProjectMeetingAgendaDesc,
              FK_PMMeetingCategoryID,
              PMProjectMeetingAgendaPriority,
              PMProjectMeetingAgendaStatus,
              PMProjectMeetingAgendaDueDate,
            } = item;

            const peopleFullObject = peopleInProject.find(
              ({PMProjectPeopleID}) =>
                PMProjectPeopleID === FK_PMProjectAssignedPeopleID,
            );

            const agendaCategory = (category || []).find(
              ({PMMeetingCategoryID}) =>
                `${PMMeetingCategoryID}` === `${FK_PMMeetingCategoryID}`,
            );

            return (
              <React.Fragment key={PMProjectMeetingAgendaID}>
                {index > 0 && (
                  <View
                    style={{...styles.divider, marginTop: 0}}
                    key={PMProjectMeetingAgendaID}
                  />
                )}
                <View style={styles.item} key={PMProjectMeetingAgendaID}>
                  <Text
                    style={{
                      fontSize: 15,
                      color: '#333',
                      maxHeight: 36,
                      fontWeight: 'bold',
                    }}>
                    #{PMProjectMeetingAgendaNo} - {PMProjectMeetingAgendaTitle}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#333',
                      fontStyle: 'italic',
                    }}>
                    {PMProjectMeetingAgendaDesc}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#245894',
                    }}>
                    {peopleFullObject
                      ? peopleFullObject.fullName
                      : FK_PMProjectAssignedPeopleID}
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      color: '#333',
                    }}>
                    Category:{' '}
                    {propOr('', 'PMMeetingCategoryName', agendaCategory)}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#333',
                    }}>
                    Priority: {PMProjectMeetingAgendaPriority}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#333',
                    }}>
                    Status: {PMProjectMeetingAgendaStatus}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#333',
                    }}>
                    Due Date:{' '}
                    {moment(PMProjectMeetingAgendaDueDate).format('DD/MM/YYYY')}
                  </Text>
                </View>
              </React.Fragment>
            );
          })}
        </>
      )}
    </View>
  );
};

const mapStateToProps = (
  {auth: {user, project}, commonData, meetingDetail},
  props,
) => {
  const {PMProjectID} = project;

  return {
    user,
    project,
    peopleInProject: pathOr(
      EMPTY.ARRAY,
      ['currentProject', 'users'],
      commonData,
    ),
    agendas: meetingDetail.agendas,
    category: meetingDetail.category,
    PMProjectID: PMProjectID,
  };
};

const mapDispatchToProps = {
  getMeetingAgenda: actions.getMeetingAgenda,
  getMeetingCatalogue: actions.getMeetingCatalogue,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Agenda);
