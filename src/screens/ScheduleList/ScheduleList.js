import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Animated, FlatList} from 'react-native';
import moment from 'moment';
import {isEmpty, propOr} from 'ramda';

import {actions} from './ScheduleList.reducer';
import Theme from '../../Theme';
import {Image, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {UniStatusBarBackground} from '../../components';
import {SearchBar} from 'react-native-elements';
import UniImageButton from '../../components/UniImageButton';
import Tab from '../../components/ScheduleTab/ScheduleTab';
import DateNavigator from '../../components/DateNavigator/DateNavigator';
import styles from './ScheduleList.styles';
import Icon from 'react-native-vector-icons/Fontisto';
import RouteNames from '../../RouteNames';
import {getTodayDateMoment} from '../../utils/DateUtils';

const tabList = ['Day', 'Week', 'Month', 'All'];
export const PRIORITY_COLOR = {
  Priority1: '#ffeb33',
  Priority2: '#f6ab2f',
  Priority3: '#ffa800',
  Overdue: '#fd4e4f',
  Complete: '#62e787',
  Verify: '#519d59',
};

const filteredDataByDate = (data, date, level) => {
  let startDate = moment(date);
  let endDate = moment(date);

  if (level === 1) {
    startDate = moment(date).startOf('isoWeek');
    endDate = moment(date).endOf('isoWeek');
  } else if (level === 2) {
    startDate = moment(date).startOf('month');
    endDate = moment(date).endOf('month');
  }

  return data.filter((item) => {
    const {PMTaskEndDate, PMTaskStartDate} = item;

    const taskStartDate = moment(PMTaskStartDate)
      .startOf('day')
      .valueOf();
    const taskEndDate = moment(PMTaskEndDate)
      .startOf('day')
      .valueOf();

    return (
      (startDate.valueOf() >= taskStartDate &&
        startDate.valueOf() <= taskEndDate) ||
      (endDate.valueOf() >= taskStartDate &&
        endDate.valueOf() <= taskEndDate) ||
      (taskStartDate >= startDate.valueOf() &&
        taskStartDate <= endDate.valueOf()) ||
      (taskEndDate >= startDate.valueOf() && taskEndDate <= endDate.valueOf())
    );
  });
};

const filterDataByUser = (data, ADUserID) =>
  data.filter((item) => {
    const {FK_ADAssignUserID, FK_ADUserID} = item;
    return (
      ADUserID === FK_ADUserID ||
      `${ADUserID}` === FK_ADUserID ||
      ADUserID === FK_ADAssignUserID ||
      `${ADUserID}` === FK_ADAssignUserID
    );
  });

const ScheduleList = (props) => {
  const {user, navigation, project, data, getScheduleList} = props;

  const [tabIndex, setTabIndex] = useState(3);
  const [searchText, setSearchText] = useState('');
  const [showSearchBox, setShowSearchBox] = useState(false);
  const [viewDate, setViewDate] = useState(getTodayDateMoment().toDate());
  const [filteredData, setFilteredData] = useState([]);

  const PMProjectID = propOr(0, 'PMProjectID', project);
  const ADUserID = propOr(0, 'ADUserID', user);

  useEffect(() => {
    const lowerCaseSearch = searchText.toLowerCase();
    const filteredData = isEmpty(lowerCaseSearch)
      ? data
      : data.filter(
          ({PMTaskName}) =>
            PMTaskName && PMTaskName.toLowerCase().includes(lowerCaseSearch),
        );

    if (tabIndex === 3) {
      setFilteredData(filteredData);
      return;
    }

    setFilteredData(
      filterDataByUser(
        filteredDataByDate(filteredData, viewDate, tabIndex),
        ADUserID,
      ),
    );
  }, [data, tabIndex, searchText, viewDate, ADUserID]);

  useEffect(() => {
    getScheduleList(PMProjectID, ADUserID);
  }, [getScheduleList, PMProjectID, ADUserID]);



  const onPressNewSchedule = () => {
    navigation.push(RouteNames.ScheduleUpdate, {
      onGoBack: () => getScheduleList(PMProjectID),
    });
  };

  const handlePressItem = (item) => () => {
    const {PMTaskID} = item;
    navigation.navigate(RouteNames.ScheduleDetail, {
      PMTaskID,
      onDelete: () => getScheduleList(PMProjectID)
    });
  };

  const handleViewPrevDate = () => {
    const date = moment(viewDate);
    if (tabIndex === 0) {
      date.subtract(1, 'days');
    } else if (tabIndex === 1) {
      date.subtract(7, 'days');
    } else {
      date.subtract(1, 'months');
    }

    setViewDate(date.toDate());
  };

  const handleViewNextDate = () => {
    const date = moment(viewDate);
    if (tabIndex === 0) {
      date.add(1, 'days');
    } else if (tabIndex === 1) {
      date.add(7, 'days');
    } else {
      date.add(1, 'months');
    }

    setViewDate(date.toDate());
  };

  const renderNavigation = () => (
    <View style={styles.navigationBar}>
      <View style={styles.row}>
        <UniImageButton
          containerStyle={{
            paddingHorizontal: Theme.margin.m16,
            paddingVertical: Theme.margin.m6,
          }}
          source={require('../../assets/img/ic_back.png')}
          onPress={() => navigation.goBack()}
        />
        <Text style={styles.textHeader}>Schedule</Text>
      </View>
      <View style={styles.row}>
        <TouchableOpacity onPress={() => setShowSearchBox(!showSearchBox)}>
          <Image
            style={styles.navIcon}
            source={require('../../assets/img/ic_search.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );

  // AACreatedDate: "2020-04-10T21:07:20.377Z"
  // AACreatedUser: "evenhieu@gmail.com"
  // AAStatus: "Alive"
  // AAUpdatedDate: "2020-04-14T16:46:02.120Z"
  // AAUpdatedUser: "evenhieu@gmail.com"
  // FK_ADAssignUserID: 73
  // FK_ADUserID: 196
  // FK_PMProjectCategoryID: 0
  // FK_PMProjectID: 117
  // FK_PMProjectLocationID: 0
  // FK_PMTaskGroupID: 0
  // FK_PPDrawingID: 0
  // FK_PPMDRID: null
  // PMTaskCompleteCheck: false
  // PMTaskCompletePct: 100
  // PMTaskCost: 0
  // PMTaskDesc: "te123"
  // PMTaskEndDate: "2020-04-17T00:00:00.000Z"
  // PMTaskID: 1410
  // PMTaskLat: 0
  // PMTaskLng: 0
  // PMTaskLocation: ""
  // PMTaskManPower: 0
  // PMTaskName: "test"
  // PMTaskNo: "0"
  // PMTaskPriorityCombo: "Priority1"
  // PMTaskStartDate: "2020-04-27T00:00:00.000Z"
  // PMTaskTags: ""
  // PMTaskTypeCombo: "task"

  const renderRowItem = (item) => {
    const {
      PMTaskID,
      PMTaskName,
      PMTaskStartDate,
      PMTaskEndDate,
      PMTaskPriorityCombo,
      PMTaskCompletePct,
    } = item;

    const todayISO = moment().toISOString();
    const isExpired = todayISO > PMTaskEndDate;

    return (
      <TouchableOpacity onPress={handlePressItem(item)}>
        <View style={styles.itemContainer}>
          <View
            style={[
              styles.priority,
              {
                backgroundColor:
                  PMTaskCompletePct >= 100
                    ? PRIORITY_COLOR.Complete
                    : isExpired
                    ? PRIORITY_COLOR.Overdue
                    : PRIORITY_COLOR[PMTaskPriorityCombo],
              },
            ]}
          />
          <View style={styles.itemMainContent}>
            <Text style={styles.itemText}>
              #{PMTaskID} {PMTaskName}
            </Text>
            <Text style={styles.itemDate}>
              {moment(PMTaskStartDate).format('M/D/YY')} -{' '}
              {moment(PMTaskEndDate).format('M/D/YY')}
            </Text>
          </View>
          <Text style={styles.itemPercentText}>{PMTaskCompletePct}%</Text>
          <Icon
            style={{marginBottom: 4}}
            size={12}
            color={'#9EA0A5'}
            name="angle-right"
          />
        </View>
      </TouchableOpacity>
    );
  };

  const position = new Animated.Value(tabIndex);
  let dateLabel = moment(viewDate).format('MMMM YYYY');

  if (tabIndex === 0) {
    dateLabel = moment(viewDate).format('MMM DD, YYYY');
  } else if (tabIndex === 1) {
    dateLabel = `Week of ${moment(viewDate).format('MMM DD, YYYY')}`;
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Theme.colors.backgroundWhite,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <StatusBar
        backgroundColor={Theme.colors.primary}
        barStyle="light-content"
      />
      <UniStatusBarBackground />
      {renderNavigation()}
      <View style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
        {showSearchBox && (
          <SearchBar
            placeholder="Type Here..."
            value={searchText}
            onChangeText={setSearchText}
            inputStyle={{
              color: Theme.colors.textNormal,
            }}
            containerStyle={{
              borderBottomColor: '#0000',
              backgroundColor: '#ffffff',
            }}
            inputContainerStyle={{
              backgroundColor: '#ddd',
            }}
          />
        )}
        <View
          style={{
            marginTop: showSearchBox ? 5 : 10,
            height: 32,
            backgroundColor: Theme.colors.backgroundColor,
            flexDirection: 'row',
            marginLeft: 10,
            marginRight: 10,
            borderRadius: 6,
            padding: 3,
          }}>
          {tabList.map((route, index) => {
            const focusAnim = position.interpolate({
              inputRange: [index - 1, index, index + 1],
              outputRange: [0, 1, 0],
            });
            return (
              <Tab
                key={route}
                style={{flex: 1}}
                focusAnim={focusAnim}
                title={route}
                onPress={() => setTabIndex(index)}
              />
            );
          })}
        </View>
        {tabIndex < 3 && (
          <DateNavigator
            style={{alignSelf: 'center'}}
            label={dateLabel}
            onPressPrev={handleViewPrevDate}
            onPressNext={handleViewNextDate}
          />
        )}
        <FlatList
          style={{marginTop: tabIndex < 3 ? 0 : 10}}
          data={filteredData}
          renderItem={({item, index}) => renderRowItem(item)}
          keyExtractor={(item, index) => item.PMTaskID}
          showsVerticalScrollIndicator={false}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
        />
      </View>
      <UniImageButton
        containerStyle={styles.floatButton}
        source={require('../../assets/img/ic_add.png')}
        onPress={onPressNewSchedule}
      />
    </View>
  );
};

const mapStateToProps = ({auth: {user, project}, commonData, scheduleList}) => {
  return {
    user,
    project,
    data: scheduleList.scheduleList,
  };
};

const mapDispatchToProps = {
  getScheduleList: actions.getScheduleList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScheduleList);
