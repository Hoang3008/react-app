import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import Theme from '../../Theme';

export default {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flexDirection: 'row',
    flex: 1,
    height: 0.5,
    backgroundColor: '#e8e8e8',
  },
  sectionHeaderText: {
    flex: 1,
    paddingHorizontal: Theme.margin.m12,
    paddingBottom: Theme.margin.m6,
    paddingTop: Theme.margin.m22,
    color: Theme.colors.textNormal,
    backgroundColor: Theme.colors.backgroundHeader,
  },
  floatButton: {
    position: 'absolute',
    bottom: Theme.margin.m16,
    right: Theme.margin.m20,
    width: Theme.specifications.floatButtonSize,
    height: Theme.specifications.floatButtonSize,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#34AA44',
    borderRadius: Theme.specifications.floatButtonSize / 2,
    borderWidth: 0,
  },
  itemContainer: {
    width: '100%',
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  priority: {
    width: 12,
    height: 12,
    borderRadius: 6,
  },
  itemMainContent: {
    marginLeft: 10,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  itemText: {
    width: '100%',
    fontSize: 16,
    fontWeight: '600',
  },
  itemDate: {
    width: '100%',
    fontSize: 13,
    fontWeight: '400',
  },
  itemPercentText: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 14,
    fontWeight: '600',
  },
  normalText: {
    fontSize: 14,
    lineHeight: 18,
    color: '#3E3F42',
  },
};
