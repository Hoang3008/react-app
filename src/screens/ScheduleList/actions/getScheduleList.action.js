import {createAction} from 'redux-actions';
import {identity, propOr} from 'ramda';
import API from '../../../api/API.service';

import {actionType as createDefectActionType} from '../../CommonProjectData/actions/createDefect.action';
import {actionType as updateDefectActionType} from '../../CommonProjectData/actions/updateDefect.action';
import {withKey} from '../../../api/urls';
import AsyncStorage from '@react-native-community/async-storage';

const actionType = 'GET_SCHEDULE_LIST';

// `/PMTasks/GetAllDataByCondition?FK_ADUserID=${userId}&FK_PMProjectID=${projectId}&PMTaskTypeCombo=schedule`,
const createTaskAPI = (projectId, userId) => {
  return AsyncStorage.getItem('token').then((token) => {
    const url = withKey(
      `/task/schedule/search`,
    );

    return API.post(
      url,
      {
        projectId: projectId,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
  });
};

const action = createAction(actionType, createTaskAPI, identity);

const handleAPISuccess = (state, {payload}) => {
  return {
    ...state,
    scheduleList: payload.data,
  };
};

const handleCreateSuccess = (state, {payload}) => {
  const {data} = payload;
  const type = propOr('', 'PMTaskTypeCombo', data);

  if (type !== 'schedule') {
    return state;
  }

  return {
    ...state,
    scheduleList: [...state.scheduleList, data],
  };
};

const handleUpdateSuccess = (state, {payload}) => {
  const {data} = payload;
  const type = propOr('', 'PMTaskTypeCombo', data);

  if (type !== 'schedule') {
    return state;
  }

  return {
    ...state,
    scheduleList: state.scheduleList.map((item) => {
      if (item.PMTaskID === data.PMTaskID) {
        return {
          ...item,
          ...data,
        };
      }

      return item;
    }),
  };
};

const reducer = {
  [actionType]: {
    BEGIN: identity,
    SUCCESS: handleAPISuccess,
    FAILURE: identity,
  },
  [createDefectActionType]: {
    SUCCESS: handleCreateSuccess,
  },
  [updateDefectActionType]: {
    SUCCESS: handleUpdateSuccess,
  },
};

export default {action, reducer};
