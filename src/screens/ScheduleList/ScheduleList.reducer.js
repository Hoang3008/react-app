import typeToReducer from 'type-to-reducer';
import getScheduleList from './actions/getScheduleList.action';

const initialState = {
  scheduleList: [],
};

export const actions = {
  getScheduleList: getScheduleList.action,
};

export default typeToReducer(
  {
    ...getScheduleList.reducer,
  },
  initialState,
);
