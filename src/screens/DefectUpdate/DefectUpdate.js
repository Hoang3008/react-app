import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import moment from 'moment';

import {
  UniImageButton,
  UniStatusBarBackground,
  UniInput,
  UniButton,
  UniDatePicker,
  UniMultiSelect
} from '../../components';
import DefectCodeDropBox from '../../components/DefectCodeDropbox';
import Theme from '../../Theme';
import {SCREEN_WIDTH} from '../../utils/DimensionUtils';
import {showAlert} from '../../utils/MessageUtils';
import {requestDeleteTask} from '../../api/defect';
import {pathOr, propOr, isEmpty, clone} from 'ramda';
import RouteNames from '../../RouteNames';
import {PRIORITY} from '../defect/DefectHelper';
import {EMPTY, OFFLINE_ACTION_TYPES} from '../../global/constants';
import {actions} from '../CommonProjectData/CommonProjectData.reducer';
import {DropDownWithListScreen} from '../SelectFromList/SelectFromList';
import {createLogList} from '../../utils/Log.utils';

class DefectUpdate extends React.Component {
  constructor(props) {
    super(props);
    let defect = pathOr(undefined, ['route', 'params', 'defect'], this.props);
    let userId = pathOr(undefined, ['user', 'ADUserID'], this.props);
    let permissions = this.props.modulePermissions.find(x => x.module === 'defect');
    this.inputingTitle = defect ? defect.PMTaskName : '';
    this.state = {
      defect: clone(defect),
      isNewDefect: !!!defect
    };
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ): void {
    const {allDefects} = this.props;

    if (prevProps.allDefects !== allDefects) {
      const lat = pathOr(0, ['route', 'params', 'lat'], this.props);
      const lng = pathOr(0, ['route', 'params', 'lng'], this.props);
      const defectFromMemory = allDefects.find(
        ({PMTaskLat, PMTaskLng}) => PMTaskLat === lat && lng === PMTaskLng,
      );

      if (defectFromMemory) {
        this.setState({
          defect: {
            ...defectFromMemory,
            ...this.state.defect,
          },
        });
      }
    }
  }

  getDefaultDefect = () => {
    const {
      user: {ADUserID, ADUserName},
      project: {PMProjectID},
    } = this.props;
    const date = new Date();
    const lat = pathOr(0, ['route', 'params', 'lat'], this.props);
    const lng = pathOr(0, ['route', 'params', 'lng'], this.props);
    const drawingId = pathOr(0, ['route', 'params', 'drawingId'], this.props);
    const fromDefect = pathOr(
      {},
      ['route', 'params', 'fromDefect'],
      this.props,
    );

    return {
      AACreatedUser: ADUserName,
      AAUpdatedUser: ADUserName,
      PMTaskNo: 0,
      PMTaskName: 'Enter title',
      PMTaskDesc: 'Enter description',
      FK_ADUserID: ADUserID,
      PMTaskLat: lat,
      PMTaskLng: lng,
      FK_PPDrawingID: propOr(drawingId, 'FK_PPDrawingID', fromDefect),
      FK_PMProjectID: PMProjectID,
      FK_ADAssignUserID: propOr(ADUserID, 'FK_ADAssignUserID', fromDefect),
      PMTaskStartDate: propOr(
        date.toISOString(),
        'PMTaskStartDate',
        fromDefect,
      ),
      PMTaskEndDate: propOr(date.toISOString(), 'PMTaskEndDate', fromDefect),
      PMTaskCompleteCheck: false,
      PMTaskLocation: '',
      PMTaskManPower: 0,
      PMTaskCost: 0,
      PMTaskTags: '',
      PMTaskTypeCombo: 'defect',
      PMTaskPriorityCombo: 'Priority1',
      FK_PMProjectCategoryID: propOr(0, 'FK_PMProjectCategoryID', fromDefect),
      FK_PMProjectLocationID: propOr(0, 'FK_PMProjectLocationID', fromDefect),
      FK_PMTaskGroupID: propOr(0, 'FK_PMTaskGroupID', fromDefect),
    };
  };

  componentWillUnmount(): void {
    this.props.route &&
      this.props.route.params &&
      this.props.route.params.onGoBack &&
      this.props.route.params.onGoBack();
  }

  componentDidMount(): void {
    const {
      getDefectCodeList,
      getProjectCategories,
      getProjectLocations,
      getPeopleInProject,
      project: {PMProjectID},
      user,
      categories,
      locations,
      defectCodeList,
      peopleInProject,
      getDefectListByDrawingId
    } = this.props;

    if (isEmpty(defectCodeList)) {
      getDefectCodeList({projectId: PMProjectID});
    }

    if (isEmpty(locations)) {
      getProjectLocations(PMProjectID);
    }

    if (isEmpty(categories)) {
      getProjectCategories(PMProjectID);
    }

    if (isEmpty(peopleInProject)) {
      getPeopleInProject({PMProjectID, UserName: user.ADUserName});
    }

    if (!this.props.master) {
      showAlert('Alert', 'Cannot get Master data!', () => {
        this.props.navigation.goBack();
      });
    }
    if (!this.state.defect) {
      this.setState({
        defect: this.getDefaultDefect(),
      });
    }
  }

  onPressPriority = (value) => {
    this.setState({defect: {...this.state.defect, PMTaskPriorityCombo: value}});
  };

  onPressDelete = () => {
    const {user, project, drawingId, getDefectListByDrawingId} = this.props;
    requestDeleteTask(this.state.defect).then((response) => {
      if (response) {
        const token = user.token;
        const projectId = project.PMProjectID;

        getDefectListByDrawingId({user, drawingId, projectId}, token).then((res) => {
          if (res) {
            this.props.route &&
            this.props.route.params &&
            this.props.route.params.onDelete &&
            this.props.route.params.onDelete();
            this.props.navigation.goBack();
          }
        })
      }
    });
  };

  onPressSave = () => {
    const {
      addActionIntoQueue,
      navigation,
      categories,
      defectCodeList,
      locations,
      peopleInProject,
    } = this.props;

    this.setState(
      {defect: {...this.state.defect, PMTaskName: this.inputingTitle}},
      () => {
        const PMTaskID =
          pathOr(0, ['defect', 'PMTaskID'], this.state) ||
          pathOr(0, ['defect', 'offlineId'], this.state);

        if (!PMTaskID) {
          addActionIntoQueue({
            type: OFFLINE_ACTION_TYPES.CREATE_DEFECT,
            data: this.state.defect,
          });
          navigation.goBack();
        } else {
          const {defect} = this.state;
          const currentData = pathOr(
            undefined,
            ['route', 'params', 'defect'],
            this.props,
            
          );

          addActionIntoQueue({
            type: OFFLINE_ACTION_TYPES.UPDATE_DEFECT,
            data: defect,
            currentData,
            logs: createLogList(currentData, defect, {
              categories,
              defectCodeList,
              locations,
              peopleInProject,
            }),
          });
          navigation.goBack();
        }
      },
    );
  };

  renderNavigation = () => {
    return (
      <View style={styles.navigationBar}>
        <View style={styles.row}>
          <UniImageButton
            containerStyle={{
              paddingHorizontal: Theme.margin.m16,
              paddingVertical: Theme.margin.m6,
            }}
            source={require('../../assets/img/ic_back.png')}
            onPress={() =>
              this.props.navigation && this.props.navigation.goBack()
            }
          />
          <View>
            <Text style={styles.textHeader}>Defect Details</Text>
          </View>
        </View>
      </View>
    );
  };

  renderPriority = (key, value, color, currentPriority = '', short) => {
    let active = currentPriority.toLowerCase() === key.toLowerCase();
    return (
      <TouchableOpacity
        style={{alignItems: 'center'}}
        onPress={() => this.onPressPriority(key)}>
        <View
          style={[
            styles.priorityContainer,
            active ? {backgroundColor: color, borderWidth: 0} : {},
          ]}>
          <Text
            style={[styles.textNormalSecond, active ? {color: 'white'} : {}]}>
            {short}
          </Text>
        </View>
        <Text
          style={[
            styles.textSmallSecond,
            active ? {color: Theme.colors.textNormal} : {},
          ]}>
          {value}
        </Text>
      </TouchableOpacity>
    );
  };

  renderSelectPriority = () => {
    let {PMTaskPriorityCombo} = this.state.defect;
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: Theme.colors.backgroundHeader,
          paddingTop: Theme.margin.m12,
          paddingBottom: Theme.margin.m10,
          paddingHorizontal: Theme.margin.m10,
        }}>
        {this.renderPriority(
          'Priority1',
          'Priority 1',
          '#FFEB33',
          PMTaskPriorityCombo,
          'P1',
        )}
        {this.renderPriority(
          'Priority2',
          'Priority 2',
          '#FFA800',
          PMTaskPriorityCombo,
          'P2',
        )}
        {this.renderPriority(
          'Priority3',
          'Priority 3',
          '#c2cc06',
          PMTaskPriorityCombo,
          'P3',
        )}
        {this.renderPriority(
          'Complete',
          'Complete',
          '#62E787',
          PMTaskPriorityCombo,
          '✓',
        )}
        {this.renderPriority(
          'Verify',
          'Verify',
          '#519D59',
          PMTaskPriorityCombo,
          '✓',
        )}
      </View>
    );
  };

  onChangeUserName = (value) => {
    this.inputingTitle = value;
  };

  handleSelectDefectCode = () => {
    const {navigation} = this.props;

    navigation.push(RouteNames.SelectDefectCode, {
      categoryId: this.state.defect.FK_PMProjectCategoryID,
      onSelectDefectCode: (item) => {
        this.setState({
          defect: {...this.state.defect, FK_PMTaskGroupID: item.PMTaskGroupID},
        });
      },
    });
  };

  canDeleteDefect = () => {
    const {user} = this.props;
    const {defect} = this.state;

    if (!defect.PMTaskID) {
      return false;
    }

    const priority = propOr('', 'priority', defect);

    const isDelete =  defect.AACreatedUser === user.ADUserName &&
    priority !== PRIORITY.COMPLETE &&
    priority !== PRIORITY.VERIFY;

    return isDelete;
  };

  render() {
    const {defectCodeList, peopleInProject, navigation, canChange, projectId} = this.props;
    const {defect} = this.state;

    if (!defect) {
      return <View />;
    }

    return (
      <View style={{flex: 1, backgroundColor: Theme.colors.backgroundWhite}}>
        <StatusBar
          backgroundColor={Theme.colors.primary}
          barStyle="light-content"
        />
        <UniStatusBarBackground />
        {this.renderNavigation()}
        <ScrollView>
          {this.renderSelectPriority()}

          <UniInput
            autoSelectAllWhenTextIs="Enter title"
            initValue={propOr('Enter title', 'PMTaskName', defect)}
            onChangeText={this.onChangeUserName}
            containerStyle={styles.editText}
          />
          <DropDownWithListScreen
            navigation={navigation}
            options={this.props.categories}
            label={'Category'}
            idKey={'PMProjectCategoryID'}
            noKey={'PMProjectCategoryNo'}
            nameKey={'PMProjectCategoryName'}
            descriptionKey={'PMProjectCategoryDesc'}
            value={propOr('', 'FK_PMProjectCategoryID', defect)}
            onSelect={({PMProjectCategoryID}) =>
              this.setState({
                defect: {
                  ...defect,
                  FK_PMProjectCategoryID: PMProjectCategoryID,
                },
              })
            }
          />

          <DefectCodeDropBox
            label={'Defect Code'}
            onClick={this.handleSelectDefectCode}
            selectedValue={defect.FK_PMTaskGroupID}
            options={defectCodeList}
          />

          <DropDownWithListScreen
            navigation={navigation}
            options={this.props.locations}
            label={'Location'}
            idKey={'PMProjectLocationID'}
            noKey={'PMProjectLocationNo'}
            nameKey={'PMProjectLocationName'}
            descriptionKey=""
            value={propOr('', 'FK_PMProjectLocationID', defect)}
            onSelect={({PMProjectLocationID}) =>
              this.setState({
                defect: {
                  ...defect,
                  FK_PMProjectLocationID: PMProjectLocationID,
                },
              })
            }
          />

          <DropDownWithListScreen
            navigation={navigation}
            options={peopleInProject}
            label={'Assignee'}
            idKey={'ADUserID'}
            noKey={'shortName'}
            nameKey={'fullName'}
            descriptionKey=""
            value={propOr('', 'FK_ADAssignUserID', defect)}
            onSelect={({ADUserID}) =>
              this.setState({
                defect: {
                  ...defect,
                  FK_ADAssignUserID: ADUserID,
                },
              })
            }
          />

          <UniDatePicker
            label={'Start date'}
            selectedValue={propOr(
              new Date().toISOString(),
              'PMTaskStartDate',
              defect,
            )}
            onSelect={(item) =>
              this.setState({
                defect: {...defect, PMTaskStartDate: item},
              })
            }
          />

          <UniDatePicker
            label={'End date'}
            selectedValue={propOr(
              new Date().toISOString(),
              'PMTaskEndDate',
              defect,
            )}
            onSelect={(item) => {
              if (moment(item).isSameOrBefore(defect.PMTaskStartDate)) {
                return;
              }

              this.setState({
                defect: {...defect, PMTaskEndDate: item},
              });
            }}
          />

          {
          defect?.PMTaskID &&  
          <UniMultiSelect 
              projectId={projectId}
              taskId={defect?.PMTaskID}
              canChange={canChange}
              type={'defect'}
            />
            }

          <View style={{alignItems: 'flex-end', marginRight: Theme.margin.m16}}>
            {this.canDeleteDefect() && (
              <TouchableOpacity onPress={this.onPressDelete} disabled={!canChange}>
                <Text
                  style={{
                    color: !canChange ? Theme.colors.black : Theme.colors.textDangerous,
                    fontSize: Theme.fontSize.normal,
                    paddingVertical: Theme.margin.m8,
                  }}>
                  Delete Task
                </Text>
              </TouchableOpacity>
            )}
            {(canChange || this.state.isNewDefect) && 
            <UniButton
            text={'Save'}
            onPress={this.onPressSave}
            containerStyle={styles.button}
          />}
          
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  navigationBar: {
    width: SCREEN_WIDTH,
    height: Theme.specifications.navigationBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Theme.colors.primary,
    paddingRight: Theme.margin.m16,
  },
  navIcon: {
    margin: Theme.margin.m8,
    width: Theme.specifications.icSmall,
    height: Theme.specifications.icSmall,
    resizeMode: 'contain',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textHeaderSmall: {
    fontSize: Theme.fontSize.small_12,
    color: Theme.colors.textNavigation,
    fontWeight: '600',
  },
  textSectionHeader: {
    fontSize: Theme.fontSize.header_16,
    color: Theme.colors.textNormal,
    fontWeight: '600',
  },
  priorityContainer: {
    width: Theme.specifications.icBig,
    height: Theme.specifications.icBig,
    marginHorizontal: Theme.margin.m5,
    borderRadius: Theme.specifications.icBig / 2,
    borderWidth: 1,
    borderColor: Theme.colors.border,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNormalSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.normal,
  },
  textSmallSecond: {
    color: Theme.colors.textSecond,
    fontSize: Theme.fontSize.small_12,
  },
  button: {
    marginLeft: Theme.margin.m15,
    marginVertical: Theme.margin.m16,
  },
  editText: {
    marginHorizontal: Theme.margin.m15,
    marginTop: Theme.margin.m15,
    marginBottom: Theme.margin.m8,
  },
};

const mapStateToProps = (
  {auth: {user, master, project}, commonData},
  props,
) => {
  // const {PMProjectID} = project; TODOs
  const drawingId = pathOr(0, ['route', 'params', 'drawingId'], props);
  const defectList = pathOr(EMPTY.ARRAY, ['defectListByDrawingId', drawingId], commonData);
  const modulePermissions = pathOr([],['Permission'],commonData);
  const categoryList = pathOr(
    EMPTY.ARRAY,
    ['currentProject', 'categories'],
    commonData,
  );
  
   //filter defect from list all
   const allDefects = defectList.map((item) => ({
    ...item,
    PMProjectCategory: categoryList.find(
      ({PMProjectCategoryID}) =>
        PMProjectCategoryID === item.FK_PMProjectCategoryID,
    ),
  }));
  const item = pathOr(undefined, ['route', 'params', 'defect'], props);
  const canChange =  item?.edit;
  const projectId = propOr(0, 'PMProjectID', project);



  return {
    user,
    master,
    project,
    categories: pathOr(EMPTY.ARRAY, ['currentProject', 'categories'], commonData),
    locations: pathOr(EMPTY.ARRAY, ['currentProject', 'locations'], commonData),
    allDefects: pathOr(EMPTY.ARRAY, ['defectListByDrawingId', drawingId], commonData),
    defectCodeList: commonData.defectCodeList,
    peopleInProject: pathOr(EMPTY.ARRAY, ['currentProject', 'users'], commonData),
    allDefects,
    drawingId,
    modulePermissions,
    canChange,
    projectId
  };
};

const mapDispatchToProps = {
  addActionIntoQueue: actions.addActionIntoQueue,
  getDefectCodeList: actions.getDefectCodeList,
  getProjectCategories: actions.getProjectCategories,
  getProjectLocations: actions.getProjectLocations,
  getPeopleInProject: actions.getPeopleInProject,
  getDefectListByDrawingId: actions.getDefectListByDrawingId
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DefectUpdate);
