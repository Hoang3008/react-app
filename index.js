/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import {isDEV} from './src/utils/PlatformUtils';
import { startNetworkLogging } from 'react-native-network-logger';


// global.XMLHttpRequest =
// global.originalXMLHttpRequest || global.XMLHttpRequest;
//  global.FormData = global.originalFormData ? global.originalFormData : global.FormData;


// if (window.FETCH_SUPPORT) {
//   window.FETCH_SUPPORT.blob = false;
// } else {
//   global.Blob = global.originalBlob || global.Blob;
//   global.FileReader = global.originalFileReader || global.FileReader;
// }

startNetworkLogging();
AppRegistry.registerComponent(appName, () => App);
