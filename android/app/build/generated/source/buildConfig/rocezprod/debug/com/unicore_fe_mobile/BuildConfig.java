/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.unicore_fe_mobile;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.unicore.mobile.rocez";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "rocezprod";
  public static final int VERSION_CODE = 121;
  public static final String VERSION_NAME = "1.6.5";
}
